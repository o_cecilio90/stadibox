<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('country_id')->nullable();          
            $table->uuid('city_id')->nullable();            
            $table->uuid('forum_id')->nullable();            
            $table->string('name')->nullable();
            $table->string('time_limit')->nullable();
            $table->string('keyword')->nullable();
            $table->boolean('featured_event')->default(0);
            $table->boolean('status')->default(0);
            $table->string('experience_image')->nullable();
            $table->string('cover_image')->nullable();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
