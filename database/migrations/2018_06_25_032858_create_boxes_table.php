<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->timestamps();
            $table->string('slug')->unique();
            $table->string('num_box');
            $table->string('another_zone')->nullable();
            $table->boolean('cleaning')->nullable();
            $table->boolean('invoice')->nullable();
            $table->boolean('annual_rent')->nullable();
            $table->boolean('administration_boxes')->nullable();
            $table->string('referenced_code')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('parking')->nullable();
            $table->integer('parking_vip')->nullable();            
            $table->string('other_amenity')->nullable();
            $table->string('rfc')->nullable();
            $table->string('identification')->nullable();
            $table->string('identification_file')->nullable();
            $table->string('proof_address')->nullable();
            $table->string('property_title')->nullable();
            $table->string('bank')->nullable();
            $table->string('account_titular')->nullable();
            $table->string('titular')->nullable();
            $table->string('reference_number')->nullable();
            $table->boolean('property_owner')->nullable();            
            
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('forum_id');
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->uuid('zone_id');
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
