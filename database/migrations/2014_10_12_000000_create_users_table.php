<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('role_id')->nullable();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->datetime('birthdate')->nullable();
            $table->boolean('term_cond')->default(0);
            $table->boolean('promotions')->default(0);
            $table->boolean('owner')->default(0);
            $table->boolean('is_verify')->default(0);
            $table->boolean('status')->default(1);
            $table->boolean('console')->default(0);
            $table->string('name_ful')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->text('reason')->nullable();
            $table->rememberToken();
            $table->timestamps();
           
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
