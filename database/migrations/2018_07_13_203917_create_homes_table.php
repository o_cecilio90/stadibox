<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->timestamps();
            $table->string('image')->nullable();
            $table->boolean('vip')->default(true);
            $table->text('tab')->nullable();
            $table->boolean('pop_status')->default(false);
            $table->string('pop_main_text')->nullable();
            $table->string('pop_call_to_action')->nullable();
            $table->string('pop_url')->nullable();
            $table->text('pop_optional_note')->nullable();
            $table->string('pop_image')->nullable();

            $table->uuid('user_id')->nullable();            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
