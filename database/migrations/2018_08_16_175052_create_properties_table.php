<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('stall_id')->nullable();
            $table->uuid('box_id')->nullable();
            $table->uuid('user_id')->nullable();
            $table->uuid('forum_id')->nullable();
            $table->uuid('event_id')->nullable();
            $table->boolean('status_event')->default(0);
            $table->boolean('rent')->default(0);
            $table->string('rent_mode')->nullable();
            $table->string('additional_benefits')->nullable();
            $table->string('ticket')->nullable();
            $table->decimal('price_ticket',10,2)->default(0);
            $table->decimal('total_utility',10,2)->default(0);
            $table->decimal('down_payment',10,2)->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('stall_id')->references('id')->on('stalls')->onDelete('cascade');
            $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
