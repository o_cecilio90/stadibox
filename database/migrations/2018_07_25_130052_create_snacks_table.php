<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snacks', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('service_id');
            $table->string('name_es')->nullable();
            $table->string('name_en')->nullable();
            $table->string('brand')->nullable();
            $table->string('image_snack')->nullable();
            $table->enum('type',['drink','food','pack'])->nullable();
            $table->decimal('price_snack',10,2)->nullable();
            $table->decimal('base_price',10,2)->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snacks');
    }
}
