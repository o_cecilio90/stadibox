<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->boolean('featured_event')->default(true);
            $table->boolean('status')->default(true);
            $table->string('alt_image')->nullable();
            $table->string('image')->nullable();
            $table->string('keywords')->nullable();
            $table->string('category')->nullable();
            $table->float('lower_topLimit')->default(0.00);
            $table->float('upper_topLimit')->default(0.00);
            $table->float('lower_middleLimit1')->default(0.00);
            $table->float('upper_middleLimit1')->default(0.00);
            $table->float('lower_middleLimit2')->default(0.00);
            $table->float('upper_middleLimit2')->default(0.00);
            $table->float('lower_downLimit')->default(0.00);
            $table->float('upper_downLimit')->default(0.00);
            $table->float('percentage_profit')->default(0.00);
            $table->float('discount')->default(0.00);

            $table->uuid('system_user_id')->nullable();            
            $table->foreign('system_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('country_id')->nullable();            
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->uuid('city_id')->nullable();            
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->uuid('forum_id')->nullable();            
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
