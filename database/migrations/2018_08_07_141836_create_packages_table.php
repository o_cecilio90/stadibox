<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('experience_id')->nullable();          
            $table->string('name_package')->nullable();
            $table->boolean('status_package')->default(0);
            $table->string('include')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('price_simple',10,2)->nullable();
            $table->decimal('price_double',10,2)->nullable();
            $table->timestamps();

            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
