<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('forum_id');

            //waiter
            $table->boolean('status_waiter')->default(0);
            $table->integer('amount')->nullable();
            $table->decimal('price_waiter',10,2)->nullable();

            // Transport
            $table->boolean('status_transport')->default(0);
            $table->decimal('price_1',10,2)->nullable();
            $table->decimal('price_2',10,2)->nullable();
            $table->decimal('price_3',10,2)->nullable();
            $table->decimal('price_4',10,2)->nullable();

            // Shipment
            $table->decimal('price_shipment',10,2)->nullable();

            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
