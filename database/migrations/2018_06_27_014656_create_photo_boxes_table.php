<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_boxes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->timestamps();
            $table->string('court')->nullable();
            $table->string('box')->nullable();
            $table->string('seat')->nullable();
            $table->string('bath')->nullable();
            $table->string('other1')->nullable();
            $table->string('other2')->nullable();
            $table->string('other3')->nullable();
            $table->string('other4')->nullable();
            $table->string('other5')->nullable();
            $table->string('other6')->nullable();
            $table->string('other7')->nullable();
            $table->string('other8')->nullable();
            $table->string('other9')->nullable();
            $table->string('other10')->nullable();
            $table->string('other11')->nullable();
            $table->string('other12')->nullable();
            $table->string('other13')->nullable();
            $table->string('other14')->nullable();
            $table->string('other15')->nullable();
            $table->string('other16')->nullable();
            $table->string('other17')->nullable();
            $table->string('other18')->nullable();
            $table->string('other19')->nullable();
            $table->string('other20')->nullable();


            $table->uuid('box_id');
            $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_boxes');
    }
}
