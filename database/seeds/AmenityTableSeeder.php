<?php

use Illuminate\Database\Seeder;

use App\Models\Amenity;
use Illuminate\Support\Str as Str;

class AmenityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/icons-04.png',
        	'slug' => Str::slug('television'),
        	'amenity' => 'Televisión',
        ]);

        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/icons-05.png',
        	'slug' => Str::slug('bano'),
        	'amenity' => 'Baño',
        ]);

        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/icons-01.png',
        	'slug' => Str::slug('refrigerador'),
        	'amenity' => 'Refrigerador',
        ]);

        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/sala-01.png',
        	'slug' => Str::slug('Sala'),
        	'amenity' => 'Sala',
        ]);

        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/icons-02.png',
        	'slug' => Str::slug('wifi'),
        	'amenity' => 'Wifi',
        ]);

        Amenity::create([
        	'id' => (string) Str::uuid(),
        	'icon' => 'web/images/barra-19.png',
        	'slug' => Str::slug('barra-de-cocina'),
        	'amenity' => 'Barra de Cocina',
        ]);
    }
}
