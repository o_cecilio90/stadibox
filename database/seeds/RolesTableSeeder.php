<?php

use Illuminate\Database\Seeder;

use App\Models\Role;
use Illuminate\Support\Str as Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'id' => (string) Str::uuid(),
        	'name' => 'Administrador',
        	'slug' => Str::slug('Administrador'),
        ]);
        Role::create([
            'id' => (string) Str::uuid(),
            'name' => 'Propietario',
            'slug' => Str::slug('Propietario'),
        ]);
        Role::create([
            'id' => (string) Str::uuid(),
            'name' => 'Cliente',
            'slug' => Str::slug('Cliente'),
        ]);
    }
}
