<?php

use Illuminate\Database\Seeder;

use App\Models\Home;
use Illuminate\Support\Str as Str;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Home::create([
        	'id' => (string) Str::uuid(),
        	'image' => 'web/images/stadium.jpeg',
            'tab' => 'Futbol;NFL;Conciertos;Toros;Otros',            
            'pop_main_text' => '¡Shakira en el Estadio Azteca!',
            'pop_optional_note' => '*Válido para eventos en la Ciudad de México.',
            'pop_image' => 'console/dist/img/foto_cuadrada.png',
        ]);
    }
}
