<?php

use Illuminate\Database\Seeder;

use App\Models\Banner;
use Illuminate\Support\Str as Str;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
        	'id' => (string) Str::uuid(),
        	'position' => '1',           
            'main_text' => '¡Aparta tus pases de palco para eventos en Febrero 2018 y el catering va por nuestra cuenta!',
            'optional_note' => '*Válido para eventos en la Ciudad de México.',
        	'image' => 'web/images/promo1.jpg',
        ]);

        Banner::create([
        	'id' => (string) Str::uuid(),
        	'position' => '2',         
            'main_text' => 'Compra tus pases en diciembre e ingresa automáticamente a nuestra rifa para asistir a la NFL en 2018',
            'optional_note' => '*Solo aplica para compras en palcos.',
        	'image' => 'web/images/promo-2.jpg',
        ]);
    }
}
