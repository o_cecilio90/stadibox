<?php

use Illuminate\Database\Seeder;

use App\Models\Country;
use Illuminate\Support\Str as Str;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
        	'id' => (string) Str::uuid(),
        	'country' => 'México',
        ]);
    }
}
