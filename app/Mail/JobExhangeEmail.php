<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobExhangeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $position;
    public $message;
    public $phone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $position, $message, $phone)
    {
        $this->name = $name;
        $this->email = $email;
        $this->position = $position;
        $this->message = $message;
        $this->phone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Talento stadibox')->markdown('web.emails.contacto_trabajo');
    }
}
