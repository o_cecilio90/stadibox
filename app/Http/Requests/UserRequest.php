<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lastname' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->users,
            'mobile' => 'required',
            'password' => 'required',
            'password_confirmed' => 'required|confirmed',
            'term_cond' => 'required',
        ];
    }

    public function messages(){
        return [
            'required' => 'El campo es requerido',
            'email' => 'El campo debe ser de tipo correo',
            'unique' => 'El correo ya existe',
        ];
    }
}
