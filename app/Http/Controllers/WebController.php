<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Forum;
use App\Models\Zone;
use App\Models\Amenity;
use App\Models\Home;
use App\Models\Banner;
use App\Models\Event;
use App\Models\Box;
use App\Models\Stall;
use App\Models\Property;
use App\Models\Experience;

use Auth;

class WebController extends Controller
{
    function eventWeb($id){

        $event = Event::where('id',$id)->first();
        $property = Property::where('event_id',$event['id'])->get(); 
        
        return view('web.events.show',compact('event','property'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     
    public function __construct()
    {
        //$this->middleware('auth', ['only' => ['console']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        $banner = Banner::orderBy('position','ASC')->get();
        $home = Home::first();
        $home->tab = explode(';', $home->tab);
        $events = Event::all();
        $experience = Experience::orderby('created_at','DESC')->take(3)->get()->load('city','forum','package');
        foreach ($experience as $key => $value) {
            $value->price = $value->package->where('price_simple',$value->package->min('price_simple'))->first();
        }
        
        // dd(array_slice($events,0,4));
        return view('web.home.index',compact('amenity','home','banner','events','experience'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home)
    {
        $image = $request['image'];
    	if (isset($image)) {
            $this->deleteFile($home['image']);
        }

        $pop_image = $request['pop_image'];
        if (isset($pop_image)) {
            $this->deleteFile($home['pop_image']);
        }
		
        $request = $this->saveFiles($request,'/home');

        $tab = $request['value'];
        if (isset($tab)) {
            $tab = explode(';', $home->tab);
            $tab[$request['key']] = $request['value'];
            $tab = implode(';', $tab);
            $request->merge(array(
                'tab' => $tab,
            ));
        }

        $vip = $request['vip'];
        if (isset($vip)) {
            if ($request['vip'] == "true") {
                $vip = 1;
            } else {
                $vip = 0;
            }

            $request->merge(array(
                'vip' => $vip,
                'request' => $request['vip'],
            ));
        }

        $pop_status = $request['pop_status'];
        if (isset($pop_status)) {
            if ($request['pop_status'] == "true") {
                $pop_status = 1;
            } else {
                $pop_status = 0;
            }

            $request->merge(array(
                'pop_status' => $pop_status,
            ));
        } 

        $request->merge(array(
            //'user_id' => Auth::user()->id,
        ));

        $home->update($request->all());
        
        return response()->json(['success'=>'Data is successfully added']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forums()
    {
        
        $forum = Forum::all();
        return response()->json($forum);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function zones($id)
    {
        
        $zone = Zone::where('forum_id',$id)->get();
        return response()->json($zone);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function console()
    {
        $box = Box::where('user_id',Auth::user()->id)->get();
        $stall = Stall::where('user_id',Auth::user()->id)->get();
        $events = Event::all();
        return view('web.console.layouts.main',compact('box','stall','events'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contacto()
    {   
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.contacto',compact('amenity'));
    }

    public function faq()
    {   
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.faq',compact('amenity'));
    }

    public function about()
    {   
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.acerca-de-nosotros',compact('amenity'));
    }

    public function renta()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.renta-anual',compact('amenity'));
    }

    public function administracion()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.administracion',compact('amenity'));
    }

    public function servicios()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.servicios',compact('amenity'));   
    }

    public function empresas()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.para-empresas',compact('amenity'));   
    }

    public function propietarios()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.para-propietarios',compact('amenity'));   
    }

    public function vip()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.vip',compact('amenity'));   
    }

    public function terminos()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.terminos-y-condiciones',compact('amenity'));   
    }

    public function privacidad()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.aviso-de-privacidad',compact('amenity'));   
    }

    public function bolsa()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.bolsa-de-trabajo',compact('amenity'));   
    }

    public function palcos()
    {
        $amenity = Amenity::orderBy('id','ASC')->get();
        return view('web.info.venta-palcos',compact('amenity'));   
    }
}
