<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserVerificationMail as VerificationMail;
use App\Models\User;
use Illuminate\Support\Str as Str;

use App\Models\Box;
use App\Models\Stall;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('console',0)->get()->load('boxes','stalls','role');
        return DataTables::of($user)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        
        $request->merge(array(
            'id' => (string) Str::uuid(),
        )); 
        /*$validator = $user->validateRegister($request);
        if ($validator->fails()) 
            \Session::flash('register','');
            return \Redirect::back()
                    ->withErrors($validator, 'register');*/
        
        $user = User::create($request->all());
        Mail::to($request->email)->send(new VerificationMail($user->id));

        \Session::flash('verify-user', '');
        return \Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $box = Box::join('forums','boxes.forum_id','forums.id')
            ->join('cities','forums.city_id','cities.id')
            ->join('countries','cities.country_id','countries.id')
            ->where('user_id',$user->id)->get();
        $stall = Stall::join('forums','stalls.forum_id','forums.id')
            ->join('cities','forums.city_id','cities.id')
            ->join('countries','cities.country_id','countries.id')
            ->where('user_id',$user->id)->get();

        return view('console.user.user',compact('user','box','stall'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //dd($request->all(),$user);
        $user->update($request->all());

        return redirect()->route('user.show',$user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Verificación de registro del usuario
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function userVerification($id){

        // Búsqueda del usuario por id y cambio de is_verify
        $user = User::findOrFail($id);
        $user->is_verify = 1;
        $user->save();

        $full_name = $user->name . ' ' . $user->lastname;
        
        return view('web.verification.index', compact('full_name'));
    }
}
