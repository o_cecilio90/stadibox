<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\JobExhangeEmail as JobExchange;
use App\Http\Requests\JobExchangeRequest;

class JobExchangeController extends Controller
{
    function index(){

        return view('web.job_exchange.index');
    }

    function jobExchangeEmail(JobExchangeRequest $request){

        $name = $request->name;
        $email = $request->email;
        $position = $request->position;
        $message = $request->message;
        $cv = $request->cv;
        $phone = $request->phone;

        Mail::to('o.cecilio90@gmail.com')->send(new JobExchange($name,$email,$position,$message,$phone));
        
        return \Redirect::back();
    }
}
