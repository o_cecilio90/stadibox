<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Models\City;

use Illuminate\Support\Str as Str;
use Auth;
use DataTables;

class CityController extends Controller
{

    protected $countries;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     
    public function __construct()
    {
        //$this->middleware('auth');
        $this->countries = new Country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city = City::all()->load('forums','country');
        return DataTables::of($city)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = $this->countries->findCountry($request['country']);
        
        if (!isset($country)) {
            $request->merge(array(
                'id' => (string) Str::uuid()
            ));

            $country = Country::create($request->all());
        }

        $request->merge(array(
            'id' => (string) Str::uuid(),
            'country_id' => $country->id,
        ));

        $city = City::create($request->all());


        return response()->json($city);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $city->update($request->all());
        
        return response()->json($city);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return response()->json(['done']);
    }
}
