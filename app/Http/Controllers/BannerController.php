<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;

use Illuminate\Support\Str as Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = array(
            'id' => (string) Str::uuid(),
            'image' => 'console/dist/img/foto_cuadrada.png',
            //'user_id' => Auth::user()->id,
        );
        $banner = Banner::create($banner);

        return response()->json(['success'=>'Data is successfully added','banner'=>$banner]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $image = $request['image'];
        if (isset($image)) {
            $this->deleteFile($banner['image']);
        }
        
        $request = $this->saveFiles($request,'/banner');

        $banner->update($request->all());
        return response()->json(['success'=>'Data is successfully edited']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $this->deleteFile($banner['image']);
        $banner->delete();
        return response()->json(['success'=>'Data is successfully Delete']);
    }
}
