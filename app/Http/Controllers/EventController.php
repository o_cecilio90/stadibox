<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

use App\Models\Country;
use App\Models\City;
use App\Models\Forum;
use App\Models\Stall;
use App\Models\Box;
use App\Models\Property;

use Illuminate\Support\Str as Str;
use Auth;
use DataTables;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::with(['country','city','forum','system_user'])->get();

        return DataTables::of($event)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event;
        $method = 'create';

        return view('console.event.event', compact('method','event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->saveFiles($request,'/event');
        
        $category = $request['category'];
        $keywords = $request['keywords'];

        $request->merge(array(
            'id' => (string) Str::uuid(),
            'featured_event' => $request['featured_event'] == true ? 1 : 0,
            //'user_id' => Auth::user()->id,
            'category' => isset($category) ? implode(';', $request['category']) : '--',
            'keywords' => isset($keywords) ? implode(';', $request['keywords']) : '--',
        ));

        $event = Event::create($request->all());

        $stall = Stall::where('forum_id',$event['forum_id'])->get();
        foreach ($stall as $key => $value) {
            Property::create([
                'id' => (string) Str::uuid(),
                'forum_id' => $event['forum_id'],
                'event_id' => $event['id'],
                'stall_id' => $value['id'],
                'user_id' => $value['user_id'],
            ]);
        }

        $box = Box::where('forum_id',$event['forum_id'])->get();
        foreach ($box as $key => $value) {
            Property::create([
                'id' => (string) Str::uuid(),
                'forum_id' => $event['forum_id'],
                'event_id' => $event['id'],
                'box_id' => $value['id'],
                'user_id' => $value['user_id'],
            ]);
        }

        return redirect()->route('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $method = 'edit';
        $event->category = explode(';', $event->category);
        $event->keywords = explode(';', $event->keywords);

        return view('console.event.event',compact('method','event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request = $this->saveFiles($request,'/event');

        $image = $request['image'];
        if (isset($image)) {
            $this->deleteFile($event['image']);
        }

        $category = $request['category'];
        $keywords = $request['keywords'];

        $request->merge(array(
            'featured_event' => $request['featured_event'] == true ? 1 : 0,
            'category' => isset($category) ? implode(';', $request['category']) : '--',
            'keywords' => isset($keywords) ? implode(';', $request['keywords']) : '--',
        ));      

        $event->update($request->all());

        return redirect()->route('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $this->deleteFile($event['image']);
        $event->delete();
        return redirect()->route('events');
    }
}
