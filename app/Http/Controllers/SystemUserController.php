<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str as Str;
use Auth;
use DataTables;

class SystemUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $systemUser = User::where('console',1)->get()->load('role');
        return DataTables::of($systemUser)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request,[
            'name_ful' => 'required|string|max:255',
            'country'  => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:system_users',
            'password' => 'required|string|min:6',
            'role_id' => 'required'
        ]);*/
        
        $request->merge(array(
            'id' => (string) Str::uuid(),
            'console' => true,
            'password' => Hash::make($request['password']),
        ));      

        $systemUser = User::create($request->all());
        return response()->json($systemUser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SystemUser  $systemUser
     * @return \Illuminate\Http\Response
     */
    public function show(User $systemUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SystemUser  $systemUser
     * @return \Illuminate\Http\Response
     */
    public function edit(User $systemUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SystemUser  $systemUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $systemUser)
    {
        $request->merge(array(
            'password' => is_null($request['password']) ? $systemUser['password'] : Hash::make($request['password']),
        ));
        $systemUser->update($request->all());
        return response()->json($systemUser);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SystemUser  $systemUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $systemUser)
    {
        $systemUser->delete();
        return response()->json(['done']);
    }
}
