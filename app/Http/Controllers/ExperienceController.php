<?php

namespace App\Http\Controllers;

use App\Models\Experience;
use App\Models\Country;
use App\Models\City;
use App\Models\Forum;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Str as Str;
use DataTables;

class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('console.experiences.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('console.experiences.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->saveFiles($request,'/experiences');

        $experience = new Experience($request->all());
        $experience->id = (string) Str::uuid();
        $experience->experience_image = $request->experience_img;
        $experience->cover_image = $request->cover_img;
        foreach ($request->keyword as $key => $value) {
            $experience->keyword = $value .'/'; 
        }
        $experience->save();
        
        $package = new Package;

        if (count($request->name_package) > 1) {
            for ($i=0; $i < count($request->name_package); $i++) { 
                if ($i >= 1) {
                    $package->id = (string) Str::uuid();
                    $package->name_package = $request->name_package[$i];
                    $package->status_package = $request->status_package[$i];
                    $package->include = $request->include[$i];
                    $package->currency = $request->currency[$i];
                    $package->price_simple = $request->price_simple[$i];
                    $package->price_double = $request->price_double[$i];
                    $package->experience_id = $experience->id;
                    $package->save();
                }
            }
        }else{
            $package->id = (string) Str::uuid();
            $package->name_package = $request->name_package[0];
            $package->status_package = $request->status_package[0];
            $package->include = $request->include[0];
            $package->currency = $request->currency[0];
            $package->price_simple = $request->price_simple[0];
            $package->price_double = $request->price_double[0];
            $package->experience_id = $experience->id;
            $package->save();
        }

        return \Redirect::route('experiencias.index');

    }

    function experiencesTable(){
        $experiences = Experience::all();
        $data = array();

        foreach ($experiences as $key => $experience) {
            $data[$key]['name'] = $experience->name;
            $data[$key]['place'] = $experience->country->country .', '. $experience->city->city;
            $data[$key]['time_limit'] = $experience->time_limit;
            $data[$key]['forum'] = $experience->forum->forum_name;
            if ($experience->status == 1) {
                $data[$key]['status'] = 'Activo';
            }else{
                $data[$key]['status'] = 'No Activo';
            }
        }

        return DataTables::of($data)->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience)
    {
        //
    }

    function getCity(Request $request){

        $cities = City::where('country_id',$request->country_id)->get();

        return \Response::json($cities);
    }

    function getForums(Request $request){

        $forums = Forum::where('city_id',$request->city_id)->get();

        return \Response::json($forums);
    }
}
