<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Home;
use App\Models\Banner;

class ConsoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function configuration()
    {
        $banner = Banner::orderBy('position','ASC')->get();;
        $home = Home::first();
        $home->tab = explode(';', $home->tab);
        
        return view('console.configuration.index',compact('home','banner'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function catalogs()
    {   
        return view('console.catalogs.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function events()
    {        
        return view('console.event.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {        
        return view('console.user.index');
    }
}
