<?php

namespace App\Http\Controllers;

use App\Models\Stall;
use Illuminate\Http\Request;

use App\Models\NumberStall;
use App\Models\Event;
use App\Models\Property;

use Illuminate\Support\Str as Str;
use Auth;
use App\Http\Controllers\Traits\FileUploadTrait;

class StallController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->saveFiles($request,'/stall');

        $stall = array(
            'id' => (string) Str::uuid(),
            'forum_id' => $request->forum_id,
            'zone_id' => $request->zone_id,
            'user_id' => Auth::user()->id,
            'stall_tickets' => $request->stall_tickets,
            'parking_stall' => $request->parking_stall,
            'invoice' => ($request->invoice ? true : false),
            'annual_rent' => ($request->annual_rent ? true : false),
            'administration_stalls' => ($request->administration_boxes ? true : false),
            'rfc' => $request->rfc,
            'identification' => $request->identification,
            'identification_file' => $request->identification_file,
            'proof_address' => $request->proof_address,
            'property_title' => $request->property_title,
            'photo_stall' => $request->photo_stall,
            'bank' => $request->bank,
            'account_titular' => $request->account_titular,
            'titular' => $request->titular,
            'reference_number' => $request->reference_number,
            'property_owner' => ($request->property_owner ? true : false),
        );

        $stall = Stall::create($stall);

        # Asientos del Platea
        foreach ($request->row as $key => $value) {
            if ($value == null) {
                break;
            }
            $numberStall = array(
                'id' => (string) Str::uuid(),
                'stall_id' => $stall->id,
                'row' => $value,
                'seat' => $request->seat[$key],

            );
            NumberStall::create($numberStall);
        }

        $event = Event::where('forum_id', $stall['forum_id'])->get();
        foreach ($event as $key => $value) {
            Property::create([
                'id' => (string) Str::uuid(),
                'forum_id' => $stall['forum_id'],
                'event_id' => $value['id'],
                'stall_id' => $stall['id'],
                'user_id' => $stall['user_id'],
            ]);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stall  $stall
     * @return \Illuminate\Http\Response
     */
    public function show(Stall $stall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stall  $stall
     * @return \Illuminate\Http\Response
     */
    public function edit(Stall $stall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stall  $stall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stall $stall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stall  $stall
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stall $stall)
    {
        //
    }
}
