<?php

namespace App\Http\Controllers;

use App\Models\Services;
use App\Models\Snack;
use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;
use DataTables;
use App\Http\Controllers\Traits\FileUploadTrait;

class ServiceController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = \DB::table('services')
                    ->join('forums','services.forum_id','forums.id')
                    ->join('cities','forums.city_id','cities.id')
                    ->join('countries','cities.country_id','countries.id')
                    ->select('countries.country','cities.city','forums.forum_name','services.*')
                    ->get();

        return DataTables::of($services)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = Services::where('id',$request->service_id)->first();
        $request = $this->saveFiles($request,'/services/snacks');
        if ($request->snacks == 'snacks') {
            $snack = array(
                'id' => (string) Str::uuid(),
                'name_es' => $request->name_es,
                'name_en' => $request->name_en,
                'brand' => $request->brand,
                'image_snack' => $request->image_snack,
                'type' => $request->type,
                'price_snack' => $request->price_snack,
                'base_price' => $request->base_price,
                'service_id' => $service->id

            );
            $snack = Snack::create($snack);
            return \Response::json([$snack]);
        }elseif($request->service_waiter == 'service_waiter'){
            $service->update($request->all());
            $service->save();
        }elseif($request->service_transport == 'service_transport'){
            $service->update($request->all());
            $service->save();
        }elseif($request->service_shipment == 'service_shipment'){
            $service->update($request->all());
            $service->save();
        }

        return \Response::json([$service]);
    }

    function serviceSnacks(Request $request)
    {
        $lang = \App::getLocale();

        if ($lang == "es") {
            $snacks = Snack::select('id','name_es AS name','price_snack','image_snack','type')->where('service_id',$request->service_id)->get();
        }else{
            $snacks = Snack::select('id','name_en AS name','price_snack','image_snack','type')->where('service_id',$request->service_id)->get();
        }
        return \Response::json($snacks);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $services)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Services $services)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $services)
    {
        //
    }
}
