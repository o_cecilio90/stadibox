<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use Illuminate\Support\Str as Str;

class PackageController extends Controller
{
    function createPackage(Request $request){

        $package = Package::create([
            'id' => (string) Str::uuid(),
        ]);

        return \Response::json(['package_id' => $package->id],200);
    }
}
