<?php

namespace App\Http\Controllers;

use App\Models\Box;
use Illuminate\Http\Request;
use App\Models\PhotoBox;
use App\Models\Event;
use App\Models\Property;

use Illuminate\Support\Str as Str;
use Auth;

class BoxController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request = $this->saveFiles($request,'/box');

        $request->merge(array(
            'id' => (string) Str::uuid(),
            'slug' => Str::slug('forum name '. $request['num_box']),
            'user_id' => Auth::user()->id,
            'cleaning' => ($request['cleaning'] ? true : false),
            'invoice' => ($request['invoice'] ? true : false),
            'annual_rent' => ($request['annual_rent'] ? true : false),
            'administration_boxes' => ($request['administration_boxes'] ? true : false),
            'rfc' => $request['RFC'],
            'property_owner' => ($request['property_owner'] ? true : false)
        )); 

        // Create a box
        $box = Box::create($request->all());

        $request->merge(array(
            'box_id' => $box['id'],
        )); 

        // Create Photo Box
        PhotoBox::create($request->all());
        foreach ($request['list_amenity'] as $key => $value) {
            $box->amenities()->syncWithoutDetaching($value);
        }

        $event = Event::where('forum_id', $box['forum_id'])->get();
        foreach ($event as $key => $value) {
            Property::create([
                'id' => (string) Str::uuid(),
                'forum_id' => $box['forum_id'],
                'event_id' => $value['id'],
                'box_id' => $box['id'],
                'user_id' => $box['user_id'],
            ]);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function show(Box $box)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function edit(Box $box)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Box $box)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        //
    }
}
