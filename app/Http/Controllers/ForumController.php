<?php

namespace App\Http\Controllers;

use App\Models\Forum;
use Illuminate\Http\Request;
use App\Models\Zone;
use App\Models\Services;

use Illuminate\Support\Str as Str;
use Auth;
use DataTables;
use App\Http\Controllers\Traits\FileUploadTrait;

class ForumController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::with(['city','zone'])->get();
        //$forum = Forum::all()->load('city','zone')->toArray();

        $forum = array();
        foreach ($forums as $key => $value) {
            $forum[$key]['id'] = $value->id;
            $forum[$key]['map'] = $value->map;
            $forum[$key]['forum_name'] = $value->forum_name;
            $forum[$key]['city'] = $value->city;
            $forum[$key]['country'] = $value->city->country;
            $forum[$key]['zone'] = $value->zone;
        }
        return DataTables::of($forum)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->saveFiles($request,'/forum');

        $forum = array(
            'id' => (string) Str::uuid(),
            'slug' => Str::slug($request->forum_name),
            'forum_name' => $request->forum_name,
            'map' => $request->map,
            'city_id' => $request->city_id,
        );      

        $forum = Forum::create($forum);
        
        $service = array(
            'id' => (string) Str::uuid(),
            'forum_id' => $forum->id,
        );
        Services::create($service);


        $zone = array(
            'id' => (string) Str::uuid(),
            'slug' => Str::slug($request->name_zone),
            'map' => $request->zone_map,
            'name_zone' => $request->name_zone,
            'color' => $request->color,
            'type' => $request->type,
            'forum_id' => $forum->id,
        );

        Zone::create($zone);

        return response()->json($forum);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function show(Forum $forum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forum $forum)
    {
        if (isset($request['map'])) {
            $this->deleteFile($forum['map']);
        }

        if (isset($request['zone_map'])) {
            $this->deleteFile($forum->zone['map']);
        }

        $request = $this->saveFiles($request,'/forum');

        $newforum = array(
            'slug' => Str::slug($request->forum_name),
            'forum_name' => $request->forum_name,
            'map' => ($request->map == null ? $forum->map : $request->map),
            'city_id' => $request->city_id,
        );      

        $forum->update($newforum);

        $zone = array(
            'slug' => Str::slug($request->name_zone),
            'map' => $request->zone_map == null ? $forum->zone->map : $request->zone_map,
            'name_zone' => $request->name_zone,
            'color' => $request->color,
            'type' => $request->type,
        );

        $forum->zone->update($zone);

        return response()->json($forum);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forum  $forum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forum $forum)
    {
        $this->deleteFile($forum['map']);
        $this->deleteFile($forum->zone['map']);
        $forum->delete();
        return response()->json(['done']);
    }

    /**
     * Show the forum for a citie
     *
     * @return \Illuminate\Http\Response
     */
    public function forCountry($id)
    {
        $forum = Forum::where('city_id',$id)->get();
        return response()->json($forum);
    }

}
