<?php

namespace App\Http\Controllers;

use App\Models\Amenity;
use Illuminate\Http\Request;

use Illuminate\Support\Str as Str;
use Auth;
use DataTables;
use App\Http\Controllers\Traits\FileUploadTrait;


class AmenityController extends Controller
{
    use FileUploadTrait;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amenity = Amenity::all();
        return DataTables::of($amenity)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->saveFiles($request,'/amenity');

        $request->merge(array(
            'id' => (string) Str::uuid(),
            'slug' => Str::slug($request->amenity),
        ));      

        $amenity = Amenity::create($request->all());
        return response()->json($amenity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function show(Amenity $amenity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function edit(Amenity $amenity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Amenity $amenity)
    {
        if (isset($request['icon'])) {
            $this->deleteFile($amenity['icon']);
        }

        $request = $this->saveFiles($request,'/amenity');
        $request->merge(array(
            'slug' => Str::slug($request->amenity),
        )); 
        $amenity->update($request->all());
        return response()->json($amenity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Amenity $amenity)
    {
        $this->deleteFile($amenity['icon']);
        $amenity->delete();

        return response()->json(['done']);
    }
}
