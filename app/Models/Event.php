<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'date',
        'time',
        'featured_event',
        'status',
        'alt_image',
        'image',
        'keywords',
        'category',
        'lower_topLimit',
        'upper_topLimit',
        'lower_middleLimit1',
        'upper_middleLimit1',
        'lower_middleLimit2',
        'upper_middleLimit2',
        'lower_downLimit',
        'upper_downLimit',
        'percentage_profit',
        'discount',
        'system_user_id',
        'country_id',
        'city_id',
        'forum_id'
    ];

    public function system_user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
