<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public $incrementing = false;

    protected $table = 'packages';

    protected $fillable = [
        'id',
        'experience_id',          
        'name_package',
        'status_package',
        'include',
        'currency',
        'price_simple',
        'price_double',
    ];

    function experience(){
        return $this->belongsTo(Experience::class);
    }
}
