<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    public $incrementing = false;
    
    protected $table = 'experiences';

    protected $fillable = [
        'id',
        'country_id',          
        'city_id',            
        'forum_id',            
        'name',
        'time_limit',
        'keyword',
        'featured_event',
        'status',
        'experience_image',
        'cover_image'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    function package(){
        return $this->hasMany(Package::class);
    }
}
