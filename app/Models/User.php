<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;
 
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'users';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'lastname',
        'mobile',
        'email',
        'password',
        'birthdate',
        'term_cond',
        'promotions',
        'owner',
        'is_verify',
        'console',
        'name_ful',
        'country',
        'city',
        'role_id',
        'status',
        'reason',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        $this->attributes['password'] = Hash::make($input);
    }

    function validateRegister($request){
        $rules = [
            'name' => 'required',
            'lastname' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'term_cond' => 'required',
        ];

        $messages = [
            'required' => 'El campo es requerido',
            'email' => 'El campo debe ser de tipo correo',
            'unique' => 'El correo ya existe',
            'confirmed' => 'Las contraseñas no son iguales'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        return $validator;
    }

    public function boxes()
    {
        return $this->hasMany(Box::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function home()
    {
        return $this->belongsTo(Home::class);
    }

    public function banners()
    {
        return $this->hasMany(Banner::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function stalls()
    {
        return $this->hasMany(Stall::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
