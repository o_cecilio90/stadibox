<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','position','main_text','call_to_action','url','optional_note','image','user_id'
    ];

    public function system_user()
    {
        return $this->belongsTo(User::class);
    }
}
