<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stall extends Model
{
    public $incrementing = false;

    protected $table = 'stalls';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','forum_id','zone_id','user_id','stall_tickets','parking_stall','invoice','annual_rent','administration_stalls','rfc','identification','identification_file','proof_address','property_title','photo_stall','bank','account_titular','titular','reference_number','property_owner'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

    public function numberStalls()
    {
        return $this->hasMany(NumberStall::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
