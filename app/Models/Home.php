<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','image','vip','tab','pop_status','pop_main_text','pop_call_to_action','pop_url','pop_optional_note','pop_image','user_id'
    ];

    public function system_user()
    {
        return $this->hasOne(User::class);
    }
}
