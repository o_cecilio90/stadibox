<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoBox extends Model
{
    public $incrementing = false;

    protected $table = 'photo_boxes';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','box_id','court','box','seat','bath','other1','other2','other3','other4','other5','other6','other7','other8','other9','other10','other11','other12','other13','other14','other15','other16','other17','other18','other19','other20',
    ];

    public function box()
    {
        return $this->belongsTo(Box::class);
    }}
