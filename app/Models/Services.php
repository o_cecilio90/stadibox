<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Services extends Model
{
    public $incrementing = false;

    protected $table = 'services';

    protected $fillable = [
        'id',
        'name_es',
        'name_en',
        'type',
        'price_snack',
        'base_price',
        'status_waiter',
        'amount',
        'price_waiter',
        'status_transport',
        'price_1',
        'price_2',
        'price_3',
        'price_4',
        'price_shipment',
        'forum_id'
    ];

    function forum(){
        return $this->belongsTo(Forum::class);
    }

    function snacks()
    {
        return $this->hasMany(Snack::class);
    }
}
