<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','name_zone','map','color','type','forum_id',
    ];

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }
}
