<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Snack extends Model
{
    public $incrementing = false;
    
    protected $table = 'snacks';

    protected $fillable = [
        'id',
        'name_es',
        'name_en',
        'brand',
        'image_snack',
        'type',
        'price_snack',
        'base_price',
        'service_id'
    ];

    function service()
    {
        return $this->belongsTo(Services::class);
    }
}
