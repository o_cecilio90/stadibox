<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','forum_id','zone_id','user_id','another_zone','num_box','cleaning','invoice','annual_rent','administration_boxes','referenced_code','capacity','parking','parking_vip','rfc','identification','identification_file','proof_address','property_title','bank','account_titular','titular','reference_number','property_owner','other_amenity',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

    public function photos()
    {
        return $this->hasOne(PhotoBox::class);
    }

    function amenities()
    {
        return $this->belongsToMany(Amenity::class, 'box_amenities')->withTimestamps();
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
