<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','country',
    ];

    //Exist Country
    public function findCountry($country)
    {
        return $this->where('country', $country)->first();
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    function experiences(){
        return $this->hasMany(Experience::class);
    }
}
