<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberStall extends Model
{
    public $incrementing = false;

    protected $table = 'number_stalls';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','row','seat','stall_id'
    ];

    public function stall()
    {
        return $this->belongsTo(Stall::class);
    }
}
