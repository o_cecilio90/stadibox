<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','amenity','icon',
    ];

    function boxes()
    {
        return $this->belongsToMany(Box::class, 'box_amenities')->withTimestamps();
    }
}
