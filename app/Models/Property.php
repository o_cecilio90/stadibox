<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id',
        'stall_id',
        'box_id',
        'user_id',
		'forum_id',
        'event_id',
		'status_event',
		'rent',
		'rent_mode',
		'additional_benefits',
		'ticket',
		'price_ticket',
		'total_utility',
		'down_payment',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function box()
    {
        return $this->belongsTo(Box::class);
    }

    public function stall()
    {
        return $this->belongsTo(Stall::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
