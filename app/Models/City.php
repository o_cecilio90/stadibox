<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','country_id','city','operations'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function forums()
    {
        return $this->hasMany(Forum::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    function experiences(){
        return $this->hasMany(Experience::class);
    }
}
