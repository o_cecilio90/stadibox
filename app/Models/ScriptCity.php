<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScriptCity extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','country_id','file'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
