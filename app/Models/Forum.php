<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','slug','forum_name','map','city_id'
    ];

    public function zone()
    {
        return $this->hasOne(Zone::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function services(){
        return $this->hasMany(Services::class);
    }
    
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    function experiences(){
        return $this->hasMany(Experience::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
