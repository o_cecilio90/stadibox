<?php

// Languages
Route::get('lang/{lang}', function ($lang) {
    session(['lang' => $lang]);
    return \Redirect::back();
})->where([
    'lang' => 'en|es'
    ]);
    
Route::get('/', 'WebController@index');
Route::resource('user', 'UserController');
Route::get('user-verification/{id}', 'UserController@userVerification')->name('user.verification');
# Palcos
Route::resource('box','BoxController');
# Placeas
Route::resource('stall','StallController');
# Foros
Route::get('selectForum','WebController@forums')->name('selectForum');
# Zone
Route::get('selectZone/{id}','WebController@zones')->name('selectZone');
# Foro
Route::get('selectForums/{id}','ForumController@forCountry')->name('selectForums');
# Console Web
Route::get('mi-cuenta','WebController@console')->name('web.console');
Route::resource('property', 'PropertyController');
Route::get('properties/{id}', 'PropertyController@propertyEvents')->name('property.events');
Route::get('propiedades','PropertyController@allProperties')->name('all.properties');
Route::get('propiedades-administradas','PropertyController@admProperties')->name('adm.properties');
#Events in web
Route::get('ver-evento/{id}', 'WebController@eventWeb')->name('event.web');

# Login Facebook

Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
    
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

#Info
Route::get('info/contacto','WebController@contacto')->name('contacto');
Route::get('info/faq','WebController@faq')->name('faq');
Route::get('info/acerca-de-nosotros','WebController@about')->name('about');
Route::get('info/renta-anual','WebController@renta')->name('renta');
Route::get('info/administracion','WebController@administracion')->name('administracion');
Route::get('info/servicios','WebController@servicios')->name('servicios');
Route::get('info/para-empresas','WebController@empresas')->name('empresas');
Route::get('info/para-propietarios','WebController@propietarios')->name('propietarios');
Route::get('experiencias/vip','WebController@vip')->name('vip');
Route::get('info/terminos-y-condiciones','WebController@terminos')->name('terminos');
Route::get('info/aviso-de-privacidad','WebController@privacidad')->name('privacidad');
Route::get('info/bolsa-de-trabajo','WebController@bolsa')->name('bolsa');
Route::get('info/venta-palcos','WebController@palcos')->name('palcos');


# Console
#Configuracion
Route::get('configuracion', 'ConsoleController@configuration')->name('configuration');
Route::resource('role','RoleController');
Route::resource('systemUser','SystemUserController');
Route::post('home/{home}', 'WebController@update')->name('home.update');
Route::resource('banner','BannerController');;
#Catalogos
Route::get('catalogos', 'ConsoleController@catalogs')->name('catalogs');
Route::resource('country','CountryController');
Route::resource('city','CityController');
Route::resource('amenity','AmenityController');
Route::resource('forum','ForumController');
#Servicios
Route::resource('services','ServiceController');
Route::post('services-snacks','ServiceController@serviceSnacks')->name('services.snacks');
Route::resource('snacks','SnackController');
Route::post('snacks','SnackController@editSnacks')->name('edit.snacks');
#Event
Route::get('eventos', 'ConsoleController@events')->name('events');
Route::resource('evento','EventController')->parameters(['evento'=>'event']);
#Experiences
Route::resource('experiencias','ExperienceController');
Route::post('get-city', 'ExperienceController@getCity')->name('get.city');
Route::post('get-forums', 'ExperienceController@getForums')->name('get.forums');
Route::get('experiences-table', 'ExperienceController@experiencesTable')->name('experiencias.table');
#Package
Route::post('create-package','PackageController@createPackage')->name('create.package');
#Usuarios
Route::get('Usuarios', 'ConsoleController@users')->name('users');
#Reports
Route::resource('reports','ReportController');
#Calendar
Route::get('calendario','CalendarController@index')->name('calendar');
#Coupons
Route::get('cupones','CouponController@index')->name('coupons');
#Job Exchange
Route::get('bolsa-de-trabajo','JobExchangeController@index')->name('job.exchange.index');
Route::post('job-exchange-email','JobExchangeController@jobExchangeEmail')->name('job.exchange.email');