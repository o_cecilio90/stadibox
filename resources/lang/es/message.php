<?php

return [
    
    //commons
    'email' => 'Correo electrónico',

    // menu header
    'register' => 'Registro',
    'login' => 'Iniciar Sesión',
    'help' => 'Ayuda',
    'property_register' => 'Registrar mi propiedad',

    //footer
    'region_options' => 'Opciones de región',
    'stadibox_service' => 'Servicios Stadibox',
        'start' => 'Inicio',
        'us' => 'Nosotros',
        'manage' => 'Administramos tu Propiedad',
        'rent' => 'Rena anual',
        'sale_boxes' => 'Venta de Palcos y Plateas',
        'additional_services' => 'Servicios Adicionales',
        'businesses' => 'Para Empresas',
        'owners' => 'Para propietarios',
        'vip_experiences' => 'Experiencias VIP',
    'contact' => 'Contacto',
    'frequent_questions' => 'Preguntas Frecuentes',
    'terms_conditions' => 'Términos y Condiciones',
    'privacy' => 'Aviso de Privacidad',

    //newsletter
    'newsletter' => '¡Recibe las mejores oportunidades para conseguir boletos para tus eventos favoritos!',
    'subscribe' => 'Suscribirme',

    // How does it work?
    'question_work' => '¿Cómo Funciona?',
    'step_1' => 'Encuentra el evento al que quieres asistir',
    'step_2' => 'Aparta tus lugares con un pago seguro online',
    'step_3' => 'Te entregamos tus boletos',

];
