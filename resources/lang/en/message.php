<?php

return [

    //commons
    'email' => 'Email',

    // menu header
    'register' => 'Sign Up',
    'login' => 'Login',
    'help' => 'Help',
    'property_register' => 'Register my property',

    //footer
    'region_options' => 'Region options',
    'stadibox_service' => 'Stadibox service',
        'start' => 'Start',
        'us' => 'US',
        'manage' => 'We manage your property',
        'rent' => 'Annual rent',
        'sale_boxes' => 'Sale of Boxes and Stalls',
        'additional_services' => 'Additional services',
        'businesses' => 'For businesses',
        'owners' => 'For Owners',
        'vip_experiences' => 'VIP experiences',
    'contact' => 'Contact',
    'frequent_questions' => 'Frequent questions',
    'terms_conditions' => 'Terms and Conditions',
    'privacy' => 'Notice of Privacy',

    //Newsletter
    'newsletter' => 'Get the best opportunities to get tickets to your favorite events!',
    'subscribe' => 'Subscribe',

    // How does it work?
    'question_work' => 'How does it work?',
    'step_1' => 'Find the event you want to attend',
    'step_2' => 'Set aside your places with secure online payment',
    'step_3' => 'We deliver your tickets',
        
];
