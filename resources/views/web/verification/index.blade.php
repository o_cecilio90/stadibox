<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Wed Nov 29 2017 06:53:30 GMT+0000 (UTC)  -->
<html data-wf-page="5a037fd7746f250001d47174" data-wf-site="57eda549f0e646e9326f231e">
    <head>
  <meta charset="utf-8">
  <title>Cuenta confirmada en Stadibox</title>
  <meta content="¡Ahora formas parte de la primera comunidad de renta de palcos y plateas en el mundo!" name="description">
  <meta content="Cuenta confirmada en Stadibox" property="og:title">
  <meta content="¡Ahora formas parte de la primera comunidad de renta de palcos y plateas en el mundo!" property="og:description">
  <meta content="summary" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="{{ asset('web/css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('web/css/webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('web/css/stadibox.webflow.css') }}" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Varela Round:400","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Inconsolata:400,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="{{ asset('web/images/stadibox_favicon.png') }}" rel="shortcut icon" type="image/x-icon">
  <link href="{{ asset('web/images/stadibox_webclip.png') }}" rel="apple-touch-icon">
</head>
    <body>
        <div data-ix="modalwrapper2" class="modal-wrapper-login">
    <div class="login-wrapper w-clearfix">
      <div class="w-clearfix">
        <div data-ix="cerrar2" class="x">x</div>
      </div>
      <div class="form-login w-form">
        <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="w-clearfix"><input type="email" class="tf w-input" maxlength="256" name="Correo-electronico-5" data-name="Correo Electronico 5" placeholder="Correo electrónico" id="Correo-electronico-5" required=""><input type="password" class="tf w-input" maxlength="256" name="Contrase-a-5" data-name="Contrase A 5" placeholder="Contraseña" id="Contrase-a-5" required=""><a href="#" class="link olvide">¿Has olvidado tu contraseña?</a></form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form</div>
        </div>
      </div><a href="#" data-ix="cerrar2" class="login w-button">Login</a>
      <div class="div-registro"></div>
      <div class="pregunta">¿Aun no tienes cuenta?</div><a href="#" data-ix="cerrar2" class="link-registro">Regístrate</a></div>
  </div>
        <div data-ix="modalwrapper2" class="modal-preregistro">
    <div class="login-wrapper">
      <div class="w-clearfix">
        <div data-ix="cerrar-registro" class="x">x</div>
      </div><a href="#" data-ix="cerrar-registro" class="login registro-con-facebook w-button">Registrarse con facebook</a><a href="#" data-ix="cerrar-registro" class="login w-button">Registrarse con un correo electrónico</a>
      <div class="div-registro-condiciones w-clearfix">
        <div class="div-registro"></div>
        <div class="pregunta">Al registrarme acepto los <span class="link2">Términos y condiciones</span> y la <span class="link2">Política de Privacidad</span>.</div>
      </div>
      <div class="div-registro-condiciones w-clearfix">
        <div class="div-registro"></div>
        <div class="pregunta">¿Ya tienes una cuenta?</div><a href="#" data-ix="cerrar2" class="link-registro">Login</a></div>
    </div>
  </div>
        <div data-ix="modalwrapper2" class="modal-wrapper-registro">
    <div class="login-wrapper w-clearfix">
      <div class="w-clearfix">
        <div data-ix="cerrar-registro" class="x">x</div>
      </div>
      <div class="form-login w-form">
        <form id="email-form-2" name="email-form-2" data-name="Email Form 2"><input type="text" class="textfield tf w-input" maxlength="256" name="Nombre-2" data-name="Nombre 2" placeholder="Nombre" id="Nombre-2" required=""><input type="text" class="textfield tf w-input" maxlength="256" name="Apellidos-2" data-name="Apellidos 2" placeholder="Apellidos" id="Apellidos-2" required=""><input type="email" class="textfield tf w-input" maxlength="256" name="Correo-electronico-9" data-name="Correo Electronico 9" placeholder="Correo electrónico" id="Correo-electronico-9" required=""><input type="email" class="textfield tf w-input" maxlength="256" name="Tel-fono-m-vil-2" data-name="Tel Fono M Vil 2" placeholder="Teléfono móvil" id="Tel-fono-m-vil-2" required=""><input type="password" class="textfield tf w-input" maxlength="256" name="contrase-a" data-name="contraseña" placeholder="Crea una contraseña" id="contrase-a-6" required="">
          <div class="bold sm text">Cumpleaños</div>
          <div class="w-row">
            <div class="c w-clearfix w-col w-col-5">
              <div data-delay="0" class="dd3 dropdown mes w-dropdown">
                <div class="dropdown-toggle w-dropdown-toggle">
                  <div class="sb sm text">Mes</div>
                  <div class="ar dd-arrow w-icon-dropdown-toggle"></div>
                </div>
                <nav class="w-dropdown-list"><a href="#" class="droplink w-dropdown-link">1</a></nav>
              </div>
            </div>
            <div class="c w-clearfix w-col w-col-3">
              <div data-delay="0" class="dd3 dropdown mes w-dropdown">
                <div class="dropdown-toggle w-dropdown-toggle">
                  <div class="sb sm text">Día</div>
                  <div class="ar dd-arrow w-icon-dropdown-toggle"></div>
                </div>
                <nav class="w-dropdown-list"><a href="#" class="droplink w-dropdown-link">1</a></nav>
              </div>
            </div>
            <div class="c w-clearfix w-col w-col-4">
              <div data-delay="0" class="dd3 dropdown mes w-dropdown">
                <div class="dropdown-toggle w-dropdown-toggle">
                  <div class="sb sm text">Año</div>
                  <div class="ar dd-arrow w-icon-dropdown-toggle"></div>
                </div>
                <nav class="w-dropdown-list"><a href="#" class="droplink w-dropdown-link">1</a></nav>
              </div>
            </div>
          </div>
          <div class="block-tyc w-clearfix">
            <div class="p2 pregunta">*Al registrarme acepto los</div>
            <div class="pregunta tyc"><a href="#" class="link">Términos y condiciones</a></div>
          </div>
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form</div>
        </div>
      </div><a href="#" data-ix="cerrar-registro" class="login w-button">Registro</a>
      <div class="div-registro"></div>
      <div class="pregunta">¿Ya tienes una cuenta?</div><a href="#" data-ix="cerrar2" class="link-registro">Login</a></div>
  </div>
        <div class="seccion-mail">
            <div class="container-bienvenida w-container"><a href="#" class="link-a-home w-inline-block"><img src="{{ asset('web/images/Logo_Stadibox-03.png') }}" alt="Stadibox.com" class="im-logo"></a>
                <h1 class="h1">¡Bienvenido a la primera comunidad de renta de palcos y plateas en el mundo!</h1>
                <div class="black-cen text_big">Estimado &quot;{!! $full_name !!}&quot;:<br>Nos alegra informarte que tu cuenta ha sido verificada con éxito.</div>
                <div class="div-block-5"><a href="{{url('/')}}" class="btn w-button">Ir a Stadibox</a></div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
        <script src="{{ asset('web/js/webflow.js') }}" type="text/javascript"></script>
        <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    </body>
</html>