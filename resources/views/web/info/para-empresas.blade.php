@extends('web.layouts.main')

@section('content')
 
    <div class="fondo-empresas hero-acerca hero-section">
            <div class="container-hero container-space w-container">
                <h1 class="h1-slogan">Aumenta tus ventas con Stadibox</h1>
                <h2 class="bold h2">¡Consiente a tus clientes con una experiencia inolvidable!</h2>
            </div>
        </div>
        
        <div class="seccion">
            <div class="w-container">
                <h3 class="cen h2-title">¿Sabías que cuesta mucho más encontrar nuevos clientes que retener a los existentes?</h3>
                <div class="cen text_med">La inversión en publicidad, contactos y el tiempo en generar nuevos clientes te puede costas hasta 9 veces más que retenerlos. Invitar a tus clientes y empleados en palcos y suites privadas creará una experiencia única e impulsará tus ventas.</div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Mejor tu relación con tus clientes</h3>
                        <div class="text_med">Una experiencia como la que te ofrece Stadibox sobrepasa obstáculos de comunicación. Crea un lazo con tus clientes a través de un evento informal y demuéstrale a tus clientes por qué deberían de darte su negocio.</div>
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div><img src="{{ asset('web/images/empresas_clientes.png') }}" alt="clientes" class="im-empresas"></div>
                    </div>
                </div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div><img src="{{ asset('web/images/empresas_asientos.png') }}" alt="asientos para empresas" srcset="{{ asset('web/images/empresas_asientos-p-500.png 500w') }}, {{ asset('web/images/empresas_asientos.png 781w') }}" sizes="(max-width: 479px) 100vw, (max-width: 767px) 29vw, (max-width: 991px) 229.328125px, 299.984375px" class="im-empresas"></div>
                    </div>
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Motiva a tus empleados</h3>
                        <div class="text_med">Motivar a tus empleados ofreciéndoles la oportunidad de asistir a los eventos de sus equipos y artistas preferidos en un palco privado es la mejor forma de incentivarlos y crear una competencia sana.</div>
                    </div>
                </div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Renta por evento o anualmente</h3>
                        <div class="text_med">En Stadibox te ofrecemos varias opciones para rentar un palco para cualquier evento en los foros que tengamos disponibles. Muchos de los propietarios han activado la opción de renta anual para ofrecerte esta opción.</div><a href="venta-palcos.php" class="btn sp10 w-button">Más información de renta anual</a></div>
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div><img src="{{ asset('web/images/empresas_palco.png') }}" alt="palco privado" srcset="{{ asset('web/images/empresas_palco-p-500.png 500w') }}, {{ asset('web/images/empresas_palco.png 737w') }}" sizes="(max-width: 479px) 100vw, (max-width: 767px) 29vw, (max-width: 991px) 229.328125px, 299.984375px" class="im-empresas"></div>
                    </div>
                </div>
            </div>
        </div>

@include('web.includes.newsletter')

@endsection