@extends('web.layouts.main')

@section('content')

    <div id="hero" class="exp hero-inicio">
            <div class="container-hero w-container">
                <h1 class="h1-slogan">Vive una experiencia inolvidable</h1>
                <h2 class="h2-home">¡Los mejores paquetes VIP solo para los más aficionados!</h2>
            </div>
        </div>
        
        <div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div>
                    <h2 class="h2-title">Experiencias VIP</h2>
                </div>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento">
                        <h4 class="h4">Clásico Español 2018</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$52,067<br xmlns="http://www.w3.org/1999/xhtml"></span>EUROS c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Bernabeu</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Madrid</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">04 may 2018 - 10 may 2018</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento steelers">
                        <h4 class="h4">STEELERS VS PATRIOTS</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$32,000<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Heinz Field</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Pittsburg</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">16 dic 2017 - 18 dic 2018</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="publicacion.php" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento superbowl">
                        <h4 class="h4">SUPER BOWL LII</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$5,990<br xmlns="http://www.w3.org/1999/xhtml"></span>USD c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">US Bank Stadium</div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-01.png') }}" class="ic_small">
                                    <div class="txt_mini">Minnesota</div>
                                </div>
                                <div class="div-info"><img src="{{ asset('web/images/fairseats-web-02.png') }}" class="ic_small">
                                    <div class="txt_mini">01 feb 2017 - 05 feb 2017</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    

@include('web.includes.newsletter')

@endsection