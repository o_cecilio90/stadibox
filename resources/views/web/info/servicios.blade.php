@extends('web.layouts.main')

@section('content')

    <div class="fondo-catering hero-acerca hero-section">
            <div class="container-hero container-space w-container">
                <h1 class="h1-slogan">Catering y Servicios Adicionales</h1>
                <h2 class="bold h2">¡Personaliza tu experiencia <strong>Stadibox</strong> y lleva tus eventos al siguiente nivel!</h2>
            </div>
        </div>
        
        <div class="seccion">
            <div class="w-container">
                <h3 class="cen h2-title">¡En Stadibox, el evento se adapta a ti y a tus invitados!</h3>
                <div class="cen text_med">Al dar de alta un evento, el propietario puede ofrecer beneficios especiales como catering, transporte (avión, auto privado, etc.), alimentos y bebidas, meet and greet, hospitality u otros. En caso de que el palco no lo ofrezca, o desees contratar servicios adicionales, en el proceso de compra te damos la opción de hacerlo:</div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Catering</h3>
                        <div class="text_med">Con la contratación del servicio de catering tu y tus amigos tendrán un mesero que los atenderá durante el evento. Así, ustedes disfrutan del evento y se dejan consentir.</div>
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div class="icon-circle"><img src="{{ asset('web/images/iconos-21_1.png') }}" alt="mesero" class="im-servicio"></div>
                    </div>
                </div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div class="icon-circle"><img src="{{ asset('web/images/iconos-22.png') }}" alt="snacks" class="im-servicio"></div>
                    </div>
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Alimentos y bebidas</h3>
                        <div class="text_med">Elige desde nuestro menú lo que deseas que te llevemos al palco para el día del evento. Contamos con una amplia gama de opciones para adaptarnos a cada cliente. Además, si tu palco cuenta con estacionamiento VIP, podrás ingresar tus propios alimentos y bebidas el día del evento.</div>
                    </div>
                </div>
                <div class="espacios row-about-us w-row">
                    <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">
                        <h3 class="h3">Transporte</h3>
                        <div class="text_med">Sabemos que transportarte al foro el día de un gran evento puede ser un dolor de cabeza. Por eso te ofrecemos pasar por ti y tus invitados en un auto privado con chofer y regresarlos a sus casas al final de éste. Indica cuantos pasajeros serán y el número de paradas por viaje que se realizarán y el monto se agregará a tu compra.</div>
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4">
                        <div class="icon-circle"><img src="{{ asset('web/images/transporte-15.png') }}" alt="hombre subiendo al coche" class="im-servicio"></div>
                    </div>
                </div>
            </div>
            <div class="w-container">
                <div class="div_20"></div>
                <div class="nota">*La disponibilidad de estos servicios depende de cada foro y país.</div>
            </div>
        </div>

@include('web.includes.newsletter')

@endsection