@extends('web.layouts.main')

@section('content')

<div class="bg2 black contact-section">
            <div class="container-hero container2 w-container">
                <div class="div">
                    <h1 class="h1-slogan">Venta de palcos y plateas</h1>
                    <p class="cen sp text_big">Stadibox conecta a propietarios que desean vender su palco o plateas con clientes que buscan adquirirlos ¡Vende tu propiedad de una forma fácil, rápida y completamente segura!<br><br><small>Para más información mándanos un WhatsApp al: <a href="#" style="color:white;">5544335566</a></small></p>
                    <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">
                        <div class="tabsmenu w-clearfix w-tab-menu">
                            <a data-w-tab="Quiero vender" class="tablink tl2 w-inline-block w-tab-link">
                                <div class="cen">Quiero vender mi palco o plateas</div>
                            </a>
                            <a data-w-tab="Quiero comprar" class="tablink tl2 w-inline-block w-tab-link">
                                <div class="cen">Quiero comprar un palco o plateas</div>
                            </a>
                        </div>
                        <div class="tabs-content w-tab-content">
                            <div data-w-tab="Quiero vender" class="tabpane w-clearfix w-tab-pane">
                                <div class="_100 div-contacto extra-height">
                                    <h3 class="benefits-title">Quiero vender mi palco o plateas</h3>
                                    <div class="sp text_med white">Ingresa los siguientes datos y uno de nuestros asesores te contactará para guiarte por el proceso de venta de tu propiedad.</div>
                                    <div class="w-form">
                                        <form id="wf-form-Venta-de-propiedad" name="wf-form-Venta-de-propiedad" data-name="Venta de propiedad">
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Nombre" data-name="Nombre" placeholder="Nombre" id="Nombre-2" required>
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <input type="text" class="der text-field w-input" maxlength="256" name="Apellido" data-name="Apellido" placeholder="Apellido" id="Apellido" required="">
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Telefono-2" data-name="Telefono 2" placeholder="Teléfono" id="Telefono-2" required="">
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <input type="email" class="der text-field w-input" maxlength="256" name="Correo-7" data-name="Correo 7" placeholder="Correo" id="Correo-7" required="">
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <select id="Foro-3" name="Foro-3" data-name="Foro 3" class="grande selector w-select" required>
                                                        <option value="">Foro</option>
                                                        <option value="Foro1">Nombre del foro</option>
                                                        <option value="Foro2">Nombre del foro </option>
                                                        <option value="Otro">Otro</option>
                                                    </select>
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Cuando-seleccionan-otro-foro-2" data-name="Cuando Seleccionan Otro Foro 2" placeholder="Indica el nombre del foro" id="Cuando-seleccionan-otro-foro-2" required="">
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <select id="Tipo-de-propiedad-2" name="Tipo-de-propiedad-2" data-name="Tipo De Propiedad 2" class="der grande selector w-select" required>
                                                        <option value="">Tipo de propiedad</option>
                                                        <option value="Propiedad1">Palco</option>
                                                        <option value="Propiedad2">Platea</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6"><select id="Numero-de-asientos-2" name="Numero-de-asientos-2" data-name="Numero De Asientos 2" class="grande selector w-select" required><option value="">Número de asientos</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select></div>
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Precio-de-venta" data-name="Precio de venta" placeholder="Precio de venta" id="Precio-de-venta" required="">
                                                </div>
                                            </div><input type="submit" value="Enviar" data-wait="Por favor espere..." class="btn-enviar w-button"></form>
                                        <div class="success w-form-done">
                                            <div>¡Gracias, hemos recibido tu mensaje! Te contactaremos a la brevedad.</div>
                                        </div>
                                        <div class="error w-form-fail">
                                            <div>¡Ups! Ocurrió un error. Por favor inténtalo de nuevo.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-w-tab="Quiero comprar" class="tabpane w-clearfix w-tab-pane">
                                <div class="_100 div-contacto extra-height">
                                    <h3 class="benefits-title">Quiero comprar un palco o plateas</h3>
                                    <div class="sp text_med white">Ingresa los siguientes datos y uno de nuestros asesores te contactará para platicarte acerca de las opciones disponibles que se alinean a lo que buscas.</div>
                                    <div class="w-form">
                                        <form id="wf-form-Venta-de-propiedad" name="wf-form-Venta-de-propiedad" data-name="Venta de propiedad">
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Nombre-3" data-name="Nombre 3" placeholder="Nombre" id="Nombre-3" required>
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <input type="text" class="der text-field w-input" maxlength="256" name="Apellido-2" data-name="Apellido 2" placeholder="Apellido" id="Apellido-2" required="">
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Telefono-2" data-name="Telefono 2" placeholder="Teléfono" id="Telefono-2" required="">
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <input type="email" class="der text-field w-input" maxlength="256" name="Correo-7" data-name="Correo 7" placeholder="Correo" id="Correo-7" required="">
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6">
                                                    <select id="Foro-3" name="Foro-3" data-name="Foro 3" class="grande selector w-select" required>
                                                        <option value="">Foro</option>
                                                        <option value="Foro1">Nombre del foro</option>
                                                        <option value="Foro2">Nombre del foro </option>
                                                        <option value="Otro">Otro</option>
                                                    </select>
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Cuando-seleccionan-otro-foro-2" data-name="Cuando Seleccionan Otro Foro 2" placeholder="Indica el nombre del foro" id="Cuando-seleccionan-otro-foro-2" required="">
                                                </div>
                                                <div class="col-2 w-col w-col-6">
                                                    <select id="Tipo-de-propiedad-2" name="Tipo-de-propiedad-2" data-name="Tipo De Propiedad 2" class="der grande selector w-select" required>
                                                        <option value="">Tipo de propiedad</option>
                                                        <option value="Propiedad1">Palco</option>
                                                        <option value="Propiedad2">Platea</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="w-row">
                                                <div class="w-col w-col-6"><select id="Numero-de-asientos-2" name="Numero-de-asientos-2" data-name="Numero De Asientos 2" class="grande selector w-select" required><option value="">Número de asientos</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select></div>
                                                <div class="w-col w-col-6">
                                                    <input type="text" class="text-field w-input" maxlength="256" name="Presupuesto-aproximado" data-name="Presupuesto aproximado" placeholder="Presupuesto aproximado" id="Presupuesto-aproximado" required="">
                                                </div>
                                            </div><input type="submit" value="Enviar" data-wait="Por favor espere..." class="btn-enviar w-button"></form>
                                        <div class="success w-form-done">
                                            <div>¡Gracias, hemos recibido tu mensaje! Te contactaremos a la brevedad.</div>
                                        </div>
                                        <div class="error w-form-fail">
                                            <div>¡Ups! Ocurrió un error. Por favor inténtalo de nuevo.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="seccion-admin">
            <div class="div_white">
                <div class="w-container">
                    <div class="div_20">
                        <h3 class="cen h2-title">¿Cómo funciona?</h3>
                    </div>
                    <div class="w-row">
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/iconos-11.png') }}" alt="icono de publicidad" class="icon p1 small">
                                <div class="text_med">Uno de nuestros especialistas te contactará para explicarte el proceso de compra-venta de palcos y plateas</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-01.png') }}" alt="icono dashboard" class="icon small">
                                <div class="text_med">Stadibox revisará que todos los documentos estén en orden para poder realizar la compra-venta</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-02.png') }}" alt="Icono de sobre con pases" class="icon small">
                                <div class="text_med">Stadibox te acompañará en todo el proceso para una transacción ágil y segura</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@include('web.includes.newsletter')

@endsection