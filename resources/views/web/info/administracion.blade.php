@extends('web.layouts.main')

@section('content')    
    <div class="black contact-section section-admin">
            <div class="container-hero container2 w-container">
                <div class="div">
                    <h1 class="h1-slogan">Programa Partners: <br>Administración de palcos y plateas</h1>
                    <h1 class="green h2 medium">¡Únete y gana hasta $1,000,000 mxn al año!</h1>
                    <p class="cen text_big">En Stadibox nos gusta consentirte, por eso te ofrecemos la opción de que administremos tu palco. Así, tu te despreocupas y nosotros nos encargamos de que tu palco o platea te genere dinero todo el año.</p>
                </div>
                <div class="block-outline">
                    <div class="w-row">
                        <div class="w-col w-col-6">
                            <div>
                                <h3 class="benefits-title">Beneficios</h3>
                                <div class="div-renta">
                                    <div class="row_20 w-row">
                                        <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                        <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                            <div class="text_med white">Optimizamos tus ganancias para cada evento.</div>
                                        </div>
                                    </div>
                                    <div class="row_20 w-row">
                                        <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                        <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                            <div class="text_med white">Visualiza tus rendimientos desde tu dashboard.</div>
                                        </div>
                                    </div>
                                    <div class="row_20 w-row">
                                        <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                        <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                            <div class="text_med white">¿Quieres asistir a un evento? ¡No hay problema! Avísanos desde tu dashbaord y te llevaremos tus boletos a donde nos indiques.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w-clearfix w-col w-col-6">
                            <div class="_100 div-contacto extra-height">
                                <h3 class="benefits-title">¡Únete como partner!</h3>
                                <div class="w-form">
                                    <form id="wf-form-Administracion-de-propiedades" name="wf-form-Administracion-de-propiedades" data-name="Administracion de propiedades"><input type="text" class="text-field w-input" maxlength="256" name="Nombre-Completo" data-name="Nombre Completo" placeholder="Nombre Completo" id="Nombre-Completo">
                                        <div class="w-row">
                                            <div class="w-col w-col-6"><input type="text" class="text-field w-input" maxlength="256" name="Telefono" data-name="Telefono" placeholder="Teléfono" id="Telefono" required=""></div>
                                            <div class="col-2 w-col w-col-6"><input type="email" class="der text-field w-input" maxlength="256" name="Correo" data-name="Correo" placeholder="Correo" id="Correo-6" required=""></div>
                                        </div><input type="submit" value="Enviar" data-wait="Por favor espere..." class="btn-enviar w-button"></form>
                                    <div class="success w-form-done">
                                        <div>¡Gracias, hemos recibido tu mensaje! Te contactaremos a la brevedad.</div>
                                    </div>
                                    <div class="error w-form-fail">
                                        <div>¡Ups! Ocurrió un error. Por favor inténtalo de nuevo.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="green text_big"><span>Déjanos</span> tus datos y te contactaremos para activar tu servicio de administración <br>¡Es completamente gratis!</div>
                </div>
            </div>
            <div class="nota-admin text_med">*Los ingresos potenciales dependen de cada foro, ubicación y los eventos programados.</div>
        </div>
        
        <div class="seccion-admin">
            <div class="div_white">
                <div class="w-container">
                    <div class="div_20">
                        <h3 class="cen h2-title">¿Cómo funciona?</h3>
                    </div>
                    <div class="w-row">
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/iconos-11.png') }}" alt="icono de publicidad" class="icon p1 small">
                                <div class="text_med">Stadibox utiliza su red de contactos y de publicidad para alcanzar al mayor número de clientes potenciales.</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/iconos-12.png') }}" alt="icono dashboard" class="icon small">
                                <div class="text_med">Ingresa a tu dashboard para visualizar tus rendimientos y tu estado de cuenta. Cada vez que se rente tu propiedad recibirás tu pago a la cuenta que hayas registrado.</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/iconos-13.png') }}" alt="Icono de sobre con pases" class="icon small">
                                <div class="text_med">Si deseas asistir a un evento, aparta tus lugares desde tu dashboard y te llevaremos tus pases a tu domicilio. Una vez finalizado el evento pasaremos por ellos.</div>
                            </div>
                        </div>
                    </div>
                    <div style="padding-top:56.17021276595745%" class="renta-video w-embed w-video"><iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Fs2vBk4DDgu4%3Ffeature%3Doembed&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Ds2vBk4DDgu4&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fs2vBk4DDgu4%2Fhqdefault.jpg&key=c4e54deccf4d4ec997a64902e9a30300&type=text%2Fhtml&schema=youtube" scrolling="no" frameborder="0" allowfullscreen=""></iframe></div>
                </div>
            </div>
        </div>
@include('web.includes.newsletter')

@endsection