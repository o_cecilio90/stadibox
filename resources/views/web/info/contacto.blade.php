@extends('web.layouts.main')

@section('content')

    <div class="black contact-section">
            <div class="container-hero w-container">
                <div class="div">
                    <h1 class="h1-slogan">Contáctanos</h1>
                    <p class="cen sp text_big">Si tienes alguna duda o comentario escríbenos y te responderemos a la brevedad.</p>
                </div>
                <div class="block-outline contact w-row">
                    <div class="w-clearfix w-col w-col-6">
                        <div class="_100 div-contacto extra-height">
                            <h3 class="benefits-title">¡Renta aquí!</h3>
                            <div class="w-form">
                                <form id="wf-form-Contacto" name="wf-form-Contacto" data-name="Contacto">
                                    <input type="text" class="text-field w-input" maxlength="256" name="Nombre-Completo" data-name="Nombre Completo" placeholder="Nombre Completo" id="Nombre-Completo-3">
                                    <div class="w-row">
                                        <div class="w-col w-col-6"><input type="text" class="text-field w-input" maxlength="256" name="Telefono" data-name="Telefono" placeholder="Teléfono" id="Telefono" required=""></div>
                                        
                                        <div class="col-2 w-clearfix w-col w-col-6"><input type="email" class="der text-field w-input" maxlength="256" name="Correo" data-name="Correo" placeholder="Correo" id="Correo-6" required=""></div>

                                    </div><input type="text" class="text-area w-input" maxlength="256" name="Mensaje" data-name="Mensaje" placeholder="Mensaje" id="Mensaje-2" required=""><input type="submit" value="Enviar" data-wait="Por favor espere..." class="btn-enviar w-button">
                                </form>
                                <div class="w-row" style="margin-top:5%;font-weight:600 !important;">O mándanos un WhatsApp al: <a href="#" style="color:white;">5533445566</a></div>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-col w-col-6"><img src="{{ asset('web/images/soccer.jpg') }}" alt="equipo Stadibox" class="soccer-contacto"></div>
                </div>
            </div>
        </div>

@include('web.includes.newsletter')

@endsection