@extends('web.layouts.main')

@section('content')

<div class="black contact-section">
            <div class="container-hero container2 w-container">
                <div class="div">
                    <h1 class="h1-slogan">Renta Anual</h1>
                    <p class="cen sp text_big">Existen muchas empresas y personas que desean rentar un palco o una platea por un período de 12 meses o más. Si te interesa rentar tu propiedad, llena este formulario.</p><a href="../renta-anual/disponibles.php" class="btn w-button">Si deseas obtener una propiedad por un año, ¡haz click aquí!</a></div>
                <div class="block-outline w-row">
                    <div class="w-col w-col-6">
                        <div>
                            <h3 class="benefits-title">Beneficios</h3>
                            <div class="div-renta">
                                <div class="row_20 w-row">
                                    <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                    <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                        <div class="text_med white">Genera un gran ingreso sin preocupaciones.</div>
                                    </div>
                                </div>
                                <div class="row_20 w-row">
                                    <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                    <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                        <div class="text_med white">Un solo cliente, un solo registro.</div>
                                    </div>
                                </div>
                                <div class="row_20 w-row">
                                    <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                    <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                        <div class="text_med white">Stadibox te brinda seguridad y apoyo en todo el proceso.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="green text_big"><span>Déjanos</span> tus datos y te contactaremos para ayudarte a poner en renta tu propiedad por 12 meses o más.</div>
                    </div>
                    <div class="w-clearfix w-col w-col-6">
                        <div class="_100 div-contacto extra-height">
                            <h3 class="benefits-title">¡Renta aquí!</h3>
                            <div class="w-form">
                                <form id="wf-form-form-renta-anual" name="wf-form-form-renta-anual" data-name="form renta anual"><input type="text" class="text-field w-input" maxlength="256" name="Nombre-Completo" data-name="Nombre Completo" placeholder="Nombre Completo" id="Nombre-Completo">
                                    <div class="w-row">
                                        <div class="w-col w-col-6"><input type="text" class="text-field w-input" maxlength="256" name="Telefono" data-name="Telefono" placeholder="Teléfono" id="Telefono" required=""></div>
                                        <div class="col-2 w-col w-col-6"><input type="email" class="der text-field w-input" maxlength="256" name="Correo" data-name="Correo" placeholder="Correo" id="Correo-6" required=""></div>
                                    </div><select id="Foro-3" name="Foro-3" data-name="Foro 3" class="grande selector w-select"><option value="">Foro</option><option value="Foro1">Nombre del foro</option><option value="Foro2">Nombre del foro </option><option value="Otro">Otro</option></select><input type="text" class="text-field w-input" maxlength="256" name="Cuando-seleccionan-otro-foro" data-name="Cuando Seleccionan Otro Foro" placeholder="Indica el nombre del foro" id="Cuando-seleccionan-otro-foro" required="">
                                    <div class="w-row">
                                        <div class="w-col w-col-6"><select id="Tipo-de-propiedad" name="Tipo-de-propiedad" data-name="Tipo De Propiedad" class="grande selector w-select"><option value="">Tipo de propiedad</option><option value="Propiedad1">Palco</option><option value="Propiedad2">Platea</option></select></div>
                                        <div class="w-clearfix w-col w-col-6"><select id="Numero-de-asientos" name="Numero-de-asientos" data-name="Numero De Asientos" class="der grande selector w-select"><option value="">Número de asientos</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option></select></div>
                                    </div><input type="submit" value="Enviar" data-wait="Por favor espere..." class="btn-enviar w-button"></form>
                                <div class="success w-form-done">
                                    <div>¡Gracias, hemos recibido tu mensaje! Te contactaremos a la brevedad.</div>
                                </div>
                                <div class="error w-form-fail">
                                    <div>¡Ups! Ocurrió un error. Por favor inténtalo de nuevo.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="seccion-admin">
            <div class="div_white">
                <div class="w-container">
                    <div class="div_20">
                        <h3 class="cen h2-title">¿Cómo funciona?</h3>
                    </div>
                    <div class="w-row">
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-01.png') }}" alt="icono de búsqueda" class="icon p1 small">
                                <div class="text_med">Uno de nuestros especialistas te contactará para explicarte el programa de renta anual.</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/iconos-03_1.png') }}" alt="icono teléfono" class="icon segundopaso small">
                                <div class="text_med">Acordaremos el precio y tiempo por el cual se rentará tu propiedad, así como las condiciones de la renta.</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4">
                            <div class="bb2 bout-block"><img src="{{ asset('web/images/profile-04.png') }}" alt="Icono de perfil" class="icon segundopaso small">
                                <div class="text_med">Se hará un match entre tu propiedad y el cliente final. Juntaremos toda la documentación necesaria para la seguridad de ambas partes.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

@include('web.includes.newsletter')

@endsection