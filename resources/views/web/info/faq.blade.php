@extends('web.layouts.main')

@section('content')

<div class="seccion-faq">
            <div class="div-preguntas">
                <h1 class="h1-slogan">Preguntas Frecuentes</h1>
            </div>
            <div class="container-2 w-container">
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h1 class="h-faqs">Preguntas Generales</h1>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Qué es Stadibox?</h3>
                            <div class="answer bold text"><strong>Stadibox</strong> es una plataforma digital que conecta a dueños de <strong>palcos, suites y plateas</strong> para eventos deportivos y culturales con personas y empresas que desean rentar dichas propiedades ya sea para un evento o durante un periodo de tiempo.<br><br><strong>Stadibox</strong> opera con seguridad y transparencia, garantizando a nuestros usuarios, tanto compradores como vendedores, una transacción confiable.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Por qué me conviene rentar en Stadibox?</h3>
                            <div class="answer bold text"><strong>Stadibox</strong> ofrece una alternativa segura, cómoda y confiable para <strong>rentar palcos y plateas</strong> para tu evento favorito a través de una plataforma especializada. <strong>Stadibox</strong> revisa la autenticidad de las propiedades y protege a los dueños de las mismas por daños o pérdida de boletos mediante de un depósito de seguridad. Además, <strong>Stadibox</strong> se encarga de pasar por los pases de acceso y regresarlos.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo registro mi palco o plateas?</h3>
                            <ol>
                                <li>
                                    <div class="answer bold li text">Los dueños <a href="http://www.stadibox.com/rentarpalco.jsp" data-ix="abrir-modal-palco-o-platea" class="gl">registran su palco o platea</a>, proporcionando información y fotos de  la misma, sus datos de contacto y brindando la documentación requerida por <strong>Stadibox</strong> para avalar su autenticidad.</div>
                                </li>
                                <li>
                                    <div class="answer bold li text"><strong>Stadibox</strong> verifica la autenticidad de la propiedad y una vez autorizado contacta al dueño para que este pueda comenzar a rentar.</div>
                                </li>
                                <li>
                                    <div class="answer bold li text">Los dueños dan de alta su propiedad para un evento, estableciendo el precio específico para ese evento o bien entran al esquema de <a target="_blank" href="../info/administracion.php" class="gl">administración de propiedades</a>, en donde <strong>Stadibox</strong> se encarga de gestionar las rentas directamente.</div>
                                </li>
                            </ol>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo pongo en renta mi palco o mi platea para un evento?</h3>
                            <ol>
                                <li>
                                    <div class="answer bold li text">Después de que tu propiedad haya sido verificada, busca el evento para el cual deseas poner en renta tu <strong>palco o platea</strong> y da click en el botón que dice &quot;Vender&quot;.</div>
                                </li>
                                <li>
                                    <div class="answer bold li text">Selecciona el <strong>palco o platea</strong> que deseas rentar y completa el proceso de <strong>renta</strong>.</div>
                                </li>
                                <li>
                                    <div class="answer bold li text">
                                        <div class="answer bold li text">Cuando tu propiedad haya sido rentada te lo notificaremos por correo electrónico.</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="answer bold li text">
                                        <div class="answer bold li text">Un representante de <strong>Stadibox</strong> recogerá los pases en la dirección que tu indiques para entregárselos al cliente, firmando siempre de entregado y recibido.</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="answer bold li text">
                                        <div class="answer bold li text">Al finalizar el evento, <strong>Stadibox</strong> recogerá los pases con el cliente y te los entregará en la dirección que hayas proporcionado.</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="answer bold li text">
                                        <div class="answer bold li text">Una vez finalizado el evento, <strong>Stadibox</strong> te transferirá el monto acordado. Los pagos se realizan los días 14 y 28 o el día siguiente hábil.</div>
                                    </div>
                                </li>
                            </ol>
                            <div class="answer bold li text"><strong>* Stadibox</strong> puede administrar tu <strong>palco o platea</strong>. Nosotros nos encargamos de que ganes dinero, tu te encargas de disfrutarlo. <a href="../info/administracion.php" target="_blank" class="gl">Ver más</a></div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cuánto puedo ganar al año rentando mi palco en Stadibox?</h3>
                            <div class="answer bold text">Si eliges que <strong>Stadibox</strong> <strong xmlns="http://www.w3.org/1999/xhtml"><a href="../info/administracion.php" class="gl">administre tu palco</a></strong>, puedes ganar hasta $100,000 pesos por boletos. En un <strong>palco</strong> con capacidad para 10 personas, esto significa hasta <strong>1 millón de pesos por año.<br><br></strong>Rentando por evento, tus ingresos podrían variar dependiendo de los eventos y lugares que rentes.<strong><br></strong></div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo rento un palco o plateas?</h3>
                            <ol>
                                <li>
                                    <div class="answer arrow-link bold li text">Busca el estadio, equipo o evento al que quieren asistir y elige la propiedad que más te guste.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Completa el proceso de compra. <br>*Aquí deberás realizar un pago por tu compra y dejar un depósito de seguridad, el cual será devuelto íntegramente una vez que finalice el evento y se hayan entregado los pases, verificando que no hubo daños a la propiedad.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text"><strong>Stadibox</strong> te entregará los boletos en la dirección que hayas indicado. Tendrás que firmar de recibido.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">¡Disfruta de tu evento!</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Al finalizar el evento, un agente de <strong>Stadibox</strong> recogerá los pases en la dirección que hayas indicado.</div>
                                </li>
                            </ol>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo funciona la administración de palcos y plateas?</h3>
                            <div class="answer bold text">Que <strong>Stadibox</strong> administre tu <strong>palco o platea</strong> tiene grandes beneficios. Para aprender más acerca de esta modalidad da click <a href="http://www.stadibox.com/adminpropiedades.jsp" target="_blank" class="gl">aquí</a>.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si Stadibox está administrando mi palco y quiero asistir a algún evento?</h3>
                            <div class="answer bold text">Si quisieras utilizar tu <strong>palco</strong> lo único que tienes que hacer es avisar a tu asesor personal y verificar que no se haya rentado para dicho evento. Te llevaremos los pases a tu dirección y pasaremos por ellos una vez que haya terminado el evento. Así puedes asistir a tus eventos favoritos y seguir generando ingresos cuando no lo quieras utilizar.<br><br>Si te gustaría que administremos tu <strong>palco o plateas</strong> da click <a href="http://www.stadibox.com/adminpropiedades.jsp" target="_blank" class="gl">aquí</a>.<span><a href="mailto:mipalco@stadibox.com?subject=Administraci%C3%B3n%20de%20palcos" class="gl"></a></span></div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Puedo rentar una propiedad durante un año?</h3>
                            <div class="answer bold text">Claro, muchos de los propietarios han activado la opción de permitir la renta de su propiedad por un año. Para más información de click <a href="http://www.stadibox.com/rentaanual.jsp" target="_blank" class="gl">aquí</a>.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué requisitos necesito para registrar mi palco?</h3>
                            <div class="answer bold spacedown text">Contar con un <strong>palco</strong> en algún foro en donde haya eventos deportivos, conciertos o eventos culturales y contar con los siguientes documentos:</div>
                            <ul>
                                <li>
                                    <div class="answer bold li text">Foto de identificación oficial (pasaporte o credencial para votar)</div>
                                </li>
                                <li>
                                    <div class="answer bold li text">CURP</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">RFC</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Comprobante de domicilio    no mayor a tres meses.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Foto de título de propiedad</div>
                                </li>
                            </ul>
                        </div>
                        <div class="q">
                            <h1 class="faq-q">¿Qué requisitos necesito para registrar mis plateas?</h1>
                            <div class="answer bold spacedown text">Contar con una <strong>platea</strong> en algún foro en donde haya eventos deportivos, conciertos o eventos culturales y contar con los siguientes documentos:</div>
                            <ul>
                                <li>
                                    <div class="answer bold li text">Foto de identificación oficial (pasaporte o credencial para votar)</div>
                                </li>
                                <li>
                                    <div class="answer bold li text">CURP</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">RFC</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Comprobante de domicilio no mayor a tres meses.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Foto de título de propiedad</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Protección al Cliente</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Es seguro comprar en Stadibox?</h3>
                            <div class="answer bold text">Por supuesto, todos los pagos se realizan mediante ______________, una plataforma que brinda privacidad absoluta por medio de encriptación de datos y protege tu dinero hasta verificar tu compra el día del evento.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué hago si encuentro algún daño en la propiedad al llegar al evento?</h3>
                            <div class="answer bold text">Si al llegar a la propiedad encuentras algún daño no especificado en la descripción de la misma, te recomendamos tomar una fotografía y enviarla inmediatamente al nuestro servicio de soporte <a href="mailto:soporte@stadibox.com?subject=Soporte%20Stadibox" class="gl">soporte@stadibox.com</a> junto con los datos de tu reservación. Esto te protegerá en caso de que hubiera una reclamación posterior por parte del dueño.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si pierdo los pases antes del evento?</h3>
                            <div class="answer bold text">Si pierdes los pases antes del evento, notifícanos inmediatamente. <strong>Stadibox</strong> hará lo posible por conseguirte un reemplazo. Toma en cuenta que cada reemplazo tiene un costo de $2,500mxn o más, dependiendo de cada foro.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si pierdo los pases después del evento?</h3>
                            <div class="answer bold text">Tu depósito de garantía se tomará a cuenta para cubrir la penalización que el foro impone por pérdida y recuperación de boletos y pases de acceso al estacionamiento. Si el monto de la penalización supera al del depósito de garantía, se te hará un cargo correspondiente a esta diferencia.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si renté un palco y el vendedor se niega a entregar sus pases?</h3>
                            <div class="answer bold text">Una vez que el pago está realizado, el propietario tiene prohibido cancelar la renta para dicho evento. Existen dos fases de aprobación, la primera es cuando el cliente confirma y la segunda es cuando Stadibox tiene los pases en su poder. Apenas tengamos los pases se te enviará un correo con esta confirmación.En algún caso extraordinario en el que el cliente no nos llegará a entregar sus pases, nosotros te conseguiremos otro palco o te regresaremos tu dinero. Esto ocasionará que el propietario sea penalizado con una suspensión temporal o permanente de la plataforma dependiendo de las circunstancias. <br><br>Para mayor información contacta a <a href="mailto:soporte@stadibox.com?subject=%C2%A1Urgente!%20Necesito%20conseguir%20pases." class="gl">soporte@stadibox.com</a></div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Protección al Propietario</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Es seguro poner en renta mi propiedad en Stadibox?</h3>
                            <div class="answer bold text">Para nosotros, tu tranquilidad es lo más importante. Para garantizar la protección de tu propiedad, pedimos un depósito de seguridad al cliente, cuyo monto tú estableces al momento de inscribirlo en un evento. Después del evento, te damos un plazo de 72 horas para verificar que no hubo daños a tu propiedad y de ser así, regresamos el depósito al cliente. Para evitar que tengas que ir a visitar tu palco o plateas, te ofrecemos la opción de que <strong>Stadibox</strong> administre tu propiedad. Si deseas más información de click <a href="../info/administracion.php" class="gl">aquí.</a><br><br>En caso de que lleguen a haber daños, se levantará una investigación tras la cual se determinará la penalización al cliente.<br><br>Al entregar tus boletos, pediremos al cliente que firme nuestro formato de entrega y devolución de pases  y tomaremos una foto a su identificación oficial (pasaporte o credencial para votar). Con eso estarás protegido ya que existirá un registro de que se entregaron tus pases.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si encuentro daños en mi propiedad?</h3>
                            <div class="answer bold text">Si encuentras algún daño en tu propiedad, notifícanos de inmediato a <a href="mailto:soporte@stadibox.com?subject=Da%C3%B1os%20a%20mi%20propiedad" class="gl">soporte@stadibox.com</a>. El depósito de garantía que estableciste para la renta de tu propiedad sirve para cubrir esos daños. <strong>Stadibox</strong> abrirá una investigación para poder solucionar el caso.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si se pierden mis boletos?</h3>
                            <div class="answer bold text">Si se llegarán a extraviar tus boletos o pases de estacionamiento te lo haremos saber de inmediato. El depósito de garantía que estableciste para la renta de tu propiedad sirve para cubrir estos gastos. <strong>Stadibox</strong> te guiará en el proceso de recuperación de tus boletos. <br><br>*Stadibox no se hace responsable por cubrir el monto de recuperación de pases extraviados.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si renté mi propiedad para un evento y me arrepentí?</h3>
                            <div class="answer bold text">Recuerda que al poner en renta tu propiedad estas aceptando un compromiso con tus clientes potenciales. Si una persona ya rentó tu <strong>palco o platea</strong>, deberás mantener dicho compromiso. Los propietarios que no respeten sus rentas tendrán una penalización del 20% de su renta y podrán ser dados de baja definitiva de la plataforma.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Disponibilidad</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">No encuentro una propiedad disponible para el evento al que quiero asistir, ¿qué hago?</h3>
                            <div class="answer bold text">Si no encuentras una propiedad disponible para el evento al que deseas asistir, envíanos un correo a<span> <a href="mailto:palcos@stadibox.com?subject=Necesito%20una%20propiedad" class="gl">palcos@stadibox.com</a></span> con tu nombre completo y un teléfono y haremos lo posible para conseguirte lo que necesitas. En caso de lograr conseguirlo, te contactaremos vía correo electrónico o mediante una llamada para confirmártelo.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">No encuentro el evento al que quiero asistir, ¿qué hago?</h3>
                            <div class="answer bold text">Si deseas asistir a un evento y no lo encuentras registrado, te aparecerá un formulario en donde podrás solicitar que este sea agregado y haremos lo posible por conseguirte una propiedad para asistir a dicho evento. En caso de lograr conseguirlo, te contactaremos vía correo electrónico o mediante una llamada para confirmártelo. Si tienes cualquier duda, escríbenos a <a href="mailto:soporte@stadibox.com?subject=No%20encuentro%20mi%20evento" class="gl">soporte@stadibox.com</a>. Actualmente solo contamos con palcos en la Ciudad de México, si quisieras asistir a un evento en otro lugar del mundo, escríbenos y haremos lo posible por conseguírtelo.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">No encuentro el evento al que quiero registrar mi propiedad, ¿cómo lo agrego?</h3>
                            <div class="answer bold text">Si al ingresar el nombre de un evento en el buscador, este le indica que el evento no se encuentra registrado, puedes enviar una solicitud de registro para el mismo. Stadibox se encargará de agregarlo al catálogo de eventos y le confirmará vía correo electrónico una vez que este quede registrado.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">No encuentro el foro en donde tengo mi propiedad, ¿cómo lo puedo agregar?</h3>
                            <div class="answer bold text">Si al ingresar el nombre de un evento o foro en el buscador, este le indica que el foro no se encuentra registrado, puedes enviar una solicitud de registro para el mismo. <strong>Stadibox</strong> se encargará de agregarlo al catálogo de foros y te confirmará vía correo electrónico una vez que este quede registrado.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Puedo rentar una propiedad durante un año?</h3>
                            <div class="answer bold text">Claro, muchos de los propietarios han activado la opción de permitir la renta de su propiedad por un año. Para ver las propiedades disponibles para renta anual, da click <a href="../renta-anual/disponibles.php" class="gl">aquí</a>. Para más información acerca de como poner tu propiedad en renta anual da click <a href="../info/venta-palcos.php" target="_blank" class="gl">aquí</a>.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Alimentos y Bebidas</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿El catering está incluido en el precio de los palcos?</h3>
                            <div class="answer bold text">No, cada propietario publicará en la descripción de su palco si incluye alimentos y bebidas y/o servicio de catering  en cada evento, así como un detalle de lo que se ofrece. Los boletos que ocupa el catering ya están contemplados por los dueños y no se descontarán del número de boletos que estás adquiriendo.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Puedo ingresar mis propios alimentos y bebidas?</h3>
                            <div class="answer bold text">Por lo general, el ingreso de alimentos y bebidas a los foros sólo se permite días antes del evento, por lo que te recomendamos que contactes a <strong>Stadibox</strong> a<a href="mailto:soporte@stadibox.com?subject=Alimentos%20y%20bebidas" class="gl"> soporte@stadibox.com </a>para que te dé más información. Las personas que cuentan con estacionamiento VIP en algunos eventos y foros, pueden ingresar alimentos y bebidas el día del evento (esto depende del evento al que vas a asistir). En el caso de plateas, generalmente está prohibido el acceso de alimentos y bebidas.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo puedo contratar servicio de catering para un evento?</h3>
                            <div class="answer bold text">Puedes contratar servicio de catering para un palco de manera independiente. No olvides que el personal de este servicio debe de contar con un pase de acceso personal para ingresar al palco.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Servicios</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Hay baños en los palcos? ¿Qué otras amenidades ofrecen?</h3>
                            <div class="answer bold text">Cada propiedad es diferente. Por eso los dueños especifican los servicios y amenidades que ofrece su palco en el momento que lo registra en la plataforma.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Restricciones</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Pueden ingresar menores de edad a las propiedades?</h3>
                            <div class="answer bold text">Es responsabilidad de los clientes verificar si los menores de edad pueden ingresar a cada evento.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Cancelaciones</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">El evento para el que había rentado una propiedad se canceló, ¿cómo recupero mi dinero?</h3>
                            <div class="answer bold text">No te preocupes, en caso de cancelación de un evento, se te regresará tu dinero íntegramente. Por favor contáctanos a <a href="mailto:soporte@stadibox.com?subject=Cancelaci%C3%B3n%20de%20evento" class="gl">soporte@stadibox.com </a>para indicarnos que esto ha sucedido y comenzaremos con el trámite de devolución.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si el propietario desea cancelar el evento que ya estaba pagado?</h3>
                            <div class="answer bold text">Una vez que el pago está realizado, el propietario tiene prohibido cancelar la renta para dicho evento. Existen dos fases de aprobación, la primera es cuando el cliente confirma y la segunda es cuando Stadibox tiene los pases en su poder. Apenas tengamos los pases se te enviará un correo con esta confirmación.<br><br>En algún caso extraordinario en el que el cliente no nos llegará a entregar sus pases, nosotros te conseguiremos otro palco o te regresaremos tu dinero. Esto ocasionará que el propietario sea penalizado con una suspensión temporal o permanente de la plataforma dependiendo de las circunstancias.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si ya pague la renta de la propiedad y no podré asistir al evento?</h3>
                            <div class="answer bold text">En este caso deberás contactarnos inmediatamente a <a href="mailto:soporte@stadibox.com?subject=No%20puedo%20asistir%20al%20evento" class="gl">soporte@stadibox.com</a> y haremos lo posible por hacer que la propiedad sea rentada por otro cliente; sin embargo, no garantizamos la devolución de tu dinero por lo que te recomendamos que te asegures de que puedes asistir a un evento antes de realizar tu pago.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo puedo cancelar mi cuenta?</h3>
                            <div class="answer bold text">Si deseas cancelar tu cuenta y que eliminemos tus datos de nuestra base, escríbenos a <a href="mailto:soporte@stadibox.com?subject=Deseo%20cancelar%20mi%20cuenta" class="gl">soporte@stadibox.com</a>.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Estacionamiento</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Todos los palcos y plateas incluyen estacionamiento?</h3>
                            <div class="answer bold text">No, el acceso al estacionamiento y el número de pases a este viene indicado en la publicación de cada propiedad.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si el palco que cuenta con pases de estacionamiento pero se renta  a más de un cliente?</h3>
                            <div class="answer bold text">Cuando el palco se renta por menos del 50% de los lugares a un cliente, no incluye los pases de estacionamiento.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cuál es la diferencia entre estacionamiento normal y estacionamiento VIP?</h3>
                            <div class="answer bold text">Algunos foros cuentan con un estacionamiento interno más cercano a las propiedades que permite el acceso de alimentos y bebidas el día del evento. A esta clase de estacionamiento lo denominamos estacionamiento VIP (dependiendo de cada evento, pueden variar los permisos y restricciones para introducir alimentos y bebidas el día del evento. Si tienes alguna duda contáctanos a <a href="mailto:soporte@stadibox.com?subject=Duda%20de%20estadionamiento%20VIP" class="gl">soporte@stadibox.com</a></div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Beneficios</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Qué significa meet and greet?</h3>
                            <div class="answer bold text">Meet &amp; Greet es un acceso VIP en donde el cliente podrá acercarse a los protagonistas del evento, ya sean artistas, deportistas u otros.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué significa catering?</h3>
                            <div class="answer bold text">Catering se refiere al servicio de meseros dentro de los palcos. Para ofrecer catering, los propietarios o clientes deben contratar a los meseros de manera independiente y tomar en cuenta sus boletos en caso de que sea necesario de que utilicen uno.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué tipo de transporte se puede ofrecer?</h3>
                            <div class="answer bold text">Se puede ofrecer todo tipo de transporte, desde un auto privado para transportar a los clientes al evento, hasta un vuelo redondo desde su ciudad de origen. El propietario establece si ofrece transporte y escribe una descripción de lo que esto incluye. Lo mismo pasa con el hospedaje.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si el propietario indicó que ofrecía un beneficio y no fue así?</h3>
                            <div class="answer bold text">En este caso se le cobrará una penalización según el valor estimado del beneficio por <strong>Stadibox</strong> y este monto se depositará en la cuenta del cliente. En caso de haber compartido el palco entre varios clientes, el monto se dividirá proporcionalmente entre ellos. El propietario podrá ser dado de baja de la plataforma.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Atención al Cliente</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Cómo contacto el servicio a cliente?</h3>
                            <div class="answer bold text">Contamos con atención al cliente por correo electrónico. Escríbenos a <a href="mailto:soporte@stadibox.com?subject=Necesito%20atenci%C3%B3n%20a%20cliente" class="gl">soporte@stadibox.com</a>, indicando tu nombre completo, número de reservación, teléfono fijo, celular y la forma en la que te podemos ayudar. Nosotros te contactaremos en seguida.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Depósito de Seguridad</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Qué cantidad debería de establecer para el depósito de seguridad?</h3>
                            <div class="answer bold text">La cantidad que debes establecer para un depósito de seguridad es una decisión personal; sin embargo, no es una que debas de tomar a la ligera ya que será lo máximo que <strong>Stadibox</strong> te garantiza por daños a tu propiedad. Toma en consideración que un depósito de seguridad demasiado alto puede ahuyentar a los clientes ya que esta cantidad será retenida de su tarjeta de crédito hasta 72 horas después de finalizar el evento. Este es el tiempo que tu tienes para levantar una queja por daños o pérdida, tras lo que se iniciará una investigación para determinar el costo de los mismos.</div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo cobro un depósito de seguridad?</h3>
                            <div class="answer bold text">Si llegarán a haber daños a tu propiedad o pérdida de pases envíanos un correo a <a href="mailto:soporte@stadibox.com?subject=Dep%C3%B3sito%20de%20seguridad" class="gl">soporte@stadibox.com</a> con su número de reservación, número de palco o plateas, nombre completo, número de teléfono y descripción de los hechos con fotografías de los daños (antes y después). Posteriormente realizaremos una investigación tras la cual se determinará el costo de los daños y se liberará el monto correspondiente al dueño.</div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Protección de Datos</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Qué tan seguros están mis datos?</h3>
                            <div class="answer bold text">Tus datos de contacto estarán protegidos por nosotros y sólo se mostrarán a los usuarios involucrados en una transacción aprobada por ambas partes al momento que el cliente realice el pago para apartar su lugar en un evento o para la renta anual de la misma.<br><br>Para más información acerca de la seguridad de tus datos personales, por favor visita nuestro <a href="../info/aviso-de-privacidad.php" target="_blank" class="gl">aviso de privacidad.</a></div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Precios y Facturación</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Cómo se establecen los precios?</h3>
                            <div class="answer bold text">Cada dueño establecerá el precio de su propiedad por evento o por renta anual. <strong>Stadibox</strong> permite a los propietarios ver los precios de que otros han establecido para el mismo evento con el fin de poder tener un punto de referencia y estar dentro de un rango adecuado de precios que convenga a ambas partes.<br><br><strong>Stadibox</strong> cobrará una tarifa sobre del costo establecido por el propietario que cubrirá los gastos de envío, gastos administrativos y comisión por transacción.<a href="#" class="gl"></a></div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">Facturación en Stadibox</h3>
                            <div class="answer bold text">Si requiere una factura por los servicios emitidos por <strong>Stadibox</strong>, por favor envíe sus datos fiscales junto con su número de reservación y correo electrónico a <a href="mailto:facturacion@stadibox.com?subject=Facturaci%C3%B3n%20de%20servicios" class="gl">facturacion@stadibox.com</a><a href="#" class="gl"></a></div>
                        </div>
                        <div class="q">
                            <h3 class="faq-q">¿Cómo facturo el monto total de mi renta?</h3>
                            <div class="answer bold text">Al momento que un propietario suba su propiedad indica si cuenta con las facultades para emitir una factura. Si tienes alguna duda acerca de una propiedad en específico, escríbenos a <a href="mailto:facturacion@stadibox.com?subject=Necesito%20facturar%20mi%20renta" class="gl">facturacion@stadibox.com<br><br></a><strong>Stadibox</strong> te emitirá una factura por nuestros servicios durante el mes en curso que se realizó el pago.<a href="#" class="gl"></a></div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Garantías</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Con qué garantías cuento en Stadibox?</h3>
                            <ul>
                                <li>
                                    <div class="answer arrow-link bold li text">Boletos originales garantizados.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Recupera el 100% de tu dinero en caso de cancelación del evento.</div>
                                </li>
                                <li>
                                    <div class="answer arrow-link bold li text">Stadibox guarda tu dinero hasta que finalice el evento. Al devolver los boletos y recibir confirmación por parte del dueño de que no hubo daños, tu depósito de seguridad se devolverá íntegramente.</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Penalizaciones</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q">¿Qué pasa si un propietario proporciona información falsa acerca de su propiedad o acerca del evento?</h3>
                            <div class="answer bold text">Si la información publicada resulta ser falsa o engañosa, se podrá penalizar al propietario con hasta el 100% del costo de publicación del evento y podría ser dado de baja de la plataforma. En caso de haber detectado o sido víctima de información falsa, por favor escríbenos a <a href="mailto:facturacion@stadibox.com?subject=Recib%C3%AD%20informaci%C3%B3n%20falsa" class="gl">palcos@stadibox.com<br></a><a href="#" class="gl"><br></a></div>
                        </div>
                    </div>
                </div>
                <div class="div-wrap">
                    <div data-ix="abrir-faq" class="barra faq w-clearfix">
                        <div class="div-arrow"><img src="{{ asset('web/images/arrow-01.png') }}" class="image-2"></div>
                        <h2 class="h-faqs">Reglamentos de los Foros</h2>
                    </div>
                    <div class="div-faq">
                        <div class="q">
                            <h3 class="faq-q"><a href="https://www.estadioazteca.com.mx/palcos-y-plateas/reglamento/" target="_blank">Reglamento del Estadio Azteca</a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@include('web.includes.newsletter')

@endsection