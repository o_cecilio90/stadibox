@extends('web.layouts.main')

@section('content') 


    <div class="hero-acerca hero-section home-propietarios">
            <div class="container-hero container-space w-container">
                <h1 class="h1-slogan">¡Genera grandes ingresos con Stadibox!</h1>
                <h2 class="bold h2">Registra tu propiedad, activa tus eventos y Stadibox se encarga de los demás</h2><a href="#" data-ix="abrir-modal-palco-o-platea" class="btn w-button">Registrar mi propiedad</a></div>
        </div>
        
        <div class="seccion">
            <div class="w-container">
                <h3 class="cen h2-title">Renta por evento o por año. Activa la modalidad de administración de propiedades y nosotros nos encargamos de todo.</h3>
                <div class="row-about-us w-row">
                    <div class="w-col w-col-6">
                        <div class="green-block">
                            <div style="padding-top:56.17021276595745%" class="video w-embed w-video"><iframe class="embedly-embed" src="https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Fs2vBk4DDgu4%3Ffeature%3Doembed&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Ds2vBk4DDgu4&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fs2vBk4DDgu4%2Fhqdefault.jpg&key=96f1f04c5f4143bcb0f2e68c87d65feb&type=text%2Fhtml&schema=youtube" scrolling="no" frameborder="0" allowfullscreen=""></iframe></div>
                        </div>
                    </div>
                    <div class="w-col w-col-6">
                        <div class="cen-movil text_med">Stadibox es la primera plataforma tecnológica que centraliza la oferta y demanda de palcos, plateas y boletos para eventos deportivos, conciertos, festivales y mucho más. Conectamos a dueños de palcos y plateas con cientos de clientes potenciales.<br><br>Contamos con un centro de atención dedicado a brindar el mejor servicio a nuestros usuarios, asegurando una experiencia inolvidable y totalmente segura.<br><br>Stadibox es una empresa 100% mexicana con un enfoque global.</div>
                    </div>
                </div>
            </div>
            <div class="w-container">
                <div class="div_20"></div>
            </div>
            <div class="about-us bg w-container">
                <h3 class="cen h2-title">¿Por qué elegir Stadibox?</h3>
                <div class="w-row">
                    <div class="w-col w-col-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-10.png') }}" alt="apretón de manos" class="icon p1 small">
                            <div class="text_med">El primer fan to fan marketplace de renta de palcos en   el mundo</div>
                        </div>
                    </div>
                    <div class="w-col w-col-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-11.png') }}" alt="escudo de seguridad" class="icon paso2 small">
                            <div class="text_med">Seguridad y eficiencia garantizadas, pasamos por tus pases y te los regresamos a donde nos indiques.</div>
                        </div>
                    </div>
                    <div class="w-col w-col-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/Pasos_Home-12.png') }}" alt="soporte al cliente" class="icon paso2 small">
                            <div class="text_med">Atención personalizada para todos nuestros usuarios.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-us w-container">
                <div class="row-about-us w-row">
                    <div class="col-block w-col w-col-6"><img src="{{ asset('web/images/seguridad.jpeg') }}" alt="protección de venta"></div>
                    <div class="col-block linea w-col w-col-6">
                        <h3 class="benefits-title black">Protección a propietarios</h3>
                        <div class="div-renta">
                            <div class="row_20 w-row">
                                <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                    <div class="text_med">Entregamos y devolvemos los pases personalmente con uno de nuestros agentes.</div>
                                </div>
                            </div>
                            <div class="row_20 w-row">
                                <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                    <div class="text_med">Para apartar sus pases, los clientes deben realizar el pagos en línea. Este se libera al finalizar el evento.</div>
                                </div>
                            </div>
                            <div class="row_20 w-row">
                                <div class="w-clearfix w-col w-col-1 w-col-small-1 w-col-tiny-1"><img src="{{ asset('web/images/check2.png') }}" srcset="{{ asset('web/images/check2-p-500x478.png 500w') }}, {{ asset('web/images/check2-p-800x765.png 800w') }}, {{ asset('web/images/check2.png 1524w') }}" sizes="(max-width: 479px) 7vw, 20px" class="check small"></div>
                                <div class="w-col w-col-11 w-col-small-11 w-col-tiny-11">
                                    <div class="text_med">Cada dueño establece un monto de depósito de seguridad para el evento.</div>
                                </div>
                            </div>
                        </div>
                        <div class="bout-block"></div>
                        <div class="bout-block"></div>
                        <div class="bout-block"></div>
                    </div>
                </div>
            </div>
            <div class="about-us bg w-container">
                <h3 class="cen h2-title">Nosotros administramos tu palco</h3>
                <div class="text_med">Si lo prefieres, nosotros nos encargamos de rentar tu palco a clientes, despreocúpate y disfruta de tus rentas</div>
                <div class="row-about-us w-row">
                    <div class="w-col w-col-4 w-col-small-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/paogs-06.png') }}" alt="reportes" class="icon p1 small">
                            <div class="text_med">Reportes mensuales</div>
                        </div>
                    </div>
                    <div class="w-col w-col-4 w-col-small-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/paogs-05.png') }}" alt="dinero" class="icon p1 small">
                            <div class="text_med">Pagos inmediatos</div>
                        </div>
                    </div>
                    <div class="w-col w-col-4 w-col-small-4">
                        <div class="bb2 bout-block"><img src="{{ asset('web/images/paogs-04.png') }}" alt="gol" class="icon p1 small">
                            <div class="text_med">Usa tu palco cuando quieras</div>
                        </div>
                    </div>
                </div><a href="../info/administracion" class="btn full w-button">Más información</a></div>
        </div>

    
@include('web.includes.newsletter')

@endsection