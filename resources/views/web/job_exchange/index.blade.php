@extends('web.layouts.web')

@section('content')

    <div class="equipo hero-acerca hero-section">
        <div class="container-hero container-space w-container">
            <h1 class="h1-slogan">Únete a Stadibox</h1>
            <h2 class="bold h2">¡Vuélvete parte de una gran empresa!</h2>
        </div>
    </div>
    
    <div class="seccion">
        <div class="w-container">
            <div class="row-about-us w-row">
                <div class="w-col w-col-6">
                    <h3 class="cenns h3">En Stadibox buscamos a jóvenes apasionados y talentosos. Contamos con puestos operativos y creativos.</h3>
                    <img src="{{asset('web/images/stadibox_equipo.jpg')}}" alt="equipo stadibox" class="imagen-equipo">
                </div>
                <div class="w-col w-col-6">
                    <div class="space2 text_med">Llena el siguiente formulario y platícanos por qué te gustaría formar parte de nuestro equipo.</div>
                    <div>
                        <div class="w-form">
                            {!! Form::open(['route' => 'job.exchange.email', 'method' => 'POST', 'files'=>true]) !!}
                                <input type="text" class="text-field w-input" maxlength="256" name="name" data-name="Nombre" placeholder="Nombre completo" id="Nombre-2">
                                @if ($errors->has('name'))
                                    <span class="text-danger"><strong>{!! $errors->first('name') !!}</strong></span><br>
                                @endif
                                <input type="email" class="text-field w-input" maxlength="256" name="email" data-name="Correo" placeholder="Correo electrónico" id="Correo-2" required="">
                                @if ($errors->has('email'))
                                    <span class="text-danger"><strong>{!! $errors->first('email') !!}</strong></span><br>
                                @endif
                                <input type="text" class="text-field w-input" maxlength="256" name="phone" data-name="Teléfono" placeholder="Teléfono" id="Tel-fono" required="">
                                @if ($errors->has('phone'))
                                    <span class="text-danger"><strong>{!! $errors->first('phone') !!}</strong></span><br>
                                @endif
                                <input type="text" class="text-field w-input" maxlength="256" name="position" data-name="Puesto" placeholder="Puesto al que deseas ingresar" id="Puesto" required="">
                                @if ($errors->has('position'))
                                    <span class="text-danger"><strong>{!! $errors->first('position') !!}</strong></span><br>
                                @endif
                                <div class="bold txt_small">Platícanos, ¿por qué deseas trabajar en Stadibox y qué crees que podrías aportar a nuestro equipo?</div>
                                <textarea id="Mensaje" maxlength="5000" name="message" class="w-input"></textarea>
                                @if ($errors->has('message'))
                                    <span class="text-danger"><strong>{!! $errors->first('message') !!}</strong></span><br>
                                @endif
                                <input type="file" name="cv" class="btn cancelar purple w-button" />
                                <input type="submit" value="Enviar" data-wait="Espera un momento..." class="aceptar btn w-button">
                            {!! Form::close() !!}
                            <div class="success w-form-done">
                                <div>¡Gracias! Hemos recibido tu mensaje</div>
                            </div>
                            <div class="error w-form-fail">
                                <div>¡Ups! Ocurrió un error. Por favor inténtalo de nuevo.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('web.includes.newsletter')

@endsection
