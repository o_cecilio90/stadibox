<!-- MODAL REGISTRO -->
<div id="register-modal" data-ix="modal-incial" class="modal-wrapper-registro modal" role="dialog">
    <div class="modal-box modal-chico w-clearfix">
        <div data-ix="cerrar-registro-inputs" class="x">x</div>
        <div class="div-close">
            <h3 class="h-popup">Registro</h3>
        </div>
        <div class="fondo-basico wrapper-fondo">
            <div class="form-login w-form">
                {!! Form::open(['url' => 'user', 'method' => 'POST', 'id' => 'wf-form-registro', 'name' => 'wf-form-registro', 'data-name' => 'registro','autocomplete' => 'off']) !!}
                    <div class="w-row">
                        <div class="c w-col w-col-6">
                            <input type="text" class="text-field w-input" maxlength="256" name="name" data-name="Nombre" placeholder="Nombre" id="Nombre" required="">
                            @if ($errors->has('name'))
                                <span class="text-danger"><strong>{!! $errors->first('name') !!}</strong></span><br>
                            @endif
                        </div>
                        <div class="w-col w-col-6">
                            <input type="text" class="text-field w-input" maxlength="256" name="lastname" data-name="Apellidos" placeholder="Apellidos" id="Apellidos" required="">
                            @if ($errors->has('lastname'))
                                <span class="text-danger"><strong>{!! $errors->first('lastname') !!}</strong></span><br>
                            @endif
                        </div>
                    </div>
                    <div class="w-row">
                        <div class="c w-col w-col-6">
                            <input type="number" class="text-field w-input" maxlength="256" name="mobile" data-name="Movil" placeholder="Teléfono móvil" id="Movil" required="">
                            @if ($errors->has('mobile'))
                                <span class="text-danger"><strong>{!! $errors->first('mobile') !!}</strong></span><br>
                            @endif
                        </div>
                        <div class="w-col w-col-6">
                            <input type="email" class="text-field w-input" maxlength="256" name="email" data-name="Correo" placeholder="Correo" id="Correo" required="">
                            @if ($errors->has('email'))
                                <span class="text-danger"><strong>{!! $errors->first('email') !!}</strong></span><br>
                            @endif
                        </div>
                    </div>
                    <div class="w-row">
                        <div class="c w-col w-col-6">
                            <input type="password" class="text-field w-input" maxlength="256" name="password" data-name="Password" placeholder="Contraseña" id="Password" required="">
                            @if ($errors->has('password'))
                                <span class="text-danger"><strong>{!! $errors->first('password') !!}</strong></span><br>
                            @endif
                        </div>
                        <div class="w-col w-col-6">
                            <input type="password" class="text-field w-input" maxlength="256" name="password_confirmation" data-name="Confirmar password" placeholder="Confirmar contraseña" id="Confirmar-password" required="">
                            @if ($errors->has('password_confirmation'))
                                <span class="text-danger"><strong>{!! $errors->first('password_confirmation') !!}</strong></span><br>
                            @endif
                        </div>
                    </div>
                    <div class="w-row">
                        <div class="c w-col w-col-12">
                            <input type="text" class="text-field w-input" maxlength="256" name="birthdate" data-name="Birthdate" placeholder="Fecha de nacimiento" id="birthdate" required="">
                            @if ($errors->has('birthdate'))
                                <span class="text-danger"><strong>{!! $errors->first('birthdate') !!}</strong></span><br>
                            @endif
                        </div>
                    </div>
                    <div class="check1 checkfield w-checkbox w-clearfix">
                        <input type="checkbox" id="term_cond" name="term_cond" data-name="Checkbox 3" class="checkbox-2 w-checkbox-input">
                        <label for="checkbox-4" class="left txt_small w-form-label">Acepto los</label>
                        
                        <div class="pregunta tyc">
                            <a href="info/terminos-y-condiciones.html" target="_blank" class="txt_link">Términos y Condiciones</a>
                        </div>
                        <div class="pregunta"> y la <span class="link2"><a href="info/aviso-de-privacidad.html" target="_blank" class="txt_link">Política de Privacidad</a></span>.
                        </div>
                    </div>
                    <div class="checkfield w-checkbox w-clearfix">
                        <input type="checkbox" id="promotions" name="promotions" data-name="Checkbox 2" class="checkbox-2 w-checkbox-input">
                        <label for="checkbox-2" class="left txt_small w-form-label">Deseo recibir promociones y actualizaciones</label>
                    </div>
                    <div class="checkfield w-checkbox w-clearfix">
                        <input type="checkbox" id="owner" name="owner" data-name="Checkbox 3" class="checkbox-3 w-checkbox-input">
                        <label for="checkbox-3" class="left txt_small w-form-label">Soy propietario de un palco o platea</label>
                    </div>

                    <button id="submit-register-user" class="btn full w-button" disabled>Registrarme</button>
                {!! Form::close() !!}
                <div class="div10 w-clearfix">
                    <div class="left txt_small">¿Ya tienes cuenta?</div><a href="#" data-ix="cerrar-registro-abrir-login" class="link-registro">Iniciar Sesión</a></div>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form</div>
                </div>
            </div>
        </div>
    </div>
</div>