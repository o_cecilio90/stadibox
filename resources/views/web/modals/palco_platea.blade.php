<!-- MODAL PALCO O PLATEA -->
<div data-ix="modal-incial" class="modal-palco-o-platea">
    <div class="modal-box modal-chico">
        <div class="div-close w-clearfix">
            <div data-ix="cerrar-tipo-propiedad" class="x">x</div>
            <h3 class="h-popup">Registrar Propiedad</h3>
        </div>
        <div class="fondo-basico wrapper-fondo">
            <p class="ns parrafo">Selecciona el tipo de propiedad que deseas registrar</p>
            <div data-ix="cerrar-tipo-y-abrir-platea" class="div-propiedad"><a href="#" class="div-tipo w-inline-block"></a>
                <div class="txt_propiedad">Platea o Abono</div>
            </div>
            <div data-ix="cerrar-tipo-y-abrir-palco" class="div-propiedad"><a href="#" class="div-tipo tipo2 w-inline-block"></a>
                <div class="txt_propiedad">Palco</div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL REGISTRO PLATEAS -->        
<div class="div-carpeta-modals-reg-plateas">
    <!-- PASO 1 PLATEAS -->
    <div class="modal-registro-platea-p1">
        <div class="div_line">
            <div class="_50 div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-modal-registro-platea-p1" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a>
        </div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 1 de 2: Información de tu Platea o Abono de Temporada</h1>
                        <div class="text_med">Completa los siguientes campos para registrar tu propiedad</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
            </div>
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-6">
                        <div class="_0 block">
                            <div class="w-form">
                                {!! Form::open(['route' => 'stall.store', 'method' => 'POST', 'id' => 'wf-form-registro-plateas', 'name' => 'wf-form-registro-plateas-1', 'data-name' => 'registro plateas 1','autocomplete' => 'off','files' => true]) !!}
                                    <div class="rubro">
                                        <h2 class="title">Foro</h2><select id="foro-platea" name="forum_id" data-name="foro platea" class="_300 select w-select"><option value="">Selecciona una opción</option></select></div>
                                    <div class="rubro">
                                        <h2 class="title">Zona</h2><a data-ix="open-estadio" class="onlymovil textlink">Ver mapa</a><select id="zona-platea" name="zone_id" data-name="zona platea" class="_300 select w-select"><option value="">Selecciona una opción</option></select></div>
                                    <div class="rubro">
                                        <h2 class="title">Número de Boletos Consecutivos</h2>
                                        <div class="cat nota">Si tus plateas no son consecutivas, las deberás registrar por separado.</div><select id="boletos-plateas" name="stall_tickets" data-name="boletos plateas" class="_300 select w-select">
                                            <option value="">Selecciona una opción</option>
                                            @for($i=0;$i<=10;$i++)
                                                <option value="{!!$i!!}">{!!$i!!}</option>
                                            @endfor                                            
                                        </select></div>
                                    <div class="rubro">
                                        <h2 class="title">Número de tus Plateas</h2>
                                        <div class="cat nota">Ingresa la zona, fila y número de cada una de tus plateas.</div>
                                        <section id="content_stall">
                                        <div class="row_filas">
                                            <input type="text" id="asiento" name="row[]" data-name="asiento" placeholder="Fila..." maxlength="7" class="text-field tfsmall2 w-input">
                                            <input type="text" id="asiento-1" name="seat[]" data-name="asiento" placeholder="Asiento..." maxlength="7" class="text-field tfsmall2 w-input">
                                        </div>
                                        </section>
                                        <br><br><br>
                                        <div class="row_filas" id="add_stalls">
                                            <a href="#" id="add_stall_btn" class="btn">+</a>
                                        </div>
                                    </div>
                                    <div class="rubro" style="clear:left;">
                                        <h2 class="title">No. de Boletos de Estacionamiento Exterior</h2><a data-ix="open-estadio" class="onlymovil textlink">Ver mapa</a><select id="Estacionamiento-plateas" name="parking_stall" data-name="Estacionamiento plateas" class="_300 select w-select"><option value="">Selecciona una opción</option>
                                            @for($i=0;$i<=10;$i++)
                                                <option value="{!!$i!!}">{!!$i!!}</option>
                                            @endfor
                                        </select></div>
                                    <div class="rubro">
                                        <h2 class="title">¿Das factura para empresas?</h2>
                                        <div class="cat nota">*Para muchos clientes, este es un punto decisivo. Si das facturas, no olvides agregar los impuestos al precio de renta.</div>
                                        <div class="radio radio1 w-radio"><input type="radio" id="Si facturo" name="invoice" value="Si facturo" data-name="radio facturas" class="w-radio-input"><label for="Si facturo-2" class="flr w-form-label">Si doy factura</label></div>
                                        <div class="radio radio1 w-radio"><input type="radio" id="No facturo" name="invoice" value="No facturo" data-name="radio facturas" class="w-radio-input"><label for="No facturo-2" class="flr w-form-label">No doy factura</label></div>
                                    </div>
                                    <div class="rubro">
                                        <h2 class="title">Servicios Especiales Stadibox</h2>
                                        <div class="cat nota">* Al activar esta opción nuestros asesores te contactarán para explicarte los beneficios que podrás obtener. Dar click en esta opción no te compromete a comenzar o mantenerte bajo  este esquema (lo podrás editar por evento).</div>
                                        <div class="checkbox-2 w-checkbox"><input type="checkbox" id="Me-Interesa-Renta-Anual-4" name="annual_rent" data-name="Me Interesa Renta Anual" class="w-checkbox-input"><label for="Me-Interesa-Renta-Anual-4" class="fieldbox w-form-label">Me interesa la renta anual</label><a href="info/renta-anual.php" data-ix="new-interaction" target="_blank" class="qs w-button">?</a></div>
                                        <div class="checkbox-2 w-checkbox"><input type="checkbox" id="Me-Interesa-Renta-Anual-5" name="administration_stalls" data-name="Me Interesa Renta Anual" class="w-checkbox-input"><label for="Me-Interesa-Renta-Anual-5" class="fieldbox w-form-label">Me interesa la administración de plateas</label><a href="#" data-ix="abrir-question" class="qs w-button">?</a></div>
                                    </div>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column w-col w-col-6">
                        <div class="block-estadio hidemovil">
                            <h2 class="h2 h2foro">Estadio Azteca</h2><a data-ix="open-estadio" class="estadio registro w-inline-block"></a>
                            <div class="div-zonas">
                                <div class="w-clearfix zona">
                                    <div class="_1 div_cuadro"></div>
                                    <div class="small txt">Preferente Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_2 div_cuadro"></div>
                                    <div class="small txt">Platea Alta Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_3 div_cuadro"></div>
                                    <div class="small txt">Platea Alta</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_4 div_cuadro"></div>
                                    <div class="small txt">Platea Baja</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_5 div_cuadro"></div>
                                    <div class="small txt">Preferente Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_6 div_cuadro"></div>
                                    <div class="small txt">Palco Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_7 div_cuadro"></div>
                                    <div class="small txt">Suites</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_8 div_cuadro"></div>
                                    <div class="small txt">Palcos Nivel 1</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div><a data-ix="abrir-modal-registro-platea-p2" class="btn full registro w-button">Siguiente Paso</a>
        </div>
    </div>
    <!-- PASO 2 PLATEAS -->
    <div class="modal-registro-platea-p2">
        <div class="div_line">
            <div class="div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-moda-lregistro-platea-p2" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a><a href="#" data-ix="ir-a-modal-registro-platea-p1" class="btn paso-anterior w-button">Paso anterior</a></div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 2 de 2: Documentos</h1>
                        <div class="text_med">Por favor carga una foto de tus documentos o llena los campos solicitados. Esto nos ayudará a verificar la autenticidad de la propiedad</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
            </div>
            <div class="block">
                <div class="w-form">
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">RFC</h2>
                                <input type="number" id="RFC-7" name="rfc" data-name="RFC" maxlength="25" class="text-field w-input">
                            </div>
                        </div>
                        <div class="column w-col w-col-6"></div>
                    </div>
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">Identificación Oficial (copia o foto)</h2>
                                <select id="identificacion" name="identification" data-name="identificacion" class="_300 select w-select">
                                    <option value="" disabled selected>Tipo de identificación</option>
                                    <option value="credencial_votar">Credencial para votar</option>
                                    <option value="pasaporte">Pasaporte</option>
                                </select>
                                <input type="file" id="stall_identification_file" name="identification_file" style="display: none;" />
                                <label for="stall_identification_file" class="btn carga w-button">Cargar archivo</label>
                            </div>
                        </div>
                        <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox">
                            <img id="show_stall_identification_file" src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" class="thumbnail"></a>
                        </div>
                    </div>
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">Comprobante de Domicilio no mayor a 3 meses</h2><input type="file" id="stall_proof_address" name="proof_address" style="display: none;" />
                                <label for="stall_proof_address" class="btn carga w-button">Cargar archivo</label>
                            </div>
                        </div>
                        <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox">
                            <img id="show_stall_proof_address" src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" class="thumbnail"></a>
                        </div>
                    </div>
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">Título de Propiedad</h2>
                                <div class="cat nota">Si no tienes tu título de propiedad a la mano, carga una foto de tus boletos y pon a un lado una hoja con la fecha del día escrita.</div><input type="file" id="stall_property_title" name="property_title" style="display: none;" />
                                <label for="stall_property_title" class="btn carga w-button">Cargar archivo</label>
                            </div>
                        </div>
                        <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox">
                            <img id="show_stall_property_title" src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" class="thumbnail"></a>
                        </div>
                    </div>
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">Foto de vista de las plateas (opcional)</h2><input type="file" id="photo_stall" name="photo_stall" style="display: none;"/>
                                <label for="photo_stall" class="btn carga w-button">Cargar archivo</label>
                            </div>
                        </div>
                        <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox">
                            <img id="show_photo_stall" src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" class="thumbnail"></a>
                        </div>
                    </div>
                    <div class="row-docs w-row">
                        <div class="w-col w-col-6">
                            <div class="rubro">
                                <h2 class="title">Banco para Depósitos</h2>
                                <select id="banco" name="bank" data-name="banco" class="_300 select w-select">
                                    <option value="">Selecciona una opción</option>
                                    <option value="First">First Choice</option>
                                    <option value="Second">Second Choice</option>
                                    <option value="Third">Third Choice</option>
                                </select>
                            </div>
                            <div class="rubro">
                                <h2 class="title">Cuenta Clabe para Depósitos (18 dígitos)</h2>
                                <input type="number" id="Clabe-4" name="account_titular" data-name="Clabe" placeholder="Cuenta Clabe" maxlength="18" class="text-field w-input">
                            </div>
                            <div class="rubro">
                                <h2 class="title">Nombre de Titular de Cuenta</h2>
                                <input type="text" id="Titular-5" name="titular" data-name="titular" placeholder="Nombre completo" maxlength="256" class="text-field w-input">
                            </div>
                            <div class="rubro">
                                <h2 class="title">Número de Referencia (opcional)</h2>
                                <input type="text" id="Numero-Referencia-4" name="reference_number" data-name="Numero Referencia" placeholder="Referencia" maxlength="256" class="text-field w-input">
                            </div>
                            <div class="checkfield w-checkbox">
                                <input type="checkbox" id="Soy-Propietario-O-Acreedor-4" name="property_owner" data-name="Soy Propietario O Acreedor" class="w-checkbox-input">
                                <label for="Soy-Propietario-O-Acreedor-4" class="fieldbox w-form-label">Declaro ser el dueño de la propiedad y/o acreedor de las facultades legales para arrendarla.</label>
                            </div>
                            <div class="text_med">Te recomendamos volver a leer nuestros <a href="info/terminos-y-condiciones.php">Términos y Condiciones</a> y nuestro <a href="info/aviso-de-privacidad.php">Aviso de Privacidad</a> si no lo haz hecho, así como revisar nuestras <a href="info/faq.php">Preguntas Frecuentes</a>.
                            </div>
                        </div>
                        <div class="column w-col w-col-6"></div>
                    </div>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
                <div class="w-row">
                    <div class="w-col w-col-6">
                        <div class="_0 block"></div>
                    </div>
                    <div class="column w-col w-col-6"></div>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
            <button type="submit" data-ix="abrir-modal-registro-platea-finalizada" class="btn full registro w-button">Guardar y Finalizar</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- MODAL REGISTRO PALCOS -->
<div class="div-carpeta-modals-reg-palcos">
    <!-- REGISTRO PASO 1 -->
    <div class="modal-registro-palco-p1">
        <div class="div_line">
            <div class="_25 div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-modal-palco-p1" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a>
        </div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 1 de 4: Información de tu Palco</h1>
                        <div class="text_med">Completa los siguientes campos para registrar tu propiedad</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
            </div>
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-6">
                        <div class="_0 block">
                            <div class="w-form">
                                {!! Form::open(['route' => 'box.store', 'method' => 'POST', 'id' => 'wf-form-registro-palco', 'name' => 'wf-form-registro-palco-1', 'data-name' => 'registro palco 1','autocomplete' => 'off','files' => true]) !!}
                                    <div class="rubro">
                                        <h2 class="title">Foro</h2><select id="foro" name="forum_id" data-name="foro" class="_300 select w-select"><option value="">Selecciona una opción</option></select>
                                    </div>
                                    <div class="rubro">
                                        <h2 class="title">Zona</h2><a data-ix="open-estadio" class="onlymovil textlink">Ver mapa</a><select id="zona" name="zone_id" data-name="zona" class="_300 select w-select"><option value="">Selecciona una opción</option></select>
                                        <h5 style="margin-top: 3%;color: gray;">Otra</h5>
                                        <input type="text" id="otra-zona" name="another_zone" data-name="otra-zona" placeholder="Nombre de la Zona..." class="text-field w-input" style="margin-top: -1%;" maxlength="256">
                                    </div>
                                    <div class="rubro">
                                        <h2 class="title">Número o Nombre de Palco</h2><input type="text" id="No-De-Palco-7" name="num_box" data-name="No De Palco" placeholder="0000" maxlength="256" class="text-field w-input">
                                    </div>
                                    <div class="rubro">
                                        <div class="checkbox-2 w-checkbox"><input type="checkbox" id="incluye-limpieza" name="cleaning" data-name="incluye limpieza" class="w-checkbox-input"><label for="incluye limpieza" class="fieldbox w-form-label">Incluye limpieza</label></div>
                                    </div><br>
                                    <div class="rubro">
                                        <h2 class="title">¿Das factura para empresas?</h2>
                                        <div class="cat nota">*Para muchos clientes, este es un punto decisivo. Si das facturas, no olvides agregar los impuestos al precio de renta.</div>
                                        <div class="radio radio1 w-radio"><input type="radio" id="Si facturo" name="invoice" value="true" data-name="radio facturas" class="w-radio-input"><label for="Si facturo" class="flr w-form-label">Si doy factura</label></div>
                                        <div class="radio radio1 w-radio"><input type="radio" id="No facturo" name="invoice" value="false" data-name="radio facturas" class="w-radio-input"><label for="No facturo" class="flr w-form-label">No doy factura</label></div>
                                    </div>
                                    <div class="rubro">
                                        <h2 class="title">Servicios Especiales Stadibox</h2>
                                        <div class="cat nota">* Al activar esta opción nuestros asesores te contactarán para explicarte los beneficios que podrás obtener. Dar click en esta opción no te compromete a comenzar o mantenerte bajo  este esquema (lo podrás editar por evento).</div>
                                        <div class="checkbox-2 w-checkbox"><input type="checkbox" id="me-interesa-renta-anual" name="annual_rent" data-name="me interesa renta anual" class="w-checkbox-input"><label for="me-interesa-renta-anual" class="fieldbox w-form-label">Me interesa la renta anual</label><a data-ix="abrir-q-anual"  target="_blank" class="qs w-button">?</a></div>
                                        <div class="checkbox-2 w-checkbox"><input type="checkbox" id="me-interesa-renta-anual-2" name="administration_boxes" data-name="Me Interesa Renta Anual 2" class="w-checkbox-input"><label for="me-interesa-renta-anual-2" class="fieldbox w-form-label">Me interesa la administración de palcos</label><a href="#" data-ix="abrir-question" class="qs w-button">?</a></div>
                                    </div>
                                    <div class="rubro" style="clear: left;">
                                        <h2 class="title" style="padding-top: 2%;">Código Invita y Gana</h2>
                                        <div class="cat nota">Si algún propietario te recomendó Stadibox ingresa aquí su código de referenciado y difruta de los beneficios extras que tenemos para tí.</div>
                                        <input type="text" id="cod-referenciado" name="referenced_code" data-name="Codigo de Referenciado" placeholder="Ej. MdB1234" maxlength="256" class="text-field w-input">
                                    </div>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column w-col w-col-6">
                        <div class="block-estadio hidemovil">
                            <h2 class="h2 h2foro">Estadio Azteca</h2><a href="#" data-ix="open-estadio" class="estadio registro w-inline-block"></a>
                            <div class="div-zonas">
                                <div class="w-clearfix zona">
                                    <div class="_1 div_cuadro"></div>
                                    <div class="small txt">Preferente Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_2 div_cuadro"></div>
                                    <div class="small txt">Platea Alta Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_3 div_cuadro"></div>
                                    <div class="small txt">Platea Alta</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_4 div_cuadro"></div>
                                    <div class="small txt">Platea Baja</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_5 div_cuadro"></div>
                                    <div class="small txt">Preferente Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_6 div_cuadro"></div>
                                    <div class="small txt">Palco Plus</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_7 div_cuadro"></div>
                                    <div class="small txt">Suites</div>
                                </div>
                                <div class="w-clearfix zona">
                                    <div class="_8 div_cuadro"></div>
                                    <div class="small txt">Palcos Nivel 1</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div><a data-ix="abrir-modal-registro-palcop2" class="btn full registro w-button">Siguiente Paso</a></div>
    </div>
    <!-- REGISTRO PASO 2 -->
    <div class="modal-registro-palco-p2">
        <div class="div_line">
            <div class="_50 div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-modal-registro-palco-p2" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a><a href="#" data-ix="ir-a-modal-registro-palco-p1" class="btn paso-anterior w-button">Paso Anterior</a></div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 2 de 4 : Boletos y Servicios</h1>
                        <div class="text_med">Completa los siguientes campos para registrar tu propiedad</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
            </div>
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-6">
                        <div class="_0 block">
                            <div class="w-form">
                                <div class="rubro">
                                    <h2 class="title">¿Qué capacidad total tiene tu palco?</h2>
                                    <div class="cat nota">Indica el número de asientos que tiene tu palco, podrás editar fácilmente el número de boletos que deseas rentar en cada evento.</div><select id="capacidad-total" name="capacity" data-name="capacidad total" class="_300 select w-select"><option value="">Selecciona una opción</option>
                                    @for($i=0;$i<=10;$i++)
                                        <option value="{!!$i!!}">{!!$i!!}</option>
                                    @endfor
                                    </select></div>
                                <div class="rubro">
                                    <h2 class="title">No. de Boletos de Estacionamiento Exterior</h2><a data-ix="open-estadio" class="onlymovil textlink">Ver mapa</a><select id="estacionamiento-exterior" name="parking" data-name="estacionamiento exterior" class="_300 select w-select"><option value="">Selecciona una opción</option>
                                    @for($i=0;$i<=10;$i++)
                                        <option value="{!!$i!!}">{!!$i!!}</option>
                                    @endfor
                                    </select></div>
                                <div class="rubro">
                                    <h2 class="title">No. de Boletos de Estacionamiento VIP</h2>
                                    <div class="cat nota">El estacionamiento VIP es aquel que permite a los clientes ingresar con alimentos y bebidas directamente al palco.</div><a data-ix="open-estadio" class="onlymovil textlink">Ver mapa</a><select id="estacionamiento-vip" name="parking_vip" data-name="estacionamiento vip" class="_300 select w-select"><option value="">Selecciona una opción</option>
                                    @for($i=0;$i<=10;$i++)
                                        <option value="{!!$i!!}">{!!$i!!}</option>
                                    @endfor
                                    </select></div>
                                <div class="rubro">
                                    <h2 class="title">Amenidades del Palco</h2>
                                    <div class="w-row">
                                        @foreach($amenity as $key => $value)
                                        <div class="w-col w-col-6">
                                            <div class="chfi w-checkbox">
                                                <input type="checkbox" name="list_amenity[]" class="w-checkbox-input" value="{{$value->id}}">
                                                <img src="{{asset($value->icon)}}" alt="sillon" class="ic_servicios">
                                                <label for="checkbox-10" class="flb w-form-label">{!! $value->amenity !!}</label>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div><input type="text" class="text-field w-input" maxlength="256" name="other_amenity" data-name="Otro Especificar" placeholder="Otro (especificar)" id="Otro-especificar"></div>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column w-col w-col-6">
                        <div class="block-estadio hidemovil"><img src="{{asset('web/images/arte_paso2.png')}}" alt="autos y boletos" srcset="{{asset('web/images/arte_paso2-p-500.png')}} 500w, {{asset('web/images/arte_paso2.png')}} 795w" sizes="100vw" class="image-paso-2"></div>
                    </div>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
            <a data-ix="abir-modal-registro-palco-p3" class="btn full registro w-button">Siguiente Paso</a>
        </div>
    </div>
    <!-- REGISTRO PASO 3 -->
    <div class="modal-registro-palco-p3">
        <div class="div_line">
            <div class="_75 div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-modal-registro-palco-p3" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a><a href="#" data-ix="ir-a-modal-registro-palco-p2" class="btn paso-anterior w-button">Paso Anterior</a></div>
        <div class="div-center">
            <div class="block"></div>
            <div class="div-fotos"></div>
            <div class="div-fotos"></div>
            <div class="div-reg">
                <div class="div-navegacion"></div>
            </div>
        </div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 3 de 4: Sube tus Fotos</h1>
                        <div class="text_med">Estas son las imágenes que Stadibox recomienda subir para lograr que tu palco se rente más rápido. Carga tus fotos como se muestran en la siguiente guía:</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
                <div class="div-foto-palco"><img src="{{asset('web/images/foto_estadio.png')}}" alt="celular con foto de cancha" srcset="{{asset('web/images/foto_estadio-p-500.png')}} 500w, {{asset('web/images/foto_estadio-p-800.png')}} 800w, {{asset('web/images/foto_estadio-p-1080.png')}} 1080w, {{asset('web/images/foto_estadio.png')}} 1197w" sizes="100vw" class="im-palco">
                    <div class="text_med">Cancha Horizontal</div>
                </div>
                <div class="div-foto-palco"><img src="{{asset('web/images/foto_palco_h.png')}}" alt="celular con foto del palco" srcset="{{asset('web/images/foto_palco_h-p-500.png')}} 500w, {{asset('web/images/foto_palco_h-p-800.png')}} 800w, {{asset('web/images/foto_palco_h.png')}} 1197w" sizes="100vw" class="im-palco">
                    <div class="text_med">Palco Horizontal</div>
                </div>
                <div class="div-foto-palco"><img src="{{asset('web/images/foto_asientos.png')}}" alt="celular con foto de asientos" class="im-palco vert">
                    <div class="sm text_med">Asientos Vertical</div>
                </div>
                <div class="div-foto-palco"><img src="{{asset('web/images/foto_wc.png')}}" alt="celular con foto del baño" srcset="{{asset('web/images/foto_wc-p-500.png')}} 500w, {{asset('web/images/foto_wc.png')}} 567w" sizes="100vw" class="im-palco vert">
                    <div class="sm text_med">Baño Vertical</div>
                </div>
                <div class="div-foto-palco"><img src="{{asset('web/images/foto_soccer.png')}}" alt="celular con foto de estadio" srcset="{{asset('web/images/foto_soccer-p-500.png')}} 500w, {{asset('web/images/foto_soccer.png')}} 567w" sizes="100vw" class="im-palco vert">
                    <div class="sm text_med">Otra(s) foto(s)</div>
                </div>
                <div>
                    <div class="block"></div>
                    <div class="div-fotos">
                        <div class="cargar-foto">
                            <input type="file" name="court" id="court" style="display: none;"/>
                            <label class="btn space-left w-button" for="court">Subir foto de cancha horizontal</label>
                        </div>
                    </div>
                    <div id="div_fotos" class="div-fotos w-clearfix">
                        <div class="cat">
                            <h2 class="title">Fotos Adicionales</h2>
                        </div>
                        <input type="file" name="box" id="box" style="display: none;" />
                        <label for="box" class="_0 anadir w-inline-block">
                            <div class="plus">+</div>
                            <div class="cen text_med">Palco Horizontal</div>
                        </label>
                        <input type="file" id="seat" name="seat" style="display: none;"/>
                        <label for="seat" class="_0 anadir w-inline-block">
                            <div class="plus">+</div>
                            <div class="cen text_med">Asientos Vertical</div>
                        </label>
                        <input type="file" id="bath" name="bath" style="display: none;"/>
                        <label for="bath" class="_0 anadir w-inline-block">
                            <div class="plus">+</div>
                            <div class="cen text_med">Baño Vertical</div>
                        </label>
                        <input type="file" id="other1" name="other1" style="display: none;"/>
                        <label for="other1" class="_0 anadir w-inline-block">
                            <div class="plus">+</div>
                            <div class="cen text_med">Agregar Otra</div>
                        </label>
                    </div>
                        <a href="#" id="add_amenity" class="anadir w-inline-block">
                            <div>
                                <div class="plus">+</div>
                            </div>
                        </a>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div><a data-ix="abrir-modal-registro-palco-p4" class="btn full registro w-button">Siguiente Paso</a></div>
        <div class="div_centered">
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div>
        </div>
    </div>
    <!-- REGISTRO PASO 4 -->
    <div class="modal-registro-paco-p4">
        <div class="div_line">
            <div class="div_line green"></div>
        </div>
        <div class="block w-clearfix">
            <a href="#" data-ix="cerrar-modal-registro-palco-p4" class="link-close w-inline-block">
                <div class="xx">X</div>
            </a><a href="#" data-ix="itr-a-modal-registro-p3" class="btn paso-anterior w-button">Paso Anterior</a></div>
        <div class="div_centered">
            <div class="block">
                <div class="w-row">
                    <div class="w-col w-col-8">
                        <h1 class="green h2-title left">Paso 4 de 4: Documentos</h1>
                        <div class="text_med">Por favor carga una foto de tus documentos o llena los campos solicitados. Esto nos ayudará a verificar la autenticidad de la propiedad</div>
                    </div>
                    <div class="colhide w-col w-col-4"></div>
                </div>
            </div>
            <div class="block">
                <div class="w-form">
                        <div class="row-docs w-row">
                            <div class="w-col w-col-6">
                                <div class="rubro">
                                    <h2 class="title">RFC</h2><input type="number" id="RFC-4" name="RFC" data-name="RFC" maxlength="25" class="text-field w-input"></div>
                            </div>
                            <div class="column w-col w-col-6"></div>
                        </div>
                        <div class="row-docs w-row">
                            <div class="w-col w-col-6">
                                <div class="rubro">
                                    <h2 class="title">Identificación Oficial (copia o foto)</h2><select id="identificaicon" name="identification" data-name="identification" class="_300 select w-select"><option value="">Tipo de identificación</option><option value="credencial_votar">Credencial para votar</option><option value="pasaporte">Pasaporte</option></select>
                                    <input type="file" name="identification_file" id="box_identification_file" style="display: none;" /><label for="box_identification_file" class="btn carga w-button">Cargar archivo</label>
                                </div>
                            </div>
                            <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox"><img src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" id="show_box_identification_file" class="thumbnail"></a></div>
                        </div>
                        <div class="row-docs w-row">
                            <div class="w-col w-col-6">
                                <div class="rubro">
                                    <h2 class="title">Comprobante de Domicilio no mayor a 3 meses</h2>
                                    <input type="file" id="box_proof_address" name="proof_address" style="display: none;">
                                    <label for="box_proof_address" class="btn carga w-button">Cargar archivo</label>
                                </div>
                            </div>
                            <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox"><img src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" id="show_box_proof_address" class="thumbnail"></a></div>
                        </div>
                        <div class="row-docs w-row">
                            <div class="w-col w-col-6">
                                <div class="rubro">
                                    <h2 class="title">Título de Propiedad</h2>
                                    <div class="cat nota">Si no tienes tu título de propiedad a la mano, carga una foto de tus boletos y pon a un lado una hoja con la fecha del día escrita.</div>
                                    <input type="file" id="box_property_title" name="property_title" style="display: none;" />
                                    <label for="box_property_title" class="btn carga w-button">Cargar archivo</label>
                                </div>
                            </div>
                            <div class="w-col w-col-6"><a href="#" class="thumbnail w-inline-block w-lightbox"><img src="https://d3e54v103j8qbb.cloudfront.net/img/placeholder-thumb.svg" id="show_box_property_title" for="box_property_title" class="thumbnail"></a></div>
                        </div>
                        <div class="row-docs w-row">
                            <div class="w-col w-col-6">
                                <div class="rubro">
                                    <h2 class="title">Banco para Depósitos</h2><select id="banco-2" name="bank" data-name="banco" class="_300 select w-select"><option value="">Selecciona una opción</option><option value="First">First Choice</option><option value="Second">Second Choice</option><option value="Third">Third Choice</option></select></div>
                                <div class="rubro">
                                    <h2 class="title">Cuenta Clabe para Depósitos (18 dígitos)</h2><input type="number" id="clabe" name="account_titular" data-name="clabe" placeholder="Cuenta Clabe" maxlength="18" class="text-field w-input"></div>
                                <div class="rubro">
                                    <h2 class="title">Nombre de Titular de Cuenta</h2><input type="text" id="Titular-2" name="titular" data-name="titular" placeholder="Nombre completo" maxlength="256" class="text-field w-input"></div>
                                <div class="rubro">
                                    <h2 class="title">Número de Referencia (opcional)</h2><input type="text" id="numero-referencia" name="reference_number" data-name="numero referencia" placeholder="Referencia" maxlength="256" class="text-field w-input"></div>
                                <div class="checkfield w-checkbox"><input type="checkbox" id="soy-propietario-o-acreedor" name="property_owner" data-name="soy propietario o acreedor" class="w-checkbox-input"><label for="soy-propietario-o-acreedor" class="fieldbox w-form-label">Declaro ser el dueño de la propiedad y/o acreedor de las facultades legales para arrendarla.</label></div>
                                <div class="text_med">Te recomendamos volver a leer nuestros <a href="info/terminos-y-condiciones.php">Términos y Condiciones</a> y nuestro <a href="info/aviso-de-privacidad.php">Aviso de Privacidad</a> si no lo haz hecho, así como revisar nuestras <a href="info/faq.php">Preguntas Frecuentes</a>.</div>
                            </div>
                            <div class="column w-col w-col-6"></div>
                        </div>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
                <div class="w-row">
                    <div class="w-col w-col-6">
                        <div class="_0 block"></div>
                    </div>
                    <div class="column w-col w-col-6"></div>
                </div>
            </div>
            <div class="block"></div>
            <div class="block"></div>
            <div class="block"></div><button type="submit" data-ix="abrir-modal-registro-palco-finalizado" class="btn btn-block full registro w-button">Guardar y Finalizar</button></div>
            {!!Form::close()!!}
    </div>
</div>