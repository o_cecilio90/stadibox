<!-- MODAL INICIO SESION -->
<div data-ix="modal-incial" class="modal-wrapper-login">
    <div class="modal-box modal-chico">
        <div class="div-close w-clearfix">
            <div data-ix="cerrar-login" class="x">x</div>
            <h3 class="h-popup">Iniciar Sesión</h3>
        </div>
        <div class="fondo-basico wrapper-fondo">
            <div class="form-login w-form">
                {!! Form::open(['route' => 'login', 'method' => 'POST', 'class' => 'w-clearfix', 'id' => 'wf-form-login', 'name' => 'wf-form-login', 'data-name' => 'login']) !!}
                    <input type="email" class="text-field w-input" maxlength="256" name="email" data-name="Correo Electronico 5" placeholder="Correo electrónico" id="Correo-electronico-5" required="">
                    <input type="password" class="text-field w-input" maxlength="256" name="password" data-name="Contrase A 5" placeholder="Contraseña" id="Contrase-a-5" required="">
                    <a href="#" data-ix="abrir-pass" class="link olvide">¿Has olvidado tu contraseña?</a>
                    <button type="submit" class="btn full w-button">Iniciar Sesión</button>
                {!! Form::close() !!}
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form</div>
                </div>
            </div>
            <div class="cen o pregunta">o</div><a href="{!! route('social.auth', 'facebook') !!}" data-ix="cerrar-login" class="btn facebook w-button">Registrarse con Facebook</a>
            <div class="div10 w-clearfix">
                <div class="left txt_small">¿Aún no tienes cuenta?</div><a href="#" data-ix="cerrar-login-abrir-registro" class="link-registro">Regístrate</a></div>
        </div>
    </div>
</div>