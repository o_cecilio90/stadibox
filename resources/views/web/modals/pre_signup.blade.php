<!-- MODAL PRE REGISTRO -->
<div data-ix="modal-incial" class="modal-preregistro">
    <div class="modal-box modal-chico">
        <div class="div-close w-clearfix">
            <div data-ix="cerrar-registro" class="x">x</div>
            <h3 class="h-popup">Regístrate</h3>
        </div>
        <div class="fondo-basico wrapper-fondo"><a href="#" data-ix="cerrar-preregistro-abri-rregistro" class="btn full w-button">Registro con Correo</a>
            <div class="div-sp2">
                <div class="form-login w-form">
                    <form id="wf-form-preregistro" name="wf-form-preregistro" data-name="preregistro">
                        <div class="check1 checkfield w-checkbox w-clearfix">
                            <input type="checkbox" id="checkbox-3" name="checkbox-3" data-name="Checkbox 3" checked="" class="checkbox-2 w-checkbox-input"><label for="checkbox-3" class="left txt_small w-form-label">Acepto los</label>
                            <div class="pregunta tyc"><a href="info/terminos-y-condiciones.html" target="_blank" class="txt_link">Términos y Condiciones</a></div>
                            <div class="pregunta"> y la <span class="link2"><a href="info/aviso-de-privacidad.html" target="_blank" class="txt_link">Política de Privacidad</a></span>.</div>
                        </div>
                    </form>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form</div>
                    </div>
                </div>
            </div><a href="#" data-ix="cerrar-registro" class="btn facebook spfacebook w-button">Registro con Facebook</a>
            <div class="div10 w-clearfix">
                <div class="left txt_small">¿Ya tienes cuenta?</div>
                <a href="#" data-ix="cerrar-preregistro-abrir-login-2r" class="link-registro">Iniciar Sesión</a>
            </div>
        </div>
    </div>
</div>