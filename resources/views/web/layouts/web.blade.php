<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('web.includes.head')
    <body>
        @include('web.includes.header')
        
        @yield('content')

        @include('web.includes.footer')

        @include('web.includes.scripts')
    </body>
</html>