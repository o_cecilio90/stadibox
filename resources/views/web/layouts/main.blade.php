<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('web.includes.head')
    <body>
        @include('web.includes.header')
        
        @yield('content')
        
        <div class="modals-home">
            <!-- MODALES DE REGISTRO -->
            <div class="gpo-registro">
                @include('web.modals.pre_signup')
                @include('web.modals.login')
                @include('web.modals.palco_platea')
                @include('web.modals.signup')
            </div>
        </div>

        @include('web.includes.footer')

        @include('web.includes.scripts')
    </body>
</html>
