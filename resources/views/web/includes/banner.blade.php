
<div id="hero" class="hero-inicio">
    <div class="container-hero w-container">
        <!-- TABS CATEGORIAS -->
        <div class="div-categorias">
            <a href="busqueda/resultados.html" class="btn_categora w-button">Prueba</a>
            <a href="busqueda/resultados.html" class="btn_categora w-button">NFL</a>
            <a href="busqueda/resultados.html" class="btn_categora w-button">Conciertos</a>
            <a href="busqueda/resultados.html" class="btn_categora w-button">Toros</a>
            <a href="busqueda/resultados.html" class="btn_categora w-button">Otros</a>
        </div>
        <h1 class="h1-slogan">Transformando la manera de rentar palcos</h1>
        <h2 class="h2-home">¡Encuentra palcos y plateas para los mejores eventos!</h2>
        <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">
            <!-- TABS ASISTIR-ENLISTAR -->
            <div class="tabsmenu w-clearfix w-tab-menu">
                <a data-w-tab="quiero asistir" class="tablink w--current w-inline-block w-tab-link">
                    <div class="cen">Quiero asistir a un evento</div>
                </a>
                <a data-w-tab="quiero enlistar" class="tablink w-inline-block w-tab-link">
                    <div class="cen">Quiero enlistar mi propiedad</div>
                </a>
            </div>
            <div class="tabs-content w-tab-content">
                <!-- CONTENIDO ASISTIR -->
                <div data-w-tab="quiero asistir" class="tabpane w--tab-active w-tab-pane">
                    <div class="text_med white">¡Busca el evento al que quieres asistir y consigue un palco o platea de forma fácil y rápida!</div>
                    <div class="div-buscador-2"><a href="busqueda/resultados.html" class="buscador-boton w-inline-block"><img src="{{asset('web/images/buscador_blanco.png')}}" width="253" height="40" class="buscador-image"></a>
                        <div class="buscar-form buscar-form2 w-form">
                            <form id="wf-form-Buscador-principal" name="wf-form-Buscador-principal" data-name="Buscador principal" class="form form2"><input type="text" id="buscador-2" placeholder="Busca tu estadio, equipo o evento" maxlength="256" name="buscador-2" data-name="Buscador 2" class="buscador buscador2 w-input">
                            </form>
                            <div class="w-form-done">
                                <div>Thank you! Your submission has been received!</div>
                            </div>
                            <div class="w-form-fail">
                                <div>Oops! Something went wrong while submitting the form</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CONTENIDO ENLISTAR -->
                <div data-w-tab="quiero enlistar" class="tabpane w-tab-pane">
                    <div class="text_med white">¿Sabías que en Stadibox podrías ganar más de 1 millón de pesos al año rentando tu palco?<br>¡No esperes más, registra tu propiedad y empieza a generar ingresos!</div>
                    <div class="div_upperspace"><a href="#" data-ix="abrir-modal-palco-o-platea" class="btn w-button">Quiero registrar una propiedad</a></div>
                    <div class="div_upperspace"><a href="mi-cuenta.html" class="link_green">Ya registré mi propiedad, ir a mi cuenta</a></div>
                </div>
            </div>
        </div>
        <div class="div-80"><a href="info/renta-anual.html" class="boton100 w-button">Quiero rentar una propiedad por un año</a></div>
    </div>
</div>