<div class="header">
    <!-- BUSCADOR -->
    <div class="modal-search w-clearfix"><a href="#" data-ix="cerrar-buscador" class="modal x">x</a>
        <div class="search-movil w-form">
            <form id="wf-form-buscador-navbar-movil" name="wf-form-buscador-navbar-movil" data-name="buscador navbar movil" class="form-buscador-movil">
                <div class="div-search"><a href="#" data-ix="cerrar-buscador" class="seach-link w-inline-block"><img src="{{ asset('web/images/search-1.png') }}" class="ic-buscador"></a><input type="text" id="buscador-movil" name="buscador-movil" data-name="buscador movil" maxlength="256" placeholder="Estadio, equipo o evento" class="tf-search w-input"></div>
            </form>
            <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form</div>
            </div>
        </div>
    </div>
    <!-- NAVBAR -->
    <div data-collapse="medium" data-animation="over-right" data-duration="400" class="navbar-home w-nav"><a href="/" class="logo w-clearfix w-nav-brand"><img src="{{ asset('web/images/Logo_Stadibox-03.png') }}" alt="Stadibox: Renta de palcos" class="logo_im"></a>
        <div class="menu-btn w-nav-button">
            <div class="w-icon-nav-menu"></div>
        </div>
        <div class="block-busqueda w-clearfix"><img src="{{ asset('web/images/search-1.png') }}" width="40" height="40" class="buscador-im"><img src="{{ asset('web/images/search-1.png') }}" width="40" height="40" data-ix="abrir-buscador" class="buscador-im lupamovil">
            <div class="form-busqueda w-form">
                <form id="wf-form-Buscador-navbar" name="wf-form-Buscador-navbar" data-name="Buscador navbar" class="form-2 w-clearfix"><input type="text" id="buscador" placeholder="Busca tu estadio, equipo o evento" maxlength="256" name="buscador" data-name="buscador" class="search-tf w-input"></form>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form</div>
                </div>
            </div>
        </div>
        <nav role="navigation" class="nav-menu-home w-nav-menu">
            @guest
                <a href="#" data-ix="abrir-registro" class="navlink w-nav-link">{{ __('message.register') }}</a>
                <a data-ix="abrir-modal-login" class="navlink w-nav-link">{{ __('message.login') }}</a>
            @else 
                {!! Form::open(['route' => 'logout', 'method' => 'POST', 'style' => 'display: inline']) !!}
                    <button type="submit" class="navlink w-nav-link" style="background-color: transparent;" > Cerrar sesión</button>
                {!! Form::close() !!}
                <a href="{{route('catalogs')}}" class="navlink w-nav-link">Panel</a>
                <a href="{{route('web.console')}}" class="navlink w-nav-link">Mi Cuenta</a>
            @endguest
            <div data-delay="0" class="w-dropdown">
                <div class="ayuda navlink w-dropdown-toggle">
                    <div>{{ __('message.help') }}</div>
                </div>
                <nav class="dropdown-list w-dropdown-list">
                    <a href="/info/acerca-de-nosotros" class="ddlink w-dropdown-link">Acerca de Nosotros</a>
                    <a href="/info/renta-anual" class="ddlink w-dropdown-link">Renta Anual</a>
                    <a href="/info/administracion" class="ddlink w-dropdown-link">Adminsitramos tu<br>Propiedad</a>
                    <a href="/info/servicios" class="ddlink w-dropdown-link">Servicios</a>
                    <a href="/info/para-empresas" class="ddlink w-dropdown-link">Para Empresas</a>
                    <a href="/info/para-propietarios" class="ddlink w-dropdown-link">Para Propietarios</a>
                    <a href="/experiencias/vip" class="ddlink w-dropdown-link">Experiencias VIP</a>
                    <a href="/info/faq" class="ddlink w-dropdown-link">Preguntas Frecuentes</a>
                    <a href="/info/contacto" class="ddlink w-dropdown-link">Contacto</a>
                </nav>
            </div>
            <div class="form-divisa w-form" style="display: inline-flex;">
                <form id="wf-form-Opciones-de-region" style="margin-right: 8px" name="wf-form-Opciones-de-region" data-name="Opciones de region">
                    <select id="Divisa-2" name="Divisa-2" data-name="Divisa 2" class="divisa select w-select">
                        <option value="MXN">MXN</option>
                        <option value="USD">USD</option>
                    </select>
                </form>
                <form id="language" name="wf-form-Opciones-de-region" data-name="Opciones de region">
                    <div class="btn-group">
                        <button type="button" style="top: -11px;" class="navlink navlinkbtn w-nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Idioma
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ url('lang', ['es']) }}">Español</a>
                            <a class="dropdown-item" href="{{ url('lang', ['en']) }}">Inglés</a>
                        </div>
                    </div>
                </form>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form.</div>
                </div>
            </div>
            @guest
                <a data-ix="abrir-modal-login" class="navlink navlinkbtn w-nav-link">{{ __('message.property_register') }}</a>
            @else
                <a href="#" data-ix="abrir-modal-palco-o-platea" class="navlink navlinkbtn w-nav-link">{{ __('message.property_register') }}</a></nav>
            @endguest
    </div>
</div>