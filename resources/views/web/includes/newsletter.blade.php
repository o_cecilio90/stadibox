<!-- Newsletter -->
<div class="seccion-suscripcion">
    <div class="container-cen w-container">
        <h3 class="h2-title white">{{__('message.newsletter')}}</h3>
        <div class="div-suscripcion">
            <div class="w-form">
                <form id="wf-form-suscripcion" name="wf-form-suscripcion" data-name="suscripcion" class="form-suscripcion">
                    <input type="email" class="tf-suscripcion w-input" maxlength="256" name="Correo-electronico" data-name="Correo Electronico" placeholder="{{__('message.email')}}" id="Correo-electronico" required="">
                    <a href="#" class="btn-suscripcion w-button">{{__('message.subscribe')}}</a
                ></form>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form.</div>
                </div>
            </div>
        </div>
    </div>
</div>