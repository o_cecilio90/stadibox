<div class="footer">
    <div class="cont-footer w-container">
        <div class="w-row">
            <div class="col-footer w-clearfix w-col w-col-4">
                <div>
                    <h5 class="hfooter">{{ __('message.region_options') }}</h5>
                    <div class="w-form">
                        <form id="wf-form-Opciones-de-region" name="wf-form-Opciones-de-region" data-name="Opciones de region"><select id="Pais" name="Pais" data-name="Pais" class="select w-select"><option value="País">País</option><option value="">México</option><option value="United States">United States</option><option value="Otro (other)">Otro (other)</option></select><select id="Divisa" name="Divisa" data-name="Divisa" class="select w-select"><option value="">Divisa</option><option value="MXN">MXN</option><option value="USD">USD</option></select></form>
                        <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                    </div>
                </div><a href="/" class="footer logo w-clearfix w-nav-brand"><img src="{{ asset('web/images/Logo_Stadibox-03.png') }}" alt="Stadibox: Renta tu palco o platea" class="logo_im"></a>
                <div class="footer txt_mini">Copyright © 2018 Stadibox</div>
            </div>
            <div class="col-footer w-col w-col-4">
                <h5 class="hfooter">{{__('message.stadibox_service')}}</h5>
                <a href="/" class="_100 link_green">{{__('message.start')}}</a>
                <a href="/info/acerca-de-nosotros" class="_100 link_green">{{__('message.us')}}</a>
                <a href="/info/administracion" class="_100 link_green">{{__('message.manage')}}</a>
                <a href="/info/renta-anual" class="_100 link_green">{{__('message.rent')}}</a>
                <a href="/info/venta-palcos" class="_100 link_green">{{__('message.sale_boxes')}}</a>
                <a href="/info/servicios" class="_100 link_green">{{__('message.additional_services')}}</a>
                <a href="/info/para-empresas" class="_100 link_green">{{__('message.businesses')}}</a>
                <a href="/info/para-propietarios" class="_100 link_green">{{__('message.owners')}}</a>
                <a href="/experiencias/vip" class="_100 link_green">{{__('message.vip_experiences')}}</a>
            </div>
            <div class="col-footer w-clearfix w-col w-col-4"><a target="_blank" href="https://twitter.com/stadibox" class="social w-inline-block"></a><a href="https://www.facebook.com/Stadibox/?fref=ts" target="_blank" class="facebook social w-inline-block"></a>
                <h5 class="hfooter">{{__('message.help')}}</h5>
                <a href="/info/contacto" class="_100 link_green">{{__('message.contact')}}</a>
                <a href="/info/faq" class="_100 link_green">{{__('message.frequent_questions')}}</a>
                <a href="/info/terminos-y-condiciones" target="_blank" class="_100 link_green">{{__('message.terms_conditions')}}</a>
                <a href="/info/aviso-de-privacidad" target="_blank" class="_100 link_green">{{__('message.privacy')}}</a>
                <a href="{{ route('job.exchange.index') }}" target="_blank" class="_100 link_green">Bolsa de Trabajo</a>
            </div>
            </div>
        </div>
    </div>
</div>