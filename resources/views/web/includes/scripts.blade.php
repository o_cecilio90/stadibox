<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('web/js/webflow.js') }}" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.bundle.min.js" integrity="sha384-CS0nxkpPy+xUkNGhObAISrkg/xjb3USVCwy+0/NMzd5VxgY4CMCyTkItmy5n0voC" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/popper.min.js" ></script>
<script>
	$( function() {
    	$( "#birthdate" ).datepicker({
	    	changeMonth: true,
	        changeYear: true,
	        yearRange: "1930:2010",
	        dateFormat: "yy-mm-dd"
	    });
  	});

  	var terms = $('#term_cond');
  	var promo = $('#promotions');
  	var owner = $('#owner');
  	
  	terms.on('click',function () {
        if (terms.is(':checked')) {
        	terms.val(1);
            $('#submit-register-user').removeAttr('disabled').attr('type', 'submit');
        } else {
        	terms.val(0);
            $('#submit-register-user').removeAttr('type').attr('disabled', 'disabled');
        }
    });

    promo.on('click',function () {
        if (promo.is(':checked')) {
        	promo.val(1);
        } else {
        	promo.val(0);
        }
    });

    owner.on('click',function () {
        if (owner.is(':checked')) {
        	owner.val(1);
        } else {
        	owner.val(0);
        }
    });

</script>
<!--SELECTS-->
<script type="text/javascript">
    // FORUM
    $(function(){
        $.ajax({
            type: "GET",
            url: '{{ route('selectForum') }}', 
            dataType: "json",
            success: function(data){
                $.each(data,function(key, registro) {
                    $('#foro-platea').append('<option value='+registro.id+'>'+registro.forum_name+'</option>');
                    $('#foro').append('<option value='+registro.id+'>'+registro.forum_name+'</option>');
                });        
            },
            error: function(data) {
                alert('¡ERROR! cargando foros contacte al soporte tecnico');
            }
        });
    })

    // ZONE
    $('#foro').on('change',function (e) {
      $('#zona').html('');
      let route = '{{ route("selectZone", ":id") }}';
        route = route.replace(':id', $('#foro').val());
      $.ajax({
            type: "GET",
            url: route, 
            dataType: "json",
            success: function(data){
                $.each(data,function(key, registro) {
                  $('#zona').append('<option value='+registro.id+'>'+registro.name_zone+'</option>');
                });        
            },
            error: function(data) {
                alert('¡ERROR! cargando zonas contacte al soporte tecnico');
            }
        });
    });
    $('#foro-platea').on('change',function (e) {
      $('#zona-platea').html('');
      let route = '{{ route("selectZone", ":id") }}';
        route = route.replace(':id', $('#foro-platea').val());
      $.ajax({
            type: "GET",
            url: route, 
            dataType: "json",
            success: function(data){
                $.each(data,function(key, registro) {
                  $('#zona-platea').append('<option value='+registro.id+'>'+registro.name_zone+'</option>');
                });        
            },
            error: function(data) {
                alert('¡ERROR! cargando zonas contacte al soporte tecnico');
            }
        });
    });
</script>
<!--ADD-->
<script type="text/javascript">
    $(function(){
        //STALLS
        let stall = '<div class="row_filas">'+
                '<input type="text" id="asiento" name="row[]" data-name="asiento" placeholder="Fila..." maxlength="7" class="text-field tfsmall2 w-input">'+
                '<input type="text" id="asiento-1" name="seat[]" data-name="asiento" placeholder="Asiento..." maxlength="7" class="text-field tfsmall2 w-input">'+
                '</div>'
        $('#add_stall_btn').on('click',function (e) {
            $('#content_stall').append(stall);
        })

        // AMENITY
        let count = 2
        $('#add_amenity').on('click', function (e) {
            
            let amenity ='<input type="file" id="other'+count+'" name="other'+count+'" style="display: none;">'+
                        '<label for="other'+count+'" class="_0 anadir w-inline-block">'+
                            '<div class="plus">+</div>'+
                            '<div class="cen text_med">Agregar Otra</div>'+
                        '</label>'
            $('#div_fotos').append(amenity);
            count = count+1
        })
    });
</script>
<!--FILE PREVIEW-->
<script type="text/javascript">
    $(function() {
        // BOX
        //identification_file
        $('#box_identification_file').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = box_IdentificationFile;
            reader.readAsDataURL(file); 
        });
        function box_IdentificationFile(e) {
            let result=e.target.result;
            $('#show_box_identification_file').attr("src",result);
        }

        //proof_address
        $('#box_proof_address').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = show_box_proof_address;
            reader.readAsDataURL(file); 
        });
        function show_box_proof_address(e) {
            let result=e.target.result;
            $('#show_box_proof_address').attr("src",result);
        }

        //property_title
        $('#box_property_title').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = show_box_property_title;
            reader.readAsDataURL(file); 
        });
        function show_box_property_title(e) {
            let result=e.target.result;
            $('#show_box_property_title').attr("src",result);
        }

        // STALL
        //identification_file
        $('#stall_identification_file').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = stall_IdentificationFile;
            reader.readAsDataURL(file); 
        });
        function stall_IdentificationFile(e) {
            let result=e.target.result;
            $('#show_stall_identification_file').attr("src",result);
        }

        //proof_address
        $('#stall_proof_address').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = show_stall_proof_address;
            reader.readAsDataURL(file); 
        });
        function show_stall_proof_address(e) {
            let result=e.target.result;
            $('#show_stall_proof_address').attr("src",result);
        }

        //property_title
        $('#stall_property_title').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = show_stall_property_title;
            reader.readAsDataURL(file); 
        });
        function show_stall_property_title(e) {
            let result=e.target.result;
            $('#show_stall_property_title').attr("src",result);
        }

        //photo_stall
        $('#photo_stall').change(function(e) {
            let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = show_photo_stall;
            reader.readAsDataURL(file); 
        });
        function show_photo_stall(e) {
            let result=e.target.result;
            $('#show_photo_stall').attr("src",result);
        }
    });
</script>

@yield('js')