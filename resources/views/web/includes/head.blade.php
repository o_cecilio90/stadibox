<head>
    <meta charset="utf-8">
    <title>Stadibox</title>
    <meta content="Stadibox es la plataforma de renta de palcos y plateas en el estadio Azteca, Omnilife, Arena Ciudad de México, Estadio BBVA Bancomer y muchos más." name="description">
    <meta content="Stadibox" property="og:title">
    <meta content="Stadibox es la plataforma de renta de palcos y plateas en el estadio Azteca, Omnilife, Arena Ciudad de México, Estadio BBVA Bancomer y muchos más." property="og:description">
    <meta content="summary" name="twitter:card">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('web/css/normalize.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('web/css/webflow.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('web/css/stadibox.webflow.css') }}" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Varela Round:400","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Inconsolata:400,700"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="{{ asset('web/images/stadibox_favicon.png') }}" rel="shortcut icon" type="image/x-icon">
    <link href="{{ asset('web/images/stadibox_webclip.png') }}" rel="apple-touch-icon">
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            /* display: none; <- Crashes Chrome on hover */
            -webkit-appearance: none;
            margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
        }       
    </style>
    @yield('styles')
</head>