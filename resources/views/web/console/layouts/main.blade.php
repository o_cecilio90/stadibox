<!DOCTYPE html>
<html data-wf-page="5a037fd7746f250001d471a7" data-wf-site="57eda549f0e646e9326f231e" lang="{{ app()->getLocale() }}">
    @include('web.console.includes.head')
    <body>
        
        
        <@include('web.console.includes.header_logged')
        
        <div class="bg-dashboard">
            <div data-duration-in="300" data-duration-out="100" class="tabs-2 w-tabs">
                <div class="dashboard-content w-tab-content">
                    <!-- CONTENIDO -->
                    <div data-w-tab="Mi Perfil" class="tab-perfil w-tab-pane">
                        <h1 class="titulo_dashboard">Mi Perfil</h1>
                        <h2 class="h2dashboard">Carga tu información de perfil.</h2>
                        <p class="chico parrafo">*Tu información de perfil no será compartida con otros usuarios. La seguridad de tus transacciones es lo más importante, estos datos nos ayudan a verificar la autenticidad de las cuentas de nuestros usuarios.</p>
                        <!-- DATOS GENERALES -->
                        <div class="_100p div-wrap">
                            <div data-ix="abrir-datos" class="barra w-clearfix">
                                <div class="div-arrow"><img src="{!!asset('web/images/arrow-01.png')!!}" class="image-2"></div>
                                <h2 class="ns text_big">Datos Generales</h2>
                            </div>
                            <div class="div-datos-perfil">
                                <div class="form-datos w-form">
                                    <form id="wf-form-datos-generales" name="wf-form-datos-generales" data-name="datos generales">
                                        <div class="w-row">
                                            <div class="w-col w-col-2 w-col-stack">
                                                <div class="div-perfil"><img src="https://d3e54v103j8qbb.cloudfront.net/img/image-placeholder.svg" alt="Foto de perfil" class="im-perfil"><a href="#" class="_160 btn full w-button">Cargar foto</a></div>
                                            </div>
                                            <div class="w-clearfix w-col w-col-5 w-col-stack">
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Nombre</div>
                                                    </div>
                                                    <div class="w-col w-col-8"><input type="text" id="nombre-4" name="nombre" data-name="nombre" placeholder="Nombre" maxlength="256" class="perfil-form w-input"></div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Apellido</div>
                                                    </div>
                                                    <div class="w-col w-col-8"><input type="text" id="Apellido-3" name="Apellido" data-name="Apellido" placeholder="Apellido" maxlength="256" class="perfil-form w-input"></div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Correo</div>
                                                    </div>
                                                    <div class="w-col w-col-8"><input type="email" id="Correo-5" name="Correo" data-name="Correo" placeholder="nombre@correo.com" maxlength="256" class="perfil-form w-input"></div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Contraseña</div>
                                                    </div>
                                                    <div class="w-col w-col-2">
                                                        <div class="text_med">*****</div>
                                                    </div>
                                                    <div class="w-col w-col-6">
                                                        <div class="div-link"><a href="#" data-ix="abrir-cambiar-contrasena" class="ns textlink">Cambiar contraseña</a></div>
                                                    </div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Código de Referenciado</div>
                                                    </div>
                                                    <div class="w-col w-col-2">
                                                        <div class="text_med">DiKa6419</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-col w-col-5 w-col-stack">
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Tel. Fijo</div>
                                                    </div>
                                                    <div class="w-clearfix w-col w-col-8"><input type="text" id="Telefono-lada" name="Telefono-lada" data-name="Telefono lada" maxlength="256" placeholder="lada" class="lada perfil-form w-input"><input type="email" id="Telefono-3-fijo" name="Telefono-3-fijo" data-name="Telefono 3 fijo" placeholder="55 55 55 55 55" maxlength="256" class="perfil-form tel w-input"></div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Tel. Móvil</div>
                                                    </div>
                                                    <div class="w-clearfix w-col w-col-8"><input type="text" id="Telefono-movil-lada" name="Telefono-movil-lada" data-name="Telefono movil lada" maxlength="256" placeholder="lada" class="lada perfil-form w-input"><input type="email" id="Telefono-movil" name="Telefono-movil" data-name="Telefono movil" placeholder="55 55 55 55 55" maxlength="256" class="perfil-form tel w-input"></div>
                                                </div>
                                                <div class="p row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Cumpleaños</div>
                                                    </div>
                                                    <div class="w-col w-col-8">
                                                        <div class="w-row">
                                                            <div class="c w-col w-col-5"><select id="Mes-3" name="Mes" data-name="Mes" class="_98 select-basico w-select"><option value="">Mes</option><option value="">Enero</option><option value="">Febrero</option><option value="">Marzo</option></select></div>
                                                            <div class="c w-col w-col-3"><select id="Dia-3" name="Dia" data-name="Dia" class="_98 select-basico w-select"><option value="">Día</option><option value="">1</option><option value="">2</option><option value="">3</option></select></div>
                                                            <div class="c w-clearfix w-col w-col-4"><select id="a-o" name="a-o" data-name="año" class="_98 der select-basico w-select"><option value="">Año</option><option value="">1999</option><option value="">1998</option><option value="">1997</option></select></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="h id row-tf w-row">
                                                    <div class="w-col w-col-4">
                                                        <div class="right text_med">Identificación</div>
                                                    </div>
                                                    <div class="w-col w-col-4"><select id="Id-Oficial-3" name="Id-Oficial" data-name="Id Oficial" class="select-basico w-select"><option value="Selecciona uno">Selecciona uno</option><option value="INE / IFE">INE / IFE</option><option value="Pasaporte">Pasaporte</option></select></div>
                                                    <div class="w-col w-col-4">
                                                        <div class="div-id-foto w-clearfix"><img src="https://d3e54v103j8qbb.cloudfront.net/img/image-placeholder.svg" alt="Foto de perfil" class="im-id"><a href="#" class="btn btn_imagen w-button"></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-clearfix w-col w-col-stack w-col-undefined"><a data-ix="abrir-guardar" class="btn color der w-button">Guardar Datos</a></div>
                                        </div>
                                    </form>
                                    <div class="w-form-done">
                                        <div>Thank you! Your submission has been received!</div>
                                    </div>
                                    <div class="w-form-fail">
                                        <div>Oops! Something went wrong while submitting the form</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- TREMINAN DATOS GENERALES -->
                        
                        <!-- DIRECCIONES DE ENTREGA Y DEVOLUCION DE BOLETOS -->
                        <div class="_100p div-wrap">
                            <div data-ix="abrir-datos" class="barra w-clearfix">
                                <div class="div-arrow"><img src="{!!asset('web/images/arrow-01.png')!!}" class="image-2"></div>
                                <h2 class="ns text_big">Direcciones de Entrega y Devolución de Boletos</h2>
                            </div>
                            <div class="div-datos-perfil">
                                <div class="row-3 w-row">
                                    <div class="column-7 w-col w-col-6"></div>
                                    <div class="c0 w-clearfix w-col w-col-6">
                                        <div class="div_btn"><a href="#" class="btn movil w-button">Agregar Nueva Dirección</a></div>
                                    </div>
                                </div>
                                <div class="div-gris recuadro">
                                    <div class="cen div-form">
                                        <div class="w-form">
                                            <form id="email-form-3" name="email-form-3" data-name="Email Form 3">
                                                <div class="row-auto w-row">
                                                    <div class="w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                        <!--<div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Tipo de dirección</div>
                                                            </div>
                                                            <div class="column-22 w-col w-col-9"><select id="Tipo-de-Direcci-n" name="Tipo-de-Direcci-n" data-name="Tipo de Dirección" class="select-basico w-select"><option value="First">Casa</option><option value="Second">Oficina</option><option value="Third">Alternativa</option></select></div>
                                                        </div>-->
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Tipo de Dirección</div>
                                                            </div>
                                                            <div class="w-col w-col-9">
                                                                <input type="text" id="Tipo-de-Direcci-n" name="Tipo-de-Direcci-n" data-name="Tipo de Dirección" placeholder="Ej. casa" class="perfil-form w-input">
                                                            </div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Calle</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><input type="text" id="Calle-2" name="Calle" data-name="Calle" maxlength="256" placeholder="Calle" class="perfil-form w-input"></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Colonia </div>
                                                            </div>
                                                            <div class="column-20 w-col w-col-9"><input type="text" id="Colonia-2" name="Colonia" data-name="Colonia" maxlength="256" placeholder="Colonia" class="perfil-form w-input"></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Delegación </div>
                                                            </div>
                                                            <div class="w-col w-col-9"><input type="text" id="Delegacio-n" name="Delegacio-n" data-name="Delegación" maxlength="256" placeholder="Delegación" class="perfil-form w-input"></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Número</div>
                                                            </div>
                                                            <div class="w-col w-col-9">
                                                                <div class="div-flex num"><input type="text" id="N-mero-exterior-2" name="N-mero-exterior" data-name="Número exterior" maxlength="256" placeholder="Ext" class="ext num perfil-form w-input"><input type="text" id="N-mero-interior-2" name="N-mero-interior" data-name="Número interior" maxlength="256" placeholder="Int" class="int num perfil-form w-input"></div>
                                                            </div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">CP. </div>
                                                            </div>
                                                            <div class="w-clearfix w-col w-col-9"><input type="text" id="CP" name="CP" data-name="CP." maxlength="256" placeholder="CP." class="cp perfil-form w-input"></div>
                                                        </div>
                                                    </div>
                                                    <div class="w-clearfix w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                        <div class="id row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med"> Ciudad</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><select id="Ciudad-2" name="Ciudad" data-name="Ciudad" class="select-basico w-select"><option value="Another Choice">Seleccione una...</option></select></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Estado</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><select id="Estado-2" name="Estado" data-name="Estado" class="select-basico w-select"><option value="Another Choice">Seleccione uno...</option></select></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Tel de Contacto</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><input type="text" name="Tel-fono-de-contacto" data-name="Teléfono de contacto" maxlength="256" id="Tel-fono-de-contacto" placeholder="55 55 55 55" class="perfil-form w-input"></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Otro Responsable</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><input type="text" id="Responsable" name="Responsable" data-name="Responsable" maxlength="256" placeholder="Si no estoy, entregar a..." class="perfil-form w-input"></div>
                                                        </div>
                                                        <div class="p row-tf w-row">
                                                            <div class="w-col w-col-3">
                                                                <div class="_20 right text_med">Referencias</div>
                                                            </div>
                                                            <div class="w-col w-col-9"><textarea id="Referencias-o-informaci-n-Adicional-3" name="Referencias-o-informaci-n-Adicional-2" maxlength="5000" data-name="Referencias o información Adicional 2" placeholder="Referencias o información adicional" class="_100 textarea-2 w-input"></textarea></div>
                                                        </div><a href="#" data-ix="abrir-guardar" class="btn color der w-button">Guardar</a><a href="#" data-ix="abrir-eliminar" class="btn der orange w-button">Eliminar Dirección</a></div>
                                                </div>
                                            </form>
                                            <div class="w-form-done">
                                                <div>Thank you! Your submission has been received!</div>
                                            </div>
                                            <div class="w-form-fail">
                                                <div>Oops! Something went wrong while submitting the form</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="div-mensaje"></div>
                                    <div class="div-block-7"></div>
                                </div>
                            </div>
                        </div>
                        <!-- TERMINAN DIRECCIONES DE ENTREGA Y DEVOLUCION DE BOLETOS -->
                        
                        <!-- ENLAZAR CUENTA PARA MIS DEPOSITOS -->
                        <div class="_100p div-wrap">
                            <div data-ix="abrir-datos" class="barra w-clearfix">
                                <div class="div-arrow"><img src="{!!asset('web/images/arrow-01.png')!!}" class="image-2"></div>
                                <h2 class="ns text_big">Enlazar Cuenta para mis Depósitos</h2>
                            </div>
                            <div class="div-datos-perfil">
                                <div class="row-3 w-row">
                                    <div class="w-col w-col-6"></div>
                                    <div class="col0 w-clearfix w-col w-col-6">
                                        <div class="div_btn movil"><a href="#" class="btn btnmovil w-button">Agregar Nueva Cuenta</a></div>
                                    </div>
                                </div>
                                <div class="div-gris recuadro">
                                    <div class="w-form">
                                        <form id="wf-form-cuenta-bancaria" name="wf-form-cuenta-bancaria" data-name="cuenta bancaria">
                                            <div class="row-auto w-row">
                                                <div class="w-clearfix w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Titular</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="Titular" name="Titular" data-name="Titular" maxlength="256" placeholder="Nombre completo" class="perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Banco</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><select id="Tipo-de-tarjeta" name="Tipo-de-tarjeta" data-name="Tipo de tarjeta" class="select-basico w-select"><option value="">Seleccione una...</option><option value="First">American Express</option><option value="Second">Visa</option><option value="Third">Master Card</option></select></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Cuenta Clabe</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="Num.-de-tarjeta" maxlength="256" name="Num.-de-tarjeta" data-name="Num. de tarjeta" placeholder="Num. de tarjeta " class="perfil-form w-input"></div>
                                                    </div><a href="#" data-ix="abrir-guardar" class="btn color der w-button">Guardar</a><a href="#" data-ix="abrir-eliminar" class="btn der orange w-button">Eliminar cuenta</a></div>
                                                <div class="w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                    <div class="div-datos"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="w-form-done">
                                            <div>Thank you! Your submission has been received!</div>
                                        </div>
                                        <div class="w-form-fail">
                                            <div>Oops! Something went wrong while submitting the form</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- TERMINA ENLAZAR CUENTA PARA MIS DEPOSITOS -->
                        
                        <!-- DATOS DE FACTURACION -->
                        <div class="_100p div-wrap">
                            <div data-ix="abrir-datos" class="barra w-clearfix">
                                <div class="div-arrow"><img src="{!!asset('web/images/arrow-01.png')!!}" class="image-2"></div>
                                <h2 class="ns text_big">Datos de Facturación</h2>
                            </div>
                            <div class="div-datos-perfil">
                                <div class="cen div-form">
                                    <div class="w-form">
                                        <form id="wf-form-direccion-de-entrega-y-devolucion" name="wf-form-direccion-de-entrega-y-devolucion" data-name="direccion de entrega y devolucion">
                                            <div class="row-auto w-row">
                                                <div class="w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">RFC</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="RFC-3" name="RFC" data-name="RFC" maxlength="256" placeholder="RFC" class="perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Razón Social</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="Raz-n-social" name="Raz-n-social" data-name="Razón social" maxlength="256" placeholder="Razón social" class="perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Calle</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="Calle-5" name="Calle" data-name="Calle" maxlength="256" placeholder="Calle" class="perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Correo</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="email" id="Correo-4" name="Correo" data-name="Correo" placeholder="nombre@correo.com" maxlength="256" class="perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Delegación</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><input type="text" id="Delegacio" name="Delegacio" data-name="Delegacio" maxlength="256" placeholder="Delegación" class="perfil-form w-input"></div>
                                                    </div>
                                                </div>
                                                <div class="w-clearfix w-col w-col-6 w-col-small-small-stack w-col-stack">
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Número</div>
                                                        </div>
                                                        <div class="w-col w-col-9">
                                                            <div class="div-flex num"><input type="text" id="Num-exterior" name="Num-exterior" data-name="Num exterior" maxlength="256" placeholder="Ext" class="ext num perfil-form w-input"><input type="text" id="Nnum-interior" name="Nnum-interior" data-name="Nnum interior" maxlength="256" placeholder="Int" class="int num perfil-form w-input"></div>
                                                        </div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">C.P.</div>
                                                        </div>
                                                        <div class="w-clearfix w-col w-col-9"><input type="text" id="CP-4" name="CP" data-name="CP" maxlength="256" placeholder="CP." class="cp perfil-form w-input"></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Estado</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><select id="Estado-4" name="Estado-4" data-name="Estado 4" class="select-basico w-select"><option value="Another Choice">Seleccione uno...</option></select></div>
                                                    </div>
                                                    <div class="id row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Ciudad</div>
                                                        </div>
                                                        <div class="w-col w-col-9"><select id="Ciudad-3" name="Ciudad-3" data-name="Ciudad 3" class="select-basico w-select"><option value="Another Choice">Seleccione una...</option></select></div>
                                                    </div>
                                                    <div class="p row-tf w-row">
                                                        <div class="w-col w-col-3">
                                                            <div class="_20 right text_med">Colonia</div>
                                                        </div>
                                                        <div class="column-20 w-col w-col-9"><input type="text" id="Colonia-6" name="Colonia" data-name="Colonia" maxlength="256" placeholder="Colonia" class="perfil-form w-input"></div>
                                                    </div><a href="#" data-ix="abrir-guardar" class="btn color der w-button">Guardar</a></div>
                                            </div>
                                        </form>
                                        <div class="w-form-done">
                                            <div>Thank you! Your submission has been received!</div>
                                         </div>
                                        <div class="w-form-fail">
                                            <div>Oops! Something went wrong while submitting the form</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- TERMINAN DATOS DE FACTURACION -->
                    </div>
                    
                    <div data-w-tab="Mis Propiedades" class="pane-dashboard w--tab-active w-tab-pane">
                        <div>
                            <h1 class="titulo_dashboard">Mis Propiedades</h1>
                            <h2 class="h2dashboard">Administra todas las rentas de tus palcos y plateas en un solo lugar.</h2>
                            <div class="form-cuenta w-form">
                                <form id="wf-form-Propiedad" name="wf-form-Propiedad" data-name="Propiedad">
                                    <select id="propiedad" name="propiedad" data-name="propiedad" class="selector-propiedad w-select">
                                        <option value="">Seleccione una Propiedad</option>
                                        @foreach($box as $boxValue)
                                        <option value={!!$boxValue->id!!}>{!!$boxValue->num_box!!}</option>
                                        @endforeach
                                        @foreach($stall as $stallValue)
                                        <option value={!!$stallValue->id!!}>{!!$stallValue->zone->name_zone!!}</option>
                                        @endforeach
                                    </select>
                                </form>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                        <div data-duration-in="300" data-duration-out="100" class="tabs-propiedades w-tabs">
                            <div class="w-tab-content">
                                <!-- INICIAN NUEVAS TABLAS -->
                                <!-- PALCO AZTECA 112 -->
                                <div data-w-tab="Palco Azteca 112" class="dash w--tab-active w-tab-pane" id="tabla1">
                                    <div class="div-buttons w-clearfix">
                                        <a href="#" data-ix="abrir-activar-renta-anual" class="link_blue inline">Activar renta anual</a>
                                        <a href="#" data-ix="abrir-q-anual" class="qs w-button">?</a>
                                        <div class="div-right">
                                            <a href="#" data-ix="editar-palco" class="link_blue inline">Editar palco</a>
                                            <a href="#" data-ix="editar-platea" class="link_blue inline">(Ej. editar platea)</a>
                                            <a class="btn_azul _100movil btn_naranja w-button" data-ix="abrir-invita-y-gana">Invita a un propietario y gana dinero</a>
                                            <a href="info/administracion.html" class="btn_azul _100movil w-button">Quiero que administren mi palco</a>
                                            <a href="#" data-ix="abrir-question" class="qs rightmovil w-button">?</a>
                                        </div>
                                    </div>
                                    <!-- INICIA TABLA DE ADMINISTRACION DE PALCOS -->
                                    <table id="administrar_palco" class="row_tabla w-row escritorio" style="text-align:center;">
                                        <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                        <thead>
                                            <th class="th_evento">Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                            <th class="th_modo">Modo de Renta</th>
                                            <th class="th_bene">Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                            <th class="th_bole">Boletos</th>
                                            <th class="th_prec">Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                            <th class="th_prec">Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                            <th class="th_dep">Depósito de Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody id="body_property">
                                            <!-- AQUI HAY QUE CHECAR QUE TIPO DE EVENTO/PROPIEDAD ES PARA MOSTRAR DIFERENTE INFO -->
                                            {{--<!-- MCCARTNEY -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-de-renta" name="Modo-de-renta" data-name="Modo de renta" required="" class="selector-tabla w-select"><option value="">Solo rentar todo el palco</option><option value="">Permitir renta por asientos</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Precio-por-boleto" data-name="Precio por boleto" placeholder="$" id="Precio-por-boleto" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público: $350,087</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Utilidad-Total" data-name="Utilidad Total" placeholder="$" id="Utilidad-Total" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td><a href="#" data-ix="abrir-tipo-de-boletos" class="btn_small w-button">Publicar</a></td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- AMERICA-CHIVAS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>América vs. Chivas</b><br><span>11/11/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form"><form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas-2" name="Alimentos-y-bebidas-2" data-name="Alimentos Y Bebidas 2" required="" class="selector-tabla w-select"><option value="">Si incluye</option><option value="">No incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div><a href="#" data-ix="abrir-benefits" class="editar link_blue">Editar</a></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-2" name="Boletos-2" data-name="Boletos 2" required="" class="selector-tabla w-select"><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo-izq">- 25% de descuento</a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público: $340,965</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <div class="cont-col"><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a></div>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- PATRIOTS-COLTS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Patriots vs. Colts</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><p class="rentado_txt">Palco Rentado</p></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Precio-por-boleto-3" data-name="Precio Por Boleto 3" placeholder="$" id="Precio-por-boleto-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-naranja" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-naranja" class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Utilidad-Total-3" data-name="Utilidad Total 3" placeholder="$" id="Utilidad-Total-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-naranja" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-naranja" class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small disabled w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- TUZOS-RAYADOS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Tuzos vs. Rayados</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputrojo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-04.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-rojo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-rojo" class="nota rojo">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputrojo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-04.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-rojo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-rojo" class="nota rojo">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- SHAKIRA -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Shakira</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-5" name="Modo-De-Renta-5" data-name="Modo De Renta 5" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="Que no quede uno">Que no quede uno</option><option value="">No tengo preferencia</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td>
                                                    <div class="rentado_txt">6/10</div>
                                                </td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="cont-col">
                                                        <div class="form-tabla w-form">
                                                          <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputgris w-input" maxlength="256" name="Precio-por-boleto-3" data-name="Precio Por Boleto 3" placeholder="$" id="Precio-por-boleto-3" required="">
                                                            <div class="gris nota">Precio al público:</div>
                                                          </form>
                                                          <div class="w-form-done">
                                                            <div>Thank you! Your submission has been received!</div>
                                                          </div>
                                                          <div class="w-form-fail">
                                                            <div>Oops! Something went wrong while submitting the form.</div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="cont-col">
                                                <div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputgris w-input" maxlength="256" name="Utilidad-Total-3" data-name="Utilidad Total 3" placeholder="$" id="Utilidad-Total-3" required="">
                                                    <div class="gris nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div>
                                              </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small disabled w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- SOBRANTES SHAKIRA -->
                                            <tr>
                                                <td>
                                                    <img src="{!!asset('web/images/right_arrow-01.png')!!}" alt="asientos pendientes del evento" class="arrow_evento">
                                                </td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-sobrantes-evento" name="Boletos-sobrantes-evento" data-name="Boletos sobrantes evento" required="" class="selector-tabla w-select"><option value="Que no quede uno">Sobrantes Shakira</option><option value="">No tengo preferencia</option><option value="Por parejas">Por parejas</option><option value="Another Choice">Que no quede uno</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas-3" name="Alimentos-y-bebidas-3" data-name="Alimentos Y Bebidas 3" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-2" name="Boletos-2" data-name="Boletos 2" required="" class="selector-tabla w-select"><option value="">1</option><option value="">3</option><option value="">4</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Precio-por-boleto-3" data-name="Precio Por Boleto 3" placeholder="$" id="Precio-por-boleto-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" class="ic_small precio-bueno"></a>
                                                    <div class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Utilidad-Total-3" data-name="Utilidad Total 3" placeholder="$" id="Utilidad-Total-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" class="ic_small precio-bueno"></a>
                                                    <div class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia-3" data-name="Deposito De Garantia 3" placeholder="$" id="Deposito-de-garantia-3" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="chico editar link_blue">Editar envío</a><a href="#" class="btn_small disabled w-button">Guardar</a>
                                                </td>
                                            </tr>--}}
                                        </tbody>
                                    </table>
                                    <!-- TERMINA TABLA DE ADMINISTRACION DE PALCOS -->
                                    <!-- INICIA TABLA MOVIL -->
                                    <table class="movil" style="text-align:center;">
                                        <colgroup><col><col></colgroup>
                                        <tbody>
                                            <!-- PAUL MCCARTNEY -->
                                            <tr>
                                                <th>Evento</th>
                                                <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Modo</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-de-renta" name="Modo-de-renta" data-name="Modo de renta" required="" class="selector-tabla w-select"><option value="">Solo rentar todo el palco</option><option value="">Permitir renta por asientos</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Boletos</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Precio-por-boleto" data-name="Precio por boleto" placeholder="$" id="Precio-por-boleto" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público: $350,087</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Utilidad-Total" data-name="Utilidad Total" placeholder="$" id="Utilidad-Total" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Acciones</th>
                                                <td><a href="#" data-ix="abrir-tipo-de-boletos" class="btn_small w-button">Publicar</a></td>
                                            </tr>
                                            <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                            <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                            <!-- TERMINA ROW VACIO -->
                                            <!-- AMERICA VS CHIVAS -->
                                            <tr>
                                                <th>Evento</th>
                                                <td><p><b>América vs. Chivas</b><br><span>11/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Modo</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form"><form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas-2" name="Alimentos-y-bebidas-2" data-name="Alimentos Y Bebidas 2" required="" class="selector-tabla w-select"><option value="">Si incluye</option><option value="">No incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div><a href="#" data-ix="abrir-benefits" class="editar link_blue">Editar</a></td>
                                            </tr>
                                            <tr>
                                                <th>Boletos</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-2" name="Boletos-2" data-name="Boletos 2" required="" class="selector-tabla w-select"><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo-izq">- 25% de descuento</a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público: $340,965</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Acciones</th>
                                                <td>
                                                    <div class="cont-col"><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- TERMINA TABLA MOVIL -->
                                </div>
                                <!-- TERMINA 112 -->
                                <!-- PALCO AZTECA 110 -->
                                <div data-w-tab="Palco Azteca 110" class="dash w-tab-pane" id="tabla2">
                                    <div class="div-palco-en-proceso">
                                        <div class="cen text_big">
                                            Tu palco está en proceso de aprobación, en las próximas horas te enviaremos un correo de confirmación y podrás activar los eventos para los cuales quieres rentar tus boletos.
                                        </div>
                                        <img src="{!!asset('web/images/vectores-reloj-bl-01.png')!!}" class="image-cen">
                                    </div>
                                </div>
                                <!-- TERMINA 110 -->
                                <!-- PALCO AZTECA 113 -->
                                <div data-w-tab="Palco Azteca 113" class="dash w-tab-pane" id="tabla3">
                                    <div class="div-buttons w-clearfix">
                                        <div class="div-right">
                                            <a href="#" data-ix="editar-palco" class="inline link_blue">Editar palco</a><a href="#" data-ix="abrir-modal-descativar-admin" class="_100movil btn_azul w-button">Desactivar administración de palco</a>
                                        </div>
                                        <div class="inline text_med">Esta propiedad está siendo administrada por <strong>Stadibox</strong>, revisa aquí el estatus de tus rentas.</div>
                                        <!-- INICIA TABLA DE PROPIEDAD ADMINISTRADA -->
                                        <table id="palcos_administrados" class="escritorio" style="text-align:center;">
                                            <colgroup><col><col><col><col><col></colgroup>
                                            <thead>
                                                <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                <th>Estatus</th>
                                                <th class="th_ut">Precio Promedio por Boleto<a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                <th class="th_ut">Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                <th class="th_acc">Acciones<a href="#" data-ix="abrir-q-asistir" class="qs w-button white">?</a></th>
                                            </thead>
                                            <tbody>
                                                <!-- PAUL MCCARTNEY -->
                                                <tr>
                                                    <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></td>
                                                    <td><p class="rentado_txt">6/10 rentados</p></td>
                                                    <td><p class="prec_num">$40,000.00</p></td>
                                                    <td><p class="prec_num">$210,000.00</p></td>
                                                    <td><a href="#" data-ix="abrir-modal-quiero-asistir" class="btn_small upper_space w-button">Quiero asistir</a></td>
                                                </tr>
                                                <!-- AMERICA-CHIVAS -->
                                                <tr>
                                                    <td><p><b>América vs. Chivas</b></p><span>12/10/2017</span></td>
                                                    <td><p class="rentado_txt gray">Aún sin rentar</p></td>
                                                    <td><p class="prec_num">$-</p></td>
                                                    <td><p class="prec_num">$-</p></td>
                                                    <td><a href="#" class="btn_small desactivado upper_space w-button">Quiero asistir</a></td>
                                                </tr>
                                                <!-- YA NO QUIERO ASISTIR -->
                                                <tr>
                                                    <td><p><b>Steelers vs. Packers</b></p><span>27/08/2018</span></td>
                                                    <td><p class="rentado_txt">3/13 rentados (2 Apartados)</p></td>
                                                    <td><p class="prec_num">$15,000</p></td>
                                                    <td><p class="prec_num">$195,000</p></td>
                                                    <td><a href="#" data-ix="abrir-modal-quiero-asistir" class="btn_small upper_space w-button">Ya no quiero asistir</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- TERMINA TABLA DE PROPIEDAD ADMINISTRADA -->
                                        <!-- INICIA TABLA MOVIL -->
                                        <table class="movil" style="text-align:center;">
                                            <colgroup><col><col></colgroup>
                                            <tbody>
                                                <tr>
                                                    <th>Evento</th>
                                                    <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></p></td>
                                                </tr>
                                                <tr>
                                                    <th>Estatus</th>
                                                    <td><p class="rentado_txt">6/10 rentados</p></td>
                                                </tr>
                                                <tr>
                                                    <th>Precio promedio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                    <td><p class="prec_num">$40,000.00</p></td>
                                                </tr>
                                                <tr>
                                                    <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                    <td><p class="prec_num">$210,000.00</p></td>
                                                </tr>
                                                <tr>
                                                    <th>Acciones<a href="#" data-ix="abrir-q-asistir" class="qs w-button white">?</a></th>
                                                    <td><a href="#" data-ix="abrir-modal-quiero-asistir" class="btn_small upper_space w-button">Quiero asistir</a></td>
                                                </tr>
                                                <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                <!-- TERMINA ROW VACIO -->
                                                <tr>
                                                    <th>Evento</th>
                                                    <td><p><b>América vs. Chivas</b><br><span>11/10/2017</span></p></td>
                                                </tr>
                                                <tr>
                                                    <th>Estatus</th>
                                                    <td>Aún sin rentar</td>
                                                </tr>
                                                <tr>
                                                    <th>Precio promedio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                    <td><p class="prec_num">$-</p></td>
                                                </tr>
                                                <tr>
                                                    <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                    <td><p class="prec_num">$-</p></td>
                                                </tr>
                                                <tr>
                                                    <th>Acciones<a href="#" data-ix="abrir-q-asistir" class="qs w-button white">?</a></th>
                                                    <td><a href="#" data-ix="abrir-modal-quiero-asistir" class="btn_small upper_space w-button">Quiero asistir</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- TERMINA TABLA MOVIL -->
                                    </div>
                                </div>
                                <!-- TERMINA 113 -->
                                <!-- PALCO AZTECA 114 -->
                                <div data-w-tab="Palco Azteca 114" class="dash w-tab-pane" id="tabla4">
                                    <div class="inline text_med">Esta propiedad está en modalidad de renta anual, revisa aquí los datos de la renta.</div>
                                    <!-- INICIA TABLA DE RENTA ANUAL -->
                                    <table id="tabla_anual">
                                    <colgroup><col><col></colgroup>
                                    <tbody>
                                        <!-- INICIO DE RENTA -->
                                        <tr>
                                            <th>Inicio de Renta</th>
                                            <td>10/01/2018</td>
                                        </tr>
                                        <!-- FIN DE RENTA -->
                                        <tr>
                                            <th class="th_obs">Fin de Renta</th>
                                            <td>10/01/2019</td>
                                        </tr>
                                        <!-- UTILIDAD TOTAL -->
                                        <tr>
                                            <th>Utilidad Total</th>
                                            <td><b>$700,000.00 MXN</b></td>
                                        </tr>
                                        <!-- ESTATUS -->
                                        <tr>
                                            <th class="th_obs">Estatus</th>
                                            <td>Pagado</td>
                                        </tr>
                                        <!-- NOMBRE DE CLIENTE -->
                                        <tr>
                                            <th>Nombre de Cliente</th>
                                            <td>Coca-Cola Company</td>
                                        </tr>
                                    </tbody>
                                </table>
                                    <!-- TERMINA TABLA DE RENTA ANUAL -->
                                </div>
                                <!-- TERMINA 114 -->
                                <!-- PALCO AZTECA 115 -->
                                <div data-w-tab="Palco Azteca 115" class="dash w-tab-pane" id="tabla5">
                                    <div class="inline text_med">Estamos buscando un cliente para la renta anual, mientras tanto puedes rentar tu propiedad por evento.</div><a href="#" data-ix="abrir-cancelar-renta" class="inline link_blue mov-full">Cancelar búsqueda de renta anual</a>
                                    <div class="div-right"><a href="#" data-ix="editar-palco" class="inline link_blue">Editar palco</a></div>
                                    <!-- INICIA TABLA BUSQUEDA RENTA ANUAL -->
                                    <table id="administrar_palco" class="escritorio" style="text-align:center;">
                                        <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                        <thead>
                                            <th class="th_evento">Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                            <th class="th_modo">Modo de Renta</th>
                                            <th class="th_bene">Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                            <th class="th_bole">Boletos</th>
                                            <th class="th_prec">Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                            <th class="th_prec">Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                            <th class="th_dep">Depósito de Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                            <!-- AQUI HAY QUE CHECAR QUE TIPO DE EVENTO/PROPIEDAD ES PARA MOSTRAR DIFERENTE INFO -->
                                            <!-- MCCARTNEY -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-de-renta" name="Modo-de-renta" data-name="Modo de renta" required="" class="selector-tabla w-select"><option value="">Solo rentar todo el palco</option><option value="">Permitir renta por asientos</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Precio-por-boleto" data-name="Precio por boleto" placeholder="$" id="Precio-por-boleto" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público: $350,087</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Utilidad-Total" data-name="Utilidad Total" placeholder="$" id="Utilidad-Total" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td><a href="#" data-ix="abrir-tipo-de-boletos" class="btn_small w-button">Publicar</a></td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- AMERICA-CHIVAS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>América vs. Chivas</b><br><span>11/11/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form"><form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas-2" name="Alimentos-y-bebidas-2" data-name="Alimentos Y Bebidas 2" required="" class="selector-tabla w-select"><option value="">Si incluye</option><option value="">No incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div><a href="#" data-ix="abrir-benefits" class="editar link_blue">Editar</a></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-2" name="Boletos-2" data-name="Boletos 2" required="" class="selector-tabla w-select"><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo-izq">- 25% de descuento</a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público: $340,965</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <div class="cont-col"><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a></div>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- PATRIOTS-COLTS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Patriots vs. Colts</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><p class="rentado_txt">Palco Rentado</p></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Precio-por-boleto-3" data-name="Precio Por Boleto 3" placeholder="$" id="Precio-por-boleto-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-naranja" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-naranja" class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputnaranja w-input" maxlength="256" name="Utilidad-Total-3" data-name="Utilidad Total 3" placeholder="$" id="Utilidad-Total-3" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-03.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-naranja" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-naranja" class="naranja nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small disabled w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- TUZOS-RAYADOS -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Tuzos vs. Rayados</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputrojo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-04.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-rojo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-rojo" class="nota rojo">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputrojo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-04.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-rojo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-rojo" class="nota rojo">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                            <!-- SHAKIRA -->
                                            <tr>
                                                <!-- INFORMACION GENERAL DEL EVENTO -->
                                                <td><p><b>Shakira</b><br><span>17/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                                <!-- TERMINA INFORMACION GENERAL DEL EVENTO -->
                                                <!-- MODO DE RENTA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-5" name="Modo-De-Renta-5" data-name="Modo De Renta 5" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="Que no quede uno">Que no quede uno</option><option value="">No tengo preferencia</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA MODO DE RENTA -->
                                                <!-- BENEFICIOS ADICIONALES -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINAN BENEFICIOS ADICIONALES -->
                                                <!-- NUMERO DE BOLETOS -->
                                                <td>
                                                    <div class="rentado_txt">6/10</div>
                                                </td>
                                                <!-- TERMINA NUMERO DE BOLETOS -->
                                                <!-- PRECIO POR BOLETO -->
                                                <td><div class="cont-col">
                                                        <div class="form-tabla w-form">
                                                          <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputgris w-input" maxlength="256" name="Precio-por-boleto-3" data-name="Precio Por Boleto 3" placeholder="$" id="Precio-por-boleto-3" required="">
                                                            <div class="gris nota">Precio al público:</div>
                                                          </form>
                                                          <div class="w-form-done">
                                                            <div>Thank you! Your submission has been received!</div>
                                                          </div>
                                                          <div class="w-form-fail">
                                                            <div>Oops! Something went wrong while submitting the form.</div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </td>
                                                <!-- TERMINA PRECIO POR BOLETO -->
                                                <!-- UTILIDAD TOTAL -->
                                                <td><div class="cont-col">
                                                <div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputgris w-input" maxlength="256" name="Utilidad-Total-3" data-name="Utilidad Total 3" placeholder="$" id="Utilidad-Total-3" required="">
                                                    <div class="gris nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div>
                                              </div></td>
                                                <!-- TERMINA UTILIDAD TOTAL -->
                                                <!-- DEPOSITO DE GARANTIA -->
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                                <!-- TERMINA DEPOSITO DE GARANTIA -->
                                                <!-- ACCIONES -->
                                                <td>
                                                    <a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small disabled w-button">Guardar</a>
                                                </td>
                                                <!-- TERMINAN ACCIONES -->
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- TERMINA TABLA BUSQUEDA RENTA ANUAL -->
                                    <!-- INICIA TABLA MOVIL -->
                                    <table class="movil" style="text-align:center;">
                                        <colgroup><col><col></colgroup>
                                        <tbody>
                                            <!-- PAUL MCCARTNEY -->
                                            <tr>
                                                <th>Evento</th>
                                                <td><p><b>Paul McCartney en el Estadio Azteca</b><br><span>10/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Modo</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-de-renta" name="Modo-de-renta" data-name="Modo de renta" required="" class="selector-tabla w-select"><option value="">Solo rentar todo el palco</option><option value="">Permitir renta por asientos</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="Alimentos-y-bebidas" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="">No incluye</option><option value="">Si incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Boletos</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select"><option value="">10</option><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Precio-por-boleto" data-name="Precio por boleto" placeholder="$" id="Precio-por-boleto" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público: $350,087</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="Utilidad-Total" data-name="Utilidad Total" placeholder="$" id="Utilidad-Total" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-verde" class="nota verde">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Acciones</th>
                                                <td><a href="#" data-ix="abrir-tipo-de-boletos" class="btn_small w-button">Publicar</a></td>
                                            </tr>
                                            <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                            <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                            <!-- TERMINA ROW VACIO -->
                                            <!-- AMERICA VS CHIVAS -->
                                            <tr>
                                                <th>Evento</th>
                                                <td><p><b>América vs. Chivas</b><br><span>11/10/2017</span></p>
                                                  <a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">
                                                    <div class="circulo"></div>
                                                    <div class="text-switch">No rentar</div>
                                                    <div class="switchon w-clearfix">
                                                      <div class="text_on">Rentar</div>
                                                    </div>
                                                  </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Modo</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-De-Renta-3" name="Modo-De-Renta" data-name="Modo De Renta" required="" class="selector-tabla w-select"><option value="">Permitir renta por asientos</option><option value="">Solo rentar todo el palco</option></select><select id="Modo-de-renta-2" name="Modo-de-renta-2" data-name="Modo De Renta 2" required="" class="selector-tabla w-select"><option value="">Elige una opción</option><option value="">No tengo preferencia</option><option value="Que no quede uno">Que no quede uno</option><option value="Por parejas">Por parejas</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form"><form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas-2" name="Alimentos-y-bebidas-2" data-name="Alimentos Y Bebidas 2" required="" class="selector-tabla w-select"><option value="">Si incluye</option><option value="">No incluye</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div><a href="#" data-ix="abrir-benefits" class="editar link_blue">Editar</a></td>
                                            </tr>
                                            <tr>
                                                <th>Boletos</th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Boletos-2" name="Boletos-2" data-name="Boletos 2" required="" class="selector-tabla w-select"><option value="">9</option><option value="">8</option></select></form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo-izq">- 25% de descuento</a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público:</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-utilidad" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputamarillo w-input" maxlength="256" name="Precio-por-boleto-2" data-name="Precio Por Boleto 2" placeholder="$" id="Precio-por-boleto-2" required=""><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-02.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-amarillo" class="ic_small precio-bueno"></a>
                                                    <div data-ix="abrir-amarillo" class="amarillo nota">Precio al público: $340,965</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                <td><div class="form-tabla w-form">
                                                  <form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="Deposito-de-garantia" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="">
                                                    <div class="nota">Por asiento</div>
                                                  </form>
                                                  <div class="w-form-done">
                                                    <div>Thank you! Your submission has been received!</div>
                                                  </div>
                                                  <div class="w-form-fail">
                                                    <div>Oops! Something went wrong while submitting the form.</div>
                                                  </div>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <th>Acciones</th>
                                                <td>
                                                    <div class="cont-col"><a href="#" data-ix="abrir-descuento" class="chico editar link_blue rojo">Descuento</a><a href="#" data-ix="abrir-tipo-de-boletos" class="editar link_blue">Editar envío</a><a href="#" class="btn_small w-button">Guardar</a></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- TERMINA TABLA MOVIL -->
                                </div>
                                <!-- TERMINA 115 -->
                                <!-- TERMINAN NUEVAS TABLAS -->
                            </div>
                            <div class="tabs-menu w-tab-menu">
                                {{--<a data-w-tab="Palco Azteca 112" class="tab-propiedad w--current w-inline-block w-tab-link">
                                    <div>Palco Azteca 112</div>
                                </a>
                                  <a data-w-tab="Palco Azteca 110" class="tab-propiedad w-inline-block w-tab-link">
                                    <div>Palco Azteca 110</div>
                                  </a>
                                  <a data-w-tab="Palco Azteca 113" class="tab-propiedad w-inline-block w-tab-link">
                                    <div class="text-block">Palco Azteca 113</div>
                                    <div>(Administrado)</div>
                                  </a>
                                  <a data-w-tab="Palco Azteca 114" class="tab-propiedad w-inline-block w-tab-link">
                                    <div class="text-block">Palco Azteca 114 (Renta anual)</div>
                                  </a>
                                  <a data-w-tab="Palco Azteca 115" class="tab-propiedad w-inline-block w-tab-link">
                                    <div>Palco Azteca 115</div>
                                  </a>--}}
                            </div>
                        </div>
                    </div>
                    
                    <div data-w-tab="Mi Tablero" class="tab-mi-tablero w-tab-pane">
                        <h1 class="titulo_dashboard">Mi Tablero</h1>
                        <h2 class="h2dashboard">Visualiza y optimiza los resultados de tus propiedades.</h2>
                        <div>
                            <div class="form-cuenta w-form">
                                <form id="wf-form-Propiedad" name="wf-form-Propiedad" data-name="Propiedad"><select id="propiedad-4" name="propiedad-4" data-name="Propiedad 4" class="selector-propiedad w-select"><option value="">Palco Azteca 110</option><option value="">Palco Azteca 112</option><option value="">Palco Azteca 113 (Administrado)</option><option value="">Plateas Omnilife 1540-1543</option><option value="">Plateas Omnilife 1140-1150</option><option value="Another Choice">Palco Azteca 115 </option><option value="">Palco Arena CDMX 21</option><option value="">Palco Azteca 118 (Renta anual)</option></select></form>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                        <div data-duration-in="300" data-duration-out="100" class="tabs-propiedades w-tabs">
                            <div class="w-tab-content">
                                <div data-w-tab="ejemplo" class="dash propiedades2 w--tab-active w-tab-pane">
                                    <div class="w-row">
                                        <div class="w-col w-col-3 w-col-small-6 w-col-tiny-6">
                                            <div class="mini-block w-clearfix">
                                                <div class="bloque"></div>
                                                <div class="mini-block-txt">
                                                    <h2 class="mini-h2 purple">Visitas </h2>
                                                    <div class="claro text_med">47</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cb w-col w-col-3 w-col-small-6 w-col-tiny-6">
                                            <div class="mini-block w-clearfix">
                                            <div class="bloque yellow"></div>
                                            <div class="mini-block-txt">
                                                <h2 class="mini-h2 yellow">Ingresos</h2>
                                                <div class="claro text_med">$675,200</div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="cb w-col w-col-3 w-col-small-6 w-col-tiny-6">
                                            <div class="mini-block w-clearfix">
                                              <div class="aqua bloque"></div>
                                              <div class="mini-block-txt">
                                                <h2 class="aqua mini-h2">Ranking</h2>
                                                <div class="claro text_med">#7</div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="cb w-col w-col-3 w-col-small-6 w-col-tiny-6">
                                            <div class="mini-block w-clearfix">
                                              <div class="bloque naranja"></div>
                                              <div class="mini-block-txt">
                                                <h2 class="mini-h2 naranja">Favoritos</h2>
                                                <div class="claro text_med">11</div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-bajo w-row">
                                      <div class="column-23 w-col w-col-6 w-col-small-small-stack w-col-stack">
                                        <div class="block-tablero">
                                          <h3 class="blocked title">Calificación</h3>
                                          <div class="grey-block">
                                            <div class="full semibold txt_small">Número de calificaciones: <strong class="green">132</strong></div>
                                            <div class="div-estrellas promedio"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/star-05.png')!!}" class="star star2"><img src="{!!asset('web/images/star-04.png')!!}" class="star star2"></div>
                                            <div class="semibold txt_small">4.6/5</div>
                                          </div>
                                          <div class="div-evaluacion w-clearfix">
                                            <div class="semibold txt_small">Roy </div>
                                            <div class="date txt_small">Marzo de 2018</div><a href="#" data-ix="abrir-flag" class="flag w-inline-block"></a>
                                            <div class="div-estrellas"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2">
                                              <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. </p>
                                            </div>
                                          </div>
                                          <div class="div-evaluacion w-clearfix">
                                            <div class="semibold txt_small">Camilo</div>
                                            <div class="date txt_small">Enero de 2018</div><a href="#" data-ix="abrir-flag" class="flag w-inline-block"></a>
                                            <div class="div-estrellas"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/star-04.png')!!}" class="star star2">
                                              <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. </p>
                                            </div>
                                          </div>
                                          <div class="div-evaluacion w-clearfix">
                                            <div class="semibold txt_small">Rafa</div>
                                            <div class="date txt_small">Diciembre de 2017</div><a href="#" data-ix="abrir-flag" class="flag w-inline-block"></a>
                                            <div class="div-estrellas"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2"><img src="{!!asset('web/images/iconos-03.png')!!}" class="star star2">
                                              <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat</p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="cb w-col w-col-6 w-col-small-small-stack w-col-stack">
                                        <div class="block-tablero no-overflow">
                                          <h3 class="blocked title">Rendimiento</h3>
                                          <p class="parrafo">Lorem ipsum sit er amet suspuisse eratum cloros testa perem atisiem ,acut nurem atos. Nala et simba ratem alocat silem.</p>
                                          <div class="espacio w-clearfix">
                                            <div class="centro">
                                              <div class="registrados">Comprados<br><span class="numero">12</span></div>
                                            </div>
                                            <div class="llave">
                                              <div class="div-llave">
                                                <div class="cuadrito"></div>
                                                <div class="ns semibold txt_small">Comprados</div>
                                              </div>
                                              <div class="div-llave">
                                                <div class="amarillo cuadrito"></div>
                                                <div class="ns semibold txt_small">Visitas a tu propiedad</div>
                                              </div>
                                              <div class="div-llave">
                                                <div class="cuadrito purple"></div>
                                                <div class="ns semibold txt_small">Visitas a tus eventos</div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="w-row">
                                      <div class="w-col w-col-4 w-col-stack">
                                        <div class="block-tablero smaller">
                                          <h3 class="blocked title">Comparativa</h3><img src="images/Screen-Shot-2017-11-03-at-12.52.36-AM.png" srcset="images/Screen-Shot-2017-11-03-at-12.52.36-AM-p-500.png 500w, images/Screen-Shot-2017-11-03-at-12.52.36-AM-p-800.png 800w, images/Screen-Shot-2017-11-03-at-12.52.36-AM-p-1080.png 1080w, images/Screen-Shot-2017-11-03-at-12.52.36-AM.png 1272w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 73vw, (max-width: 991px) 76vw, (max-width: 5300px) 24vw, 1272px"></div>
                                      </div>
                                      <div class="cb w-col w-col-4 w-col-stack">
                                        <div class="block-tablero smaller">
                                          <h3 class="blocked title">Visitas</h3><img src="images/Screen-Shot-2017-11-03-at-12.43.44-AM.png" srcset="images/Screen-Shot-2017-11-03-at-12.43.44-AM-p-500.png 500w, images/Screen-Shot-2017-11-03-at-12.43.44-AM-p-800.png 800w, images/Screen-Shot-2017-11-03-at-12.43.44-AM-p-1080.png 1080w, images/Screen-Shot-2017-11-03-at-12.43.44-AM.png 1118w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 73vw, (max-width: 991px) 76vw, (max-width: 4658px) 24vw, 1118px"></div>
                                      </div>
                                      <div class="cb w-col w-col-4 w-col-stack">
                                        <div class="block-tablero smaller">
                                          <h3 class="blocked title">Ventas</h3><img src="images/Screen-Shot-2017-11-03-at-12.47.07-AM.png" srcset="images/Screen-Shot-2017-11-03-at-12.47.07-AM-p-500.png 500w, images/Screen-Shot-2017-11-03-at-12.47.07-AM-p-800.png 800w, images/Screen-Shot-2017-11-03-at-12.47.07-AM-p-1080.png 1080w, images/Screen-Shot-2017-11-03-at-12.47.07-AM.png 1158w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 73vw, (max-width: 991px) 76vw, (max-width: 4825px) 24vw, 1158px"></div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-tab-menu">
                              <a data-w-tab="ejemplo" class="tab-propiedad w--current w-inline-block w-tab-link">
                                <div>Palco Azteca 112</div>
                              </a>
                            </div>
                        </div>
                    </div>
                    
                    <div data-w-tab="Mis Compras" class="tab-pane-3 w-tab-pane">
                        <h1 class="titulo_dashboard">Mis Rentas</h1>
                        <h2 class="h2dashboard">Consulta el estatus e historial de tus compras.</h2>
                        <div data-duration-in="300" data-duration-out="100" class="tabs-propiedades w-tabs">
                            <div class="w-tab-content">
                                <div data-w-tab="Ejemplo" class="dash w--tab-active w-tab-pane">
                                    <div data-duration-in="300" data-duration-out="100" class="w-tabs">
                                        <div class="tabs-menu-estatus w-tab-menu">
                                            <a data-w-tab="Próximas" class="tab-estatus w--current w-inline-block w-tab-link cinc">
                                                <div>Próximas</div>
                                            </a>
                                            <a data-w-tab="Finalizadas" class="tab-estatus w-inline-block w-tab-link cinc">
                                                <div>Finalizadas</div>
                                            </a>
                                        </div>
                                        <div class="w-tab-content">
                                            <div data-w-tab="Próximas" class="tabpane tabpaneestatus w--tab-active w-tab-pane">
                                                <!-- TABLA PROXIMAS -->
                                                <table id="compras_prox" class="escritorio" style="text-align:center;">
                                                    <colgroup><col><col><col><col><col><col><col><col><col> </colgroup>
                                                    <thead>
                                                        <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Estatus</th>
                                                        <th>Beneficios</th>
                                                        <th>Modo de Renta</th>
                                                        <th>Boletos</th>
                                                        <th>Parking</th>
                                                        <th>Pago</th>
                                                        <th>Depósito de Garantía</th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        <!-- MCCARTNEY -->
                                                        <tr>
                                                            <td>
                                                                <p><b>Paul McCartney en el Azteca</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 12543</span></p>
                                                            </td>
                                                            <td><p class="rentado_txt">Por recibir</p></td>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-ver-benefits" class="text-link">Ver</a></td>
                                                            <td>Todo el Palco</td>
                                                            <td>10</td>
                                                            <td>2 (VIP)</td>
                                                            <td><p><b>$120,000.00</b></p></td>
                                                            <td>$10,000.00<br>(Total)</td>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                    <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                        <td>
                                                            <p><b>América vs. Chivas</b><br><span>12/10/2017</span><br><span class="green">ID Publicación: 43265</span></p>
                                                        </td>
                                                        <td><p class="rentado_txt">Recibidos</p></td>
                                                        <td>No incluye</td>
                                                        <td>Palco Compartido</td>
                                                        <td>4</td>
                                                        <td>1 (VIP)</td>
                                                        <td><p><b>$80,000.00</b></p></td>
                                                        <td>$4,000.00<br>(Total)</td>
                                                        <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA PROXIMAS -->
                                                <!-- INICIA TABLA MOVIL -->
                                                <table class="movil" style="text-align:center;">
                                                    <colgroup><col><col></colgroup>
                                                    <tbody>
                                                        <!-- PAUL MCCARTNEY -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>10/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Estatus</th>
                                                            <td><p class="rentado_txt">Por recibir</p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-ver-benefits" class="text-link">Ver</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Todo el Palco</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Parking</th>
                                                            <td>2 (VIP)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Pago</th>
                                                            <td><p><b>$120,000.00</b></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                        <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                        <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                        <!-- TERMINA ROW VACIO -->
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>America vs. Chivas</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Estatus</th>
                                                            <td><p class="rentado_txt">Recibidos</p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>No incluye</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Palco Compartido</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>4</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Parking</th>
                                                            <td>1 (VIP)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Pago</th>
                                                            <td><p><b>$80,000.00</b></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$4,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA MOVIL -->
                                            </div>
                                            <div data-w-tab="Finalizadas" class="w-tab-pane">
                                                <!-- TABLA FINALIZADAS -->
                                                <table id="compras_fin" class="escritorio" style="text-align:center;">
                                                    <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                                    <thead>
                                                        <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Modo de Renta</th>
                                                        <th>Beneficios Adicionales</th>
                                                        <th>Boletos</th>
                                                        <th>Parking</th>
                                                        <th>Pago</th>
                                                        <th>Depósito de Garantía</th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                            <td>Todo el Palco</td>
                                                            <td>No incluye</td>
                                                            <td>10</td>
                                                            <td>2 (VIP)</td>
                                                            <td><b>$120,000.00</b></td>
                                                            <td>$10,000.00<br>(Total)</td>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar Problema</a><a href="comunicacion/rating.html" class="btn_small w-button" style="margin-top: 2%;">Calificar</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td><p><b>América vs. Chivas</b><br><span>12/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                                <td>Palco Compartido</td>
                                                                <td>No incluye</td>
                                                                <td>4</td>
                                                                <td>1 (VIP)</td>
                                                                <td><b>$80,000.00</b></td>
                                                                <td>$4,000.00<br>(Total)</td>
                                                                <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar Problema</a><a href="comunicacion/rating.html" class="btn_small w-button" style="margin-top:2%;">Calificar</a></td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                                <!-- TERMINA TABLA FINALIZADAS -->
                                                <!-- INICIA TABLA MOVIL -->
                                                <table class="movil" style="text-align:center;">
                                                    <colgroup><col><col></colgroup>
                                                    <tbody>
                                                        <!-- PAUL MCCARTNEY -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>10/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Todo el Palco</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios Adicionales</th>
                                                            <td>No incluye</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Parking</th>
                                                            <td>2 (VIP)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><p><b>$120,000.00</b></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                        <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                        <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                        <!-- TERMINA ROW VACIO -->
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>America vs. Chivas</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Palco Compartido</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>No incluye</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>4</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Parking</th>
                                                            <td>1 (VIP)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><p><b>$80,000.00</b></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$4,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA MOVIL -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-tab-menu">
                                <a data-w-tab="Ejemplo" class="tab-propiedad w--current w-inline-block w-tab-link">
                                    <div class="text-block">Plateas Azteca 1121-1231</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div data-w-tab="Mis Ventas" class="tab-pane-3 ventas w-tab-pane">
                        <h1 class="titulo_dashboard">Mis Ventas</h1>
                        <h2 class="h2dashboard">Consulta el estatus de las ventas de tus propiedades.</h2>
                        <div class="form-cuenta w-form">
                            <form id="wf-form-Propiedad" name="wf-form-Propiedad" data-name="Propiedad"><select id="propiedad-3" name="propiedad-3" data-name="Propiedad 3" class="selector-propiedad w-select"><option value="">Palco Azteca 110</option><option value="">Palco Azteca 112</option><option value="">Palco Azteca 113 (Administrado)</option><option value="">Plateas Omnilife 1540-1543</option><option value="">Plateas Omnilife 1140-1150</option><option value="Another Choice">Palco Azteca 115 </option><option value="">Palco Arena CDMX 21</option><option value="">Palco Azteca 118 (Renta anual)</option></select></form>
                            <div class="w-form-done">
                                <div>Thank you! Your submission has been received!</div>
                            </div>
                            <div class="w-form-fail">
                                <div>Oops! Something went wrong while submitting the form.</div>
                            </div>
                        </div>
                        <div data-duration-in="300" data-duration-out="100" class="tabs-propiedades w-tabs">
                            <div class="w-tab-content">
                                <div data-w-tab="ejemplo" class="dash w--tab-active w-tab-pane">
                                    <div data-duration-in="300" data-duration-out="100" class="w-tabs">
                                        <div class="ns tabs-menu-estatus w-tab-menu">
                                            <a data-w-tab="En venta" class="tab-estatus w--current w-inline-block w-tab-link">
                                                <div>En venta</div>
                                            </a>
                                            <a data-w-tab="Pausadas" class="tab-estatus w-inline-block w-tab-link">
                                              <div>Pausadas</div>
                                            </a>
                                            <a data-w-tab="Vendidas" class="tab-estatus w-inline-block w-tab-link">
                                              <div>Vendidas</div>
                                            </a>
                                        </div>
                                        <div class="w-tab-content">
                                            <div data-w-tab="En venta" class="w--tab-active w-tab-pane">
                                                <!-- INICIA TABLA DE EN VENTA -->
                                                <table class="escritorio" style="text-align:center;">
                                                    <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                                    <thead>
                                                        <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Estatus</th>
                                                        <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                        <th>Boletos</th>
                                                        <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                        <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Depósito de Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        <!-- MCCARTNEY -->
                                                        <tr>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 12543</span></td>
                                                            <td>Solo rentar todo el palco</td>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                            <td>10/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <td><p><b>América vs. Chvias</b><br><span>12/10/2017</span><br><span class="green">ID Publicación: 54612</span></td>
                                                            <td>Permitir renta por asientos:<br>-Por parejas</td>
                                                            <td>No incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                            <td>6/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA DE EN VENTA -->
                                                <!-- INICIA TABLA MOVIL -->
                                                <table class="movil" style="text-align:center;">
                                                    <colgroup><col><col></colgroup>
                                                    <tbody>
                                                        <!-- PAUL MCCARTNEY -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>10/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Solo rentar Todo el Palco</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios Adicionales</th>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>10/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                        <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                        <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                        <!-- TERMINA ROW VACIO -->
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>America vs. Chivas</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Permitir renta por asientos:<br>- Por parejas</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>No incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>6/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA MOVIL -->
                                            </div>
                                            <div data-w-tab="Pausadas" class="w-tab-pane">
                                                <!-- INICIA TABLA PAUSADAS -->
                                                <table class="escritorio" style="text-align:center;color:gray !important;">
                                                    <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                                    <thead>
                                                        <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Estatus</th>
                                                        <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                        <th>Boletos</th>
                                                        <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                        <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Depósito de Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        <!-- MCCARTNEY -->
                                                        <tr>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 12543</span></td>
                                                            <td>Solo rentar todo el palco</td>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                            <td>10/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" class="btn_small w-button">Activar</a></td>
                                                        </tr>
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <td><p><b>América vs. Chvias</b><br><span>12/10/2017</span><br><span class="green">ID Publicación: 54612</span></td>
                                                            <td>Permitir renta por asientos:<br>-Por parejas</td>
                                                            <td>No incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                            <td>6/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" class="btn_small w-button">Activar</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA PAUSADAS -->
                                                <!-- INICIA TABLA MOVIL -->
                                                <table class="movil" style="text-align:center;color; gray !important">
                                                    <colgroup><col><col></colgroup>
                                                    <tbody>
                                                        <!-- PAUL MCCARTNEY -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>10/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Solo rentar Todo el Palco</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios Adicionales</th>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>10/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                        <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                        <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                        <!-- TERMINA ROW VACIO -->
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>America vs. Chivas</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td>Permitir renta por asientos:<br>- Por parejas</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>No incluye<br><a href="#" data-ix="abrir-benefits" class="text-link">Editar</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>6/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" class="btn_small space w-button">Editar</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA MOVIL -->
                                            </div>
                                            <div data-w-tab="Vendidas" class="w-tab-pane">
                                                <!-- INICIA TABLA VENDIDAS -->
                                                <table class="escritorio" style="text-align:center;">
                                                    <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                                    <thead>
                                                        <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Estatus</th>
                                                        <th>Beneficios Adicionales<a href="#" data-ix="abrir-q-beneficios" class="qs w-button white">?</a></th>
                                                        <th>Boletos</th>
                                                        <th>Precio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"><a href="#" data-ix="abrir-q-precios" class="qs w-button white">?</a></th>
                                                        <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                                        <th>Depósito de Garantía<a href="#" data-ix="abrir-q-deposito" class="qs w-button white">?</a></th>
                                                        <th>Acciones</th>
                                                    </thead>
                                                    <tbody>
                                                        <!-- MCCARTNEY -->
                                                        <tr>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 12543</span></td>
                                                            <td class="rentado_txt">En acuerdo para la entrega</td>
                                                            <td>Si incluye<a href="#" data-ix="abrir-ver-benefits" class="text-link">Ver</a></td>
                                                            <td>10/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <td><p><b>América vs. Chivas</b><br><span>12/10/2017</span><br><span class="green">ID Publicación: 54612</span></td>
                                                            <td class="rentado_txt">Liquidado</td>
                                                            <td>No incluye</td>
                                                            <td>6/10</td>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                            <td>$10,000.00<br><div class="inline nota">(Total)</div></td>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA VENDIDAS -->
                                                <!-- INICIA TABLA MOVIL -->
                                                <table class="movil" style="text-align:center;">
                                                    <colgroup><col><col></colgroup>
                                                    <tbody>
                                                        <!-- PAUL MCCARTNEY -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>Paul McCartney en el Azteca</b><br><span>10/10/2017</span><br><span class="green">ID Publicación: 12543</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td class="rentado_txt">En acuerdo para la entrega</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios Adicionales</th>
                                                            <td>Si incluye<br><a href="#" data-ix="abrir-ver-benefits" class="text-link">Ver</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>10/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                        <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                                        <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                                        <!-- TERMINA ROW VACIO -->
                                                        <!-- AMERICA-CHIVAS -->
                                                        <tr>
                                                            <th>Evento</th>
                                                            <td><p><b>America vs. Chivas</b><br><span>11/10/2017</span><br><span class="green">ID Publicación: 43265</span></p></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Modo de Renta</th>
                                                            <td class="rentado_txt">Liquidado</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Beneficios</th>
                                                            <td>No incluye</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Boletos</th>
                                                            <td>6/10</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Precio por Boleto</th>
                                                            <td>$10,000.00<br><div class="inline nota">Precio al público: $12,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Utilidad Total</th>
                                                            <td><b>$100,000.00</b><br><div class="inline nota">Precio al público: $112,000.00</div></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Depósito de Garantía</th>
                                                            <td>$10,000.00<br>(Total)</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <td><a href="#" data-ix="abrir-problema" class="btn_small w-button">Reportar problema</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- TERMINA TABLA MOVIL -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="w-tab-menu">
                                <a data-w-tab="ejemplo" class="tab-propiedad w--current w-inline-block w-tab-link">
                                    <div>ejemplo</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div data-w-tab="Estado de Cuenta" class="edo-cta tab-pane-3 w-tab-pane">
                        <h1 class="titulo_dashboard">Estado de cuenta</h1>
                        <h2 class="h2dashboard">Consulta tu estado de cuenta general.</h2>
                        <div class="dash">
                            <!-- INICIA TABLA ESTADO DE CUENTA -->
                            <table class="escritorio" style="text-align:center;">
                                <colgroup><col><col><col><col><col><col></colgroup>
                                <thead>
                                    <th>Evento<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                    <th>Fecha de pago<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                    <th>Boletos Rentados</th>
                                    <th>Precio Promedio por Boleto<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                    <th>Utilidad Total<img src="{!!asset('web/images/flecha-12.png')!!}" data-ix="ordenar" class="ordenar"></th>
                                    <th>Estatus</th>
                                </thead>
                                <tbody>
                                    <!-- MCCARTNEY -->
                                    <tr>
                                        <td><p><b>Paul McCartney en el Estadio Azteca<br>(Palco Azteca 113)</b><br><span>10/10/2017</span></p></td>
                                        <td>10/10/17</td>
                                        <td class="rentado_txt">6/10 rentados</td>
                                        <td>$-</td>
                                        <td><b>$-</b></td>
                                        <td class="rentado_txt">Pago Efectuado</td>
                                    </tr>
                                    <!-- AME-CHIVAS-->
                                    <tr>
                                        <td><p><b>América vs. Chivas<br>(Palco Azteca 112)</b><br><span>11/10/2017</span></p></td>
                                        <td>3/10/17</td>
                                        <td>Reservado para ti</td>
                                        <td>$-</td>
                                        <td><b>$-</b></td>
                                        <td class="rentado_txt">Liquidado</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- TERMINA TABLA ESTADO DE CUENTA -->
                            <!-- INICIA TABLA MOVIL -->
                            <table class="movil" style="text-align:center;">
                                <colgroup><col><col></colgroup>
                                <tbody>
                                    <tr>
                                        <th>Evento</th>
                                        <td><p><b>Paul McCartney en el Estadio Azteca<br>(Palco Azteca 113)</b><br><span>10/10/2017</span></p></td>
                                    </tr>
                                    <tr>
                                        <th>Fecha de Renta</th>
                                        <td>10/10/2017</td>
                                    </tr>
                                    <tr>
                                        <th>Boletos Rentados</th>
                                        <td class="rentado_txt">6/10 rentados</td>
                                    </tr>
                                    <tr>
                                        <th>Precio Promedio por Boleto</th>
                                        <td>$-</td>
                                    </tr>
                                    <tr>
                                        <th>Utilidad Total</th>
                                        <td>$-</td>
                                    </tr>
                                    <tr>
                                        <th>Estatus</th>
                                        <td class="rentado_txt">Pago Efectuado</td>
                                    </tr>
                                    <!-- ROW VACIO PARA CREAR ESPACIO ENTRE EVENTOS -->
                                    <tr class="no_pad"><th>&nbsp;</th><td>&nbsp;</td></tr>
                                    <!-- TERMINA ROW VACIO -->
                                    <tr>
                                        <th>Evento</th>
                                        <td><p><b>América vs. Chivas<br>(Palco Azteca 112)</b></td>
                                    </tr>
                                    <tr>
                                        <th>Fecha de Renta</th>
                                        <td>11/10/2017</td>
                                    </tr>
                                    <tr>
                                        <th>Boletos Rentados</th>
                                        <td class="rentado_txt">Reservado para ti</td>
                                    </tr>
                                    <tr>
                                        <th>Precio Promedio por Boleto</th>
                                        <td>$-</td>
                                    </tr>
                                    <tr>
                                        <th>Utilidad Total</th>
                                        <td>$-</td>
                                    </tr>
                                    <tr>
                                        <th>Estatus</th>
                                        <td class="rentado_txt">Liquidado</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- TERMINA TABLA MOVIL -->
                        </div>
                    </div>
                    
                    <div data-w-tab="Mis favoritos" class="tab-pane-3 tabfavs w-tab-pane">
                      <h1 class="titulo_dashboard">Favoritos</h1>
                      <h2 class="h2dashboard">Visualiza las propiedades que has marcado como favoritas</h2>
                      <div class="dash ns">
                        <div class="favoritas propiedad_renta w-clearfix">
                          <div class="foto-propiedad"></div>
                          <div class="info_propiedad">
                            <h4 class="h4">Palco Estadio Azteca</h4>
                            <div class="bold txt_small">$500,000mxn anuales</div>
                            <div class="bold txt_small">Zona</div>
                            <div class="bold txt_small">10 asientos</div><a href="busqueda/resultados.php" class="boton ver-propiedad w-button">Ver eventos</a></div><a href="#" class="x x2">x</a></div>
                        <div class="favoritas propiedad_renta w-clearfix">
                          <div class="foto-propiedad"></div>
                          <div class="info_propiedad">
                            <h4 class="h4">Palco Estadio Azteca</h4>
                            <div class="bold txt_small">$500,000mxn anuales</div>
                            <div class="bold txt_small">Zona</div>
                            <div class="bold txt_small">10 asientos</div><a href="busqueda/resultados.php" class="boton ver-propiedad w-button">Ver eventos</a></div><a href="#" class="x x2">x</a></div>
                        <div class="favoritas propiedad_renta w-clearfix">
                          <div class="foto-propiedad"></div>
                          <div class="info_propiedad">
                            <h4 class="h4">Palco Estadio Azteca</h4>
                            <div class="bold txt_small">$500,000mxn anuales</div>
                            <div class="bold txt_small">Zona</div>
                            <div class="bold txt_small">10 asientos</div><a href="busqueda/resultados.php" class="boton ver-propiedad w-button">Ver eventos</a></div><a href="#" class="x x2">x</a></div>
                        <div class="favoritas propiedad_renta w-clearfix">
                          <div class="foto-propiedad"></div>
                          <div class="info_propiedad">
                            <h4 class="h4">Palco Estadio Azteca</h4>
                            <div class="bold txt_small">$500,000mxn anuales</div>
                            <div class="bold txt_small">Zona</div>
                            <div class="bold txt_small">10 asientos</div><a href="busqueda/resultados.php" class="boton ver-propiedad w-button">Ver eventos</a></div><a href="#" class="x x2">x</a></div>
                      </div>
                    </div>
                    <!-- TERMINA CONTENIDO -->
                </div>
                @include('web.console.includes.menu')
            </div>
        </div>
        @include('web.console.includes.scripts')
    </body>
</html>