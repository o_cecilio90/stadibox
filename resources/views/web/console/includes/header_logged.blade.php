<!-- INICIA HEADER -->
<div data-collapse="medium" data-animation="over-right" data-duration="400" class="navbar-home w-nav"><a href="{!!url('/')!!}" class="logo w-clearfix w-nav-brand"><img src="{!!asset('web/images/Logo_Stadibox-03.png')!!}" alt="Stadibox: Renta de palcos" class="logo_im"></a>
    <div class="menu-btn w-nav-button">
        <div class="w-icon-nav-menu"></div>
    </div>
    <div class="block-busqueda w-clearfix"><img src="{!!asset('web/images/search-1.png')!!}" width="40" height="40" class="buscador-im"><img src="{!!asset('web/images/search-1.png')!!}" width="40" height="40" data-ix="abrir-buscador" class="buscador-im lupamovil">
        <div class="form-busqueda w-form">
            <form id="wf-form-Buscador-navbar" name="wf-form-Buscador-navbar" data-name="Buscador navbar" class="form-2 w-clearfix"><input type="text" id="buscador-7" placeholder="Busca tu estadio, equipo o evento" maxlength="256" name="buscador-7" data-name="Buscador 7" class="search-tf w-input"></form>
            <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form</div>
            </div>
        </div>
    </div>
    <nav role="navigation" class="nav-menu-home w-nav-menu">
        <div data-delay="0" class="w-dropdown">
            <div class="ayuda navlink w-dropdown-toggle">
                <div>Ayuda</div>
            </div>
            <nav class="dropdown-list w-dropdown-list">
                <a href="info/acerca-de-nosotros.php" class="ddlink w-dropdown-link">Acerca de Nosotros</a>
                <a href="info/renta-anual.php" class="ddlink w-dropdown-link">Renta Anual</a>
                <a href="info/administracion.php" class="ddlink w-dropdown-link">Administramos tu<br>Propiedad</a>
                <a href="info/servicios.php" class="ddlink w-dropdown-link">Servicios</a>
                <a href="info/para-empresas.php" class="ddlink w-dropdown-link">Para Empresas</a>
                <a href="info/para-propietarios.php" class="ddlink w-dropdown-link">Para Propietarios</a>
                <a href="experiencias/vip.php" class="ddlink w-dropdown-link">Experiencias VIP</a>
                <a href="info/faq.php" class="ddlink w-dropdown-link">Preguntas Frecuentes</a>
                <a href="info/contacto.php" class="ddlink w-dropdown-link">Contacto</a>
            </nav>
        </div>
        <div class="form-divisa w-form">
            <form id="wf-form-Opciones-de-region" name="wf-form-Opciones-de-region" data-name="Opciones de region"><select id="Divisa-2" name="Divisa-2" data-name="Divisa 2" class="divisa select w-select"><option value="MXN">MXN</option><option value="USD">USD</option></select></form>
            <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
            </div>
        </div><a href="#" data-ix="abrir-modal-palco-o-platea" class="navlink navlinkbtn w-nav-link">Registrar mi Propiedad</a>
        <div data-delay="0" class="dd w-dropdown">
            <div class="ddtoggle-notificaciones w-dropdown-toggle"><img src="{!!asset('web/images/notificacion-02.png')!!}" alt="foto de perfil" class="user">
                <div class="nuevo">
                    <div>1</div>
                </div>
                <div class="flechita sinflecha w-icon-dropdown-toggle"></div>
            </div>
            <nav class="ddlist w-dropdown-list">
                <div class="div-notificaciones">
                    <a href="#" class="notificacion nueva w-inline-block">
                        <div class="hora txt_small">Hace 5 horas</div>
                        <div class="bold txt_small">¡Aprovecha para comprar boletos para Paul McCartney de último minuto! (max. 120 caracteres)</div>
                    </a>
                    <a href="#" class="notificacion w-inline-block">
                        <div class="hora txt_small">Ayer</div>
                        <div class="bold txt_small">Hemos abierto el Estadio BBVA en Monterrey, ¡busca tus eventos favoritos y obtén los mejores lugares! Ver</div>
                    </a>
                    <a href="#" class="notificacion w-inline-block">
                        <div class="hora txt_small">Lun 18 de enero</div>
                        <div class="bold txt_small">Ahora puedes revisar tu historial de compras en la sección de &quot;Mi Cuenta&quot;</div>
                    </a>
                </div>
            </nav>
        </div>
        <div data-delay="0" class="w-dropdown">
            <div class="dd w-dropdown-toggle"><img src="{!!asset('web/images/user-01.png')!!}" alt="foto de perfil" class="user">
                <div class="flechita w-icon-dropdown-toggle"></div>
            </div>
            <nav class="ddlist ddlist-2 w-dropdown-list"><a href="{!!url('/')!!}" class="ddlink w-dropdown-link">Inicio</a><a href="{!!route('web.console')!!}" class="ddlink w-dropdown-link">Mi Cuenta</a><a href="index.php" class="ddlink w-dropdown-link">Cerrar Sesión</a></nav>
        </div>
    </nav>
</div>