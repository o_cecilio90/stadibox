<!-- MENU LATERAL -->
<div class="menu-lateral w-tab-menu">
    <a data-w-tab="Mi Perfil" class="block-perfil w-inline-block w-tab-link">
        <div class="w-row">
            <div class="col1menu w-col w-col-4 w-col-stack">
                <div class="profile-pic"></div>
            </div>
            <div class="col2menu w-col w-col-8 w-col-stack">
                <div class="bold nombre txt_mini">Diego Kawas</div>
                <div class="editar-perfil">
                    <div class="bold txt_mini">Editar Perfil</div>
                </div>
            </div>
        </div>
    </a>
    <a data-w-tab="Mis Propiedades" class="tab_btn w--current w-inline-block w-tab-link">
        <div class="txtmenu">Mis Propiedades</div>
    </a>
    <a data-w-tab="Mi Tablero" class="mitablero tab_btn w-inline-block w-tab-link">
        <div class="txtmenu">Mi Tablero</div>
        <div class="nuevo">
            <div>1</div>
        </div>
    </a>
    <a data-w-tab="Mis Compras" class="miscompras tab_btn w-inline-block w-tab-link">
        <div class="txtmenu">Mis Rentas</div>
    </a>
    <a data-w-tab="Mis Ventas" class="tab_btn ventas w-inline-block w-tab-link">
        <div class="txtmenu">Mis Ventas</div>
    </a>
    <a data-w-tab="Estado de Cuenta" class="edocuenta tab_btn w-inline-block w-tab-link">
        <div class="txtmenu">Estado de Cuenta</div>
    </a>
    <a data-w-tab="Mis favoritos" class="favs tab_btn w-inline-block w-tab-link">
        <div class="txtmenu">Mis Favoritos</div>
    </a>
</div>