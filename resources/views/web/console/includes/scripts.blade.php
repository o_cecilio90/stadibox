<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
<script src="{!!asset('web/js/webflow.js')!!}" type="text/javascript"></script>
<!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
<script type="text/javascript">
	$("#propiedad").on("change", (e)=> {
		let id = $("#propiedad").val();
		let route = '{{ route("property.events", ":id") }}';
        route = route.replace(':id', id);
		$.getJSON(route, function(data) {
			$('#body_property').html('');
			$.each(data.forum, function(index, value) {
				$('#body_property').append(
					'<!-- INFORMACION GENERAL DEL EVENTO -->'+
					'<tr>'+
						'<td><p><b>'+value.event.name+'</b><br><span>'+value.event.date+'</span></p>'+
							'<a href="#" data-ix="switch" class="switch w-clearfix w-inline-block">'+
                                '<div class="circulo"></div>'+
                                '<div class="text-switch">No rentar</div>'+
                                '<div class="switchon w-clearfix">'+
                                  	'<div class="text_on">Rentar</div>'+
                                '</div>'+
                             '</a>'+
                        '</td>'+					
						'<!-- MODO DE RENTA -->'+
	                    '<td><div class="form-tabla w-form">'+
	                      '<form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Modo-de-renta" name="rent_mode" data-name="Modo de renta" required="" class="selector-tabla w-select"><option value="todo">Solo rentar todo el palco</option><option value="asiento">Permitir renta por asientos</option></select></form>'+
	                      '<div class="w-form-done">'+
	                        '<div>Thank you! Your submission has been received!</div>'+
	                      '</div>'+
	                      '<div class="w-form-fail">'+
	                        '<div>Oops! Something went wrong while submitting the form.</div>'+
	                      '</div>'+
	                    '</div></td>'+
	                    '<!-- BENEFICIOS ADICIONALES -->'+
                        '<td><div class="form-tabla w-form">'+
                         ' <form id="wf-form-evento" name="wf-form-evento" data-name="evento" class="tabla-form"><select id="Alimentos-y-bebidas" name="additional_benefits" data-name="Alimentos y bebidas" required="" class="selector-tabla w-select"><option value="no">No incluye</option><option value="si">Si incluye</option></select></form>'+
                          '<div class="w-form-done">'+
                            '<div>Thank you! Your submission has been received!</div>'+
                          '</div>'+
                          '<div class="w-form-fail">'+
                            '<div>Oops! Something went wrong while submitting the form.</div>'+
                          '</div>'+
                        '</div></td>'+
                        '<!-- NUMERO DE BOLETOS -->'+
                        '<td><div class="form-tabla w-form">'+
                          '<form id="wf-form-evento" name="ticket" data-name="evento" class="tabla-form"><select id="Boletos" name="Boletos" data-name="Boletos" required="" class="selector-tabla w-select">'+
	                          	'@for($i=0;$i<=10;$i++)'+
	                          		'<option value="{!!$i!!}">{!!$i!!}</option>'+
	                          	'@endfor'+
                        		'</select></form>'+
                          '<div class="w-form-done">'+
                            '<div>Thank you! Your submission has been received!</div>'+
                          '</div>'+
                          '<div class="w-form-fail">'+
                            '<div>Oops! Something went wrong while submitting the form.</div>'+
                          '</div>'+
                        '</div></td>'+
                        '<!-- PRECIO POR BOLETO -->'+
                        '<td><div class="form-tabla w-form">'+
                          '<form id="wf-form-Precio-por-boleto" name="wf-form-Precio-por-boleto" data-name="Precio por boleto" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="price_ticket" data-name="Precio por boleto" placeholder="$" id="Precio-por-boleto" required="" value="'+value.price_ticket+'"><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>'+
                            '</form>'+
                          '<div class="w-form-done">'+
                            '<div>Thank you! Your submission has been received!</div>'+
                         ' </div>'+
                          '<div class="w-form-fail">'+
                            '<div>Oops! Something went wrong while submitting the form.</div>'+
                          '</div>'+
                        '</div></td>'+
                        '<!-- UTILIDAD TOTAL -->'+
                        '<td><div class="form-tabla w-form">'+
                          '<form id="wf-form-Utilidad-total" name="wf-form-Utilidad-total" data-name="Utilidad total" class="tabla-form w-clearfix"><input type="text" class="input inputverde w-input" maxlength="256" name="total_utility" data-name="Utilidad Total" placeholder="$" id="Utilidad-Total" required="" value="'+value.total_utility+'"><a href="#" class="link-semaforo w-inline-block"><img src="{!!asset('web/images/semaforo-01.png')!!}" alt="Administrado por Stadibox" data-ix="abrir-verde" class="ic_small precio-bueno"></a>'+
                            '</form>'+
                          '<div class="w-form-done">'+
                            '<div>Thank you! Your submission has been received!</div>'+
                          '</div>'+
                            '<div class="w-form-fail">'+
                                '<div>Oops! Something went wrong while submitting the form.</div>'+
                            '</div>'+
                        '</div></td>'+
                        '<!-- DEPOSITO DE GARANTIA -->'+
                        '<td><div class="form-tabla w-form">'+
                          '<form id="wf-form-Deposito-de-garantia" name="wf-form-Deposito-de-garantia" data-name="Deposito de garantia" class="tabla-form w-clearfix"><input type="text" class="deposito input w-input" maxlength="256" name="down_payment" data-name="Deposito de garantia" placeholder="$" id="Deposito-de-garantia" required="" value="'+value.down_payment+'">'+
                              '<div class="nota">Por asiento</div>'+
                            '</form>'+
                          '<div class="w-form-done">'+
                            '<div>Thank you! Your submission has been received!</div>'+
                          '</div>'+
                          '<div class="w-form-fail">'+
                            '<div>Oops! Something went wrong while submitting the form.</div>'+
                          '</div>'+
                        '</div></td>'+
                        '<!-- ACCIONES -->'+
                        '<td><a href="#" class="btn_small w-button">Guardar</a></td>'+
                    '</tr>'
				);
			});
		});
	});
</script>