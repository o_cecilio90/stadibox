@extends('web.layouts.web')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('web/slick-1.8.0/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('web/slick-1.8.0/slick/slick-theme.css')}}"/>
@endsection

@section('content')

    <div class="bg-evento s-background">
        <div class="container-evento">
            <div class="row-evento w-row">
                <div class="col1 w-col w-col-4 w-col-medium-6 w-col-small-6">
                    <div class="image-evento" style="background-color: #e0e0e0;
                    background-image: url({{asset($event->image)}});
                    background-position: 50% 50%;
                    background-size: cover;"></div>
                </div>
                <div class="col2-evento w-col w-col-8 w-col-medium-6 w-col-small-6">
                    <h1 class="h2-title hevento">{{$event->name}}</h1>
                    <div class="full text_med white">{{$event->forum->forum_name}}</div>
                    <div class="full text_med white">Fecha: {{$event->date}}</div>
                    <div class="full text_med white">Hora: {{$event->time}}</div>
                </div>
            </div>
            <div class="listado-de-boletos">
                <div class="w-row">
                    <!-- ESTADIO INTERACTIVO -->
                    <div class="col-mapa w-col w-col-4 w-col-small-small-stack w-col-stack">
                        <h2 class="h2 h2foro">{{$event->forum->forum_name}}</h2>
                        <br>
                        <figure>
                            <img src="{{asset($event->forum->map)}}" id="my_image" class="trans" >
                        </figure>
                        <br><br>
                        {{-- <div id="botones">
                            <!-- ALTO -->
                            <p class="no_activo" onmouseover="alto()" onmouseout="base()" id="alto"><span class="rosa"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Alto</p>
                            <!-- ESPECIAL ALTO LATERAL -->
                            <p class="no_activo" onmouseover="alto_lateral()" onmouseout="base()" id="alto_lateral"><span class="verde"><i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                                </span>&nbsp;Especial Alto Lateral</p>
                            <!-- ESPECIAL BAJO -->
                            <p class="no_activo" onmouseover="bajo()" onmouseout="base()" id="bajo"><span class="azul_c"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Especial Bajo</p>
                            <!-- PLATEA ALTA PLUS -->
                            <p class="no_activo" onmouseover="platea_alta_plus()" onmouseout="base()" id="platea_alta_plus"><span class="verde_c"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Platea Alta Plus</p>
                            <!-- PALCOS PLUS -->
                            <p class="no_activo" onmouseover="palcos_plus()" onmouseout="base()" id="palcos_plus"><span class="morado"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Palcos Plus</p>
                            <!-- PLATEA BAJA -->
                            <p class="no_activo" onmouseover="platea_baja()" onmouseout="base()" id="platea_baja"><span class="naranja"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Platea Baja</p>
                            <!-- PALCOS CLUB -->
                            <p class="no_activo" onmouseover="palcos_club()" onmouseout="base()" id="palcos_club"><span class="rojo"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Palcos Club</p>
                            <!-- ASIENTOS CLUB -->
                            <p class="no_activo" onmouseover="asientos_club()" onmouseout="base()" id="asientos_club"><span class="azul"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Asientos Club</p>
                            <!-- PREFERENTE PLUS -->
                            <p class="no_activo" onmouseover="preferente_plus()" onmouseout="base()" id="preferente_plus"><span class="rosa_c"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Preferente Plus</p>
                            <!-- PLATEA ALTA CABECERA -->
                            <p class="no_activo" onmouseover="platea_alta_cabecera()" onmouseout="base()" id="platea_alta_cabecera"><span class="morado_o"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Platea Alta Cabecera</p>
                            <!-- PREFERENTE -->
                            <p class="no_activo" onmouseover="preferente()" onmouseout="base()" id="preferente"><span class="verde_o"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Preferente</p>
                            <!-- PALCO AZTECA -->
                            <p class="no_activo" onmouseover="palco_azteca()" onmouseout="base()" id="palco_azteca"><span class="amarillo"><i class="fa fa-eye fa-fw" aria-hidden="true"></i></span>&nbsp;Palco Azteca</p>
                        </div> --}}
                    </div>
                    <!-- TABLA DE BOLETOS -->
                    <div class="col2-boletos w-col w-col-8 w-col-small-small-stack w-col-stack">
                        <div class="filtros-boletos">
                            <div class="w-form">
                                <form id="wf-form-Filtros-evento" name="wf-form-Filtros-evento" data-name="Filtros evento" class="w-clearfix"><select id="Precio" name="Precio" data-name="Precio" class="selector-small w-select"><option value="">Precio</option><option value="First">Menor a mayor</option><option value="Second">Mayor a menor</option></select><select id="No.-pases" name="No.-pases" data-name="No. pases" class="selector-small w-select"><option value="">No. pases</option><option value="First">1</option><option value="Second">2</option><option value="Third">3</option><option value="Another Choice">4</option><option value="Another Choice">5</option><option value="Another Choice">6</option><option value="Another Choice">7</option><option value="Another Choice">8</option><option value="Another Choice">9</option><option value="Another Choice">10</option></select><select id="Tipo-de-pases" name="Tipo-de-pases" data-name="Tipo de pases" class="selector-small w-select"><option value="">Tipo de pases</option><option value="First">Especial Bajo A</option><option value="Second">Platea Plus</option><option value="Third">Preferente Plus</option><option value="Another Choice">Especial bajo B</option><option value="Another Choice">Palcos Plus</option><option value="Another Choice">Platea Alta</option><option value="Another Choice">General</option><option value="Another Choice">Platea Baja</option><option value="Another Choice">Hospitality</option><option value="Another Choice">Preferente</option></select><select id="field" name="field" class="selector-small w-select"><option value="">Modo de renta</option><option value="Second">Por asiento</option><option value="First">Todos juntos</option><option value="Third">Palco privado</option></select><select id="field-2" name="field-2" data-name="Field 2" class="selector-small w-select"><option value="">Clase</option><option value="First">Palcos</option><option value="Second">Plateas</option><option value="Third">Abonos</option></select></form>
                                <div class="w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div class="w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row-cat w-row">
                        @foreach($property as $key => $value)
                            <div class="tarjeta_palcos">
                                <p class="fondo_azul fverde">
                                @if(isset($value['box_id']))
                                    Palco
                                @elseif(isset($value['stall_id']))
                                    Platea
                                @endif
                                </p>
                                <div class="tarjeta1">
                                @if($value['box_id'] != null)
                                    @if($value->box->photos['court'])
                                        <div><img src="{{asset($value->box->photos['court'])}}"></div>
                                    @endif
                                    @if($value->box->photos['box'])
                                        <div><img src="{{asset($value->box->photos['box'])}}"></div>
                                    @endif
                                    @if($value->box->photos['seat'])
                                        <div><img src="{{asset($value->box->photos['seat'])}}"></div>
                                    @endif
                                    @if($value->box->photos['bath'])
                                        <div><img src="{{asset($value->box->photos['bath'])}}"></div>
                                    @endif
                                @elseif($value['stall_id'] != null)
                                    <div><img src="{{asset($value->stall['photo_stall'])}}"></div>
                                @endif
                                </div>
                                <p class="datos_palco"><b>
                                    @if(isset($value['box_id']))
                                        Palco
                                    @elseif(isset($value['stall_id']))
                                        Platea
                                    @endif</b> - de 0 a {!!$value['ticket']!!} pases
                                    @if($value['ticket'] == 'todo')
                                    <br><span>{!!$value['total_utility']!!} </span>Todos juntos
                                    @elseif($value['ticket'] == 'asiento')
                                    <br><span>{!!$value['price_ticket']!!} <small>(por asiento)</small></span>Compartido
                                    @endif
                                </p>
                                @if(isset($value->box->amenities))
                                <p class="datos_palco">Incluye: 
                                    @foreach($value->box->amenities as $amenity)
                                    <img src="{!!asset($amenity['icon'])!!}">
                                    @endforeach
                                </p>
                                @endif
                                <a href="#" class="bot-compr">Rentar</a>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{asset('web/js/estadio.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/slick-1.8.0/slick/slick.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('.your-class').slick({
                    dots: true,
                    dotsClass: 'slick-dots',
                    swipe: true
                });
                
                $('.tarjeta1').slick({
                    dots: true,
                    dotsClass: 'slick-dots',
                    swipe: true
                });
            });
    </script>
@endsection