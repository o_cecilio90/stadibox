@extends('web.layouts.main')

@section('content')
<!-- BANNER -->
<div id="hero" class="hero-inicio" style="
  height: auto;
  padding-bottom: 60px;
  background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, .37), rgba(0, 0, 0, .37)), url('{!!$home->image!!}');
  background-image: linear-gradient(180deg, rgba(0, 0, 0, .37), rgba(0, 0, 0, .37)), url('{!!$home->image!!}');
  background-position: 0px 0px, 50% 50%;
  background-size: auto, cover;
  background-repeat: repeat, no-repeat;
  color: #fff;">
    <div class="container-hero w-container">
        <!-- TABS CATEGORIAS -->
        <div class="div-categorias">
            @foreach($home->tab as $key => $value)
            @if($value != '')
                <a href="busqueda/resultados.html" class="btn_categora w-button">{!!$value!!}</a>
            @endif
            @endforeach
        </div>

        <h1 class="h1-slogan">Transformando la manera de rentar palcos</h1>
        <h2 class="h2-home">¡Encuentra palcos y plateas para los mejores eventos!</h2>
        <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">
            <!-- TABS ASISTIR-ENLISTAR -->
            <div class="tabsmenu w-clearfix w-tab-menu">
                <a data-w-tab="quiero asistir" class="tablink w--current w-inline-block w-tab-link">
                    <div class="cen">Quiero asistir a un evento</div>
                </a>
                <a data-w-tab="quiero enlistar" class="tablink w-inline-block w-tab-link">
                    <div class="cen">Quiero enlistar mi propiedad</div>
                </a>
            </div>
            <div class="tabs-content w-tab-content">
                <!-- CONTENIDO ASISTIR -->
                <div data-w-tab="quiero asistir" class="tabpane w--tab-active w-tab-pane">
                    <div class="text_med white">¡Busca el evento al que quieres asistir y consigue un palco o platea de forma fácil y rápida!</div>
                    <div class="div-buscador-2"><a href="busqueda/resultados.html" class="buscador-boton w-inline-block"><img src="{{asset('web/images/buscador_blanco.png')}}" width="253" height="40" class="buscador-image"></a>
                        <div class="buscar-form buscar-form2 w-form">
                            <form id="wf-form-Buscador-principal" name="wf-form-Buscador-principal" data-name="Buscador principal" class="form form2"><input type="text" id="buscador-2" placeholder="Busca tu estadio, equipo o evento" maxlength="256" name="buscador-2" data-name="Buscador 2" class="buscador buscador2 w-input">
                            </form>
                            <div class="w-form-done">
                                <div>Thank you! Your submission has been received!</div>
                            </div>
                            <div class="w-form-fail">
                                <div>Oops! Something went wrong while submitting the form</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CONTENIDO ENLISTAR -->
                <div data-w-tab="quiero enlistar" class="tabpane w-tab-pane">
                    <div class="text_med white">¿Sabías que en Stadibox podrías ganar más de 1 millón de pesos al año rentando tu palco?<br>¡No esperes más, registra tu propiedad y empieza a generar ingresos!</div>
                    @guest
                    <div class="div_upperspace"><a href="#" data-ix="abrir-modal-login" class="btn w-button">Quiero registrar una propiedad</a></div>
                    <div class="div_upperspace"><a href="#" data-ix="abrir-modal-login" class="link_green">Ya registré mi propiedad, ir a mi cuenta</a></div>
                    @else
                    <div class="div_upperspace"><a href="#" data-ix="abrir-modal-palco-o-platea" class="btn w-button">Quiero registrar una propiedad</a></div>
                    <div class="div_upperspace"><a href="{!!route('web.console')!!}" class="link_green">Ya registré mi propiedad, ir a mi cuenta</a></div>
                    @endguest
                </div>
            </div>
        </div>
        @guest
            <div class="div-80"><a data-ix="abrir-modal-login" class="boton100 w-button">Quiero rentar una propiedad por un año</a></div>
        @else
            <div class="div-80"><a href="info/renta-anual.html" class="boton100 w-button">Quiero rentar una propiedad por un año</a></div>
        @endguest
    </div>
</div>

<!-- EVENTOS -->
    <div>
        <div class="seccion-eventos">
            {{-- <div class="container-eventos w-container">
                <div class="div-titulo">
                    <h2 class="h2-title">Los mejores eventos en la Ciudad de México</h2><a href="#" data-ix="abrir-modal-ubicacion" class="link_blue">Cambiar mi ubicación</a></div>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento foto1">
                        <h4 class="h4">Pats vs Raiders</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="crossed price"><span class="crossed">$15,000 </span><br xmlns="http://www.w3.org/1999/xhtml"></span><span class="price red">$14,000</span> mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento foto2">
                        <h4 class="h4">Paul McCartney</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento foto3">
                        <h4 class="h4">América vs. Chivas</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>`--}}
        </div> 
        <div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div data-delay="4000" data-animation="over" data-autoplay="1" data-easing="ease-in-out-sine" data-duration="500" data-infinite="1" class="slider-promocional w-slider">
                    <div class="mask w-slider-mask">
                        @foreach($banner as $key => $value)
                        <div class="slide w-slide" style="background-image: url('{{$value->image}}');">
                            <div class="div-flexed">
                                <div class="text_big txt_promo">{!!$value->main_text!!}</div><a href="busqueda/resultados.html" class="btn btn_promo w-button">Comprar</a>
                                <div class="nota nota-promo">{!!$value->optional_note!!}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="arrow-small w-slider-arrow-left">
                        <div class="w-icon-slider-left"></div>
                    </div>
                    <div class="arrow-small w-slider-arrow-right">
                        <div class="w-icon-slider-right"></div>
                    </div>
                    <div class="slide-nav w-round w-slider-nav"></div>
                </div>
            </div>
        </div>
        @if($home->vip == 1)
        <div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div>
                    <h2 class="h2-title">Experiencias VIP</h2>
                </div>
                @foreach($experience as $key => $value)
                <a href="experiencias/publicacion.html" class="div-evento div-experiencia w-inline-block">
                    <div class="foto-alta foto-evento" style="height: 160px;
  background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, .3), rgba(0, 0, 0, .3)), url({!!asset($value->experience_image)!!});
  background-image: linear-gradient(180deg, rgba(0, 0, 0, .3), rgba(0, 0, 0, .3)), url({!!asset($value->experience_image)!!});
  background-position: 0px 0px, 0px 0px;
  background-size: auto, cover;">
                        <h4 class="h4">{!!$value->name!!}</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">${!!$value->price['price_simple']!!}<br xmlns="http://www.w3.org/1999/xhtml"></span>{!!$value->price['currency']!!} c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">{!!$value->forum->forum_name!!}</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">{!!$value->city->city!!}</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">{!!"04 may 2018 - 10 may 2018"!!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
                <a href="experiencias/vip.html" class="btn experiencas w-button">Ver todas las experiencias VIP</a></div>
        </div>
        @endif
        <div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div>
                    <h2 class="h2-title">Boletos de último minuto</h2>
                </div>
                @forelse($events as $event)
                    <a href="{{route('event.web',$event->id)}}" class="div-evento w-inline-block">
                        <div class="foto-evento" style="
                            height: auto;
                            padding-top: 70px;
                            background-image: -webkit-linear-gradient(270deg, rgba(0, 0, 0, .37), rgba(0, 0, 0, .37)), url('{!!$event->image!!}');
                            background-image: linear-gradient(180deg, rgba(0, 0, 0, .37), rgba(0, 0, 0, .37)), url('{!!$event->image!!}');
                            background-position: 0px 0px, 50% 50%;
                            background-size: auto, cover;
                            background-repeat: repeat, no-repeat;
                            color: #fff;">
                            <h4 class="h4">{{$event->name}}</h4>
                        </div>
                        <div class="div-info-evento">
                            <div class="row-info-evento w-row">
                                <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                    <div class="div-precio">
                                        <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                    </div>
                                </div>
                                <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                    <div class="semibold txt_small">{{$event->forum->forum_name}}</div>
                                    <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                        <div class="txt_mini">{{$event->city->city}}</div>
                                    </div>
                                    <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                        <div class="txt_mini">{{\Carbon\Carbon::parse($event->date)->format('D d M')}} {{$event->time}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                    <p>No hay eventos</p>
                @endforelse
            </div>
        </div>
        {{--<div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div>
                    <h2 class="h2-title">Recomendado para ti</h2>
                </div>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="seccion-eventos">
            <div class="container-eventos w-container">
                <div>
                    <h2 class="h2-title">Los más vendidos</h2>
                </div>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="busqueda/evento.html" class="div-evento w-inline-block">
                    <div class="foto-evento">
                        <h4 class="h4">Final Copa MX</h4>
                    </div>
                    <div class="div-info-evento">
                        <div class="row-info-evento w-row">
                            <div class="col0 w-col w-col-4 w-col-small-4 w-col-tiny-4">
                                <div class="div-precio">
                                    <div class="text-precio">Desde<br><span class="price">$15,000<br xmlns="http://www.w3.org/1999/xhtml"></span>mxn c/u</div>
                                </div>
                            </div>
                            <div class="col-info-evento w-col w-col-8 w-col-small-8 w-col-tiny-8">
                                <div class="semibold txt_small">Estadio Azteca</div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-01.png')}}" class="ic_small">
                                    <div class="txt_mini">CDMX</div>
                                </div>
                                <div class="div-info"><img src="{{asset('web/images/fairseats-web-02.png')}}" class="ic_small">
                                    <div class="txt_mini">Sab 28 Abr 18:00</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>--}}       
    </div>

    <div>
        <div class="seccion-como-funciona">
            <div>
                <div id="como-funciona" class="w-container">
                    <h3 class="cen h2-title">{{__('message.question_work')}}</h3>
                    <div class="w-row">
                        <div class="w-col w-col-4">
                            <div class="div-infografia"><img src="{{asset('web/images/Pasos_Home-01.png')}}" alt="Paso 1: Encuentra el evento" class="im-infografia">
                                <div class="text_med">{{__('message.step_1')}}</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4"><a href="#" class="div-infografia w-inline-block"></a>
                            <div class="div-infografia"><img src="{{asset('web/images/Pasos_Home-02.png')}}" alt="Paso 2: Aparta tus lugares" class="im-infografia im-paso-2">
                                <div class="text_med">{{__('message.step_2')}}</div>
                            </div>
                        </div>
                        <div class="w-col w-col-4"><a href="#" class="div-infografia w-inline-block"></a>
                            <div class="div-infografia"><img src="{{asset('web/images/Pasos_Home-03.png')}}" alt="Paso 3: Recibe tus boletos" class="im-infografia im-paso-3">
                                <div class="text_med">{{__('message.step_3')}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('web.includes.newsletter')

@endsection