<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('web.includes.head')
    <body>
        @include('web.includes.header')
        @include('web.includes.banner')
        
        <div class="modals-home">
            <!-- MODALES DE REGISTRO -->
            <div class="gpo-registro">
                @include('web.modals.pre_signup')
                @include('web.modals.login')
                @include('web.modals.palco_platea')
            </div>
        </div>

        @include('web.includes.footer')

        @include('web.includes.scripts')
    </body>
</html>
