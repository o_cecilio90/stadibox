@extends('console.layouts.main')

@section('title', 'Experiencias')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Propiedades
                <small>Stadibox</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Propiedades</li>
            </ol>
        </section>

        <!-- Main content -->            
        <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
            <!-- LEFT COLUMN -->
            <div class="col-md-10">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
                    <div class="tab-content no-padding">
                        <!-- TODAS -->
                        <div class="table tab-pane active" id="todas" style="position: relative;">
                            <div class="row">
                                <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Todas las Propiedades</h3>
                                        
                                            <div class="box-tools">
                                                <div class="input-group input-group-sm" style="width:250px;">
                                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="ID, No. de Propiedad, Titular, Foro...">

                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-hover" style="text-align: center;">
                                                <tr>
                                                    <th style="text-align: center">ID</th>
                                                    <th style="text-align: center">Lugar</th>
                                                    <th style="text-align: center">Foro</th>
                                                    <th style="text-align: center">Propiedad</th>
                                                    <th style="text-align: center">Titular</th>
                                                    <th style="text-align: center">Capacidad</th>
                                                    <th style="text-align: center">Modalidad</th>
                                                    <th style="text-align: center">Estatus</th>
                                                </tr>
                                                <tr onclick="document.location = 'propiedad.php'">
                                                    <td>0034</td>
                                                    <td><b>México</b><br>CDMX</td>
                                                    <td><b>Estadio Azteca</b></td>
                                                    <td><b>P0052</b></td>
                                                    <td><b>Nombre y Apellido</b><br>correo@correo.com</td>
                                                    <td><b>10</b></td>
                                                    <td><b>Por Evento</b></td>
                                                    <td class="nueva"><b>Nueva</b></td>
                                                </tr>
                                                <tr onclick="document.location = 'propiedad.php'">
                                                    <td>0033</td>
                                                    <td><b>México</b><br>CDMX</td>
                                                    <td><b>Estadio Azteca</b></td>
                                                    <td><b>P0321</b></td>
                                                    <td><b>Nombre y Apellido</b><br>correo@correo.com</td>
                                                    <td><b>15</b></td>
                                                    <td><b>En Busca de<br>Renta Anual</b></td>
                                                    <td class="activo"><b>Aceptada</b></td>
                                                </tr>
                                                <tr onclick="document.location = 'propiedad.php'">
                                                    <td>0032</td>
                                                    <td><b>México</b><br>CDMX</td>
                                                    <td><b>Estadio Azteca</b></td>
                                                    <td><b>P0189</b></td>
                                                    <td><b>Nombre y Apellido</b><br>correo@correo.com</td>
                                                    <td><b>20</b></td>
                                                    <td><b>Administrada</b></td>
                                                    <td class="activo"><b>Aceptada</b></td>
                                                </tr>
                                                <tr onclick="document.location = 'propiedad.php'">
                                                    <td>0031</td>
                                                    <td><b>México</b><br>CDMX</td>
                                                    <td><b>Estadio Azteca</b></td>
                                                    <td><b>P0765</b></td>
                                                    <td><b>Nombre y Apellido</b><br>correo@correo.com</td>
                                                    <td><b>10</b></td>
                                                    <td><b>En Renta Anual</b></td>
                                                    <td class="bloqueado"><b>Bloqueada</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div>
                        <!-- FINALIZA TODAS -->
                    </div>
                </div>
            </div>
            <!-- END OF LEFT COLUMN -->
            <!-- RIGHT COLUMN -->
            <div class="col-md-2" style="text-align: center;">
                <div class="box filtros" style="width: 110%;padding: 1% 0%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filtrar Resultados</h3>
                    </div>
                    <!-- /.box-header -->
                    <button type="button" class="btn btn-block btn-success" style="width: 90%;margin: 0% auto;">Aplicar</button>
                    <a href="#" style="color: #77c510;">Limpiar Filtros</a>
                    <!-- checkbox -->
                    <div class="form-group" style="text-align: left;">
                        <h4 class="hcuatro">Ubicación:</h4>
                        <!-- select -->
                        <select class="form-control" style="width: 90%;margin: 0% auto;">
                            <option>Todos los Países</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select><br>
                        <select class="form-control" style="width: 90%;margin: 0% auto;">
                            <option>Todas las Ciudades</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select><br><br>
                        <h4 class="hcuatro foro_h4" style="margin-top: -9%;">Foro:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Estadio Azteca<br>
                                <input type="checkbox">
                                Estadio Cuauhtémoc<br>
                                <input type="checkbox">
                                Arena CDMX<br>
                                <input type="checkbox">
                                Estadio BBVA<br>
                                <input type="checkbox">
                                Estadio Lobos BUAP<br>
                            </label>
                        </div>
                        <h4 class="hcuatro">Estatus:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Nueva<br>
                                <input type="checkbox">
                                Aceptada<br>
                                <input type="checkbox">
                                Bloqueada<br>
                            </label>
                        </div>
                        <h4 class="hcuatro">Modalidad:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Por Evento<br>
                                <input type="checkbox">
                                En Busca de Renta Anual<br>
                                <input type="checkbox">
                                En Renta Anual<br>
                                <input type="checkbox">
                                Administrada<br>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF RIGHT COLUMN -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection