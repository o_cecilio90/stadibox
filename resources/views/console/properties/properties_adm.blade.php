@extends('console.layouts.main')

@section('title', 'Experiencias')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Propiedades
                <small>Stadibox</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Propiedades</li>
            </ol>
        </section>

        <!-- Main content -->            
        <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
            <!-- LEFT COLUMN -->
            <div class="col-md-12">
                <div class="table">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Propiedades Administradas por Stadibox</h3>
                                    
                                    <div class="box-tools tools_propiedades_admon" style="width: 100%;right: -487px !important;">
                                        <div class="form-group form-group-sm">
                                            <select class="form-control" style="width: 20%; float: left;">
                                                <option>Foro 1</option>
                                                <option>Foro 2</option>
                                                <option>Foro 3</option>
                                                <option>Foro 4</option>
                                                <option>Foro 5</option>
                                            </select>
                                            <select class="form-control" style="width: 20%;float: left;margin-left: 1%;">
                                                <option>No. de Propiedad 1</option>
                                                <option>No. de Propiedad 2</option>
                                                <option>No. de Propiedad 3</option>
                                                <option>No. de Propiedad 4</option>
                                                <option>No. de Propiedad 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" style="text-align: center;">
                                        <tr>
                                            <th style="text-align: center">ID</th>
                                            <th style="text-align: center">Evento</th>
                                            <th style="text-align: center">Modo de Renta</th>
                                            <th style="text-align: center;">Beneficios Adicionales</th>
                                            <th style="text-align: center">Número de Boletos</th>
                                            <th style="text-align: center">Precio por Boleto</th>
                                            <th style="text-align: center">Utilidad Total</th>
                                            <th style="text-align: center">Depósito de Garantía</th>
                                            <th style="text-align: center">Acciones</th>
                                        </tr>
                                        <tr>
                                            <td>183</td>
                                            <td>América vs. Pachuca</td>
                                            <td>32</td>
                                            <td>35</td>
                                            <td>12</td>
                                            <td>15</td>
                                            <td>15</td>
                                            <td>16</td>
                                            <td>ACCIÓN</td>
                                        </tr>
                                        <tr>
                                            <td>352</td>
                                            <td>Shakira</td>
                                            <td>32</td>
                                            <td>35</td>
                                            <td>12</td>
                                            <td>15</td>
                                            <td>15</td>
                                            <td>16</td>
                                            <td>ACCIÓN</td>
                                        </tr>
                                        <!-- ROW PARA DESHABILITAR -->
                                        <tr>
                                            <td><button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default" style="margin-top: 4%;width: 100%;">Deshabilitar Administración</button></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <!-- MODAL -->
                                    <div class="modal fade" id="modal-default">
                                        <div class="modal-dialog">
                                            <div class="modal-content" style="width: 63%;margin: 0% auto;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Deshabilitar Administración</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p style="text-align: center;font-size: 1.1em;"><b>¿Estás seguro que quieres deshabilitar la administración de esta propiedad?</b></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;">Cancelar</button>
                                                    <button type="button" class="btn btn-danger" style="width: 48%;">Deshabilitar</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- FINALIZA ADMINISTRADAS -->
                    </div>
                </div>
            </div>
            <!-- END OF LEFT COLUMN -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection