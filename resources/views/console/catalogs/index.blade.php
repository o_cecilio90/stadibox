@extends('console.layouts.main')

@section('title', 'Reportes')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Catálogos</h1>
        <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Catálogos</li>
        </ol>
        <!-- /.content -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
                <li><a href="#servicios" data-toggle="tab">Servicios</a></li>
                <li><a href="#amenidades" data-toggle="tab">Amenidades</a></li>
                <li><a href="#foros" data-toggle="tab">Foros</a></li>
                <li class="active"><a href="#ciudades" data-toggle="tab">Ciudades</a></li>
                <li class="pull-left header"><i class="fa fa-book"></i> Catálogos</li>
            </ul>
            <div class="tab-content no-padding">
                <!-- CONTENIDO DE LAS TABS -->
                <!-- CIUDADES -->
                <div class="table tab-pane active" id="ciudades" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Catálogo de Ciudades</h3>
                                    
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="width: 150px;">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-country-create" style="width: 100%;">
                                                Agregar Ciudad
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id="table_city" style="text-align: center;">
                                        <thead>
                                            <tr>   
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">País</th>
                                                <th style="text-align: center">Ciudad</th>
                                                <th style="text-align: center;">Presencia Física</th>
                                                <th style="text-align: center">Foros</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>                                        
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            @include('console.catalogs.modals.country')
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA CIUDADES -->
                <!-- FOROS -->
                <div class="table tab-pane" id="foros" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Catálogo de Foros</h3>
                                    
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="width: 150px;">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default-forum" style="width: 100%;">
                                                Agregar Foro
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table id="table_forum" class="table table-hover" style="text-align: center;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center; width: 12%;">Mapa</th>
                                                <th style="text-align: center;">Foro</th>
                                                <th style="text-align: center;">País</th>
                                                <th style="text-align: center">Ciudad</th>
                                                <th style="text-align: center">Zonas</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            @include('console.catalogs.modals.forum')
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA FOROS -->
                <!-- AMENIDADES -->
                <div class="table tab-pane" id="amenidades" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Catálogo de Amenidades</h3>
                                    
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="width: 150px;">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default-amenity" style="width: 100%;">
                                                Agregar Amenidad
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table  id="table_amenity" class="table table-hover" style="text-align: center;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">ID</th>
                                                <th style="text-align: center; width: 10%;">Ícono</th>
                                                <th style="text-align: center">Amenidad</th>
                                                <th style="text-align: center;">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            @include('console.catalogs.modals.amenity')
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA AMENIDADES -->
                <!-- SERVICIOS -->
                <div class="table tab-pane" id="servicios" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Catálogo de Servicios</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table id="table_services" class="table table-hover" style="text-align: center;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Foro</th>
                                                <th style="text-align: center">País</th>
                                                <th style="text-align: center;">Ciudad</th>
                                                <th style="text-align: center"><i class="fa fa-plus"></i> Adicionales</th>
                                                <th style="text-align: center"><i class="fa fa-lemon-o"></i> Alimentos y Bebidas</th>
                                                <th style="text-align: center"><i class="fa fa-cutlery"></i> Mesero</th>
                                                <th style="text-align: center"><i class="fa fa-motorcycle"></i> Transporte</th>
                                                <th style="text-align: center">Envíos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>01</td>
                                                <td>Estadio Azteca</td>
                                                <td>México</td>
                                                <td>CDMX</td>
                                                <td style="width: 13%;"><button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-adicionales">Editar</button></td>
                                                <td style="width: 13%;"><button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-snacks">Editar</button></td>
                                                <td style="width: 13%;"><button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-mesero">Editar</button></td>
                                                <td style="width: 13%;"><button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-transporte">Editar</button></td>
                                                <td style="width: 13%;"><button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-envios">Editar</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            @include('console.catalogs.modals.services')
                        </div>
                    </div>
                </div>
                <!-- TERMINA SERVICIOS -->
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
    </section>
    <!-- wrapper Main Content -->
</div>
@endsection

@section('js')
<!-- Page script -->
<script>
    $(function () {
        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()
    })
</script>

<!--SELECTS-->
<script type="text/javascript">

    // Image snacks preload 

    function readImageSnack(input){
        if (input.files && input.files[0]) {
            var reader2 = new FileReader();

            reader2.onload = function (e) {
                $('#img-snack').attr('src', e.target.result);
            }

            reader2.readAsDataURL(input.files[0]);
        }
    }

    $("#input_snack").change(function(){
        readImageSnack(this);
    });


    // FORUM
    let country;
    function selectCountry() {
        $.ajax({
            type: "GET",
            url: '{{ route('country.index') }}', 
            dataType: "json",
            success: function(data){
                country = data;
                $('#country_country option').remove();
                $('#country_country').append('<option>País</option>');
                $('#country_country_edit option').remove();
                $('#country_country_edit').append('<option>País</option>');
                $.each(data,function(key, registro) {
                    $('#country_country').append('<option value='+registro.id+'>'+registro.country+'</option>');
                    $('#country_country_edit').append('<option value='+registro.id+'>'+registro.country+'</option>');
                });        
            },
            error: function(data) {
                alert('error');
            }
        });
    };
    $("#country_country").change(function() {
        // get the id of the option which has the country code.
       let countryCode = $("#country_country")
          .children(":selected").attr("value");
        $.each(country,function(key, registro) {
            if (registro.id == countryCode) {                
                $('#country_city option').remove();
                $('#country_city').append('<option>Ciudad</option>');
                $.each(registro.cities, function(keyCity, argument) {
                    $('#country_city').append('<option value='+argument.id+'>'+argument.city+'</option>');
                });
            }
        });
    });
    $("#country_country_edit").change(function() {
        // get the id of the option which has the country code.
       let countryCode = $("#country_country_edit")
          .children(":selected").attr("value");
        $.each(country,function(key, registro) {
            if (registro.id == countryCode) {                
                $('#country_city_edit option').remove();
                $('#country_city_edit').append('<option>Ciudad</option>');
                $.each(registro.cities, function(keyCity, argument) {
                    $('#country_city_edit').append('<option value='+argument.id+'>'+argument.city+'</option>');
                });
            }
        });
    });
    $(function() {
        selectCountry();
    });
</script>

<!-- DATATABLE SCRIPT -->
<script type="text/javascript">
    // COUNTRY
    $(function(){
        $('#table_city').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('city.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'city-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'country.country' , name: 'country'},
                { data: 'city' , name: 'city'},
                { data: 'operations', name: 'operations'},
                { data: 'forums', name: 'forums'},
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return '<td>'+
                                    number+
                                '</td>'
                    }
                },
                { targets : [3],
                    render : function (data, type, row) {
                        switch(data) {
                            case 0 : return 'No'; break;
                            case '1' : return 'Si'; break;
                            default  : return 'Sin Registro';
                        }
                    }
                },
                { targets : [4],
                    render : function (data, type, row) {
                        let forum = (data).length;
                        return forum;
                    }
                },
                { targets : [5],
                    render : function (data, type, row) {
                        return '<button type="button" id="edit_country" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal_country_edit" data-id="'+row['id']+'" data-country="'+row['country'].country+'" data-city="'+row['city']+'" data-operations="'+row['operations']+'">'+
                                        '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                                    '</button>\n'+
                                    '<button type="button" id="delete-country" class="btn btn-danger" data-toggle="modal" data-target="#modal-country-eliminar" data-id="'+row['id']+'"><i class="fa fa-times-circle" aria-hidden="true"></i></button>';
                    }
                }
            ]
        });
    });

    // FORUM
    $(function(){
        $('#table_forum').DataTable({
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{{ route('forum.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'forum-' + aData['slug']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'map' , name: 'map'},
                { data: 'forum_name' , name: 'forum_name'},
                { data: 'city.country.country' , name: 'city.country.country'},
                { data: 'city.city' , name: 'city.city'},
                { data: 'zone.name_zone' , name: 'zone.name_zone'},
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return number
                    }
                },
                { targets : [1],
                    render : function (data, type, row) {
                        return '<img src="'+data+'">'
                                
                    }
                },
                { targets : [6],
                    render : function (data, type, row) {
                        return '<button type="button" id="edit_forum" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-edit-forum" data-id="'+row['id']+'" data-map="'+row['map']+'" data-forum_name="'+row['forum_name']+'" data-name_zone="'+row['zone'].name_zone+'" data-zone_map="'+row['zone'].map+'" data-color="'+row['zone'].color+'" data-type="'+row['zone'].type+'" data-country_id="'+row['country'].id+'" data-city_id="'+row['city'].id+'"><i class="fa fa-pencil" aria-hidden="true"></i></button>\n'+
                                '<button type="button" id="delete_forum" class="btn btn-danger" data-toggle="modal" data-target="#modal-eliminar-forum" data-id="'+row['id']+'"><i class="fa fa-times-circle" aria-hidden="true"></i></button>';
                    }
                }
            ]
        });
    });

    // AMENITY
    $(function(){
        $('#table_amenity').DataTable({
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{{ route('amenity.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'amenity-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'icon' , name: 'icon'},
                { data: 'amenity' , name: 'amenity'}
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return number
                    }
                },
                { targets : [1],
                    render : function (data, type, row) {
                        return '<img src="'+data+'" >'
                                
                    }
                },
                { targets : [3],
                    render : function (data, type, row) {
                        return '<button type="button" id="edit-amenity" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-edit-amenity" data-id="'+row['id']+'" data-icon="'+row['icon']+'" data-amenity="'+row['amenity']+'"><i class="fa fa-pencil" aria-hidden="true"></i></button>\n'+
                                '<button type="button" id="delete-amenity" class="btn btn-danger" data-toggle="modal" data-target="#modal-eliminar-amenity" data-id="'+row['id']+'"><i class="fa fa-times-circle" aria-hidden="true"></i></button>';
                    }
                }
            ]
        });
    });

    //Services
    $(function(){
        $('#table_services').DataTable({
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{{ route('services.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'service-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'forum_name' , name: 'forum_name'},
                { data: 'country' , name: 'country'},
                { data: 'city' , name: 'city'},
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return number
                    }
                },
                { targets : [4],
                    render : function (data, type, row) {
                        return '<button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-adicionales">Editar</button>';
                    }
                },
                { targets : [5],
                    render : function (data, type, row) {
                        return '<button type="button" onclick="add_serviceId('+"'"+row['id']+"'"+');getSnacks('+"'"+row['id']+"'"+');" class="btn btn-block btn-success" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal-snacks">Editar</button>';
                    }
                },
                { targets : [6],
                    render : function (data, type, row) {
                        return '<button type="button" onclick="add_serviceId('+"'"+row['id']+"'"+');" class="btn btn-block btn-success" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal-mesero">Editar</button>';
                    }
                },
                { targets : [7],
                    render : function (data, type, row) {
                        return '<button type="button" onclick="add_serviceId('+"'"+row['id']+"'"+');" class="btn btn-block btn-success" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal-transporte">Editar</button>';
                    }
                },
                { targets : [8],
                    render : function (data, type, row) {
                        return '<button type="button" onclick="add_serviceId('+"'"+row['id']+"'"+');" class="btn btn-block btn-success" data-id="'+row['id']+'" data-toggle="modal" data-target="#modal-envios">Editar</button>';
                    }
                }
            ]
        });
    });
</script>

<!-- MODAL SCRIPT -->
<script type="text/javascript">

    //ADD_SNACKS
    // $(document).on('click', '#add_snacks,#add_waiter,#add_transport,#add_shipment', function() {
    //     console.log($(this).data('id'));
    //     $('#service_snack,#service_waiter,#service_transport,#service_shipment').val($(this).data('id'));
    // });

    //ADD_SERVICE_ID_INTO_MODALS
    function add_serviceId(service_id){
        console.log(service_id);
        $('#service_snack').val(service_id);
        $('#service_waiter').val(service_id);
        $('#service_transport').val(service_id);
        $('#service_shipment').val(service_id);
    };

    // SERVICES
    // ADD A NEW SNACK
    $('#create_snacks').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('services.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                if(data[0]['type'] == 'drink'){
                    let box = '<div class="snack_info">'+
                                    '<button onclick="editSnack('+"'"+data[0]['id']+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                    '<img width="auto" height="90" src="'+data[0]['image_snack']+'">'+
                                    '<p>'+data[0]['name_es']+'</p>'+
                                    '<p style="font-size: 1.2em"><b>'+data[0]['price_snack']+'</b></p>'+
                                '</div>';
                    $('#drinks').append(box);
                }else if(data[0]['type'] == 'pack'){
                    let box = '<div class="snack_info">'+
                                    '<button onclick="editSnack('+"'"+data[0]['id']+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                    '<img width="auto" height="90" src="'+data[0]['image_snack']+'">'+
                                    '<p>'+data[0]['name_es']+'</p>'+
                                    '<p style="font-size: 1.2em"><b>'+data[0]['price_snack']+'</b></p>'+
                                '</div>';
                    $('#packs').append(box);
                }else{
                    let box = '<div class="snack_info">'+
                                    '<button onclick="editSnack('+"'"+data[0]['id']+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                    '<img width="auto" height="90" src="'+data[0]['image_snack']+'">'+
                                    '<p>'+data[0]['name_es']+'</p>'+
                                    '<p style="font-size: 1.2em"><b>'+data[0]['price_snack']+'</b></p>'+
                                '</div>';
                    $('#foods').append(box);
                }
                $('#create_snacks')[0].reset();
                $('#agregar-snacks').modal('hide');
            },
        });
    });

    // EDIT SNACK
    $('#edit_snack').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        id = $('#snack_id').val();
        let route = '{{ route("snacks.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                // $('#table_amenity').DataTable().ajax.reload();
                $('#editar-snacks').modal('hide');
            },
        });
    });

    // GET SNACKS
    function getSnacks(service_id){
        $.ajax({
            type: 'POST',
            url: '{{ route('services.snacks')}}',
            data: {'service_id': service_id, '_token': $('input[name=_token]').val() },
            success: function(data) {
                $('#foods,#drinks,#packs').html('');
                // if(data[0].type == 'drink'){
                //     let title = '<h4>Bebidas</h4>'
                //     $('#bebidas').append(title);
                // }else if(data[0].type == 'food'){
                //     let title = '<h4>Alimentos</h4>'
                //     $('#alimentos').append(title);
                // }else{
                //     let title = '<h4>Paquetes</h4>'
                //     $('#paquetes').append(title);
                // }
                $.each(data, function (i, item) {
                    console.log(item.price_snack)
                    if(item.type == 'drink'){
                        let box = '<div class="snack_info">'+
                                        '<button onclick="editSnack('+"'"+item.id+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                        '<img width="auto" height="90" src="'+item.image_snack+'">'+
                                        '<p>'+item.name+'</p>'+
                                        '<p style="font-size: 1.2em"><b>'+item.price_snack+'</b></p>'+
                                    '</div>';
                        $('#drinks').append(box);
                    }else if(item.type == 'pack'){
                        let box = '<div class="snack_info">'+
                                        '<button onclick="editSnack('+"'"+item.id+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                        '<img width="auto" height="90" src="'+item.image_snack+'">'+
                                        '<p>'+item.name+'</p>'+
                                        '<p style="font-size: 1.2em"><b>'+item.price_snack+'</b></p>'+
                                    '</div>';
                        $('#packs').append(box);
                    }else{
                        let box = '<div class="snack_info">'+
                                        '<button onclick="editSnack('+"'"+item.id+"'"+')" class="btn btn-info pull-right" data-toggle="modal" data-target="#editar-snacks"><i class="fa fa-pencil"></i></button>'+
                                        '<img width="auto" height="90" src="'+item.image_snack+'">'+
                                        '<p>'+item.name+'</p>'+
                                        '<p style="font-size: 1.2em"><b>'+item.price_snack+'</b></p>'+
                                    '</div>';
                        $('#foods').append(box);
                    }
                });	      
                // $('#table_amenity').DataTable().ajax.reload();
                // $('#modal-default-amenity').modal('hide');
            },
        });
    }

    // EDIT SNACK
    function editSnack(snack_id)
    {
        $('#snack_id').val(snack_id);
        $.ajax({
            type: 'POST',
            url: '{{ route('edit.snacks')}}',
            data: {'snack_id': snack_id, '_token': $('input[name=_token]').val() },
            success: function(data) {
                console.log(data);
                $('#img_snack').attr("src",data.image_snack);
                var type1 = $('#edit_snack #type1');
                var type2 = $('#edit_snack #type2');
                var type3 = $('#edit_snack #type3');
                if (type1.val() == data.type) {
                    $('#edit_snack #type1').attr( 'checked', true );
                }else if(type2.val() == data.type){
                    $('#edit_snack #type2').attr( 'checked', true );                    
                }else if(type3.val() == data.type){
                    $('#edit_snack #type3').attr( 'checked', true );
                }
                $('#edit_snack input[name="name_es"]').val(data.name_es);
                $('#edit_snack input[name="name_en"]').val(data.name_en);
                $('#edit_snack input[name="brand"]').val(data.brand);
                $('#edit_snack input[name="price_snack"]').val(data.price_snack);
                // $('#table_amenity').DataTable().ajax.reload();
                // $('#modal-default-amenity').modal('hide');
            },
        });
    }

    // ADD A NEW WAITER
    $('#create_waiter').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('services.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                // $('#table_amenity').DataTable().ajax.reload();
                // $('#modal-default-amenity').modal('hide');
            },
        });
    });

    // ADD A NEW TRANSPORT
    $('#create_transport').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('services.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                // $('#table_amenity').DataTable().ajax.reload();
                // $('#modal-default-amenity').modal('hide');
            },
        });
    });

    // ADD A NEW SHIPMENT
    $('#create_shipment').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('services.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                // $('#table_amenity').DataTable().ajax.reload();
                // $('#modal-default-amenity').modal('hide');
            },
        });
    });

    var BATTUTA_KEY = "00000000000000000000000000000000";
    // COUNTRY
    // ADD A NEW COUNTRY
    $('#create-country').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('city.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_city').DataTable().ajax.reload();
                $('#modal-country-create').modal('hide');
                selectCountry();
                $('#create-country')[0].reset();
            },
        });
    });
    // EDIT A COUNTRY
    $(document).on('click', '#edit_country', function() {
        //$('#id_edit').val($(this).data('id'));
        $('#edit_country_id').val($(this).data('country'));
        // get the id of the option which has the country code.
        let city = $(this).data('city');
        countryCode = $('#edit_country_id')
            .children(":selected")
            .attr("id");
        // Populate country select box from battuta API
        url =
          "https://battuta.medunes.net/api/region/" +
          countryCode +
          "/all/?key=" +
          BATTUTA_KEY +
          "&callback=?";
        $.getJSON(url, function(data) {
          $(".region option").remove();
          $('.region').append('<option value="">--Ciudad--</option>');
          $.each(data, function(index, value) {
            // APPEND OR INSERT DATA TO SELECT ELEMENT.
            $(".region").append(
              '<option value="' + value.region + '">' + value.region + "</option>"
            );
          });
          $('#edit_city').val(city);
        });
        
        if ($(this).data('operations') == '1') {
            $('#edit_optionsRadios1').attr('checked','checked');
        } else if($(this).data('operations') == '0'){
            $('#edit_optionsRadios2').attr('checked','checked');
        }
        id = $(this).data('id');
    });
    $('#form_edit_country').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("city.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_city').DataTable().ajax.reload(); 
                $('#modal_country_edit').modal('hide');
            }
        });
    });
    // DELETE A COUNTRY
    $(document).on('click', '#delete-country', function() {
        $('#delete_country_id').val($(this).data('id'));
        id = $('#delete_country_id').val();
    });
    $('#form_delete_country').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("city.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_city').DataTable().ajax.reload();
                $('#modal-country-eliminar').modal('hide');
                selectCountry();
            }
        });
    });

    // AMENITY
    // ADD A NEW AMENITY
    $('#create-amenity').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('amenity.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_amenity').DataTable().ajax.reload();
                $('#modal-default-amenity').modal('hide');
                $('#create-amenity')[0].reset();

            },
        });
    });
    // EDIT A AMENITY
    $(document).on('click', '#edit-amenity', function() {
        $('#id_edit').val($(this).data('id'));
        $('#amenity_edit').val($(this).data('amenity'));
        $('#icon_amenity_edit').attr('src',$(this).data('icon'));
        id = $('#id_edit').val();
    });
    $('#form_edit_amenity').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("amenity.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_amenity').DataTable().ajax.reload();
                $('#modal-edit-amenity').modal('hide');
            }
        });
    });
    // DELETE A AMENITY
    $(document).on('click', '#delete-amenity', function() {
        $('#amenity_id').val($(this).data('id'));
        id = $('#amenity_id').val();
    });
    $('#delete_amenity').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("amenity.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_amenity').DataTable().ajax.reload();
                $('#modal-eliminar-amenity').modal('hide');
            }
        });
    });

    //FORUM
    // ADD A NEW FORUM
    $('#form_create_forum').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('forum.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_forum').DataTable().ajax.reload();
                $('#modal-default-forum').modal('hide');
                $('#form_create_forum')[0].reset();
            },
        });
    });
    // DELETE A FORUM
    $(document).on('click', '#delete_forum', function() {
        //$('#amenity_id').val($(this).data('id'));
        id = $(this).data('id')
    });
    $('#form_delete_forum').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("forum.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_forum').DataTable().ajax.reload();
                $('#modal-eliminar-forum').modal('hide');
            }
        });
    });
    // EDIT A FORUM
    $(document).on('click', '#edit_forum', function() {
        $('#show_edit_map').attr('src',$(this).data('map'));
        $('#forum_name').val($(this).data('forum_name'));
        $('#country_country_edit').val($(this).data('country_id'));
        $('#name_zone').val($(this).data('name_zone'));
        $('#show_edit_zone_map').attr('src',$(this).data('zone_map'));
        $('#color').val($(this).data('color'));
        $('#type').val($(this).data('type'));
        let countryCode = $("#country_country_edit")
          .children(":selected").attr("value");
        $.each(country,function(key, registro) {
            if (registro.id == countryCode) {                
                $('#country_city_edit option').remove();
                $('#country_city_edit').append('<option>Ciudad</option>');
                $.each(registro.cities, function(keyCity, argument) {
                    $('#country_city_edit').append('<option value='+argument.id+'>'+argument.city+'</option>');
                });
            }
        });
        $('#country_city_edit').val($(this).data('city_id'));
        id = $(this).data('id');
    });
    $('#form_edit_forum').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("forum.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_forum').DataTable().ajax.reload(); 
                $('#modal-edit-forum').modal('hide');
            }
        });
    });
</script>

<!--FILE PREVIEW-->
<script type="text/javascript">
    $(function() {
        $('#icon_create').change(function(e) {
            addImage(e); 
        });

        $('#icon_edit').change(function(e) {
            addImage(e); 
        });

        function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        }

        function fileOnload(e) {
            var result=e.target.result;
            $('#icon_amenity_create').attr("src",result);
            $('#icon_amenity_edit').attr("src",result);
        }

        //FORUM
        $('#create_map').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = createMapOnload;
            reader.readAsDataURL(file); 
        });

        function createMapOnload(e) {
            let result=e.target.result;
            $('#show_create_map').attr("src",result);
        }

        $('#edit_map').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = editMapOnload;
            reader.readAsDataURL(file); 
        });

        function editMapOnload(e) {
            let result=e.target.result;
            $('#show_edit_map').attr("src",result);
        }

        //ZONE
        $('#create_zone_map').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = createZoneMapOnload;
            reader.readAsDataURL(file); 
        });

        function createZoneMapOnload(e) {
            let result=e.target.result;
            $('#show_create_zone_map').attr("src",result);
        }

        $('#edit_zone_map').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = editZoneMapOnload;
            reader.readAsDataURL(file); 
        });

        function editZoneMapOnload(e) {
            let result=e.target.result;
            $('#show_edit_zone_map').attr("src",result);
        }
    });
</script>

@endsection