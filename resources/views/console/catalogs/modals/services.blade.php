<!-- BODY MODAL SNACKS -->
<div class="modal fade" id="modal-snacks">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Editar Snacks</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">   
                    <button class="btn btn-success pull-right" data-toggle="modal" data-target="#agregar-snacks">
                        Agregar Snacks
                    </button><br>
                    <p>Precio Base:</p>
                    <input type="text" class="form-control" placeholder="$..." style="width: 40%;">
                </div><br>
                <div id="alimentos">
                    <h4>Alimentos</h4>
                    <div id="foods"></div>
                </div>
                <div id="bebidas" style="margin-top: 68%;clear: left;">
                    <h4>Bebidas</h4>
                    <div id="drinks"></div>
                </div>                                        
                <div id="paquetes" style="clear: left;margin-top: 72%;">
                    <h4>Paquetes</h4>
                    <div id="packs"></div>
                </div>
            </div>
            <div class="modal-footer" style="clear: left; margin-top: 36%;">
                <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                <button type="button" class="btn btn-success" style="width: 49%;">Aceptar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BODY MODAL AGREGAR SNACKS -->
<div class="modal fade" id="agregar-snacks">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar Snacks</h4>
            </div>
            <form id="create_snacks" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="snacks" value="snacks" >
                <input type="hidden" name="service_id" id="service_snack" >
                <div class="modal-body">
                    <div class="col-xs-4">
                        <img id="img-snack" src="{{ asset('console/dist/img/boxed-bg.jpg') }}">
                        <br><br>
                        <input id="input_snack" name="image_snack" type="file" class="form-control">
                    </div>
                    <div class="col-xs-8">
                        <p><b>Tipo:</b></p>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type1" value="food" checked>
                                    Alimento
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type2" value="drink">
                                    Bebida
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type2" value="pack">
                                    Paquete
                                </label>
                            </div>
                        </div>
                        <p><b>Nombre (Español):</b></p>
                        <input type="text" name="name_es" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Nombre (Inglés):</b></p>
                        <input type="text" name="name_en" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Marca:</b></p>
                        <input type="text" name="brand" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Precio: <small>(MXN)</small></b></p>
                        <input type="text" name="price_snack" class="form-control" placeholder="$...">
                        <br><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px; width: 49%;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BODY MODAL EDITAR SNACKS -->
<div class="modal fade" id="editar-snacks">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Editar Snacks</h4>
                <p style="text-align: center;">Se editará en todos los foros en donde esté publicado. Si deseas que el snack tenga un precio diferente por foro, dalo de alta como un nuevo producto.</p>
            </div>
            <form id="edit_snack" role="form" enctype="multipart/form-data">
                @csrf
                {{ method_field('Put') }}
                <div class="modal-body">
                    <div class="col-xs-4">
                        <img id="img_snack" >
                        <br><br>
                        <input type="file" class="form-control">
                    </div>
                    <input type="hidden" name="snack_id" id="snack_id">
                    <div class="col-xs-8">
                        <p><b>Tipo:</b></p>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type1" value="food">
                                    Alimento
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type2" value="drink">
                                    Bebida
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="type" id="type3" value="pack">
                                    Paquete
                                </label>
                            </div>
                        </div>
                        <p><b>Nombre (Español):</b></p>
                        <input type="text" name="name_es" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Nombre (Inglés):</b></p>
                        <input type="text" name="name_en" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Marca:</b></p>
                        <input type="text" name="brand" class="form-control" placeholder="30 caracteres..."><br>
                        <p><b>Precio: <small>(MXN)</small></b></p>
                        <input type="text" name="price_snack" class="form-control" placeholder="$...">
                        <br><br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 4px; width: 32%;">Eliminar</button>
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px; width: 32%;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 32%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY MODAL MESERO -->
<div class="modal fade" id="modal-mesero">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Estadio Azteca</h4>
            </div>
            <form id="create_waiter" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <h4>Catering</h4>
                        <p>¿Ofrecemos servicio de mesero en este foro?</p>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="hidden" name="service_waiter" value="service_waiter">
                                    <input type="hidden" id="service_waiter" name="service_id">
                                    <input type="radio" name="status_waiter" id="status_waiter1" value="1" checked>
                                    Si
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="status_waiter" id="status_waiter2" value="0">
                                    No
                                </label>
                            </div>
                        </div>
                        <p>Cantidad de meseros disponibles:</p>
                        <input type="number" name="amount" class="form-control" placeholder="Cantidad..." style="width: 20%;"><br>
                        <p>Precio por mesero:</p>
                        <input type="number" name="price_waiter" class="form-control" placeholder="$...">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 25%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY MODAL TRANSPORTE -->
<div class="modal fade" id="modal-transporte">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Estadio Azteca</h4>
            </div>
            <form id="create_transport" role="form" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <h4>Transporte</h4>
                        <p>¿Ofrecemos transporte en este foro?</p>
                        <!-- radio -->
                        <div class="form-group">
                        <input type="hidden" name="service_transport" value="service_transport">
                        <input type="hidden" id="service_transport" name="service_id">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="status_transport" id="status_transport1" value="1" checked>
                                    Si
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="status_transport" id="status_transport2" value="0">
                                    No
                                </label>
                            </div>
                        </div>
                        <p>Precio por pasajeros:<br><small>(se multiplicará por el número de paradas) Ej. 4 pasajeros = $200mxn * 2 paradas (200*1.2=240)</small></p>
                        <p>De 1 a 4</p><input type="number" name="price_1" class="form-control" placeholder="$...">
                        <p>De 5 a 8</p><input type="number" name="price_2" class="form-control" placeholder="$...">
                        <p>De 9 a 12</p><input type="number" name="price_3" class="form-control" placeholder="$...">
                        <p>De 13 a 16</p><input type="number" name="price_4" class="form-control" placeholder="$...">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 25%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY MODAL ENVÍOS -->
<div class="modal fade" id="modal-envios">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 50%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Estadio Azteca</h4>
            </div>
            <form id="create_shipment" role="form" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <h4>Envíos:</h4>
                        <p>Precio por envío nacional:</p>
                        <input type="hidden" name="service_shipment" value="service_shipment">
                        <input type="hidden" id="service_shipment" name="service_id">
                        <input type="number" name="price_shipment" class="form-control" placeholder="$...">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px; width: 48%;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 48%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.box -->