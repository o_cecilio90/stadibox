
<!-- BODY MODALES FOROS -->
<div class="modal fade" id="modal-default-forum">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Foro</h4>
            </div>
            <form id="form_create_forum" class="form-horizontal" role="form">
                @csrf
                <div class="modal-body">
                    <div class="col-xs-7">
                        <div class="form-group">                            
                            <p>Seleccionar un país:</p>
                            <select id="country_country" class="form-control">
                                <option>País</option>
                            </select><br>
                            <p>Seleccionar una ciudad:</p>
                            <select id="country_city" name="city_id" class="form-control">
                                <option>Ciudad</option>
                            </select><br>
                            <input type="text" name="forum_name" class="form-control" placeholder="Nombre del foro...">
                            <br>
                            </div>
                        </div>
                    <div class="col-xs-5">
                        <img id="show_create_map" src="{{asset('console/dist/img/azteca_zonas.png')}}">
                        <br><br>
                        <input type="file" name="map" id="create_map" class="form-control">
                    </div>
                    <h4 style="clear: left;">Zonas <i class="fa fa-plus-circle"></i></h4>
                    <div class="zona_n" style="overflow: auto;">
                        <div class="col-xs-7">
                            <select name="type" class="form-control">
                                <option value="Palcos">Palcos</option>
                                <option value="Plateas">Plateas</option>
                                <option value="Abonos">Abonos</option>
                            </select><br>
                            <input type="text" name="name_zone" class="form-control" placeholder="Nombre..."><br>
                            <!-- Color Picker -->
                            <div class="form-group">
                                <p style="width: 25%;">Color:</p>
                                <div class="input-group my-colorpicker2" style="width: 50%;">
                                    <input name="color" type="text" class="form-control">

                                    <div class="input-group-addon">
                                        <i></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <img id="show_create_zone_map" src="{{asset('console/dist/img/azteca_alto.png')}}">
                            <br><br>
                            <input type="file" id="create_zone_map" name="zone_map" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear: left;">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY ELIMINAR FORO -->
<div class="modal fade" id="modal-eliminar-forum">
    <div class="modal-dialog" style="width: 23%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminar Foro</h4>
            </div>
            <form class="form-horizontal" id="form_delete_forum" role="form">
                @csrf
                {{ method_field('Delete') }}
                <div class="modal-body">
                    <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar este foro?</b></P>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                    <button type="submit" id="delete-forum" class="btn btn-danger" style="width: 49%;">Eliminar Foro</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BODY EDIT FOROS -->
<div class="modal fade" id="modal-edit-forum">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Foro</h4>
            </div>
            <form id="form_edit_forum" class="form-horizontal" role="form">
                @csrf
                {{ method_field('Put') }}
                <div class="modal-body">
                    <div class="col-xs-7">
                        <div class="form-group">                            
                            <p>Seleccionar un país:</p>
                            <select id="country_country_edit" class="form-control">
                                <option>País</option>
                            </select><br>
                            <p>Seleccionar una ciudad:</p>
                            <select id="country_city_edit" name="city_id" class="form-control">
                                <option>Ciudad</option>
                            </select><br>
                            <input type="text" id="forum_name" name="forum_name" class="form-control" placeholder="Nombre del foro...">
                            <br>
                            </div>
                        </div>
                    <div class="col-xs-5">
                        <img id="show_edit_map" src="{{asset('console/dist/img/azteca_zonas.png')}}">
                        <br><br>
                        <input type="file" name="map" id="edit_map" class="form-control">
                    </div>
                    <h4 style="clear: left;">Zonas <i class="fa fa-plus-circle"></i></h4>
                    <div class="zona_n" style="overflow: auto;">
                        <div class="col-xs-7">
                            <select id="type" name="type" class="form-control">
                                <option value="Palcos">Palcos</option>
                                <option value="Plateas">Plateas</option>
                                <option value="Abonos">Abonos</option>
                            </select><br>
                            <input type="text" id="name_zone" name="name_zone" class="form-control" placeholder="Nombre..."><br>
                            <!-- Color Picker -->
                            <div class="form-group">
                                <p style="width: 25%;">Color:</p>
                                <div class="input-group my-colorpicker2" style="width: 50%;">
                                    <input id="color" name="color" type="text" class="form-control">

                                    <div class="input-group-addon">
                                        <i></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <img id="show_edit_zone_map" src="{{asset('console/dist/img/azteca_alto.png')}}">
                            <br><br>
                            <input type="file" id="edit_zone_map" name="zone_map" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear: left;">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->