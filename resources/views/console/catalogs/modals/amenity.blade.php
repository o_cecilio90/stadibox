<!-- BODY MODALES AMENIDAD -->
<div class="modal fade" id="modal-default-amenity">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Amenidad</h4>
            </div>
            <form class="form-horizontal" id="create-amenity" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">                
                    <div class="col-xs-5">
                        <img id="icon_amenity_create" src="{{asset('console/dist/img/wifi-connection-signal-symbol.png')}}">
                        <br><br>
                        <input type="file" id="icon_create" name="icon" class="form-control">
                    </div>
                    <div class="col-xs-7">
                        <div class="form-group">
                            <p>Amenidad:</p>
                            <input type="text" id="amenity_create" name="amenity" class="form-control" placeholder="Nombre de amenidad...">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="clear: left;margin-top: 35%;">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY EDIT AMENIDAD -->
<div class="modal fade" id="modal-edit-amenity">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 90%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Amenidad</h4>
            </div>
            <form class="form-horizontal" id="form_edit_amenity" role="form" enctype="multipart/form-data">
                @csrf
                {{ method_field('Put') }}
                <div class="modal-body">                
                    <div class="col-xs-5">
                        <img id="icon_amenity_edit" src="{{asset('console/dist/img/wifi-connection-signal-symbol.png')}}">
                        <br><br>
                        <input type="file" id="icon_edit" name="icon" class="form-control">
                    </div>
                    <div class="col-xs-7">
                        <div class="form-group">
                            <p>Amenidad:</p>
                            <input type="text" id="amenity_edit" name="amenity" value="" class="form-control" placeholder="Nombre de amenidad...">
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="id_edit" value="">
                </div>
                <div class="modal-footer" style="clear: left;margin-top: 35%;">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY ELIMINAR AMENIDAD -->
<div class="modal fade" id="modal-eliminar-amenity">
    <div class="modal-dialog" style="width: 25%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminar Amenidad</h4>
            </div>
            <form id="delete_amenity" class="form-horizontal" role="form">
                @csrf
                {{ method_field('Delete') }}
                <div class="modal-body">                    
                    <input type="hidden" class="form-control" id="amenity_id">
                    <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar esta amenidad?</b></P>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                    <button type="submit" class="btn btn-danger" style="width: 49%;">Eliminar Amenidad</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->