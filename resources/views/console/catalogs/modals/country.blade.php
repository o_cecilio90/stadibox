<!-- BODY MODALES CIUDAD -->
<div class="modal fade" id="modal-country-create">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 65%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Ciudad</h4>
            </div>
            <form id="create-country" role="form">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <p>Seleccionar un país:</p>
                        <select class="form-control country" id="country_id" name="country">
                            <option>País</option>
                        </select><br>
                        <p>Seleccionar una ciudad:</p>
                        <select class="form-control region" id="city_city" name="city">
                            <option>Ciudad</option>
                        </select><br>
                        <p>¿Tenemos operación física en esa ciudad?</p>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="operations" id="optionsRadios1" value="1">
                                    Si
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="operations" id="optionsRadios2" value="0">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY ELIMINAR CIUDAD -->
<div class="modal fade" id="modal-country-eliminar">
    <div class="modal-dialog" style="width: 23%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminar Ciudad</h4>
            </div>
            <form class="form-horizontal" id="form_delete_country" role="form">
                @csrf
                {{ method_field('Delete') }}
                <div class="modal-body">
                    <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar esta ciudad?</b></P>
                    <input type="hidden" class="form-control" id="delete_country_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                    <button type="submit" class="btn btn-danger" style="width: 49%;">Eliminar Ciudad</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY EDITAR CIUDAD -->
<div class="modal fade" id="modal_country_edit">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 65%;margin: 0% auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Agregar/Editar Ciudad</h4>
            </div>
            <form id="form_edit_country" role="form">
                @csrf
                {{ method_field('Put') }}
                <div class="modal-body">
                    <div class="form-group">
                        <p>Seleccionar un país:</p>
                        <select class="form-control country" id="edit_country_id" name="country">
                            <option>País</option>
                        </select><br>
                        <p>Seleccionar una ciudad:</p>
                        <select class="form-control region" id="edit_city" name="city">
                            <option>Ciudad</option>
                        </select><br>
                        <p>¿Tenemos operación física en esa ciudad?</p>
                        <!-- radio -->
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="operations" id="edit_optionsRadios1" value="1">
                                    Si
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="operations" id="edit_optionsRadios2" value="0">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 49%;border-radius: 4px;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->