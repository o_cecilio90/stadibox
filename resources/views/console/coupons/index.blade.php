@extends('console.layouts.main')

@section('title', 'Cupones')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Cupones</h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Cupones</li>
            </ol>
            <!-- /.content -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                    <li class="pull-left header"><i class="fa fa-ticket"></i> Cupones</li>
                </ul>
                <div class="tab-content no-padding">
                    <!-- CONTENIDO DE LAS TABS -->
                    <!-- CUPONES -->
                    <div class="table tab-pane active" id="cupones" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header" style="padding: 1% 0%;">
                                        <h3 class="box-title">Catálogo de Cupones</h3>
                                        
                                        <div class="box-tools">
                                            <div class="form-group form-group-sm" style="width: 150px;">
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default" style="width: 100%;">
                                                    Agregar Cupón
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Código</th>
                                                <th style="text-align: center">Evento</th>
                                                <th style="text-align: center;">Vigencia</th>
                                                <th style="text-align: center">Estatus</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>
                                            <tr>
                                                <td>01</td>
                                                <td>1234hgft</td>
                                                <td>América vs. Chivas<br><small>13/07/2018</small></td>
                                                <td>04/02/2018</td>
                                                <td>Activo</td>
                                                <td>
                                                    <button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default">Ver/Editar</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>02</td>
                                                <td>54321edws</td>
                                                <td>NFL MX 2018<br><small>27/10/2018</small></td>
                                                <td>15/06/2018</td>
                                                <td>Activo</td>
                                                <td>
                                                    <button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default">Ver/Editar</button>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>03</td>
                                                <td>9821dhek</td>
                                                <td>Shakira<br><small>10/11/2018</small></td>
                                                <td>25/09/2018</td>
                                                <td>Inactivo</td>
                                                <td>
                                                    <button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default">Ver/Editar</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- BODY MODAL AGREGAR CUPÓN -->
                                <div class="modal fade" id="modal-default">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="width: 65%;margin: 0% auto;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>     </button>
                                                <h4 class="modal-title">Agregar/Editar Cupón</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <p>Código de Cupón:</p>
                                                    <input type="text" class="form-control" placeholder="Código de Cupón...">
                                                    <br>
                                                    <p>Descuento:</p>
                                                    <input type="number" class="form-control" placeholder="%">
                                                    <br>
                                                    <p>Vigencia:</p>
                                                    <div class="input-group">
                                                        <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                                                            <span>
                                                                <i class="fa fa-calendar"></i> Date range picker
                                                            </span>
                                                            <i class="fa fa-caret-down"></i>
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <p>Evento:</p>
                                                    <select class="form-control">
                                                        <option>Evento 1</option>
                                                        <option>Evento 2</option>
                                                        <option>Evento 3</option>
                                                    </select>
                                                    <br>
                                                    <p>Estatus:</p>
                                                    <select class="form-control">
                                                        <option>Activo</option>
                                                        <option>Inactivo</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="width: 32%;border-radius: 4px;">Cancelar</button>
                                                <button type="button" class="btn btn-danger" style="width: 32%;" data-dismiss="modal">Eliminar</button>
                                                <button type="button" class="btn btn-success" style="width: 32%;" data-dismiss="modal">Aceptar</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <!-- BODY ELIMINAR CUPÓN -->
                                <div class="modal fade" id="modal-eliminar">
                                    <div class="modal-dialog" style="width: 23%;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Eliminar Cupón</h4>
                                            </div>
                                            <div class="modal-body">
                                                <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar este cupón?</b></P>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                                                <button type="button" class="btn btn-danger" style="width: 49%;" data-dismiss="modal">Eliminar Ciudad</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- FINALIZA CUPONES -->
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </section>
        <!-- wrapper Main Content -->
    </div>

@endsection