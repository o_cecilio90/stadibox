<!-- jQuery 3 -->
<script src="{{asset('console/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('console/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('console/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('console/select2/dist/js/select2.full.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('console/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('console/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('console/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('console/raphael/raphael.min.js')}}"></script>
<script src="{{asset('console/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('console/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('console/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('console/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('console/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('console/moment/min/moment.min.js')}}"></script>
<script src="{{asset('console/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<!-- bootstrap color picker -->
<script src="{{asset('console/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('console/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{asset('console/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('console/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('console/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{asset('console/plugins/iCheck/icheck.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('console/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('console/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('console/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('console/dist/js/demo.js')}}"></script>
<!-- Bootstrap slider -->
<script src="{{asset('console/plugins/bootstrap-slider/bootstrap-slider.js')}}"></script>
<!-- Bootstrap Toggle -->
<script src="{{asset('console/bootstrap-toggle-master/js/bootstrap-toggle.js')}}"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- fullCalendar -->
<script src="{{ asset('console/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<!--Country Select-->
<script type="text/javascript">
	//Country
	$(document).ready(function() {
		//-------------------------------SELECT CASCADING-------------------------//
		var selectedCountry = (selectedRegion = selectedCity = countryCode = "");

		// This is a demo API key for testing purposes. You should rather request your API key (free) from http://battuta.medunes.net/
		var BATTUTA_KEY = "00000000000000000000000000000000";
		// Populate country select box from battuta API
		url =
		"https://battuta.medunes.net/api/country/all/?key=" +
		BATTUTA_KEY +
		"&callback=?";

		// EXTRACT JSON DATA.
		$.getJSON(url, function(data) {
			$.each(data, function(index, value) {
			  // APPEND OR INSERT DATA TO SELECT ELEMENT. Set the country code in the id section rather than in the value.
			  $(".country").append(
			    '<option id="' +
			      value.code +
			      '" value="' +
			      value.name +
			      '">' +
			      value.name +
			      "</option>"
			  );
			});
		});
		// Country selected --> update region list .
		$(".country").change(function() {
			selectedCountry = this.options[this.selectedIndex].text;
			// get the id of the option which has the country code.
			countryCode = $(this)
			  .children(":selected")
			  .attr("id");
			// Populate country select box from battuta API
			url =
			  "https://battuta.medunes.net/api/region/" +
			  countryCode +
			  "/all/?key=" +
			  BATTUTA_KEY +
			  "&callback=?";
			$.getJSON(url, function(data) {
			  $(".region option").remove();
			  $('.region').append('<option>Ciudad</option>');
			  $.each(data, function(index, value) {
			    // APPEND OR INSERT DATA TO SELECT ELEMENT.
			    $(".region").append(
			      '<option value="' + value.region + '">' + value.region + "</option>"
			    );
			  });
			});
		});
		// Region selected --> updated city list
		$("#region").on("change", function() {
		selectedRegion = this.options[this.selectedIndex].text;
		// Populate country select box from battuta API
		// countryCode = $("#country").val();
		region = $("#region").val();
		url =
		  "https://battuta.medunes.net/api/city/" +
		  countryCode +
		  "/search/?region=" +
		  region +
		  "&key=" +
		  BATTUTA_KEY +
		  "&callback=?";
		$.getJSON(url, function(data) {
		  console.log(data);
		  $("#city option").remove();
		  $('#city').append('<option value="">Please select your city</option>');
		  $.each(data, function(index, value) {
		    // APPEND OR INSERT DATA TO SELECT ELEMENT.
		    $("#city").append(
		      '<option value="' + value.city + '">' + value.city + "</option>"
		    );
		  });
		});
	  });
	  // city selected --> update location string
	  $("#city").on("change", function() {
	    selectedCity = this.options[this.selectedIndex].text;
	    $("#location").html(
	      "Locatation: Country: " +
	        selectedCountry +
	        ", Region: " +
	        selectedRegion +
	        ", City: " +
	        selectedCity
	    );
	  });
	});
</script>
<!--Selects-->
<script type="text/javascript">
	// Role
    $(function(){
        $.ajax({
            type: "GET",
            url: '{{ route('role.index') }}', 
            dataType: "json",
            success: function(data){
                $.each(data['data'],function(key, registro) {
                    $('#role_id').append('<option value='+registro.id+'>'+registro.name+'</option>');
                    $('#edit_role_id').append('<option value='+registro.id+'>'+registro.name+'</option>');
                });        
            },
            error: function(data) {
                alert('¡ERROR! cargando Perfiles contacte al soporte tecnico');
            }
        });
    })
</script>
@yield('js')