<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('console/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Diego Kawas</p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVEGACIÓN</li>
            <li class="">
                <a href="index.php">
                    <i class="fa fa-dashboard"></i> <span>Tablero</span>
                </a>
            </li>
            <li>
                <a href="{!!route('events')!!}">
                    <i class="fa fa-futbol-o"></i>
                    <span>Eventos</span>
                </a>
            </li>
            <li>
                <a href="{{route('reports.index')}}">
                    <i class="fa fa-file-text"></i> <span>Reportes/Transacciones</span>
                </a>
            </li>
            <li>
                <a href="{!!route('users')!!}">
                    <i class="fa fa-user"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <li>
                <a href="renta_anual.php">
                    <i class="fa fa-bars"></i>
                    <span>Renta Anual</span>
                </a>
            </li>
            <li>
                <a href="{!! route('experiencias.index') !!}">
                    <i class="fa fa-star"></i>
                    <span>Experiencias</span>
                </a>
            </li>
            <li class="treeview menu">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>Propiedades</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    <ul class="treeview-menu">
                        <li><a href="{{route('all.properties')}}"><i class="fa fa-circle-o"></i> Todas</a></li>
                        <li><a href="{{route('adm.properties')}}"><i class="fa fa-circle-o"></i> Administradas</a></li>
                    </ul>
                </a>
            </li>
            <li class="treeview menu">
                <a href="#">
                    <i class="fa fa-motorcycle"></i>
                    <span>Logística</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    <ul class="treeview-menu">
                        <li><a href="logistica.html"><i class="fa fa-circle-o"></i> En Curso</a></li>
                        <li><a href="logistica_cierre.html"><i class="fa fa-circle-o"></i> Cierre del Día</a></li>
                        <li><a href="logistica_historial.html"><i class="fa fa-circle-o"></i> Historial</a></li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="{{route('catalogs')}}">
                    <i class="fa fa-book"></i> <span>Catálogos</span>
                </a>
            </li>
            <li>
                <a href="{{route('calendar')}}">
                    <i class="fa fa-calendar"></i> <span>Calendario</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                    </span>
                </a>
            </li>
            <li>
                <a href="{{route('coupons')}}">
                    <i class="fa fa-ticket"></i>
                    <span>Cupones</span>
                </a>
            </li>
            <li><a href="{{route('configuration')}}"><i class="fa fa-cog"></i> <span>Configuración</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>