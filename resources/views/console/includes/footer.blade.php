<!-- FOOTER wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">
	    <b>Stadibox</b> 1.1
	</div>
	<strong>Stadibox &copy; 2018.</strong> Powered by: <a href="https://www.bluepixel.mx">BluePixel</a>
</footer>