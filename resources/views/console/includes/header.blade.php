<header class="main-header">
  <!-- Logo -->
  <a href="{{url('/')}}" target="_blank" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>S</b>B</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Stadibox</b> Admin</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">Tienes 4 mensajes</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li><!-- start message -->
                  <a href="#">
                    <div class="pull-left">
                      <img src="{{asset('console/dist/img/user2-160x160.jpg')}}" class="img-  circle" alt="User Image">
                    </div>
                    <h4>
                      Equipo de Soporte
                      <small><i class="fa fa-clock-o"></i> 5 mins</small>
                    </h4>
                    <p>Hay que subir imágenes del sitio</p>
                  </a>
                </li>
                <!-- end message -->
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="{{asset('console/dist/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Israel Baca
                      <small><i class="fa fa-clock-o"></i> 2 hours</small>
                    </h4>
                    <p>Checar nuevos palcos</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="{{asset('console/dist/img/user4-128x128.jpg')}}" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Diseñadores
                      <small><i class="fa fa-clock-o"></i> Today</small>
                    </h4>
                    <p>Están listas las imágenes!</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="{{asset('console/dist/img/user3-128x128.jpg')}}" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Usuario #33019
                      <small><i class="fa fa-clock-o"></i> Yesterday</small>
                    </h4>
                    <p>Necesito ayuda.</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer">
              <a href="#">Ver todos los mensajes</a>
            </li>
          </ul>
        </li>
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning">10</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">Tienes 10 notificaciones</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                <li>
                  <a href="#">
                    <i class="fa fa-users text-aqua"></i> 5 personas se registraron hoy
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-warning text-yellow"></i> Así se verá una notificación con mucho texto que no se verá completa
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-users text-red"></i> 5 personas se unieron
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-shopping-cart text-green"></i> 25 ventas hechas
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="fa fa-user text-red"></i> Cambiaste tu nombre de usuario
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"><a href="#">Ver todas</a></li>
          </ul>
        </li>
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{asset('console/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
            <span class="hidden-xs">Diego Kawas</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{asset('console/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

              <p>
                Diego Kawas - Administrador
                <small>Miembro desde: Marzo 2018</small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Perfil</a>
              </div>
              <div class="pull-right">
                <a href="#" class="btn btn-default btn-flat">Cerrar Sesión</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>