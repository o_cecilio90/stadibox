<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('console.includes.head')
    <body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper">
            @include('console.includes.header')
            @include('console.includes.aside')
            @yield('content')
        	@include('console.includes.footer')
        </div>           

        @include('console.includes.scripts')
    </body>
</html>