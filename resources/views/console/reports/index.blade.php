@extends('console.layouts.main')

@section('title', 'Experiencias')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Reportes</h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Reportes</li>
            </ol>
            <!-- /.content -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                    <li><a href="#usuarios" data-toggle="tab">Usuarios</a></li>
                    <li><a href="#venta" data-toggle="tab">Propiedades</a></li>
                    <li><a href="#logistica" data-toggle="tab">Logística</a></li>
                    <li><a href="#renta" data-toggle="tab">Renta Anual</a></li>
                    <li><a href="#historial" data-toggle="tab">Reporte General</a></li>
                    <li><a href="#transacciones" data-toggle="tab">Transacciones</a></li>
                    <li><a href="#dispersion" data-toggle="tab">Dispersión por Eventos</a></li>
                    <li class="active"><a href="#publicacion" data-toggle="tab">Publicación</a></li>
                    <li class="pull-left header"><i class="fa fa-file"></i> Reportes</li>
                </ul>
                <div class="tab-content no-padding">
                    <!-- CONTENIDO DE LAS TABS -->
                    <!-- PUBLICACION -->
                    <div class="table tab-pane active" id="publicacion" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Publicaciones</h3>
                                        
                                        <div class="box-tools">
                                            <div class="form-group form-group-sm" style="width: 170px;">
                                                <select class="form-control" style="float: left;">
                                                    <option>Evento 1</option>
                                                    <option>Evento 2</option>
                                                    <option>Evento 3</option>
                                                    <option>Evento 4</option>
                                                    <option>Evento 5</option>
                                                </select>
                                                <!--
                                                <select class="form-control" style="float: left;">
                                                    <option>Estatus 1</option>
                                                    <option>Estatus 2</option>
                                                    <option>Estatus 3</option>
                                                    <option>Estatus 4</option>
                                                    <option>Estatus 5</option>
                                                </select>-->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">ID Propiedad</th>
                                                <th style="text-align: center">Titular de la Propiedad</th>
                                                <th style="text-align: center">Costo</th>
                                                <th style="text-align: center;">Depósito</th>
                                                <th style="text-align: center">No. de Boletos</th>
                                                <th style="text-align: center">Evento</th>
                                                <th style="text-align: center">Estatus</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>
                                            <tr>
                                                <td>3022</td>
                                                <td>Diego Kawas Tafich</td>
                                                <td>$190,000.00</td>
                                                <td>$5,000.00</td>
                                                <td>10</td>
                                                <td>NFL: Raiders vs. Patriots</td>
                                                <td>Activo</td>
                                                <td><button class="btn btn-info">Pausar</button></td>
                                            </tr>
                                            <tr>
                                                <td>2007</td>
                                                <td>Diego Kawas Tafich</td>
                                                <td>$250,000.00</td>
                                                <td>$5,000.00</td>
                                                <td>10</td>
                                                <td>NFL: Raiders vs. Patriots</td>
                                                <td>Activo</td>
                                                <td><button class="btn btn-info">Pausar</button></td>
                                            </tr>
                                            <tr>
                                                <td>N52</td>
                                                <td>Israel Bk</td>
                                                <td>$10,000.00</td>
                                                <td>$2,000.00</td>
                                                <td>10</td>
                                                <td>América vs. Puebla</td>
                                                <td>Activo</td>
                                                <td><button class="btn btn-info">Pausar</button></td>
                                            </tr>
                                            <tr>
                                                <td>2944</td>
                                                <td>Iván</td>
                                                <td>$8,000.00</td>
                                                <td>$3,000.00</td>
                                                <td>5</td>
                                                <td>América vs. Puebla</td>
                                                <td>Activo</td>
                                                <td><button class="btn btn-info">Pausar</button></td>
                                            </tr>
                                            <tr>
                                                <td>2248</td>
                                                <td>Israel Bk</td>
                                                <td>$6,000.00</td>
                                                <td>$1,000.00</td>
                                                <td>2</td>
                                                <td>América vs. Puebla</td>
                                                <td>En Pausa</td>
                                                <td><button class="btn btn-info">Activar</button></td>
                                            </tr>
                                            <tr>
                                                <td>N85</td>
                                                <td>Israel Bk</td>
                                                <td>$10,000.00</td>
                                                <td>$1,000.00</td>
                                                <td>2</td>
                                                <td>NFL: Raiders vs. Patriots</td>
                                                <td>En Pausa</td>
                                                <td><button class="btn btn-info">Activar</button></td>
                                            </tr>
                                            <tr>
                                                <td>1500</td>
                                                <td>Diego Kawas Tafich</td>
                                                <td>$150,000.00</td>
                                                <td>$20,000.00</td>
                                                <td>10</td>
                                                <td>NFL: Raiders vs. Patriots</td>
                                                <td>Activo</td>
                                                <td><button class="btn btn-info">Pausar</button></td>
                                            </tr>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    </div>
                    <!-- FINALIZA PUBLICACION -->
                    <!-- DISPERSION POR EVENTO -->
                    <div class="table tab-pane" id="dispersion" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Dispersión por Evento</h3>
                                        
                                        <div class="box-tools">
                                            <div class="form-group form-group-sm" style="width: 150px;">
                                                <select class="form-control">
                                                    <option>Evento 1</option>
                                                    <option>Evento 2</option>
                                                    <option>Evento 3</option>
                                                    <option>Evento 4</option>
                                                    <option>Evento 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">I.D.</th>
                                                <th style="text-align: center">Modo</th>
                                                <th style="text-align: center">Comprador</th>
                                                <th style="text-align: center;">Método de Pago</th>
                                                <th style="text-align: center">Clave de Rastreo</th>
                                                <th style="text-align: center">Tipo de Cuenta Beneficiario</th>
                                                <th style="text-align: center">Monto</th>
                                                <th style="text-align: center">Institución</th>
                                                <th style="text-align: center">Cuenta PayPal o CLABE</th>
                                                <th style="text-align: center">Concepto</th>
                                                <th style="text-align: center">Referencia</th>
                                                <th style="text-align: center">Correo Beneficiario</th>
                                                <th style="text-align: center">Institución Operante</th>
                                                <th style="text-align: center">Estatus</th>
                                            </tr>
                                            <tr>
                                                <td>3045</td>
                                                <td>Depósito</td>
                                                <td>Israel Baca</td>
                                                <td>PayPal</td>
                                                <td>1234567890</td>
                                                <td>Tipo 1</td>
                                                <td>$1,000.00</td>
                                                <td>Institución</td>
                                                <td>israel@hotmail.com</td>
                                                <td>Regreso de depósito</td>
                                                <td>304567</td>
                                                <td>israel@hotmail.com</td>
                                                <td></td>
                                                <td>Pendiente</td>
                                            </tr>
                                            <tr>
                                                <td>3040</td>
                                                <td>Venta</td>
                                                <td>Diego Kawas</td>
                                                <td>Transferencia</td>
                                                <td>0987654321</td>
                                                <td>Tipo 2</td>
                                                <td>$10,000.00</td>
                                                <td>Institución</td>
                                                <td>123456543456212321</td>
                                                <td>Pago de Renta: 344098</td>
                                                <td>763497</td>
                                                <td>diego@hotmail.com</td>
                                                <td></td>
                                                <td>Realizado</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- FINALIZA DISPERSION -->
                    <!-- TRANSACCIONES -->
                    <div class="table tab-pane" id="transacciones" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Transacciones</h3>
                                        
                                        <div class="box-tools tools_transacciones" style="width: 80%;right: -7px !important;">
                                            <div class="form-group form-group-sm">
                                                <select class="form-control" style="width: 24%; float: left;">
                                                    <option>Evento 1</option>
                                                    <option>Evento 2</option>
                                                    <option>Evento 3</option>
                                                    <option>Evento 4</option>
                                                    <option>Evento 5</option>
                                                </select>
                                                <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                                    <option>Estatus 1</option>
                                                    <option>Estatus 2</option>
                                                    <option>Estatus 3</option>
                                                    <option>Estatus 4</option>
                                                    <option>Estatus 5</option>
                                                </select>
                                                <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                                    <option>Otros 1</option>
                                                    <option>Otros 2</option>
                                                    <option>Otros 3</option>
                                                    <option>Otros 4</option>
                                                    <option>Otros 5</option>
                                                </select>
                                                <input type="text" class="form-control" placeholder="ID, No. Palco, Evento, Foro, Correo, Titular..." style="float: left;width: 24%; margin-left: 1%;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center;">ID</th>
                                                <th style="text-align: center;">Fecha de Compra</th>
                                                <th style="text-align: center">Evento</th>
                                                <th style="text-align: center;">Foro</th>
                                                <th style="text-align: center;">Propiedad o Paquete</th>
                                                <th style="text-align: center">Titular</th>
                                                <th style="text-align: center">Comprador</th>
                                                <th style="text-align: center">No. de Boletos</th>
                                                <th style="text-align: center">Monto</th>
                                                <th style="text-align: center">D. Garantía</th>
                                                <th style="text-align: center">Estatus</th>
                                            </tr>
                                            <tr onclick="document.location = 'transaccion.php'">
                                                <td>183</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>América vs. Pachuca</b><br><small>4/02/18</small></td>
                                                <td>Azteca</td>
                                                <td>P0035</td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>10</td>
                                                <td>$25,500</td>
                                                <td>$1,500</td>
                                                <td>En espera de Pago</td>
                                            </tr>
                                            <tr class="alerta" onclick="document.location = 'transaccion.php'">
                                                <td>53</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>Shakira</b><br><small>16/02/18</small></td>
                                                <td>Azteca</td>
                                                <td>P0035</td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>10</td>
                                                <td>$25,500</td>
                                                <td>$1,500</td>
                                                <td>Recolectados del cliente,<br>en propiedad de Stadibox</td>
                                            </tr>
                                            <tr onclick="document.location = 'transaccion.php'">
                                                <td>822</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>SuperBowl</b><br><small>13/05/18</small></td>
                                                <td>Azteca</td>
                                                <td>P0035</td>
                                                <td><b>Meza Sports</b></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>2</td>
                                                <td>$25,500</td>
                                                <td>N/A</td>
                                                <td>Pago Procesado</td>
                                            </tr>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    </div>
                    <!-- FINALIZA TRANSACCIONES -->
                    <!-- HISTORIAL DE VENTAS -->
                    <div class="table tab-pane" id="historial" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;padding-top: 2%;">
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-purple">
                                            <div class="inner">
                                                <h3>210</h3>
                                                <p>Total de Pases Vendidos</p>
                                            </div>
                                            <div class="icon icono_2">
                                                <i class="ion ion-bag"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-purple">
                                            <div class="inner">
                                                <h3>115</h3>
                                                <p>Total de Transacciones Realizadas</p>
                                            </div>
                                            <div class="icon icono_2">
                                                <i class="ion ion-stats-bars"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-purple">
                                            <div class="inner">
                                                <h3>$47,500</h3>
                                                <p>Monto en Ventas</p>
                                            </div>
                                            <div class="icon icono_2">
                                                <i class="ion ion-social-usd"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-purple">
                                            <div class="inner">
                                                <h3>68<sup style="font-size: 20px">%</sup></h3>
                                                <p>Porcentaje de Ventas Sobre Publicados</p>
                                            </div>
                                            <div class="icon icono_2">
                                                <i class="ion ion-pie-graph"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                </div>
                                
                                <!-- BOXES 2 -->
                                <div class="row">
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$210,000</h3>
                                                <p>Por Pagar Propietarios</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$115,000</h3>
                                                <p>Pagado a Propietarios</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$47,500</h3>
                                                <p>Depósitos de Garantía</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$150,000</h3>
                                                <p>Ganancia de Stadibox</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$42,500</h3>
                                                <p>Alimentos y Bebidas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-2 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>$7,500</h3>
                                                <p>Transporte</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                </div>
                                <!-- /.row -->
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte General de Ventas</h3>
                                        
                                        <div class="box-tools tools_historial" style="width: 80%;">
                                            <div class="form-group form-group-sm">
                                                <select class="form-control" style="width: 24%;float: left;">
                                                    <option>Foro 1</option>
                                                    <option>Foro 2</option>
                                                    <option>Foro 3</option>
                                                    <option>Foro 4</option>
                                                    <option>Foro 5</option>
                                                </select>
                                                <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                                    <option>Otro 1</option>
                                                    <option>Otro 2</option>
                                                    <option>Otro 3</option>
                                                    <option>Otro 4</option>
                                                    <option>Otro 5</option>
                                                </select>
                                                <!-- Date range -->
                                                <div class="input-group" style="width: 17%; margin-left: 1%;float: left;">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="reservation">
                                                </div>
                                                <!-- /.input group -->
                                                <input type="text" class="form-control" placeholder="ID, No. Palco, Evento, Foro, Correo, Titular..." style="float: left;width: 24%; margin-left: 1%;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Fecha de Compra</th>
                                                <th style="text-align: center">Evento</th>
                                                <th style="text-align: center;">Propiedad o Paquete</th>
                                                <th style="text-align: center">Titular</th>
                                                <th style="text-align: center">Comprador</th>
                                                <th style="text-align: center">No. de Boletos</th>
                                                <th style="text-align: center">Monto</th>
                                            </tr>
                                            <tr class="alerta" onclick="document.location = 'transaccion_experiencia.php'">
                                                <td>183</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>América vs. Pachuca</b><br><small>4/02/18</small></td>
                                                <td><b>P0035</b></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>10</td>
                                                <td><b>$25,500</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'transaccion_experiencia.php'">
                                                <td>183</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>Shakira</b><br><small>10/02/18</small></td>
                                                <td><b>P0035</b></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>10</td>
                                                <td><b>$25,500</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'transaccion_experiencia.php'">
                                                <td>183</td>
                                                <td><b>01/02/18</b><br><small>14:13</small></td>
                                                <td><b>SuperBowl</b><br><small>6/02/18</small></td>
                                                <td><b>P0035</b></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td><b>Nombre Apellido</b><br><small>correo@correo.com</small></td>
                                                <td>10</td>
                                                <td><b>$25,500</b></td>
                                            </tr>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    </div>
                    <!-- TERMINA HISTORIAL DE VENTAS -->
                    <!-- RENTA ANUAL -->
                    <div class="table tab-pane" id="renta" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Renta Anual</h3>
                                        
                                        <div class="box-tools">
                                            <div class="form-group form-group-sm" style="width: 150px;">
                                                <select class="form-control">
                                                    <option>Foro 1</option>
                                                    <option>Foro 2</option>
                                                    <option>Foro 3</option>
                                                    <option>Foro 4</option>
                                                    <option>Foro 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Propietario</th>
                                                <th style="text-align: center">Cliente</th>
                                                <th style="text-align: center">Foro</th>
                                                <th style="text-align: center;">Tipo de Propiedad</th>
                                                <th style="text-align: center">No. de Propiedad</th>
                                                <th style="text-align: center">Pases</th>
                                                <th style="text-align: center">Parking</th>
                                                <th style="text-align: center">Parking VIP</th>
                                                <th style="text-align: center">Modo</th>
                                                <th style="text-align: center">Precio Propietario</th>
                                                <th style="text-align: center">Precio Publicado</th>
                                                <th style="text-align: center">Inicio Renta</th>
                                                <th style="text-align: center">Terminación Renta</th>
                                                <th style="text-align: center">Tiempo Restante</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>
                                            <tr>
                                                <td>456</td>
                                                <td>Diego Kawas</td>
                                                <td>Israel Baca</td>
                                                <td>Estadio Azteca</td>
                                                <td>Palco</td>
                                                <td>3099</td>
                                                <td>10</td>
                                                <td>1</td>
                                                <td>0</td>
                                                <td>Completo</td>
                                                <td>$100,000.00</td>
                                                <td>$120,000.00</td>
                                                <td>01/01/2018</td>
                                                <td>01/01/2019</td>
                                                <td>350 días</td>
                                                <td><a href="propiedad.php" class="btn btn-info"><i class="fa fa-chevron-right"></i></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- TERMINA RENTA ANUAL -->
                    <!-- LOGÍSTICA -->
                    <div class="table tab-pane" id="logistica" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Logística</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: left;">
                                            <tr>
                                                <th style="text-align: left">Concepto</th>
                                                <th style="text-align: left"></th>
                                            </tr>
                                            <tr>
                                                <td>Viajes Exitosos</td>
                                                <td>20</td>
                                            </tr>
                                            <tr>
                                                <td>Viajes No Exitosos</td>
                                                <td>20</td>
                                            </tr>
                                            <tr>
                                                <td>Pérdida de Boletos</td>
                                                <td>20</td>
                                            </tr>
                                            <tr>
                                                <td>Viajes Reprogramados</td>
                                                <td>20</td>
                                            </tr>
                                        </table>
                                    </div>      
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="float: right;margin: 1%;">
                                            <a href="logistica.php" class="btn btn-success">Ir a Logística</a>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- TERMINA LOGÍSTICA -->
                    <!-- PROPIEDADES -->
                    <div class="table tab-pane" id="venta" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Propiedades</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">Foro</th>
                                                <th style="text-align: center">Palcos Registrados</th>
                                                <th style="text-align: center;">Plateas Registradas</th>
                                                <th style="text-align: center">Palcos por Evento</th>
                                                <th style="text-align: center;">Palcos Administrados</th>
                                                <th style="text-align: center;">Palcos en Renta Anual</th>
                                                <th style="text-align: center">Plateas por Evento</th>
                                                <th style="text-align: center">Plateas Administradas</th>
                                                <th style="text-align: center;">Plateas en Renta Anual</th>
                                                <th style="text-align: center">Propiedades En Venta</th>
                                                <th style="text-align: center">Vendidas</th>
                                            </tr>
                                            <tr>
                                                <td><b>Estadio Azteca</b></td>
                                                <td>100</td>
                                                <td>60</td>
                                                <td>90</td>
                                                <td>8</td>
                                                <td>2</td>
                                                <td>60</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>2</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td><b>Estadio Chivas</b></td>
                                                <td>100</td>
                                                <td>60</td>
                                                <td>90</td>
                                                <td>8</td>
                                                <td>2</td>
                                                <td>60</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>2</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td><b>Acrópolis Puebla</b></td>
                                                <td>100</td>
                                                <td>60</td>
                                                <td>90</td>
                                                <td>8</td>
                                                <td>2</td>
                                                <td>60</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>2</td>
                                                <td>4</td>
                                            </tr>
                                            <tr>
                                                <td><b>Arena CDMX</b></td>
                                                <td>100</td>
                                                <td>60</td>
                                                <td>90</td>
                                                <td>8</td>
                                                <td>2</td>
                                                <td>60</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>2</td>
                                                <td>4</td>
                                            </tr>
                                        </table>
                                    </div>      
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="float: right;margin: 1%;">
                                            <a href="propiedades.php" class="btn btn-success">Ver Todas las Propiedades</a>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- TERMINA PROPIEDADES -->
                    <!-- USUARIOS -->
                    <div class="table tab-pane" id="usuarios" style="position: relative;">
                        <div class="row">
                            <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Reporte de Usuarios</h3>
                                        
                                        <div class="box-tools tools_usuarios" style="width: 80%;right: -9px !important;">
                                            <div class="form-group form-group-sm">
                                                <select class="form-control" style="width: 24%;float: left;">
                                                    <option>Tipo de Usuario 1</option>
                                                    <option>Tipo de Usuario 2</option>
                                                    <option>Tipo de Usuario 3</option>
                                                    <option>Tipo de Usuario 4</option>
                                                    <option>Tipo de Usuario 5</option>
                                                </select>
                                                <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                                    <option>Foro 1</option>
                                                    <option>Foro 2</option>
                                                    <option>Foro 3</option>
                                                    <option>Foro 4</option>
                                                    <option>Foro 5</option>
                                                </select>
                                                <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                                    <option>Estatus 1</option>
                                                    <option>Estatus 2</option>
                                                    <option>Estatus 3</option>
                                                    <option>Estatus 4</option>
                                                    <option>Estatus 5</option>
                                                </select>
                                                <input type="text" class="form-control" placeholder="ID, No. Palco, Evento, Foro, Correo, Titular..." style="float: left;width: 24%; margin-left: 1%;">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" style="text-align: center;">
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Nombre</th>
                                                <th style="text-align: center">Apellido</th>
                                                <th style="text-align: center;">Correo</th>
                                                <th style="text-align: center">Tipo</th>
                                                <th style="text-align: center">Propiedades</th>
                                                <th style="text-align: center">Estatus</th>
                                            </tr>
                                            <tr onclick="document.location = 'usuario.php'">
                                                <td>0001</td>
                                                <td><b>Andrés</b></td>
                                                <td><b>Montero</b></td>
                                                <td>aemg500@gmail.com</td>
                                                <td><b>Propietario</b></td>
                                                <td>2 palcos</td>
                                                <td class="activo"><b>Activo</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'usuario.php'">
                                                <td>0002</td>
                                                <td><b>Israel</b></td>
                                                <td><b>Baca</b></td>
                                                <td>diego@gmail.com</td>
                                                <td><b>Propietario</b></td>
                                                <td>5 palcos<br>1 grupo de plateas</td>
                                                <td class="bloqueado"><b>Bloqueado</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'usuario.php'">
                                                <td>0003</td>
                                                <td><b>María</b></td>
                                                <td><b>de Buen</b></td>
                                                <td>maria@gmail.com</td>
                                                <td><b>Propietario</b></td>
                                                <td>1 palco</td>
                                                <td class="activo"><b>Activo</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'usuario.php'">
                                                <td>0004</td>
                                                <td><b>Fernando</b></td>
                                                <td><b>Estañol</b></td>
                                                <td>fernando@gmail.com</td>
                                                <td><b>Cliente</b></td>
                                                <td>N/A</td>
                                                <td class="activo"><b>Activo</b></td>
                                            </tr>
                                            <tr onclick="document.location = 'usuario.php'">
                                                <td>0004</td>
                                                <td><b>Alan</b></td>
                                                <td><b>Torres</b></td>
                                                <td>alant@gmail.com</td>
                                                <td><b>Cliente</b></td>
                                                <td>N/A</td>
                                                <td class="activo"><b>Activo</b></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                    <!-- TERMINA USUARIOS -->
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </section>
        <!-- wrapper Main Content -->
    </div>

@endsection