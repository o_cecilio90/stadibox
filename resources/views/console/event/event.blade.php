@extends('console.layouts.main')

@section('title', 'Eventos')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agregar Evento
            <small>Stadibox</small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="eventos.php"><i class="fa fa-futbol-o"></i> Eventos</a></li>
            <li class="active">Agregar Evento</li>
        </ol>
    </section>

    <!-- Main content -->            
    <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
        @if($method == 'create')
        {!! Form::open(['route' => 'evento.store', 'method' => 'POST', 'files'=>true]) !!}
        @elseif($method == 'edit')
        {!! Form::open(['route' => ['evento.update',$event->id], 'method' => 'PUT', 'files'=>true]) !!}
        @endif
        <!-- LEFT COLUMN -->
        <div class="col-md-8">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table tabla_agregar_evento">
                        <tr>
                            <td style="width: 30%;">
                                <div class="form-group">
                                    <img src="{!! isset($event->image) ? asset($event->image) : asset('console/dist/img/boxed-bg.jpg')!!}" id="show_image" style="width: 80%;"><br>
                                    <label for="image">Imagen del Evento</label>
                                    <input type="file" id="image" name="image">

                                    <p class="help-block">50x50px</p>
                                </div>
                            </td>
                            <td>
                                <form role="form">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nombre del Evento:</label>
                                        <input type="text" class="form-control" placeholder="Nombre del evento..." maxlength="120" name="name" value="{!! old('name',$event->name) !!}">
                                        <br>
                                        <label>Fecha y Hora:</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="date" class="form-control pull-right" id="datepicker" value="{!! old('namedate',$event->date) !!}">
                                        </div><br>
                                        <div class="bootstrap-timepicker">
                                            <label>Time picker:</label>

                                            <div class="input-group">
                                                <input type="text" name="time" class="form-control timepicker" value="{!! old('time',$event->time) !!}">
                                                
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="featured_event" {!! isset($event->featured_event) && $event->featured_event == 1 ? 'checked' : ''!!}>
                                                Evento Destacado
                                            </label>
                                        </div>
                                        <label>Estatus:</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="status" id="optionsRadios1" value=1 {!! isset($event->status) && $event->status == 1 ? 'checked' : ''!!}>
                                                Activo
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="status" id="optionsRadios2" value=0 {!! isset($event->status) && $event->status == 0 ? 'checked' : ''!!}>
                                                En Pausa
                                            </label>
                                        </div>
                                        <label>ALT Imagen:</label>
                                        <input type="text" id="alt_image" name="alt_image" class="form-control" placeholder="Alt Imagen..." value="{!! old('alt_image',$event->alt_image) !!}">
                                        <!-- /.input group -->
                                    </div>
                                </form>
                                <!-- /.form group -->
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                <!--<div class="form-group">
                                    <img src="{!! isset($event->alt_image) ? asset($event->alt_image) : asset('console/dist/img/boxed-bg.jpg')!!}" id="show_alt_image" style="width: 80%;">
                                </div>-->
                            </td>
                            <td>
                                <div class="form-group">
                                    <label>Ubicación:</label><br>
                                    <select class="form-control col-xs-3" id="country_country" name="country_id" style="width: 32%;">
                                        <option value="">País</option>
                                    </select>
                                    <select class="form-control col-xs-3" id="country_city" name="city_id" style="width: 32%;margin-left: 1%;">
                                        <option value="">Ciudad</option>
                                    </select>
                                    <select class="form-control col-xs-3" id="country_forum" name="forum_id" style="width: 32%;margin-left: 1%;">
                                        <option value="">Foro</option>
                                    </select><br><br><br>
                                    
                                    <label>Categoría:</label>
                                    <select class="form-control select2" multiple="multiple" data-placeholder="Selecciona las Categorías del Evento" name="category[]">
                                        <option {!!isset($event->category) && in_array("Concierto", $event->category) ? 'selected' : ''!!} value="Concierto">Concierto</option>
                                        <option {!!isset($event->category) && in_array("NFL", $event->category) ? 'selected' : ''!!} value="NFL">NFL</option>
                                        <option {!!isset($event->category) && in_array("Futbol", $event->category) ? 'selected' : ''!!} value="Futbol">Futbol</option>
                                        <option {!!isset($event->category) && in_array("USA", $event->category) ? 'selected' : ''!!} value="USA">USA</option>
                                        <option {!!isset($event->category) && in_array("Mexico", $event->category) ? 'selected' : ''!!} value="Mexico">México</option>
                                        <option {!!isset($event->category) && in_array("Informativo", $event->category) ? 'selected' : ''!!} value="Informativo">Informativo</option>
                                    </select><br><br>
                                    
                                    <label>Palabras Clave:</label>
                                    <select class="form-control select2" multiple="multiple" data-placeholder="Selecciona las Palabras Clave del Evento" name="keywords[]">
                                        <option {!!isset($event->keywords) && in_array("Deportivo", $event->keywords) ? 'selected' : ''!!} value="Deportivo">Deportivo</option>
                                        <option {!!isset($event->keywords) && in_array("Concierto", $event->keywords) ? 'selected' : ''!!} value="Concierto">Concierto</option>
                                        <option {!!isset($event->keywords) && in_array("NFL", $event->keywords) ? 'selected' : ''!!} value="NFL">NFL</option>
                                        <option {!!isset($event->keywords) && in_array("Futbol", $event->keywords) ? 'selected' : ''!!} value="Futbol">Futbol</option>
                                        <option {!!isset($event->keywords) && in_array("USA", $event->keywords) ? 'selected' : ''!!} value="USA">USA</option>
                                        <option {!!isset($event->keywords) && in_array("Mexico", $event->keywords) ? 'selected' : ''!!} value="Mexico">México</option>
                                        <option {!!isset($event->keywords) && in_array("Informativo", $event->keywords) ? 'selected' : ''!!} value="Informativo">Informativo</option>
                                    </select><br><br>
                                    
                                    <label>Límite de Precio:</label>
                                    <!-- input states -->
                                    <div class="form-group has-success">
                                        <label class="control-label" for="inputSuccess" style="float: left;" ><i class="fa fa-thumbs-up"></i></label>
                                        <input type="text" class="form-control" id="inputSuccess" placeholder="Inferior..." name="lower_topLimit" value="{!! old('lower_topLimit',$event->lower_topLimit) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                        <input type="text" name="upper_topLimit" class="form-control" id="inputSuccess" placeholder="Superior..." value="{!! old('upper_topLimit',$event->upper_topLimit) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                    </div><br><br>
                                    <div class="form-group has-warning">
                                        <label class="control-label" for="inputWarning" style="float: left;"><i class="fa fa-exclamation-triangle"></i></label>
                                        <input type="text" name="lower_middleLimit1" class="form-control" id="inputWarning" placeholder="Inferior..." value="{!! old('lower_middleLimit1',$event->lower_middleLimit1) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                        <input type="text" name="upper_middleLimit1" class="form-control" id="inputWarning" placeholder="Superior..." value="{!! old('upper_middleLimit1',$event->upper_middleLimit1) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                    </div><br><br>
                                    <div class="form-group has-warning">
                                        <label class="control-label" for="inputWarning" style="float: left;"><i class="fa fa-exclamation-triangle"></i></label>
                                        <input type="text" name="lower_middleLimit2" class="form-control" id="inputWarning" placeholder="Inferior..." value="{!! old('lower_middleLimit2',$event->lower_middleLimit2) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                        <input type="text" name="upper_middleLimit2" class="form-control" id="inputWarning" placeholder="Superior..." value="{!! old('upper_middleLimit2',$event->upper_middleLimit2) !!}" style="width: 47%;float: left;margin-left: 1%;">
                                    </div><br><br>
                                    <div class="form-group has-error">
                                        <label class="control-label" for="inputError" style="float: left;"><i class="fa fa-thumbs-down"></i></label>
                                        <input type="text" name="lower_downLimit" class="form-control" id="inputError" placeholder="Inferior..." value="{!! old('lower_downLimit',$event->lower_downLimit) !!}" style="float: left;width: 47%;margin-left: 1%;">
                                        <input type="text" name="upper_downLimit" class="form-control" id="inputError" placeholder="Superior..." value="{!! old('upper_downLimit',$event->upper_downLimit) !!}" style="float: left;width: 47%;margin-left: 1%;">
                                    </div><br><br>
                                    
                                    <label>Porcentaje de Ganancia:</label>
                                    <input type="text" name="percentage_profit" class="slider form-control" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[0,25]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue" value="{!! old('percentage_profit',$event->percentage_profit) !!}">
                                    
                                    <label>Aplicar Descuento sobre el Total:</label>
                                    <input type="text" value="{!! old('discount',$event->discount) !!}" name="discount" class="slider form-control" data-slider-max="25" data-slider-step="1" data-slider-value="[0,0]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- END OF LEFT COLUMN -->
        <!-- RIGHT COLUMN -->
        <div class="col-md-2" style="text-align: center;">
            <div class="box agregar_right" style="width: 110%;padding: 5% 0%;">
                <!-- /.box-header -->
                <button type="submit" class="btn btn-block btn-success" style="width: 90%;margin: 0% auto;">Publicar</button>
                {!! Form::close() !!}
                @if($method == 'edit')                
                    <button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default" style="margin-top: 4%;width: 90%;">Eliminar Evento</button>             
                @endif
            </div>
            @include('console.event.modals.event')
        </div>
        <!-- END OF RIGHT COLUMN -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
<script>
    $(function () {
        /* BOOTSTRAP SLIDER */
        $('.slider').slider()
    })
</script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    })
</script>
<!--FILE PREVIEW-->
<script type="text/javascript">
    $(function() {
        //ZONE
        $('#image').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = imageOnload;
            reader.readAsDataURL(file); 
        });

        function imageOnload(e) {
            let result=e.target.result;
            $('#show_image').attr("src",result);
        }

        $('#alt_image').change(function(e) {
             let file = e.target.files[0],
            imageType = /image.*/;

            if (!file.type.match(imageType))
                return;

            let reader = new FileReader();
            reader.onload = alt_imageOnload;
            reader.readAsDataURL(file); 
        });

        function alt_imageOnload(e) {
            let result=e.target.result;
            $('#show_alt_image').attr("src",result);
        }
    });
</script>
<!--SELECT-->
<script type="text/javascript">
    // Country
    let country;
    function selectCountry() {
        $.ajax({
            type: "GET",
            url: '{{ route('country.index') }}', 
            dataType: "json",
            success: function(data){
                country = data;
                $('#country_country option').remove();
                $('#country_country').append('<option>País</option>');
                $('#country_country_edit option').remove();
                $('#country_country_edit').append('<option>País</option>');
                $.each(data,function(key, registro) {
                    $('#country_country').append('<option value='+registro.id+'>'+registro.country+'</option>');
                    $('#country_country_edit').append('<option value='+registro.id+'>'+registro.country+'</option>');
                });
                if("{!!isset($event->country_id)!!}"){
                    loadCountryEvent();
                }

            },
            error: function(data) {
                alert('error');
            }
        });
    };
    $("#country_country").change(function() {
        // get the id of the option which has the country code.
       let countryCode = $("#country_country")
          .children(":selected").attr("value");
        $.each(country,function(key, registro) {
            if (registro.id == countryCode) {                
                $('#country_city option').remove();
                $('#country_city').append('<option>Ciudad</option>');
                $.each(registro.cities, function(keyCity, argument) {
                    $('#country_city').append('<option value='+argument.id+'>'+argument.city+'</option>');
                });
            }
        });
    });
    $("#country_city").change(function() {
        let cityCode = $("#country_city")
          .children(":selected").attr("value");
        let route = '{{ route("selectForums", ":id") }}';
        route = route.replace(':id', cityCode);
        $.ajax({
            type: "GET",
            url: route, 
            dataType: "json",
            success: function(data){
                $('#country_forum option').remove();
                $('#country_forum').append('<option value="">Foro</option>');
                $.each(data, function(key, element) {
                    $('#country_forum').append('<option value='+element.id+'>'+element.forum_name+'</option>');
                });
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
    $(function() {
        selectCountry();
    });
    
    let loadCountryEvent = ()=>{
        let countryCode =  "{!!$event->country_id!!}";
        let cityCode =  "{!!$event->city_id!!}";
        $('#country_country').val("{!!$event->country_id!!}");
        
        $.each(country,function(key, registro) {
            if (registro.id == countryCode) {                
                $('#country_city option').remove();
                $('#country_city').append('<option>Ciudad</option>');
                $.each(registro.cities, function(keyCity, argument) {
                    $('#country_city').append('<option value='+argument.id+'>'+argument.city+'</option>');
                });
            }
        });
        $('#country_city').val(cityCode);
        let route = '{{ route("selectForums", ":id") }}';
        route = route.replace(':id', cityCode);
        $.ajax({
            type: "GET",
            url: route, 
            dataType: "json",
            success: function(data){
                $('#country_forum option').remove();
                $('#country_forum').append('<option value="">Foro</option>');
                $.each(data, function(key, element) {
                    $('#country_forum').append('<option value='+element.id+'>'+element.forum_name+'</option>');
                });
                $('#country_forum').val("{!!$event->forum_id!!}")
            },
            error: function(data) {
                console.log(data);
            }
        });


    }
    
</script>
@endsection