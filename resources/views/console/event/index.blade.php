@extends('console.layouts.main')

@section('title', 'Eventos')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Eventos
            <small>Stadibox</small>
        </h1><br>
        <a class="btn btn-app" style="background-color: #77c510;color: white;" href="{!!route('evento.create')!!}">
            <i class="fa fa-plus"></i> Agregar Evento
        </a>
        <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Eventos</li>
        </ol>
    </section>

    <!-- Main content -->            
    <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
        <!-- LEFT COLUMN -->
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Eventos en la Página de Stadibox</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar evento...">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover" id="table_city" style="text-align: center;">
                    	<thead>
                    		<tr>
	                            <th style="text-align: center;">Evento</th>
	                            <th style="text-align: center;">Fecha</th>
	                            <th style="text-align: center;">Lugar</th>
	                            <th style="text-align: center;">Foro</th>
	                            <th style="text-align: center;">Categoría</th>
	                            <th style="text-align: center;">Estatus</th>
	                            <th style="text-align: center;"></th>
	                        </tr>
                    	</thead>
                    	<tbody>                    		
                    	</tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- END OF LEFT COLUMN -->
        @include('console.event.modals.event')
        <!-- RIGHT COLUMN -->
        <div class="col-md-2" style="text-align: center;">
            <div class="box filtros" style="width: 110%;padding: 1% 0%;">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtrar Resultados</h3>
                </div>
                <!-- /.box-header -->
                <button type="button" class="btn btn-block btn-success" style="width: 90%;margin: 0% auto;">Aplicar</button>
                <a href="#" style="color: #77c510;">Limpiar Filtros</a>
                <!-- checkbox -->
                <div class="form-group" style="text-align: left;">
                    <h4 class="hcuatro">Ubicación:</h4>
                    <!-- select -->
                    <select class="form-control" style="width: 90%;margin: 0% auto;">
                        <option>Todos los Países</option>
                    </select><br>
                    <select class="form-control" style="width: 90%;margin: 0% auto;">
                        <option>Todas las Ciudades</option>
                    </select><br><br>
                    <h4 class="hcuatro foro_h4" style="margin-top: -9%;">Foro:</h4>
                    <div class="checkbox" style="padding-left: 6%;">
                        <label>
                            <input type="checkbox">
                            Estadio Azteca<br>
                            <input type="checkbox">
                            Estadio Cuauhtémoc<br>
                            <input type="checkbox">
                            Arena CDMX<br>
                            <input type="checkbox">
                            Estadio BBVA<br>
                            <input type="checkbox">
                            Estadio Lobos BUAP<br>
                        </label>
                    </div>
                    <h4 class="hcuatro">Categoría:</h4>
                    <div class="checkbox" style="padding-left: 6%;">
                        <label>
                            <input type="checkbox">
                            Futbol<br>
                            <input type="checkbox">
                            NFL<br>
                            <input type="checkbox">
                            Conciertos<br>
                            <input type="checkbox">
                            Toros<br>
                            <input type="checkbox">
                            Todas<br>
                        </label>
                    </div>
                    <h4 class="hcuatro">Estatus:</h4>
                    <div class="checkbox" style="padding-left: 6%;">
                        <label>
                            <input type="checkbox">
                            Activo<br>
                            <input type="checkbox">
                            Pausado<br>
                            <input type="checkbox">
                            Finalizado<br>
                        </label>
                    </div>
                    <h4 class="hcuatro">Clase:</h4>
                    <div class="checkbox" style="padding-left: 6%;">
                        <label>
                            <input type="checkbox">
                            Eventos<br>
                            <input type="checkbox">
                            Experiencias VIP<br>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- END OF RIGHT COLUMN -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
<!--Event-->
<script type="text/javascript">
	//DATATABLE ROL
    $(function(){
        $('#table_city').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('evento.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'role-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'name' , name: 'name'},
                { data: 'date', name: 'date'},
                { data: 'country.country', name: 'country'},
                { data: 'forum.forum_name', name: 'forum'},
                { data: 'category', name: 'category'},
                { data: 'status', name: 'status'},
            ],
            columnDefs :[
            	{ targets : [0],
                    render : function (data, type, row) {                        
                        return '<td><b>'+data+'</b></td>';
                    }
                },
                { targets : [1],
                    render : function (data, type, row) {                        
                        return '<td><b>'+data+'</b><br>'+row.time+'</td>';
                    }
                },
                { targets : [2],
                    render : function (data, type, row) {                        
                        return '<td><b>'+data+'</b><br>'+row['city'].city+'</td>';
                    }
                },
                { targets : [3],
                    render : function (data, type, row) {                        
                        return '<td><b>'+data+'</b></td>';
                    }
                },
                { targets : [4],
                    render : function (data, type, row) {                        
                        return data.replace(';',',');
                    }
                },
                { targets : [5],
                    render : function (data, type, row) {
                        let result = null;
                        if (data == 1) {
                        	result = '<b class="activo">Activo</b>';
                        }else{
                        	result = '<b class="bloqueado">No Activo</b>';
                        }
                        
                        return result;
                    }
                },
                { targets : [6],
                    render : function (data, type, row) {
                        let route = '{{ route("evento.edit", ":id") }}';
                        route = route.replace(':id', row.id);
                        return '<a type="button" href="'+route+'" id="edit_event" class="btn btn-default bg-navy" id="'+row['id']+'">'+
                                '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                            '</a>\n'+
                            '<button type="button" id="delete_event" class="btn btn-danger" data-toggle="modal" data-target="#modal-default" data-id="'+row['id']+'">'+
                                '<i class="fa fa-times-circle" aria-hidden="true"></i>'+
                            '</button>';
                    }
                }
            ]
        });

        // delete a system user
        $(document).on('click', '#delete_event', function() {
            $('#event_id').val($(this).data('id'));
            id = $('#event_id').val();
        });
        $('#form_delete_event').on('submit', function(e) {
            e.preventDefault();
            // agrego la data del form a formData
            let formData = new FormData(this);
            let route = '{{ route("evento.destroy", ":id") }}';
            route = route.replace(':id', id);
            console.log(route);
            $.ajax({
                type: 'POST',
                url: route,
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
                    $('#table_city').DataTable().ajax.reload();
                    $('#modal-default').modal('hide');
                },error: function(data) {
                    console.log(data);
                }
            });
        });
    });
</script>
@endsection