@extends('console.layouts.main')

@section('title', 'Configuración')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Configuración</h1>
        <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Configuración</li>
        </ol>
        <!-- /.content -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
                <li><a href="#diseno" data-toggle="tab">Diseño</a></li>
                <li><a href="#usuarios" data-toggle="tab">Usuarios Sistema</a></li>
                <li class="active"><a href="#perfiles" data-toggle="tab">Perfiles</a></li>
                <li class="pull-left header"><i class="fa fa-cog"></i> Configuración</li>
            </ul>
            <div class="tab-content no-padding">
                <!-- CONTENIDO DE LAS TABS -->
                <!-- PERFILES -->
                <div class="table tab-pane active" id="perfiles" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Perfiles</h3>
                                    
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="width: 150px;">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default" style="width: 100%;">
                                                Agregar Perfil
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id="table_role" style="text-align: center;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">ID</th>
                                                <th style="text-align: center;">Nombre</th>
                                                <th style="text-align: center;">Usuarios</th>
                                                <th style="text-align: center;">Teléfono</th>
                                                <th style="text-align: center;">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                @include('console.configuration.modals.role.role')
                                @include('console.configuration.modals.role.edit')
                                @include('console.configuration.modals.role.delete')
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA PERFILES -->
                <!-- USUARIOS SISTEMA -->
                <div class="table tab-pane" id="usuarios" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Usuarios Sistema</h3>
                                    
                                    <div class="box-tools tools_usuarios2">
                                        <div class="form-group form-group-sm" style="width: 640px;">
                                            <select class="form-control" style="float: left;width: 20%;">
                                                <option>Todos los Perfiles</option>
                                                <option>Perfil 2</option>
                                                <option>Perfil 3</option>
                                                <option>Perfil 4</option>
                                                <option>Perfil 5</option>
                                            </select>
                                            <select class="form-control" style="float: left;width: 20%;margin-left: 2%;">
                                                <option>Pais 1</option>
                                                <option>Pais 2</option>
                                                <option>Pais 3</option>
                                                <option>Pais 4</option>
                                                <option>Pais 5</option>
                                            </select>
                                            <select class="form-control" style="float: left; width: 20%;margin-left: 2%;">
                                                <option>Ciudad 1</option>
                                                <option>Ciudad 2</option>
                                                <option>Ciudad 3</option>
                                                <option>Ciudad 4</option>
                                                <option>Ciudad 5</option>
                                            </select>
                                            <button class="btn btn-success" data-toggle="modal" data-target="#modal-usuario" style="width: 20%;margin-left: 2%;">
                                                Agregar Usuario
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table id="table_systemUser" class="table table-hover" style="text-align: center;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center">ID</th>
                                                <th style="text-align: center">Username</th>
                                                <th style="text-align: center">Password</th>
                                                <th style="text-align: center;">Nombre</th>
                                                <th style="text-align: center">Perfil</th>
                                                <th style="text-align: center">País</th>
                                                <th style="text-align: center">Ciudad</th>
                                                <th style="text-align: center">Acciones</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody></tbody>                                        
                                    </table>
                                    @include('console.configuration.modals.system_user.user')
                                	@include('console.configuration.modals.system_user.delete')
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA USUARIOS SISTEMA -->
                <!-- DISEÑO -->
                <div class="table tab-pane" id="diseno" style="position: relative">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;padding-bottom: 2%;">
                            @csrf
                            <h4>Editar Diseño del Sitio</h4>
                            <br>
                            <p><b>Foto de Fondo de Home:</b></p>
                            <img src="{{asset($home->image)}}" id="show_image" style="border:2px solid #77c510;"><br><br>
                            <input type="file" id="image" name="image" class="form-control">
                            <br>

                            <p><b>Tabs del HomePage</b></p>
                            @foreach($home->tab as $key => $value)
                            @if($value != '')
                            <div class="col-xs-2">
                                <input type="text" class="form-control" placeholder="Tab {{$key +1}}..." id="tab" name="{!!$key!!}" value="{!!$value!!}">
                            </div>
                            @endif
                            @endforeach
                            
                            <p style="clear:left;margin-top:4%;"><b>Experiencias VIP en el HomePage</b></p>
                            <input type="checkbox" id="vip" name="vip" {!! $home->vip ? 'checked' : '' !!} data-toggle="toggle" data-onstyle="success">
                            <br><br>
                            
                            <p><b>Pop-Up de HomePage</b></p>
                            <div class="banner_info">
                                <div class="col-xs-4">
                                    <img src="{{asset($home->pop_image)}}" id="show_pop_image" style="border: 2px solid #77c510;"><br><br>
                                    <input type="file" id="pop_image" name="pop_image" class="form-control">
                                </div>
                                <div class="col-xs-8">
                                	<input type="checkbox" id="pop_status" name="pop_status" {!! $home->pop_status ? 'checked' : '' !!} data-toggle="toggle" data-onstyle="success">
                                    <br><br>
                                    <p><b>Texto Principal:<small> (140 caracteres)</small></b></p>
                                    <textarea class="form-control" id="pop_main_text" name="pop_main_text" placeholder="Texto principal del banner...">{!!$home->pop_main_text!!}</textarea>
                                    <br>
                                    <p><b>Call-to-Action:</b></p>
                                    <input type="text" id="pop_call_to_action" name="pop_call_to_action" value="{!! $home->pop_call_to_action !!}" class="form-control" placeholder="Call-to-Action" style="width: 40%;float: left;">
                                    <p style="float: left;width: 7%;margin-left: 2%;">Dirige a:</p>
                                    <input type="text" id="pop_url" name="pop_url" value="{!! $home->pop_url !!}" class="form-control" placeholder="Ingresar URL..." style="width: 51%;float: left;">
                                    <br><br><br>
                                    <p><b>Nota Opcional: <small>(140 caracteres)</small></b></p>
                                    <textarea class="form-control" id="pop_optional_note" name="pop_optional_note" placeholder="Texto opcional del banner...">{!!$home->pop_optional_note!!}</textarea>
                                </div>
                            </div>
                            <br>
                            
                            <p><b>Banners Promocionales:</b>
                                <button class="btn btn-success pull-right" data-toggle="modal" data-target="modal-agregar-banner" style="clear: right;" onclick="createBanner()">
                                Agregar Otro
                            </button>
                            </p>
                            <div id="newBanner">
                            @foreach($banner as $key => $value)
                            <div class="banner_info" id="{!!$value->id!!}">
                                <div class="col-xs-4">
                                    <img src="{{asset($value->image)}}" id="show_banner_image_{!!$value->id!!}" style="border: 2px solid #77c510;"><br><br>
                                    <input type="file" id="banner_image_{!!$value->id!!}" class="form-control" onchange="bannerImage('{!!$value->id!!}','image','banner_image_{!!$value->id!!}',this)">
                                </div>
                                <div class="col-xs-8">
                                    <button type="button" onclick="deleteBanner('{!!$value->id!!}')" id="banner_delete" class="btn btn-danger pull-right">
                                        <i class="fa fa-times-circle" style="font-size: 1.2em;"></i>
                                    </button><br>
                                    <p><b>Posición:</b></p>
                                    <select class="form-control" id="banner_position_{!!$value->id!!}" style="width: 20%;" onchange="editBanner('{!!$value->id!!}','position',$('#banner_position_{!!$value->id!!}').val())">
                                        @for($i=1;$i<=20;$i++)
                                        <option {!!$value->position == $i ? 'selected':''!!} value="{!!$i!!}">{!!$i!!}</option>
                                        @endfor
                                    </select>
                                    <br>
                                    <p><b>Texto Principal:<small> (140 caracteres)</small></b></p>
                                    <textarea class="form-control" id="banner_main_text_{!!$value->id!!}" placeholder="Texto principal del banner..." onchange="editBanner('{!!$value->id!!}','main_text',$('#banner_main_text_{!!$value->id!!}').val())" >{!!$value->main_text!!}</textarea>
                                    <br>
                                    <p><b>Call-to-Action:</b></p>
                                    <input type="text" id="banner_call_to_action_{!!$value->id!!}" class="form-control" placeholder="Call-to-Action" style="width: 40%;float: left;" value="{!!$value->call_to_action!!}" onchange="editBanner('{!!$value->id!!}','call_to_action',$('#banner_call_to_action_{!!$value->id!!}').val())">
                                    <p style="float: left;width: 7%;margin-left: 2%;">Dirige a:</p>
                                    <input type="text" id="banner_url_{!!$value->id!!}" class="form-control" placeholder="Ingresar URL..." style="width: 51%;float: left;" value="{!!$value->url!!}" onchange="editBanner('{!!$value->id!!}','url',$('#banner_url_{!!$value->id!!}').val())">
                                    <br><br><br>
                                    <p><b>Nota Opcional: <small>(140 caracteres)</small></b></p>
                                    <textarea class="form-control" id="banner_optional_note_{!!$value->id!!}" placeholder="Texto opcional del banner..." onchange="editBanner('{!!$value->id!!}','optional_note',$('#banner_optional_note_{!!$value->id!!}').val())">{!!$value->optional_note!!}</textarea>
                                </div>
                            </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FINALIZA DISEÑO -->
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
    </section>
    <!-- wrapper Main Content -->
</div>

@endsection

@section('js')
<!--ROL-->
<script type="text/javascript">
    //DATATABLE ROL
    $(function(){
        $('#table_role').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('role.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'role-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'name' , name: 'name'},
                { data: 'system_users', name: 'system_users'},
                { data: 'slug', name: 'slug'},
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return '<td>'+
                                    number+
                                '</td>'
                    }
                },
                { targets : [2],
                    render : function (data, type, row) {
                        let system_users = (data).length;
                        return system_users;
                    }
                },
                { targets : [3],
                    render : function (data, type, row) {
                        return '5555555555';
                    }
                },
                { targets : [4],
                    render : function (data, type, row) {
                        return '<button type="button" id="edit_role" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-edit" data-id="'+row['id']+'" data-name="'+row['name']+'">'+
                                '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                            '</button>\n'+
                            '<button type="button" id="delete_role" class="btn btn-danger" data-toggle="modal" data-target="#modal-eliminar" data-id="'+row['id']+'">'+
                                '<i class="fa fa-times-circle" aria-hidden="true"></i>'+
                            '</button>';
                    }
                }
            ]
        });
    });
    // Create Role
    $('#form_create_role').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('role.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modal-default').modal('hide');
                $('#table_role').DataTable().ajax.reload();
                $('#role_id').append('<option value='+data.id+'>'+data.name+'</option>');
                $('#edit_role_id').append('<option value='+data.id+'>'+data.name+'</option>');
                $('#form_create_role')[0].reset();
            },
        });
    });

    // Edit a role
    $(document).on('click', '#edit_role', function() {
        id = $(this).data('id');
        $('#role_name_value').val($(this).data('name'));
    });
    $('#form_edit_role').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("role.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modal-edit').modal('hide');
                $('#table_role').DataTable().ajax.reload();
            }
        });
    });

    // delete a Role
    $(document).on('click', '#delete_role', function() {
        $('#id_delete').val($(this).data('id'));
        id = $('#id_delete').val();
    });
    $('#form_delete_role').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("role.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modal-eliminar').modal('hide');
                $('#table_role').DataTable().ajax.reload();
                $('#role_id option[value='+id+']').remove();
                $('#edit_role_id option[value='+id+']').remove();
            }
        });
    });
</script>
<!--USER-->
<script type="text/javascript">
    //DATATABLE SYSTEM USER
    $(function(){
        $('#table_systemUser').DataTable({
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '{{ route('systemUser.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'systemUser-' + aData['id']); // or whatever you choose to set as the id
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'email' , name: 'email'},
                { data: 'slug', name: 'slug'},
                { data: 'name_ful' , name: 'name_ful'},
                { data: 'role.name', name: 'roleName'},
                { data: 'country', name: 'country'},
                { data: 'city', name: 'city'},
            ],
            columnDefs :[
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return '<td>'+
                                    number+
                                '</td>'
                    }
                },
                { targets : [2],
                    render : function (data, type, row) {
                        return '* * * * * * * * *';
                    }
                },
                { targets : [7],
                    render : function (data, type, row) {
                        return '<button type="button" id="edit_systemUser" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal_user_edit" data-id="'+row['id']+'" data-email="'+row['email']+'" data-name_ful="'+row['name_ful']+'" data-role_id="'+row['role_id']+'" data-country="'+row['country']+'" data-city="'+row['city']+'">'+
                                '<i class="fa fa-pencil" aria-hidden="true"></i>'+
                            '</button>\n'+
                            '<button type="button" id="delete_systemUser" class="btn btn-danger" data-toggle="modal" data-target="#modal-eliminar-usuario" data-id="'+row['id']+'">'+
                                '<i class="fa fa-times-circle" aria-hidden="true"></i>'+
                            '</button>';
                    }
                }
            ]
        });
    });
    // add a new system user
    $('#form_create_systemUser').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('systemUser.store')}}',
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modal-usuario').modal('hide');
                $('#table_systemUser').DataTable().ajax.reload();
                $('#table_role').DataTable().ajax.reload();
                $('#form_create_systemUser')[0].reset();
            },
        });
    });
    // Edit a user
    $(document).on('click', '#edit_systemUser', function(e) {
        $('#id_edit').val($(this).data('id'));
        id = $(this).data('id');
        $('#edit_role_id').val($(this).data('role_id'));
        $('#edit_name_ful').val($(this).data('name_ful'));
        $('#edit_email').val($(this).data('email'));
        $('#edit_country').val($(this).data('country'));
        //$('#edit_city').val($(this).data('city'));
        let BATTUTA_KEY = "00000000000000000000000000000000";
        // get the id of the option which has the country code.
        let city = $(this).data('city');
        countryCode = $('#edit_country')
            .children(":selected")
            .attr("id");
        // Populate country select box from battuta API
        url =
          "https://battuta.medunes.net/api/region/" +
          countryCode +
          "/all/?key=" +
          BATTUTA_KEY +
          "&callback=?";
        $.getJSON(url, function(data) {
          $(".region option").remove();
          $('.region').append('<option value="">--Ciudad--</option>');
          $.each(data, function(index, value) {
            // APPEND OR INSERT DATA TO SELECT ELEMENT.
            $(".region").append(
              '<option value="' + value.region + '">' + value.region + "</option>"
            );
          });
          $('#edit_city').val(city);
        });
    });
    $('#form_edit_systemUser').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("systemUser.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) { 
                $('#table_systemUser').DataTable().ajax.reload();
                $('#modal_user_edit').modal('hide');              
            }
        });
    });
    // delete a system user
    $(document).on('click', '#delete_systemUser', function() {
        $('#id_delete_user').val($(this).data('id'));
        id = $('#id_delete_user').val();
    });
    $('#form_delete_systemUser').on('submit', function(e) {
        e.preventDefault();
        // agrego la data del form a formData
        let formData = new FormData(this);
        let route = '{{ route("systemUser.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#table_systemUser').DataTable().ajax.reload();
                $('#modal-eliminar-usuario').modal('hide');
            }
        });
    });
</script>
<!--Diseno-->
<script type="text/javascript">
    let urlDiseno = '{{route("home.update","$home->id")}}';
    let token = $('input[name=_token]').val();

    //image
    $('#image').change(function(e) {
        let file = e.target.files[0],
        imageType = /image.*/;

        if (!file.type.match(imageType))
            return;

        let reader = new FileReader();
        reader.onload = imageload;
        reader.readAsDataURL(file);

        let formData = new FormData(this);
        formData.append('_token',token);
        formData.append('image',file);
        $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            data: formData,
            success: function(data){
                console.log(data);
            },
            error: function(err) {
                console.log(err);
            },
      });
    });

    function imageload(e) {
        let result=e.target.result;
        $('#show_image').attr("src",result);
    }

    //VIP
    $('#vip').change(function(e) {
        let formData = new FormData(this);
        formData.append('_token',token);
        formData.append('vip',$(this).prop("checked"));

        $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            data: formData,
            success: function(data){
             console.log(data);
            },
            error: function(err) {
              console.log(err);
            }
      });
    });

    //pop_status
    $('#pop_status').change(function(e) {
        let formData = new FormData(this);
        formData.append('_token',token);
        formData.append('pop_status',$(this).prop("checked"));

        $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            data: formData,
            success: function(data){
             console.log(data);
            },
            error: function(err) {
              console.log(err);
            }
      });
    });

    //pop_main_text
    $('#pop_main_text').on('change',function(e) {
         $.ajax({
          url: urlDiseno,
          type: 'POST',
          dataType: 'json',
          data: { 
            '_token': token,
            'pop_main_text': e.target.value,
           },
          success: function(data){
             console.log(data);
          }          
      });
    });

    //pop_call_to_action
    $('#pop_call_to_action').on('change',function(e) {
         $.ajax({
          url: urlDiseno,
          type: 'POST',
          dataType: 'json',
          data: { 
            '_token': token,
            'pop_call_to_action': e.target.value,
           },
          success: function(data){
             console.log(data);
          }          
      });
    });

    //pop_url
    $('#pop_url').on('change',function(e) {
         $.ajax({
          url: urlDiseno,
          type: 'POST',
          dataType: 'json',
          data: { 
            '_token': token,
            'pop_url': e.target.value,
           },
          success: function(data){
             console.log(data);
          }          
      });
    });

    //pop_optional_note
    $('#pop_optional_note').on('change',function(e) {
         $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            data: { 
            '_token': token,
            'pop_optional_note': e.target.value,
            },
            success: function(data){
             console.log(data);
            }          
        });
    });

    //pop_image
    $('#pop_image').change(function(e) {
        let file = e.target.files[0],
        imageType = /image.*/;

        if (!file.type.match(imageType))
            return;

        let reader = new FileReader();
        reader.onload = pop_imageload;
        reader.readAsDataURL(file);

        let formData = new FormData(this);
        formData.append('_token',token);
        formData.append('pop_image',file);
        $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            data: formData,
            success: function(data){
                console.log(data.success);
            }
      });
    });

    function pop_imageload(e) {
        let result=e.target.result;
        $('#show_pop_image').attr("src",result);
    }

    //tab
    $('input[id=tab]').on('change',function(e) {
        $.ajax({
            url: urlDiseno,
            type: 'POST',
            dataType: 'json',
            data: { 
            '_token': token,
            'value': e.target.value,
            'key': e.target.name,
            },
            success: function(data){
             console.log(data);
            }          
        });
    });
</script>
<!--BANNER-->
<script type="text/javascript">
    //let token = $('input[name=_token]').val();
    // add a new banner
    function createBanner() {
        let formData = new FormData();
        formData.append('_token',token);
        let route = '{{ route("banner.store") }}';
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data.success);
                $('#newBanner').prepend('<div class="banner_info" id="'+data.banner.id+'">'+
                    '<div class="col-xs-4">'+
                        '<img src="'+data.banner.image+'" id="show_banner_image_'+data.banner.id+'" style="border: 2px solid #77c510;"><br><br>'+
                        '<input type="file" id="banner_image_'+data.banner.id+'" class="form-control" onchange="bannerImage('+"'"+data.banner.id+"'"+','+"'image'"+','+"'banner_image_"+data.banner.id+"'"+',this)">'+
                    '</div>'+
                    '<div class="col-xs-8">'+
                        '<button type="button" onclick="deleteBanner('+"'"+data.banner.id+"'"+')" id="banner_delete" class="btn btn-danger pull-right">'+
                            '<i class="fa fa-times-circle" style="font-size: 1.2em;"></i>'+
                        '</button><br>'+
                        '<p><b>Posición:</b></p>'+
                        '<select class="form-control" id="banner_position_'+data.banner.id+'" style="width: 20%;" onchange="editBanner('+"'"+data.banner.id+"'"+','+"'position'"+',$('+"'#banner_position_"+data.banner.id+"'"+').val())">'+
                            '@for($i=1;$i<=20;$i++)'+
                            '<option {!!'+data.banner.position+' == $i ? 'selected':''!!} value="{!!$i!!}">{!!$i!!}</option>'+
                            '@endfor'+
                        '</select>'+
                        '<br>'+
                        '<p><b>Texto Principal:<small> (140 caracteres)</small></b></p>'+
                        '<textarea class="form-control" id="banner_main_text_'+data.banner.id+'" placeholder="Texto principal del banner..." onchange="editBanner('+"'"+data.banner.id+"'"+','+"'main_text'"+',$('+"'#banner_main_text_"+data.banner.id+"'"+').val())" ></textarea>'+
                        '<br>'+
                        '<p><b>Call-to-Action:</b></p>'+
                        '<input type="text" id="banner_call_to_action_'+data.banner.id+'" class="form-control" placeholder="Call-to-Action" style="width: 40%;float: left;" onchange="editBanner('+"'"+data.banner.id+"'"+','+"'call_to_action'"+',$('+"'#banner_call_to_action_"+data.banner.id+"'"+').val())">'+
                        '<p style="float: left;width: 7%;margin-left: 2%;">Dirige a:</p>'+
                        '<input type="text" id="banner_url_'+data.banner.id+'" class="form-control" placeholder="Ingresar URL..." style="width: 51%;float: left;" onchange="editBanner('+"'"+data.banner.id+"'"+','+"'url'"+',$('+"'#banner_url_"+data.banner.id+"'"+').val())">'+
                        '<br><br><br>'+
                        '<p><b>Nota Opcional: <small>(140 caracteres)</small></b></p>'+
                        '<textarea class="form-control" id="banner_optional_note_'+data.banner.id+'"  placeholder="Texto opcional del banner..." onchange="editBanner('+"'"+data.banner.id+"'"+','+"'optional_note'"+',$('+"'#banner_optional_note_"+data.banner.id+"'"+').val())"></textarea>'+
                    '</div>'+
                '</div>');
            },
            error: function(err){
                console.log(err);
            }
        });
    }
    // Edit a banner
    function editBanner(id,element,value) {
        let formData = new FormData();
        formData.append('_token',token);
        formData.append('_method','PUT');
        formData.append(element,value);
        let route = '{{ route("banner.update", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
            },
            error: function(err){
                console.log(err);
            }
        });
    }
    // delete a banner
    function deleteBanner(id) {
        let formData = new FormData();
        formData.append('_token',token);
        formData.append('_method','DELETE');
        formData.append('id',id);
        let route = '{{ route("banner.destroy", ":id") }}';
        route = route.replace(':id', id);
        $.ajax({
            type: 'POST',
            url: route,
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                $('#'+id).remove();
            },
            error: function(err){
                console.log(err);
            }
        });
    }
    //image
    var imageTagID = null;
    function bannerImage(id,element,tagID,target) {
        let file = target.files[0];
        imageType = /image.*/;

        imageTagID = tagID;
        editBanner(id,element,file);

        if (!file.type.match(imageType))
            return;

        let reader = new FileReader();
        reader.onload = bannerImageload;
        reader.readAsDataURL(file);
    }

    function bannerImageload(e) {
        let result=e.target.result;
        $('#show_'+imageTagID).attr("src",result);
    }

</script>
@endsection