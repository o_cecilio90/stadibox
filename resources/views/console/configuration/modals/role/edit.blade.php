<!-- BODY MODALES PERFILES -->
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar/Editar Perfil</h4>
            </div>            
            <form id="form_edit_role" class="form-horizontal" role="form">
            @csrf
            {{ method_field('Put') }}
                <div class="modal-body">
                    <input type="text" id="role_name_value" name="name" placeholder="Nombre del perfil..." class="form-control" required autofocus style="width: 100%;padding: 1% 1%;"><br>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                    <p>Acceso a menús:</p>
                    
                    <div class="form-group2" style="width: 95%; margin: 0% auto;">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Submenú
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Sumbenú
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Submenú
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Sumbenú
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Submenú
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Menú, Sumbenú
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                    <button type="submit" class="btn btn-success edit" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->