<!-- BODY ELIMINAR USUARIO -->
<div class="modal fade" id="modal-eliminar-usuario">
    <div class="modal-dialog" style="width: 25%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminar Usuario</h4>
            </div>
            <form id="form_delete_systemUser" class="form-horizontal" role="form">
                @csrf
                {{ method_field('Delete') }}
                <div class="modal-body">
                    <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar este usuario?</b></P>
                    <input type="hidden" class="form-control" id="id_delete_user">                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                    <button type="submit" class="btn btn-danger delete-user" style="width: 49%;">Eliminar Usuario</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->