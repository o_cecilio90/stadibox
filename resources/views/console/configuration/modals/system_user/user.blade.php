<!-- BODY MODAL USUARIO -->
<div class="modal fade" id="modal-usuario">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar/Editar Usuario</h4>
            </div>
            <form id="form_create_systemUser" class="form-horizontal" role="form">
            @csrf
                <div class="modal-body">
                    <select class="form-control" id="role_id" name="role_id" required autofocus>
                        <option>Perfil</option>
                    </select><br>
                    @if ($errors->has('role_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('role_id') }}</strong>
                        </span>
                    @endif
                    <input type="text" class="form-control" id="name_ful" name="name_ful" placeholder="Nombre..." value="{{ old('name_ful') }}" required autofocus><br>
                    @if ($errors->has('name_ful'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name_ful') }}</strong>
                        </span>
                    @endif
                    <input type="email" class="form-control" id="email" name="email" placeholder="Username..." value="{{ old('email') }}" required autofocus><br>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password..." value="{{ old('password') }}" required autofocus><br>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <select class="form-control country" id="country" name="country" required autofocus>
                        <option>País</option>
                    </select><br>
                    @if ($errors->has('country'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
                    <select class="form-control region" id="city" name="city" required autofocus>
                        <option>Ciudad</option>
                    </select><br>
                    @if ($errors->has('city'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- BODY MODAL EDIT USUARIO -->
<div class="modal fade" id="modal_user_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar/Editar Usuario</h4>
            </div>
            <form id="form_edit_systemUser" class="form-horizontal" role="form">
            @csrf
            {{ method_field('Put') }}
                <div class="modal-body">
                    <select class="form-control" id="edit_role_id" name="role_id" required autofocus>
                        <option>Perfil</option>
                    </select><br>
                    @if ($errors->has('role_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('role_id') }}</strong>
                        </span>
                    @endif
                    <input type="text" class="form-control" id="edit_name_ful" name="name_ful" placeholder="Nombre..." value="{{ old('name_ful') }}" required autofocus><br>
                    @if ($errors->has('name_ful'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name_ful') }}</strong>
                        </span>
                    @endif
                    <input type="email" class="form-control" id="edit_email" name="email" placeholder="Username..." value="{{ old('email') }}" required autofocus><br>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input type="password" class="form-control" id="edit_password" name="password" placeholder="Password..." value="{{ old('password') }}" autofocus><br>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <select class="form-control country" id="edit_country" name="country" required autofocus>
                        <option>País</option>
                    </select><br>
                    @if ($errors->has('country'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
                    <select class="form-control region" id="edit_city" name="city" required autofocus>
                        <option>Ciudad</option>
                    </select><br>
                    @if ($errors->has('city'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                    <button type="submit" class="btn btn-success" style="width: 49%;">Aceptar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->