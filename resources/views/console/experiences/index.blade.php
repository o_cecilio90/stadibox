@extends('console.layouts.main')

@section('title', 'Experiencias')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Experiencias
                <small>Stadibox</small>
            </h1><br>
            <a class="btn btn-app" style="background-color: #77c510;color: white;" href="{!! route('experiencias.create') !!}">
                <i class="fa fa-plus"></i> Agregar Experiencia
            </a>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Experiencias</li>
            </ol>
        </section>

        <!-- Main content -->            
        <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
            <!-- LEFT COLUMN -->
            <div class="col-md-10">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Experiencias en la Página de Stadibox</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="table_experiences" class="table table-hover" style="text-align: center;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Evento</th>
                                    <th style="text-align: center;">Fecha</th>
                                    <th style="text-align: center;">Lugar</th>
                                    <th style="text-align: center;">Foro</th>
                                    <th style="text-align: center;">Estatus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- END OF LEFT COLUMN -->
            <!-- RIGHT COLUMN -->
            <div class="col-md-2" style="text-align: center;">
                <div class="box filtros" style="width: 110%;padding: 1% 0%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filtrar Resultados</h3>
                    </div>
                    <!-- /.box-header -->
                    <button type="button" class="btn btn-block btn-success" style="width: 90%;margin: 0% auto;">Aplicar</button>
                    <a href="#" style="color: #77c510;">Limpiar Filtros</a>
                    <!-- checkbox -->
                    <div class="form-group" style="text-align: left;">
                        <h4 class="hcuatro">Ubicación:</h4>
                        <!-- select -->
                        <select class="form-control" style="width: 90%;margin: 0% auto;">
                            <option>Todos los Países</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select><br>
                        <select class="form-control" style="width: 90%;margin: 0% auto;">
                            <option>Todas las Ciudades</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select><br><br>
                        <h4 class="hcuatro foro_h4" style="margin-top: -9%;">Foro:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Estadio Azteca<br>
                                <input type="checkbox">
                                Estadio Cuauhtémoc<br>
                                <input type="checkbox">
                                Arena CDMX<br>
                                <input type="checkbox">
                                Estadio BBVA<br>
                                <input type="checkbox">
                                Estadio Lobos BUAP<br>
                            </label>
                        </div>
                        <h4 class="hcuatro">Categoría:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Futbol<br>
                                <input type="checkbox">
                                NFL<br>
                                <input type="checkbox">
                                Conciertos<br>
                                <input type="checkbox">
                                Toros<br>
                                <input type="checkbox">
                                Todas<br>
                            </label>
                        </div>
                        <h4 class="hcuatro">Estatus:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Activo<br>
                                <input type="checkbox">
                                Pausado<br>
                                <input type="checkbox">
                                Finalizado<br>
                            </label>
                        </div>
                        <h4 class="hcuatro">Clase:</h4>
                        <div class="checkbox" style="padding-left: 6%;">
                            <label>
                                <input type="checkbox">
                                Eventos<br>
                                <input type="checkbox">
                                Experiencias VIP<br>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OF RIGHT COLUMN -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    <!--Experiences-->
    <script type="text/javascript">
        $(function(){
            $('#table_experiences').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('experiencias.table') }}',
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).attr('id', 'role-' + aData['id']); // or whatever you choose to set as the id
                },
                columns: [
                    { data: 'name' , name: 'name'},
                    { data: 'time_limit', name: 'time_limit'},
                    { data: 'place', name: 'place'},
                    { data: 'forum', name: 'forum'},
                    { data: 'status', name: 'status'},
                ],
                columnDefs :[
                    { targets : [4],
                        render : function (data, type, row) {                        
                            if (data == 'Activo') {
                                return '<td><b style="color:green">Activo</b></td>'
                            }else{
                                return '<td><b style="color:red">Activo</b></td>'
                            }
                        }
                    },
                ]
            });
        });
    </script>

@endsection