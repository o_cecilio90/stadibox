@section('js')
    <script>
        $(function () {
                //Initialize Select2 Elements
                $('.select2').select2()

                //Timepicker
                $('.timepicker').timepicker({
                    showInputs: false
                })
        });
        // Get all cities in select
        function getCity(){
            var selectBox = document.getElementById("countries");
            var country_id = selectBox.options[selectBox.selectedIndex].value;
            $.ajax({
		 		type: "POST",
	            url: "{{ route('get.city') }}",
	            data : {'country_id':country_id, '_token': $('input[name=_token]').val()},
	            dataType: 'JSON',
	            success: function (data) {
                    $('#city')[0].setAttribute("onchange", "getForums()");
                    $.each(data, function (i, item) {
                        $('#city').append('<option value='+item.id+'>'+item.city+'</option>');
                    });	            
	            },
	            error: function (data) {
	                console.log(data);
	            }
	        });
        }

        // Get all forums in select
        function getForums(){
            var selectBox = document.getElementById("city");
            var city_id = selectBox.options[selectBox.selectedIndex].value;
            $.ajax({
                type: "POST",
	            url: "{{ route('get.forums') }}",
	            data : {'city_id':city_id, '_token': $('input[name=_token]').val()},
	            dataType: 'JSON',
	            success: function (data) {
                    $.each(data, function (i, item) {
                        $('#forums').append('<option value='+item.id+'>'+item.forum_name+'</option>');
                    });	            
	            },
	            error: function (data) {
	                console.log(data);
	            }
	        });
        }

        // Image experience preload 

		function readImageExperience(input){
            if (input.files && input.files[0]) {
                var reader2 = new FileReader();

                reader2.onload = function (e) {
                    $('#img-experience').attr('src', e.target.result);
                }

                reader2.readAsDataURL(input.files[0]);
            }
        }

        $("#input_experience").change(function(){
            readImageExperience(this);
        });

        // Image featured preload 

		function readCover(input){
            if (input.files && input.files[0]) {
                var reader2 = new FileReader();

                reader2.onload = function (e) {
                    $('#cover-img').attr('src', e.target.result);
                }

                reader2.readAsDataURL(input.files[0]);
            }
        }

        $("#cover").change(function(){
            readCover(this);
        });

        // Add Package
        $('#addPackage').on('click',function(){
            $.ajax({
		 		type: "POST",
	            url: "{{ route('create.package') }}",
	            data : {'_token': $('input[name=_token]').val()},
	            dataType: 'JSON',
	            success: function (data) {
                    console.log(data);
                    var html =  '<tr>'+
                            '<td style="width: 30%;">'+
                                '<div class="form-group">'+
                                    '<img id="img-pack-'+data.package_id+'" src="{{ asset('console/dist/img/boxed-bg.jpg')}}" style="width: 80%;">'+
                                    '<label for="exampleInputFile">Imagen del Paquete</label>'+
                                    '<input type="file" onchange="preloadImagePackage('+"'"+data.package_id+"'"+');void(0);" name="experience_img">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group">'+
                                    '<label>Nombre del Paquete:</label>'+
                                    '<input type="text" name="name_package[]" class="form-control" placeholder="Nombre del Paquete..." maxlength="120">'+
                                    '<br>'+  
                                    '<label>Estatus:</label>'+
                                    '<select name="status_package[]" class="form-control">'+
                                        '<option value="1">Activo</option>'+
                                        '<option value="2">Pausado</option>'+
                                        '<option value="3">Agotado</option>'+
                                    '</select>'+
                                    '<br>'+
                                    '<label>Rango de Fechas:</label>'+
                                    '<br>'+
                                    '<label>Incluye:</label>'+
                                    '<input type="text" name="include[]" class="form-control" placeholder="Agregar punto...">'+
                                    '<br>'+
                                    '<label>Divisa:</label>'+
                                    '<select name="currency[]" class="form-control">'+
                                        '<option value="usd">USD</option>'+
                                        '<option value="mxn">MXN</option>'+
                                    '</select>'+
                                    '<br>'+
                                    '<label>Precio por persona en ocupación sencilla:</label>'+
                                    '<input type="text" name="price_simple[]" class="form-control" placeholder="$500.00...">'+
                                    '<br>'+
                                    '<label>Precio por persona en ocupación doble:</label>'+
                                    '<input type="text" name="price_double[]" class="form-control" placeholder="$500.00...">'+
                                    '<br>'+
                                    '<label>Adicionales:</label>'+
                                    '<br><br><br>'+
                                '</div>'+
                            '</td>'+
                        '</tr>';
                    $('#my_table tbody').append(html);           
	            },
	            error: function (data) {
	                console.log(data);
	            }
	        });
        })

        // Image featured preload 

		function readImagePackage(input){
            console.log(input)
            if (input.files && input.files[0]) {
                var reader2 = new FileReader();

                reader2.onload = function (e) {
                    $('#img-pack').attr('src', e.target.result);
                }

                reader2.readAsDataURL(input.files[0]);
            }
        }

        $("#input_img_pack").change(function(){
            readImagePackage(this);
        });

        // Image package preload more 

		function readImagePackageMore(id,input){
            void(0);
            console.log('este es el id '+id);
            alert('este es el input '+input);
            if (input.files && input.files[0]) {
                var reader2 = new FileReader();

                reader2.onload = function (e) {
                    $('#img-pack-'+id).attr('src', e.target.result);
                }

                reader2.readAsDataURL(input.files[0]);
            }
        }

        function preloadImagePackage(id){
            readImagePackageMore(id,this);
        }

        
    </script>
@endsection