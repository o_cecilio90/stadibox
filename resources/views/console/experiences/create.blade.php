@extends('console.layouts.main')

@section('title', 'Experiencias')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Agregar Experiencia
                <small>Stadibox</small>
            </h1>
            
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="experiencias.php"><i class="fa fa-futbol-o"></i> Experiencias</a></li>
                <li class="active">Agregar Evento</li>
            </ol>
        </section>

        <!-- Main content -->            
        <div class="row" style="width: 100%;margin: 1% auto 0% auto;">
            <!-- LEFT COLUMN -->
            <div class="col-md-8">
                <div class="box">
                {!! Form::open(['route' => 'experiencias.store', 'method' => 'POST', 'files'=>true]) !!}
                    <div class="box-body table-responsive no-padding">
                        <table id="my_table" class="table tabla_agregar_evento">
                            <tr>
                                <td style="width: 30%;">
                                    <div class="form-group">
                                        <img id="img-experience" src="{{asset('console/dist/img/boxed-bg.jpg')}}" style="width: 80%;"><br>
                                        <label for="exampleInputFile">Imagen de la Experiencia</label>
                                        <input type="file" id="input_experience" name="experience_img">
                                        <br>
                                        <img id="cover-img" src="{{asset('console/dist/img/boxed-bg.jpg')}}" style="width: 80%;"><br>
                                        <label for="exampleInputFile">Imagen de Portada</label>
                                        <input type="file" id="cover" name="cover_img">
                                    </div>
                                </td>
                                <td>
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Nombre del Evento:</label>
                                            <input type="text" class="form-control" name="name" placeholder="Nombre del evento..." maxlength="120">
                                            <br>
                                            
                                            <label>Ubicación:</label><br>
                                            <select id="countries" name="country_id" onchange="getCity();" class="form-control col-xs-3" style="width: 32%;">
                                                <option>País</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{!! $country->country !!}</option>
                                                @endforeach
                                            </select>
                                            <select id="city" name="city_id" class="form-control col-xs-3" style="width: 32%;margin-left: 1%;">
                                                <option>Ciudad</option>
                                            </select>
                                            <select id="forums" name="forum_id" class="form-control col-xs-3" style="width: 32%;margin-left: 1%;">
                                                <option>Foro</option>
                                            </select><br><br>
                                            
                                            <div class="bootstrap-timepicker">
                                                <label>Hora límite de Compra:</label>
                                                
                                                <div class="input-group">
                                                    <input name="time_limit" type="text" class="form-control timepicker">
                                                    
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div><br>
                                            
                                            <label>Palabras Clave:</label>
                                            <select name="keyword[]" class="form-control select2" multiple="multiple" data-placeholder="Selecciona las Palabras Clave del Evento" name="keyword">
                                                <option>Deportivo</option>
                                                <option>Concierto</option>
                                                <option>NFL</option>
                                                <option>Futbol</option>
                                                <option>USA</option>
                                                <option>México</option>
                                                <option>Informativo</option>
                                            </select><br><br>
                                            
                                            <div class="checkbox">
                                                <label>
                                                    <input name="featured_event" type="checkbox" value="1">
                                                    Evento Destacado
                                                </label>
                                            </div>
                                            <label>Estatus:</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" value="1" name="status" id="status1" value="Activo" checked>
                                                    Activo
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" value="1" name="status" id="status2" value="En Pausa">
                                                    En Pausa
                                                </label>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    <!-- /.form group -->
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a id="addPackage" class="btn btn-success" style="float:right">Agregar Nuevo Paquete</a></td>
                            </tr>
                            <tr>
                                <td style="width: 30%;">
                                    <div class="form-group">
                                        <img id="img-pack" src="{{ asset('console/dist/img/boxed-bg.jpg')}}" style="width: 80%;">
                                        <label for="exampleInputFile">Imagen del Paquete</label>
                                        <input type="file" id="input_img_pack" name="experience_img">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label>Nombre del Paquete:</label>
                                        <input type="text" class="form-control" name="name_package[]" placeholder="Nombre del Paquete..." maxlength="120">
                                        <br>
                                        
                                        <label>Estatus:</label>
                                        <select name="status_package[]" class="form-control">
                                            <option value="1">Activo</option>
                                            <option value="2">Pausado</option>
                                            <option value="3">Agotado</option>
                                        </select>
                                        <br>
                                        <label>Rango de Fechas:</label>
                                        <br>
                                        <label>Incluye:</label>
                                        <input type="text" name="include[]" class="form-control" placeholder="Agregar punto...">
                                        <br>
                                        <label>Divisa:</label>
                                        <select name="currency[]" class="form-control">
                                            <option value="usd">USD</option>
                                            <option value="mxn">MXN</option>
                                        </select>
                                        <br>
                                        <label>Precio por persona en ocupación sencilla:</label>
                                        <input type="text" name="price_simple[]" class="form-control" placeholder="$500.00...">
                                        <br>
                                        <label>Precio por persona en ocupación doble:</label>
                                        <input type="text" name="price_double[]" class="form-control" placeholder="$500.00...">
                                        <br>
                                        <label>Adicionales:</label>
                                        <br><br><br>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-success" style="float:right; margin: 5px 3px;">Guardar</button>
                    </div>
                    <!-- /.box-body -->
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            </div>
            <!-- END OF LEFT COLUMN -->
            <!-- RIGHT COLUMN -->
            <div class="col-md-2" style="text-align: center;">
                <div class="box agregar_right" style="width: 110%;padding: 5% 0%;">
                    <!-- /.box-header -->
                    <button type="button" class="btn btn-block btn-success" style="width: 90%;margin: 0% auto;">Guardar Cambios</button>
                    <button type="button" class="btn btn-default bg-navy" data-toggle="modal" data-target="#modal-default" style="margin-top: 4%;width: 90%;">Eliminar Evento</button>
                </div>
                <!-- CONTENIDO DEL MODAL -->
                <div class="modal fade" id="modal-default">
                    <div class="modal-dialog">
                        <div class="modal-content" style="width: 62%;margin: 0% auto;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar Evento</h4>
                            </div>
                            <div class="modal-body">
                                <p style="font-size: 1.1em;"><b>¿Estás seguro de que deseas eliminar este evento?</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;">Cancelar</button>
                                <button type="button" class="btn btn-danger" style="width: 48%;">Eliminar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
            <!-- END OF RIGHT COLUMN -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@include('console.experiences.scripts.script_create')