@extends('console.layouts.main')

@section('title', 'Reporte de Usuario')

@section('css')
<!-- Page style -->
<style>
    a{
        color: #77c510;
    }
    a:hover{
        color: forestgreen;
    }
</style>
<style>
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }

    .example-modal .modal {
        background: transparent !important;
    }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reporte de Usuario
            <small>Stadibox</small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="reportes.php"><i class="fa fa-file-text"></i> Reportes</a></li>
            <li class="active">Reporte de Usuario</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content reporte_usuario">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom" style="width: 100%;margin: 0% auto;">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
                <li><a href="#estado" data-toggle="tab">Estado de Cuenta</a></li>
                <li><a href="#compras" data-toggle="tab">Compras</a></li>
                <li class="active"><a href="#datos" data-toggle="tab">Datos</a></li>
                <li class="pull-left header"><i class="fa fa-user"></i> Reporte de Usuario</li>
            </ul>
            <div class="tab-content no-padding">
                <!-- CONTENIDO DE LAS TABS -->
                <!-- DATOS -->
                <div class="table tab-pane active" id="datos" style="position: relative;">
                    <div class="row">
                        @if($user->status == 0)
                        <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#modal-desbloquear" style="width: 15%;margin: 1% 0% 1% 3%;">
                            Usuario Bloqueado
                        </button>
                        @endif
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;background-color: rgba(0,0,0,0.03);"> 
                            <div class="box" style="border:none;margin-top: 1%;background-color: rgba(0,0,0,0.005)"> 
                                <div class="box-body no-padding">
                                    <!-- LEFT COLUMN -->
                                    <div class="col-md-6">
                                        <!-- DATOS DEL USUARIO -->
                                        <div class="box" style="width: 100%;">
                                            <div class="box-body">
                                                <h3>Datos del Usuario</h3>
                                                <div class="foto_izq">
                                                    <img src="{!!asset('console/dist/img/avatar2.png')!!}">
                                                    <p><b>{!!$user->name ." ". $user->lastname!!}</b></p>
                                                </div>
                                                <div class="info_der">
                                                    <p><i class="fa fa-at"></i>{!!$user->email!!}</p>
                                                    <p><i class="fa fa-phone"></i> <a href="tel:55">{!!$user->mobile!!}</a></p>
                                                    <p><i class="fa fa-mobile"></i> <a href="tel:55">{!!$user->mobile!!}</a></p>
                                                    <p class="datepicker"><i class="fa fa-calendar"></i></p>
                                                    <p><i class="fa fa-id-card-o"></i> RFC: {!! $box->count()>0 ? $box->first()->rfc : $stall->count()>0 ? $stall->first()->rfc : "--"!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- RIGHT COLUMN -->
                                    <div class="col-md-6">
                                        <!-- DOCUMENTOS DEL USUARIO -->
                                        <div class="box" style="width: 100%;">
                                            <div class="box-body docs_us">
                                                <h3>Documentos del Usuario</h3>
                                                <div style="float: left;width: 32%;text-align: center;">
                                                    <p>CURP</p>
                                                    <img src="{!!asset('console/dist/img/boxed-bg.jpg')!!}">
                                                </div>
                                                <div style="float: left;width: 32%;text-align: center;">
                                                    <p>I.D.</p>
                                                    <img src="{!! $box->count()>0 ? asset($box->first()->identification_file) : $stall->count()>0 ? asset($stall->first()->identification_file) : asset('console/dist/img/boxed-bg.jpg')!!}">
                                                </div>
                                                <div style="float: left;width: 32%;text-align: center;">
                                                    <p>Comprobante de Domicilio</p>
                                                    <img src="{!! $box->count()>0 ? asset($box->first()->property_title) : $stall->count()>0 ? asset($stall->first()->property_title) : asset('console/dist/img/boxed-bg.jpg')!!}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body detalle_propiedades">
                                    <h3>Propiedades<a class="btn btn-success" style="background-color: #77c510;color: white;font-size: 0.9em;margin-left: 1%;" href="#">
                                        <i class="fa fa-pencil"></i>
                                    </a></h3>
                                    <!-- TABLA PROPIEDADES -->
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Foro</th>
                                                <th>País/Ciudad</th>
                                                <th>I.D.</th>
                                                <th>Propiedad</th>
                                                <th>Capacidad</th>
                                                <th>Modalidad</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody></tbody>
                                        @foreach($box as $key => $value)
                                        <tr>
                                            <td>{!!$value->forum_name!!}</td>
                                            <td>{!!$value->country!!}, {!!$value->city!!}</td>
                                            <td>--</td>
                                            <td>Palco {!!$value->num_box!!}</td>
                                            <td>{!!$value->capacity!!}</td>
                                            <td>--</td>
                                        </tr>
                                        @endforeach
                                        @foreach($stall as $key => $value)
                                        <tr>
                                            <td>{!!$value->forum_name!!}</td>
                                            <td>{!!$value->country!!}, {!!$value->city!!}</td>
                                            <td>--</td>
                                            <td>Plateas</td>
                                            <td>{!!$value->stall_tickets!!}</td>
                                            <td>--</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    
                                    <h3>Direcciones de Entrega y Devolución de Pases</h3>
                                    <!-- CASA -->
                                    <div class="col-md-6" style="float: left;">
                                        <div class="box" style="padding: 2%;">
                                            <h4>Casa</h4>
                                            <p><a href="#">Paseo de los Laureles #1302, Bosques de Cuajimalpa, 05320, CDMX, México</a></p>
                                            <p>"Si no estoy entregar a Tere Chávez"</p>
                                        </div>
                                    </div>
                                    <!-- OFICINA -->
                                    <div class="col-md-6" style="float: left;">
                                        <div class="box" style="padding: 2%;">
                                            <h4>Oficina</h4>
                                            <p><a href="#">Hegel 130, Polanco, Miguel Hidalgo, 04320, CDMX, México</a><br>Es una reja verde y casa blanca</p>
                                            <p>"Si no estoy entregar a Luis Soto (55224433)"</p>
                                        </div>
                                    </div>
                                    <!-- CUENTA PARA DEPOSITOS -->
                                    <div class="col-md-6" style="float: left;">
                                        <div class="box" style="padding: 2%;">
                                            <h4>Cuenta Para Depósitos</h4>
                                            <table class="table">
                                                <tr>
                                                    <th>Concepto</th>
                                                    <th style="width: 60%;">Detalle</th>
                                                </tr>
                                                <tr>
                                                    <td>Titular</td>
                                                    <td>{!! $box->count()>0 ? $box->first()->titular : $stall->count()>0 ? $stall->first()->titular : "--"!!}</td>
                                                </tr>
                                                <tr>
                                                    <td>Banco</td>
                                                    <td>{!! $box->count()>0 ? $box->first()->bank : $stall->count()>0 ? $stall->first()->bank : "--"!!}</td>
                                                </tr>
                                                <tr>
                                                    <td>Cuenta</td>
                                                    <td>{!! $box->count()>0 ? $box->first()->account_titular : $stall->count()>0 ? $stall->first()->account_titular : "--"!!}</td>
                                                </tr>
                                                <tr>
                                                    <td>Clabe</td>
                                                    <td>--</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- DATOS DE FACTURACIÓN -->
                                    <div class="col-md-6" style="float: left;">
                                        <div class="box" style="padding: 2%;">
                                            <h4>Datos de Facturación</h4>
                                            <table class="table">
                                                <tr>
                                                    <th>Concepto</th>
                                                    <th style="width: 60%;">Detalle</th>
                                                </tr>
                                                <tr>
                                                    <td>RFC</td>
                                                    <td>TYYM890506TR4</td>
                                                </tr>
                                                <tr>
                                                    <td>Razón Social</td>
                                                    <td>Ana Vargas Rivera</td>
                                                </tr>
                                                <tr>
                                                    <td>Dirección</td>
                                                    <td>Paseo de Los Laureles #1302, Bosques de Cuajimalpa 03520, CDMX, México</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    @if($user->status==1)
                                    <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#modal-bloquear" style="width: 15%;">
                                        Bloquear Usuario
                                    </button>
                                    @endif
                                </div>
                                
                                <!-- /.box-body -->
                            </div>
                            @include('console.user.modal.user')
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA DATOS -->
                <!-- COMPRAS -->
                <div class="table tab-pane" id="compras" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Compras del Usuario (Próximas)</h3>
                                    
                                    <div class="box-tools">
                                        <div class="form-group form-group-sm" style="width: 150px;">
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default-f" style="width: 100%;">
                                                Finalizadas
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" style="text-align: center;">
                                        <tr>
                                            <th style="text-align: center">Evento</th>
                                            <th style="text-align: center">Estatus</th>
                                            <th style="text-align: center">Beneficios</th>
                                            <th style="text-align: center;">Modo de Renta</th>
                                            <th style="text-align: center">Boletos</th>
                                            <th style="text-align: center">Parking</th>
                                            <th style="text-align: center">Pago</th>
                                            <th style="text-align: center">Depósito de Garantía</th>
                                            <th style="text-align: center">Reportes</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Paul McCartney en el Azteca<br>
                                                <small>11/10/18<br>I.D. Publicación: 12345</small>
                                            </td>
                                            <td><b class="green">Por Recibir</b></td>
                                            <td>Si incluye<br><a href="#">Ver</a></td>
                                            <td>Todo el Palco</td>
                                            <td>10</td>
                                            <td>2 (VIP)</td>
                                            <td><b>$120,000.00</b></td>
                                            <td>$10,000.00<br><small>(total)</small></td>
                                            <td>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-problema">
                                                    Problema Reportado
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                América vs. Chivas<br>
                                                <small>12/06/18<br>I.D. Publicación: 9876</small>
                                            </td>
                                            <td><b class="green">Recibidos</b></td>
                                            <td>No incluye</td>
                                            <td>Palco Compartido</td>
                                            <td>4</td>
                                            <td>1 (VIP)</td>
                                            <td><b>$80,000.00</b></td>
                                            <td>$4,000.00<br><small>(total)</small></td>
                                            <td>
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-problema">
                                                    Problema Resuelto
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- BODY MODAL PROBLEMA REPORTADO -->
                            <div class="modal fade" id="modal-problema">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="width: 70%;margin: 0% auto;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>     </button>
                                            <h4 class="modal-title">Reporte de Problema</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p><b>Problema:</b><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                            <p><b>Correo del Usuario:</b><br><a href="mailto:#">correo@correo.com</a></p>
                                            <p><b>Estatus:</b><br>
                                                <select class="form-control">
                                                    <option>Sin Resolver</option>
                                                    <option>Resuelto</option>
                                                </select></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 48%;">Cancelar</button>
                                            <button type="button" class="btn btn-success" style="width: 48%;%;">Aceptar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA COMPRAS -->
                <!-- ESTADO DE CUENTA -->
                <div class="table tab-pane" id="estado" style="position: relative;">
                    <div class="row">
                        <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                            <div class="box">
                                <div class="box-header" style="padding: 1% 0%;">
                                    <h3 class="box-title">Estado de Cuenta del Usuario</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" style="text-align: center;">
                                        <tr>
                                            <th style="text-align: center;">Evento</th>
                                            <th style="text-align: center;">Fecha de Pago</th>
                                            <th style="text-align: center">Boletos Rentados</th>
                                            <th style="text-align: center;">Precio promedio x boleto</th>
                                            <th style="text-align: center;">Utilidad Total</th>
                                            <th style="text-align: center;">Estatus</th>
                                        </tr>
                                        <tr>
                                            <td>Paul McCartney<br>(Palco Azteca 113)<br><small>10/11/18</small></td>
                                            <td>03/05/18</td>
                                            <td><b class="green">6/10 rentados</b></td>
                                            <td>$10,000.00</td>
                                            <td><b>$60,000.00</b></td>
                                            <td><b class="green">Pago Efectuado</b></td>
                                        </tr>
                                        <tr>
                                            <td>América vs. Chivas<br>(Palco Azteca 112)<br><small>07/06/18</small></td>
                                            <td>14/03/18</td>
                                            <td><b class="green">Reservado para ti</b></td>
                                            <td>$5,000.00</td>
                                            <td><b>$75,000.00</b></td>
                                            <td><b class="green">Liquidado</b></td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- BODY MODALES AMENIDAD -->
                            <div class="modal fade" id="modal-default-a">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="width: 90%;margin: 0% auto;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>     </button>
                                            <h4 class="modal-title">Agregar/Editar Amenidad</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <p>Amenidad:</p>
                                                <input type="text" class="form-control" placeholder="Nombre de amenidad...">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left bg-navy" data-dismiss="modal" style="border-radius: 4px;">Cancelar</button>
                                            <button type="button" class="btn btn-success" style="width: 25%;">Aceptar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <!-- BODY ELIMINAR AMENIDAD -->
                            <div class="modal fade" id="modal-eliminar-a">
                                <div class="modal-dialog" style="width: 25%;">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Eliminar Amenidad</h4>
                                        </div>
                                        <div class="modal-body">
                                            <P style="text-align: center;font-size: 1.2em;"><b>¿Estás seguro que deseas eliminar esta amenidad?</b></P>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="width: 48%;border-radius: 4px;padding: 2% 0%;">Cancelar</button>
                                            <button type="button" class="btn btn-danger" style="width: 49%;">Eliminar Amenidad</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- FINALIZA AMENIDADES -->
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        let dob = new Date('{!!$user->birthdate!!}');
        let age = GetAge(dob);
        $('.datepicker').append(age +' años - '+ dob.getDate() +'/'+ dob.getMonth() +'/'+ dob.getFullYear());
    });

    function GetAge(birthDate) {
        let today = new Date();
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
</script>


@endsection