@extends('console.layouts.main')

@section('title', 'Reportes')

@section('css')
<!-- Page style -->
<style>
    th{
        cursor: default;
    }
    tr{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Lista de Usuarios</h1>
        <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Lista de Usuarios</li>
        </ol>
        <!-- /.content -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="table tab-pane active" id="usuarios" style="position: relative;">
            <div class="row">
                <div class="col-xs-12" style="width: 98%; margin: 0% 1%;">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-user"></i> Lista de Usuarios</h3>
                            
                            <div class="box-tools tools_usuarios" style="width: 80%;right: -7px !important;">
                                <div class="form-group form-group-sm">
                                    <select class="form-control" style="width: 24%;float: left;">
                                        <option>Tipo de Usuario 1</option>
                                        <option>Tipo de Usuario 2</option>
                                        <option>Tipo de Usuario 3</option>
                                        <option>Tipo de Usuario 4</option>
                                        <option>Tipo de Usuario 5</option>
                                    </select>
                                    <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                        <option>Foro 1</option>
                                        <option>Foro 2</option>
                                        <option>Foro 3</option>
                                        <option>Foro 4</option>
                                        <option>Foro 5</option>
                                    </select>
                                    <select class="form-control" style="width: 24%;float: left;margin-left: 1%;">
                                        <option>Estatus 1</option>
                                        <option>Estatus 2</option>
                                        <option>Estatus 3</option>
                                        <option>Estatus 4</option>
                                        <option>Estatus 5</option>
                                    </select>
                                    <input type="text" class="form-control" placeholder="ID, No. Palco, Evento, Foro, Correo, Titular..." style="float: left;width: 24%; margin-left: 1%;">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover" id="table_user" style="text-align: center;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">ID</th>
                                        <th style="text-align: center">Nombre</th>
                                        <th style="text-align: center">Apellido</th>
                                        <th style="text-align: center;">Correo</th>
                                        <th style="text-align: center">Tipo</th>
                                        <th style="text-align: center">Propiedades</th>
                                        <th style="text-align: center">Estatus</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
        <!-- TERMINA USUARIOS -->
    </section>
    <!-- wrapper Main Content -->
</div>
@endsection

@section('js')
<!-- Page script -->
<script>
    $(function () {
        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })
    })
</script>
<!--USER-->
<script type="text/javascript">
    //DATATABLE USER
    $(function(){
        $('#table_user').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('user.index') }}',
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                let route = '{{ route("user.show", ":id") }}';
                route = route.replace(':id', aData['id']);
                $(nRow).attr('id', 'role-' + aData['id']); // or whatever you choose to set as the id
                $(nRow).attr('onclick','document.location = "'+route+'"');
            },
            columns: [
                { data: 'id' , name: 'id'},
                { data: 'name' , name: 'name'},
                { data: 'lastname', name: 'lastname'},
                { data: 'email', name: 'email'},
                { data: 'boxes', name: 'role'},
                { data: 'boxes', name: 'box'},
                { data: 'status', name: 'status'},
            ],
            columnDefs :[ 
                { targets : [0],
                    render : function (data, type, row,index) {
                        let number = index['row']+1;
                        return '<td>'+
                                    number+
                                '</td>'
                    }
                },
                { targets : [1],
                    render : function (data, type, row) {
                        return '<td><b>'+data+'</b></td>';
                    }
                },
                { targets : [2],
                    render : function (data, type, row) {
                        return '<td><b>'+data+'</b></td>';
                    }
                },
                { targets : [4],
                    render : function (data, type, row) {
                        let box = (data).length;
                        let stall = (row['stalls']).length;

                        if (box > 0 || stall > 0) {
                            return '<td><b>Propietario</b></td>';
                        }else{
                            return '<td><b>Cliente</b></td>';
                        }
                    }
                },
                { targets : [5],
                    render : function (data, type, row) {
                        let box = (data).length;
                        let stall = (row['stalls']).length;

                        if (box > 0 || stall > 0) {
                            return '<td>'+box+' Palcos<br>'+stall+' grupo de plateas</td>';
                        }else{
                            return '<td>N/A</td>';
                        }

                        
                    }
                },
                { targets : [6],
                    render : function (data, type, row) {
                        let result = null; 
                        if (data == 1) { 
                          result = '<b class="activo">Activo</b>'; 
                        }else{ 
                          result = '<b class="bloqueado">Bloqueado</b>'; 
                        } 
                         
                        return result; 
                    }
                },
            ]
        });
    });
</script>
@endsection