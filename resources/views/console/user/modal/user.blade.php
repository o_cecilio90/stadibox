<!-- BODY MODAL BLOQUEAR -->
<div class="modal fade" id="modal-bloquear">
    <div class="modal-dialog">
        <div class="modal-content">
        	{!! Form::open(['route' => ['user.update',$user->id], 'method' => 'PUT']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <input type="hidden" name="status" value=0>
                <h4 class="modal-title">Bloquear Usuario</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Por qué desea bloquear a este usuario?</b></p>
                <textarea class="form-control" name="reason" placeholder="El usuario ha..."></textarea>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                <button type="submit" class="btn btn-danger" style="width: 49%;">Bloquear</button>
            </div>
            {!!Form::close()!!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BODY MODAL DESBLOQUEAR -->
<div class="modal fade" id="modal-desbloquear">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 70%;margin: 0% auto;">
        	
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <h4 class="modal-title">Desbloquear Usuario</h4>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.2em;text-align: center;"><b>Este usuario fue bloqueado porque {!!$user->reason!!}</b></p>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                <button type="button" class="btn btn-success" style="width: 49%;" data-toggle="modal" data-target="#modal-desbloquear_aceptar" data-dismiss="modal">Desbloquear</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BODY MODAL DESBLOQUEAR ACEPTAR -->
<div class="modal fade" id="modal-desbloquear_aceptar">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 70%;margin: 0% auto;">
        	{!! Form::open(['route' => ['user.update',$user->id], 'method' => 'PUT']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>     </button>
                <input type="hidden" name="status" value=1>
                <h4 class="modal-title">Desbloquear Usuario</h4>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.2em;text-align: center;"><b>¿Seguro que deseas desbloquear a este usuario?</b></p>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-default bg-navy" data-dismiss="modal" style="border-radius: 4px;width: 49%;">Cancelar</button>
                <button type="submit" class="btn btn-success" style="width: 49%;">Desbloquear</button>
            </div>
            {!!Form::close()!!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->