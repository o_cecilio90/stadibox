var pic = document.getElementById("my_image");
var altoP = document.getElementById("alto");
var altoLateral = document.getElementById("alto_lateral");
var bajoP = document.getElementById("bajo");
var plateaAltaPlus = document.getElementById("platea_alta_plus");
var palcosPlus = document.getElementById("palcos_plus");
var plateaBaja = document.getElementById("platea_baja");
var palcosClub = document.getElementById("palcos_club");
var asientosClub = document.getElementById("asientos_club");
var preferentePlus = document.getElementById("preferente_plus");
var plateaAltaCabecera = document.getElementById("platea_alta_cabecera");
var preferenteP = document.getElementById("preferente");
var palcoAzteca = document.getElementById("palco_azteca");

function base() {
    pic.src = "../images/estadio_azteca/azteca_zonas.png";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    bajoP.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function alto() {
    pic.src = "../images/estadio_azteca/azteca_alto.png";
    altoP.className = "rosa";
    altoLateral.className = "no_activo";
    bajoP.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function alto_lateral() {
    pic.src = "../images/estadio_azteca/azteca_alto_lateral.png";
    altoLateral.className = "verde";
    altoP.className = "no_activo";
    bajoP.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function bajo() {
    pic.src = "../images/estadio_azteca/azteca_bajo.png";
    bajoP.className = "azul_c";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function platea_alta_plus() {
    pic.src = "../images/estadio_azteca/azteca_alta_plus.png";
    plateaAltaPlus.className = "verde_c";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function palcos_plus() {
    pic.src = "../images/estadio_azteca/azteca_palcos_plus.png";
    palcosPlus.className = "morado";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function platea_baja() {
    pic.src = "../images/estadio_azteca/azteca_platea_baja.png";
    plateaBaja.className = "naranja";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function palcos_club() {
    pic.src = "../images/estadio_azteca/azteca_palcos_club.png";
    palcosClub.className = "rojo";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function asientos_club() {
    pic.src = "../images/estadio_azteca/azteca_asientos_club.png";
    asientosClub.className = "azul";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function preferente_plus() {
    pic.src = "../images/estadio_azteca/azteca_preferente_plus.png";
    preferentePlus.className = "rosa_c";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function platea_alta_cabecera() {
    pic.src = "../images/estadio_azteca/azteca_platea_alta_cabecera.png";
    plateaAltaCabecera.className = "morado_o";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    preferenteP.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function preferente() {
    pic.src = "../images/estadio_azteca/azteca_preferente.png";
    preferenteP.className = "verde_o";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    palcoAzteca.className = "no_activo";
}
function palco_azteca() {
    pic.src = "../images/estadio_azteca/azteca_palco_azteca.png";
    palcoAzteca.className = "amarillo";
    altoP.className = "no_activo";
    altoLateral.className = "no_activo";
    plateaAltaPlus.className = "no_activo";
    bajoP.className = "no_activo";
    palcosPlus.className = "no_activo";
    plateaBaja.className = "no_activo";
    palcosClub.className = "no_activo";
    asientosClub.className = "no_activo";
    preferentePlus.className = "no_activo";
    plateaAltaCabecera.className = "no_activo";
    preferenteP.className = "no_activo";
}